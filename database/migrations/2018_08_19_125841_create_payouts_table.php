<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->float('total', 10, 2)->default(0);
            $table->float('tds', 10, 2)->default(0);
            $table->float('admin_charge', 10, 2)->default(0);
            $table->float('amount', 10, 2)->default(0);
            $table->tinyInteger('transfer_type')->nullable()->comment('1: Cheque, 2: NEFT, 3: IMPS, 4: CASH');
            $table->tinyInteger('income_type')->nullable()->comment('1: Default')->default(1);
            $table->tinyInteger('status')->default(1)->comment('1: Pending, 2: Transferred');
            $table->string('reference_number')->nullable();
            $table->tinyInteger('process_type')->default(1)->comment('1: Admin, 2: User');
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payouts');
    }
}
