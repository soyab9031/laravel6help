<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreSupplyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_supply_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supply_id');
            $table->integer('product_price_id')->index()->unsigned();
            $table->float('price',10,2);
            $table->float('distributor_price',10,2);
            $table->float('supply_price',10,2)->default(0);
            $table->integer('qty');
            $table->string('gst')->comment('Json: percentage & hsn/sac code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_supply_details');
    }
}
