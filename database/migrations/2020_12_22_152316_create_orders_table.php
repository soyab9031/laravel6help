<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->integer('admin_id')->index()->nullable()->unsigned();
            $table->integer('offer_id')->index()->nullable()->unsigned();
            $table->integer('store_id')->index()->unsigned()->nullable();
            $table->string('customer_order_id')->nullable();
            $table->float('amount', 10, 2);
            $table->float('wallet', 12, 2);
            $table->float('discount', 10, 2);
            $table->float('shipping_charge', 10, 2);
            $table->float('total', 10, 2);
            $table->float('total_bv', 10, 2);
            $table->text('remarks')->nullable();
            $table->text('admin_remarks')->nullable();
            $table->text('shipping_address')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: Checkout, 2: Placed, 3: Approved, 4: Failed, 5: Rejected');
            $table->tinyInteger('bv_calculated')->default(0)->comment('0: Pending, 1: Done');
            $table->tinyInteger('payment_status')->default(1)->comment('1: Checkout, 2: Success, 3: Pending, 4: Failed');
            $table->string('payment_reference')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->tinyInteger('delivery_status')->default(1)->comment('1: Pending Delivery, 1: Delivered');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
