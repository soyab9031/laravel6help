<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('sponsor_user_id')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('password');
            $table->string('name');
            $table->string('owner_name')->nullable();
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('district');
            $table->string('pincode');
            $table->integer('state_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('gst_number')->nullable();
            $table->tinyInteger('status')->comment('1: Pending, 2: Approved, 3:Rejected')->default(1);
            $table->tinyInteger('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_requests');
    }
}
