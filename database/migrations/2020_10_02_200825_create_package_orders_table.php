<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('admin_id')->nullable()->index();
            $table->integer('placed_by')->nullable()->index();
            $table->integer('package_id')->unsigned()->index();
            $table->integer('pin_id')->unsigned()->index();
            $table->float('amount', 10,2);
            $table->float('pv');
            $table->tinyInteger('pv_calculation')->default(0)->comment('0: No, 1: Yes, 2: Not Available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_orders');
    }
}
