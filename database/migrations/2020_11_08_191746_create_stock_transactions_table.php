<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->index()->unsigned();
            $table->integer('store_id')->index()->unsigned()->nullable();
            $table->integer('product_price_id')->index()->unsigned()->nullable();
            $table->integer('stock_purchase_id')->unsigned()->nullable();
            $table->integer('supply_id')->unsigned()->nullable();
            $table->integer('order_id')->unsigned()->nullable();
            $table->tinyInteger('type')->comment('1: Credit, 2: Debit');
            $table->integer('opening')->default(0);
            $table->integer('qty');
            $table->integer('closing')->default(0);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transactions');
    }
}
