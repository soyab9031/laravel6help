<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('address')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('bank_account')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('pancard')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('invoice')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('aadhar_card')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('fake')->default(0)->comment('0: No, 1: Yes (Consider as Fake ID)');
            $table->dateTime('binary_qualified_at')->nullable();
            $table->dateTime('sponsor_qualified_at')->nullable();
            $table->tinyInteger('tree_calculation')->comment('0: No, 1: Yes')->default(0);
            $table->tinyInteger('nested_set_calculated')->comment('0: No, 1: Yes')->default(0);
            $table->tinyInteger('payment')->comment('0: Disallow, 1: Allow, (User Payment Status)')->default(1);
            $table->tinyInteger('terminated')->comment('0: No, 1: Yes')->default(0);
            $table->tinyInteger('dispatch')->comment('0: No, 1: Yes (Package Dispatch Status)')->default(0);
            $table->tinyInteger('email')->comment('0: No, 1: Yes (Welcome Email)')->default(0);
            $table->tinyInteger('sms')->comment('0: No, 1: Yes (Welcome SMS)')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_statuses');
    }
}
