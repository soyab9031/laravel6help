<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreStockRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_stock_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->index()->nullable();
            $table->integer('store_id')->unsigned()->index();
            $table->integer('receiver_store_id')->unsigned()->nullable();
            $table->float('discount',10,2)->unsigned()->index();
            $table->float('amount', 12, 2);
            $table->tinyInteger('status')->comment('1: Pending, 2: Approved, 3: Rejected');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_stock_requests');
    }
}
