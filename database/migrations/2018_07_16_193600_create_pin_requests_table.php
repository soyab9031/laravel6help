<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pin_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('details');
            $table->string('payment_mode');
            $table->string('reference_number');
            $table->string('bank_name');
            $table->dateTime('deposited_at');
            $table->string('image')->nullable();
            $table->tinyInteger('status')->comment('1: Pending, 2: Approved, 3: Rejected')->default(1);
            $table->text('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pin_requests');
    }
}
