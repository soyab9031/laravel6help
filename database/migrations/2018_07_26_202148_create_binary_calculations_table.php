<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinaryCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('left')->default(0);
            $table->integer('right')->default(0);
            $table->double('total_left', 10, 2)->default(0);
            $table->double('total_right', 10, 2)->default(0);
            $table->double('current_left', 10, 2)->default(0);
            $table->double('current_right', 10, 2)->default(0);
            $table->double('forward_left', 10, 2)->default(0);
            $table->double('forward_right', 10, 2)->default(0);
            $table->tinyInteger('status')->default('0')->comment('0: Open, 1: Closed, 2: Rejected');
            $table->text('remarks')->nullable();
            $table->datetime('closed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('binary_calculations');
    }
}
