<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('payout_id')->unsigned()->nullable();
            $table->tinyInteger('type')->comment('1:Credit, 2:Debit');
            $table->tinyInteger('income_type')->comment('1:Binary, 2:Sponsor, 3: Other, 4:Re-Purchase, 5:Level, 6:Royalty, 7:Reward, 8:ROI, 9:Matrix');
            $table->double('total', 10, 2)->default(0);
            $table->double('tds', 10, 2)->default(0);
            $table->double('admin_charge', 10, 2)->default(0);
            $table->double('amount', 10, 2)->default(0);
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallets');
    }
}
