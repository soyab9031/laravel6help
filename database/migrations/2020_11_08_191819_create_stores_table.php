<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('tracking_id')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('pincode');
            $table->integer('state_id')->nullable();
            $table->tinyInteger('status')->comment('1: Active, 2: Inactive')->default(1);
            $table->tinyInteger('type')->comment('1: Premium, 2: Pro, 3: Standard, 4: Basic')->default(1);
            $table->string('last_logged_in_ip')->nullable();
            $table->dateTime('last_logged_in_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
