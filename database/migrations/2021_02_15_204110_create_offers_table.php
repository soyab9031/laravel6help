<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->nullable()->index();
            $table->string('name');
            $table->tinyInteger('type')->comment('1: Billing to Product, 2: Product to Product, 3: Discount, 4: Cashback');
            $table->float('min_amount', 10,2);
            $table->float('max_amount', 10,2);
            $table->text('discount_details')->comment('Percentage & Maximum Discount')->nullable();
            $table->text('cashback_details')->nullable();
            $table->tinyInteger('status')->comment('1: Active, 2: Inactive')->default(1);
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
