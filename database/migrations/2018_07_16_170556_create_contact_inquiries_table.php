<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('contact', 20);
            $table->string('email', 100)->nullable();
            $table->text('message');
            $table->tinyInteger('status')->default(0)->comment('0: Pending, 1: Seen, 2: Pined');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_inquiries');
    }
}
