<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('prefix', 20);
            $table->integer('amount')->unsigned()->default(0);
            $table->integer('capping')->unsigned()->nullable();
            $table->integer('sponsor_income')->nullable();
            $table->float('pv')->default(0);
            $table->text('declaration');
            $table->tinyInteger('status')->default(1)->comment('1: Active, 0: InActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packages');
    }
}
