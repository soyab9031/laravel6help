<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('pin_payment_id')->unsigned()->nullable();
            $table->integer('package_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable()->comment('Pin Holder User');
            $table->string('number')->unique();
            $table->integer('amount')->default(0);
            $table->tinyInteger('status')->default(0)->comment('0: Unused, 1: Used, 2: Block');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pins');
    }
}
