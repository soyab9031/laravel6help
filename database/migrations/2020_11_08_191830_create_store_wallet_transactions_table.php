<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_wallet_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->index()->unsigned()->nullable();
            $table->integer('supply_id')->index()->unsigned()->nullable();
            $table->integer('store_id')->index()->unsigned();
            $table->double('opening_amount');
            $table->double('amount');
            $table->double('closing_amount');
            $table->tinyInteger('type')->comment('1: Credit, 2: Debit');
            $table->string('remarks')->nullable();
            $table->tinyInteger('status')->comment('1: Active, 2: Inactive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_wallet_transactions');
    }
}
