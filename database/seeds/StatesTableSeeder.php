<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = \App\Models\Country::whereName('India')->first();

        collect(\App\Library\Helper::arrayToObject($this->getStates()))->map( function ($state) use ($country) {

            DB::table('states')->insert([
                'code' => $state->code,
                'name' => $state->name,
                'gst_code' => $state->gst_code,
                'country_id' => $country->id,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);

        });
    }

    public function getStates()
    {
        return [
            ['code' => 'AN', 'name' => 'Andaman and Nicobar Islands', 'gst_code' => 35],
            ['code' => 'AP', 'name' => 'Andhra Pradesh', 'gst_code' => 37],
            ['code' => 'AR', 'name' => 'Arunachal Pradesh', 'gst_code' => 12],
            ['code' => 'AS', 'name' => 'Assam', 'gst_code' => 18],
            ['code' => 'BR', 'name' => 'Bihar', 'gst_code' => 10],
            ['code' => 'CH', 'name' => 'Chandigarh', 'gst_code' => 4],
            ['code' => 'CT', 'name' => 'Chhattisgarh', 'gst_code' => 22],
            ['code' => 'DN', 'name' => 'Dadra and Nagar Haveli', 'gst_code' => 26],
            ['code' => 'DD', 'name' => 'Daman and Diu', 'gst_code' => 26],
            ['code' => 'DL', 'name' => 'Delhi', 'gst_code' => 7],
            ['code' => 'GA', 'name' => 'Goa', 'gst_code' => 30],
            ['code' => 'GJ', 'name' => 'Gujarat', 'gst_code' => 24],
            ['code' => 'HR', 'name' => 'Haryana', 'gst_code' => 6],
            ['code' => 'HP', 'name' => 'Himachal Pradesh', 'gst_code' => 2],
            ['code' => 'JK', 'name' => 'Jammu and Kashmir', 'gst_code' => 1],
            ['code' => 'JH', 'name' => 'Jharkhand', 'gst_code' => 20],
            ['code' => 'KA', 'name' => 'Karnataka', 'gst_code' => 29],
            ['code' => 'KL', 'name' => 'Kerala', 'gst_code' => 32],
            ['code' => 'LA', 'name' => 'Ladakh', 'gst_code' => 28],
            ['code' => 'LD', 'name' => 'Lakshadweep', 'gst_code' => 31],
            ['code' => 'MP', 'name' => 'Madhya Pradesh', 'gst_code' => 23],
            ['code' => 'MH', 'name' => 'Maharashtra', 'gst_code' => 27],
            ['code' => 'MN', 'name' => 'Manipur', 'gst_code' => 14],
            ['code' => 'ML', 'name' => 'Meghalaya', 'gst_code' => 17],
            ['code' => 'MZ', 'name' => 'Mizoram', 'gst_code' => 15],
            ['code' => 'NL', 'name' => 'Nagaland', 'gst_code' => 13],
            ['code' => 'OR', 'name' => 'Odisha', 'gst_code' => 21],
            ['code' => 'PY', 'name' => 'Puducherry', 'gst_code' => 34],
            ['code' => 'PB', 'name' => 'Punjab', 'gst_code' => 3],
            ['code' => 'RJ', 'name' => 'Rajasthan', 'gst_code' => 8],
            ['code' => 'SK', 'name' => 'Sikkim', 'gst_code' => 11],
            ['code' => 'TN', 'name' => 'Tamil Nadu', 'gst_code' => 33],
            ['code' => 'TR', 'name' => 'Tripura', 'gst_code' => 16],
            ['code' => 'TS', 'name' => 'Telangana', 'gst_code' => 36],
            ['code' => 'UP', 'name' => 'Uttar Pradesh', 'gst_code' => 9],
            ['code' => 'UT', 'name' => 'Uttarakhand', 'gst_code' => 5],
            ['code' => 'WB', 'name' => 'West Bengal', 'gst_code' => 19],
        ];
    }
}
