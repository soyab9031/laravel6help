<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $upline_tracking_id = $this->command->ask('Enter Upline Tracking ID', '100000');

        if ($this->command->ask('Is Sponsor Parent Tracking ID same as Direct Parent', 'Yes') == 'No') {

            $sponsor_upline_tracking_id = $this->command->ask('Sponsor Parent Tracking ID', '100000');
        }
        else {
            $sponsor_upline_tracking_id = $upline_tracking_id;
        }

        $leg = $this->command->ask('Select Leg L or R', 'L');

        $parent = \App\Models\User::whereTrackingId($upline_tracking_id)->first();

        $sponsor = \App\Models\User::whereTrackingId($sponsor_upline_tracking_id)->first();

        if (\App\Models\User::whereParentId($parent->id)->whereLeg($leg)->exists()) {

            $this->command->comment($leg . ' is already filled');
            exit();
        }

        $faker = Faker\Factory::create('en_IN');

        $package = \App\Models\Package::first();

        $pin = \App\Models\Pin::create([
            'admin_id' => \App\Models\Admin::first()->id,
            'package_id' => $package->id,
            'number' =>  \Str::random(20),
            'amount' => 0,
        ]);

        $user = \App\Models\User::create([
            'parent_id' => $parent->id,
            'sponsor_by' => $sponsor->id,
            'pin_id' => $pin->id,
            'leg' => $leg,
            'package_id' => $package->id,
            'tracking_id' => rand(1000000, 9999999),
            'username' => $faker->userName,
            'password' => $faker->password,
            'paid_at' => now(),
            'wallet_password' => \Str::random(6),
            'email' => $faker->email,
            'token' => strtoupper(\Str::random(15)),
            'mobile' => $faker->numberBetween(7894561230, 9876543210)
        ]);

        $pin->user_id = $user->id;
        $pin->status = \App\Models\Pin::USED;
        $pin->save();

        \App\Models\PackageOrder::create([
            'user_id' => $user->id, 'pin_id' => $pin->id, 'pv_calculation' => 1, 'package_id' => $package->id, 'amount' => $package->amount, 'pv' => $package->pv
        ]);

        \App\Models\UserDetail::create([
            'user_id' => $user->id,
            'title' => 'Mr',
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'gender' => 1,
            'birth_date' => \Carbon\Carbon::now()->subYears(18),
            'nominee_name' => $faker->name,
            'nominee_relation' => 'Father',
            'nominee_birth_date' => \Carbon\Carbon::now()->subYears(25)
        ]);

        \App\Models\UserStatus::create([
            'user_id' => $user->id,
        ]);

        \App\Models\UserBank::create([
            'user_id' => $user->id
        ]);

        \App\Models\UserAddress::create([
            'user_id' => $user->id,
            'address' => $faker->address,
            'landmark' => $faker->address,
            'city' => $faker->city,
            'district' => $faker->city,
            'state_id' => 8,
            'pincode' => $faker->numberBetween(365560, 375000)
        ]);

        (new \App\Library\Calculation\Binary\Builder())->addToTree();

        (new \App\Models\NestedSetUser())->setData();

        $this->command->comment('New User has been generated: '. $user->tracking_id);


    }
}
