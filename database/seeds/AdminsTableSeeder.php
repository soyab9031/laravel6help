<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            $project = Str::slug(config('project.brand'), '-');

            $admin = \App\Models\Admin::create([
                'name' => 'admin ' . $project,
                'password' => Hash::make('admin#'.$project),
                'email' => $project.'@tymk.dev',
                'mobile' => '9876543211',
                'type' => \App\Models\Admin::MASTER,
                'token' => \Str::random(80)
            ]);

            collect(\App\Models\AdminRoute::restrictedRoutes())->map( function ($route) use ($admin) {

                \App\Models\AdminRoute::create([
                    'admin_id' => $admin->id,
                    'route' => $route->key
                ]);

            });

            $real_admin = \App\Models\Admin::create([
                'name' => 'admin ' . $project,
                'password' => Hash::make('admin#'.$project),
                'email' => $project.'@gmail.com',
                'mobile' => '9876543210',
                'type' => \App\Models\Admin::MASTER,
                'token' => \Str::random(80)
            ]);

            collect(\App\Models\AdminRoute::restrictedRoutes())->map( function ($route) use ($real_admin) {

                \App\Models\AdminRoute::create([
                    'admin_id' => $real_admin->id,
                    'route' => $route->key
                ]);

            });

            \App\Models\Setting::create([
               'name' => 'Company Profile',
               'value' => '[{"title":"company_name","input_type":"text","value":""},{"title":"pan_number","input_type":"text","value":""},{"title":"gst_no","input_type":"text","value":""},{"title":"city","input_type":"text","value":""},{"title":"address","input_type":"text","value":""},{"title":"state","input_type":"text","value":""},{"title":"pincode","input_type":"text","value":""},{"title":"country","input_type":"text","value":""}]'
            ]);

        });
    }
}
