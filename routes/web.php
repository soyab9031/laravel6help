<?php

Route::group(['namespace' => 'Website'], function () {

    Route::get('/', 'HomeController@index')->name('website-home');

    Route::get('about', 'CompanyController@about')->name('website-company-about');
    Route::get('legals', 'CompanyController@legals')->name('website-company-legals');
    Route::get('vision', 'CompanyController@vision')->name('website-company-vision');
    Route::get('mission', 'CompanyController@mission')->name('website-company-mission');
    Route::get('terms', 'CompanyController@terms')->name('website-company-terms');

    Route::get('privacy-policy','CompanyController@privacyPolicy')->name('website-company-privacy-policy');
    Route::get('cancellation-policy','CompanyController@cancellationPolicy')->name('website-company-cancellation-policy');

    Route::get('contact', 'ContactController@index')->name('website-contact');
    Route::post('contact', 'ContactController@index')->name('website-contact');

    Route::get('product/{type}/','ProductController@listProducts')->name('website-products-list');

    Route::get('products/{slug?}','ProductController@index')->name('website-products-view');
    Route::get('product-details/{slug}/{product_code}', 'ProductController@details')->name('website-products-details');

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/','CartController@index')->name('website-cart');
        Route::get('retrieve', 'CartController@retrieveItems')->name('website-cart-retrieve-items');
        Route::get('add', 'CartController@addItem')->name('website-cart-add-item');
        Route::get('remove', 'CartController@removeItem')->name('website-cart-remove-item');
        Route::post('checkout', 'CartController@createCheckout')->name('website-cart-create-checkout');
    });

    Route::group(['prefix' => 'checkout'], function () {

        Route::group([ 'middleware' => 'website.checkout'], function(){
            Route::group(['prefix' => 'address'], function () {
                Route::post('create', 'AddressController@create')->name('website-checkout-address-create');
                Route::post('update', 'AddressController@update')->name('website-checkout-address-update');
            });

            Route::get('shipping-address', 'OrderProcessController@address')->name('website-checkout-shipping-address');
            Route::post('shipping-address', 'OrderProcessController@address')->name('website-checkout-shipping-address');

            Route::get('order-process', 'OrderProcessController@process')->name('website-checkout-order-process');
            Route::post('order-process', 'OrderProcessController@process')->name('website-checkout-order-process');

            Route::get('order-payment-response', 'OrderProcessController@paymentResponse')->name('website-order-payment-response');

            Route::get('payment-overview', 'OrderProcessController@paymentOverview')->name('website-checkout-payment-overview');
            Route::post('payment-overview', 'OrderProcessController@paymentOverview')->name('website-checkout-payment-overview');
        });

        Route::get('order-overview', 'OrderProcessController@overview')->name('website-order-overview');
    });

    Route::get('support','CompanyController@support')->name('website-support');
    Route::post('support','CompanyController@support')->name('website-support');
});
