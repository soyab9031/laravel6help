<?php

Route::group(['namespace' => 'User', 'prefix' => 'user'], function () {

    Route::group(['prefix' => 'register'], function () {

        Route::get('/', 'RegisterController@index')->name('user-register');
        Route::post('/', 'RegisterController@index')->name('user-register');

        Route::get('details', 'RegisterController@details')->name('user-register-details');
        Route::post('details', 'RegisterController@details')->name('user-register-details');

        Route::get('overview', 'RegisterController@overview')->name('user-register-overview');
        Route::post('overview', 'RegisterController@overview')->name('user-register-overview');

        Route::get('thanks', 'RegisterController@thanks')->name('user-register-thanks');

    });

    Route::get('/', 'LoginController@index')->name('user-login');
    Route::post('/', 'LoginController@index')->name('user-login');
    Route::get('logout', 'LoginController@logout')->name('user-logout');

    Route::get('forget-password', 'LoginController@forgetPassword')->name('user-forget-password');
    Route::post('forget-password', 'LoginController@forgetPassword')->name('user-forget-password');

    /* Access from Admin */
    Route::get('admin-view-access/{id}/{token}', 'LoginController@adminViewAccess')->name('user-admin-view-access');

    Route::group(['middleware' => 'user.auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('user-dashboard');

        Route::group(['prefix' => 'account'], function () {

            Route::get('profile', 'ProfileController@index')->name('user-account-profile');
            Route::post('profile', 'ProfileController@index')->name('user-account-profile');

            Route::get('identity-card', 'ProfileController@identityCard')->name('user-account-identity-card');

            Route::get('password', 'ProfileController@password')->name('user-account-password');
            Route::post('password', 'ProfileController@password')->name('user-account-password');

            Route::get('wallet-password', 'ProfileController@walletPassword')->name('user-account-wallet-password');
            Route::post('wallet-password', 'ProfileController@walletPassword')->name('user-account-wallet-password');

            Route::get('welcome-letter', 'ProfileController@welcomeLetter')->name('user-welcome-letter');
            Route::get('package-invoice', 'ProfileController@packageInvoice')->name('user-package-invoice');
            Route::get('package-receipt', 'ProfileController@packageReceipt')->name('user-package-receipt');

            Route::group(['prefix' => 'kyc'], function () {

                Route::get('/', 'KycController@dashboard')->name('user-kyc-dashboard');

                Route::get('pancard', 'KycController@pancard')->name('user-kyc-pancard');
                Route::post('pancard', 'KycController@pancard')->name('user-kyc-pancard');

                Route::get('bank', 'KycController@bank')->name('user-kyc-bank');
                Route::post('bank', 'KycController@bank')->name('user-kyc-bank');

                Route::get('address', 'KycController@address')->name('user-kyc-address');
                Route::post('address', 'KycController@address')->name('user-kyc-address');

                Route::get('aadhaar', 'KycController@aadhaarNumber')->name('user-kyc-aadhaar');
                Route::post('aadhaar', 'KycController@aadhaarNumber')->name('user-kyc-aadhaar');

            });

            Route::get('documents','KycController@viewCompanyDocuments')->name('user-company-documents-view');

            /* Internal API */
            Route::get('get-children', 'ProfileController@getChildren')->name('user-get-children');
            Route::get('get-bank-details', 'ProfileController@getBankDetails')->name('user-get-bank-details');

        });

        Route::group(['prefix' => 'upgrade-account'], function () {

            Route::get('/', 'UpgradeAccountController@index')->name('user-upgrade-account');
            Route::post('/', 'UpgradeAccountController@index')->name('user-upgrade-account');

        });

        Route::group(['prefix' => 'pin'], function() {

            Route::get('/', 'PinController@index')->name('user-pins');
            Route::post('/', 'PinController@index')->name('user-pins');

            Route::group(['prefix' => 'request'], function () {

                Route::get('view', 'PinController@viewRequest')->name('user-pin-request');

                Route::get('make', 'PinController@makeRequest')->name('user-pin-make-request');
                Route::post('make', 'PinController@makeRequest')->name('user-pin-make-request');
            });

        });

        Route::group(['prefix' => 'business', 'namespace' => 'Business'], function () {

            Route::get('sponsors', 'BusinessController@sponsors')->name('user-business-sponsors');
            Route::get('sponsors-tree', 'BusinessController@sponsorLevelTree')->name('user-business-sponsors-tree');
            Route::get('children/{leg}', 'BusinessController@children')->name('user-business-children');

            Route::group(['prefix' => 'binary'], function () {

                Route::get('tree', 'BinaryController@tree')->name('user-business-binary-tree');
                Route::get('status', 'BinaryController@status')->name('user-business-binary-status');
            });

        });

        Route::group(['prefix' => 'wallet'], function () {
            Route::get('income/{type?}', 'WalletController@index')->name('user-wallet-view');
            Route::get('total-report', 'WalletController@totalReport')->name('user-wallet-total-report');
        });

        Route::group(['prefix' => 'payout'], function () {
            Route::get('/', 'PayoutController@index')->name('user-payout-view');
            Route::get('make-request', 'PayoutController@makeRequest')->name('user-payout-make-request');
            Route::post('make-request', 'PayoutController@makeRequest')->name('user-payout-make-request');
        });

        Route::group(['prefix' => 'shopping'], function () {

            Route::group(['prefix' => 'products'], function () {

                Route::get('/', 'ProductController@index')->name('user-products-view');
                Route::get('details/{code}', 'ProductController@details')->name('user-products-details');

            });

            Route::group(['prefix' => 'cart'], function () {
                Route::get('view','CartController@index')->name('user-cart');
                Route::get('retrieve', 'CartController@retrieveItems')->name('user-cart-retrieve-items');
                Route::get('add', 'CartController@addItem')->name('user-cart-add-item');
                Route::get('remove', 'CartController@removeItem')->name('user-cart-remove-item');
            });

            Route::group(['prefix' => 'order'], function () {

                Route::get('/','OrderController@index')->name('user-shopping-order-view');
                Route::get('details/{customer_order_id}','OrderController@details')->name('user-shopping-order-details');
                Route::post('place','OrderController@place')->name('user-shopping-order-place');

            });

        });


        Route::group(['prefix' => 'support'], function () {

            Route::get('/', 'SupportController@index')->name('user-support-view');
            Route::post('/', 'SupportController@index')->name('user-support-view');

            Route::get('chat/{id}', 'SupportController@chat')->name('user-support-chat');
            Route::post('chat/{id}', 'SupportController@chat')->name('user-support-chat');

        });

    });


});