<?php

Route::group(['namespace' => 'Admin\StoreManager', 'prefix' => 'admin-panel/store-manager', 'middleware' => 'admin.auth'], function () {

    Route::group(['prefix' => 'supplier'], function () {

        Route::get('/', 'SupplierController@index')->name('admin-store-manager-supplier-view');
        Route::get('create', 'SupplierController@create')->name('admin-store-manager-supplier-create');
        Route::post('create', 'SupplierController@create')->name('admin-store-manager-supplier-create');
        Route::get('update/{id}', 'SupplierController@update')->name('admin-store-manager-supplier-update');
        Route::post('update/{id}', 'SupplierController@update')->name('admin-store-manager-supplier-update');

    });

    Route::group(['prefix' => 'purchase-order'], function () {

        Route::get('/', 'PurchaseOrderController@index')->name('admin-store-manager-purchase-order-view');
        Route::get('details/{id}', 'PurchaseOrderController@detail')->name('admin-store-manager-purchase-order-detail');
        Route::get('download/{id}', 'PurchaseOrderController@download')->name('admin-store-manager-purchase-order-download');

        Route::get('create', 'PurchaseOrderController@create')->name('admin-store-manager-purchase-order-create');
        Route::post('create', 'PurchaseOrderController@create')->name('admin-store-manager-purchase-order-create');
        Route::get('update/{id}', 'PurchaseOrderController@update')->name('admin-store-manager-purchase-order-update');
        Route::post('update/{id}', 'PurchaseOrderController@update')->name('admin-store-manager-purchase-order-update');

    });

    Route::group(['prefix' => 'stock-purchase'], function () {

        Route::get('/', 'StockPurchaseController@index')->name('admin-store-manager-stock-purchase-view');
        Route::get('details/{id}', 'StockPurchaseController@detail')->name('admin-store-manager-stock-purchase-detail');

        Route::get('create', 'StockPurchaseController@create')->name('admin-store-manager-stock-purchase-create');
        Route::post('create', 'StockPurchaseController@create')->name('admin-store-manager-stock-purchase-create');

    });

    Route::get('stock-list', 'StockPurchaseController@stockList')->name('admin-store-manager-stock-list');
    Route::get('stock-list/update-minimum-qty', 'StockPurchaseController@updateMinimumQty')->name('admin-store-manager-stock-minimum-qty-update');

    Route::group(['prefix' => 'store'], function () {

        Route::get('/', 'StoreController@index')->name('admin-store-manager-store-view');
        Route::get('create', 'StoreController@create')->name('admin-store-manager-store-create');
        Route::post('create', 'StoreController@create')->name('admin-store-manager-store-create');
        Route::get('update/{id}', 'StoreController@update')->name('admin-store-manager-store-update');
        Route::post('update/{id}', 'StoreController@update')->name('admin-store-manager-store-update');
        Route::get('stock-list', 'StoreController@stockList')->name('admin-store-manager-store-stock-list');

        Route::get('api-get-store', 'StoreController@apiGetStoreDetails')->name('admin-api-store-manager-get-store-details');
        Route::get('account-access', 'StoreController@accountAccess')->name('admin-store-manager-store-account-access');


        Route::group(['prefix' => 'request'], function () {
            Route::get('/', 'StoreRequestController@index')->name('admin-store-manager-store-register-request');
            Route::get('details/{id}', 'StoreRequestController@details')->name('admin-store-manager-store-register-details');
            Route::post('details/{id}', 'StoreRequestController@details')->name('admin-store-manager-store-register-details');
        });

        Route::group(['prefix' => 'wallet'], function () {

            Route::get('/', 'StoreWalletController@index')->name('admin-store-manager-store-wallet-view');
            Route::get('create-transaction', 'StoreWalletController@createTransaction')->name('admin-store-manager-store-wallet-create-transaction');
            Route::post('create-transaction', 'StoreWalletController@createTransaction')->name('admin-store-manager-store-wallet-create-transaction');

            Route::get('request', 'StoreWalletController@requestView')->name('admin-store-manager-wallet-request-view');
            Route::get('request/update/{id}', 'StoreWalletController@requestUpdate')->name('admin-store-manager-wallet-request-update');
            Route::post('request/update/{id}', 'StoreWalletController@requestUpdate')->name('admin-store-manager-wallet-request-update');

        });

        Route::group(['prefix' => 'supply'], function () {

            Route::get('/', 'StoreSupplyController@index')->name('admin-store-manager-store-supply-view');
            Route::get('details/{id}', 'StoreSupplyController@detail')->name('admin-store-manager-store-supply-detail');
            Route::post('details/{id}', 'StoreSupplyController@detail')->name('admin-store-manager-store-supply-detail');

            Route::get('delivery-note/{id}', 'StoreSupplyController@getDelhiveryNote')->name('admin-store-manager-store-supply-delivery-note');

            Route::get('create', 'StoreSupplyController@create')->name('admin-store-manager-store-supply-create');
            Route::post('create', 'StoreSupplyController@create')->name('admin-store-manager-store-supply-create');

        });

        Route::group(['prefix' => 'stock-request'], function () {

            Route::get('/', 'StoreStockRequestController@index')->name('admin-store-manager-store-request-view');
            Route::get('details/{id}', 'StoreStockRequestController@details')->name('admin-store-manager-store-request-details');
            Route::post('transfer-supply/{id}', 'StoreStockRequestController@transferToSupply')->name('admin-store-manager-store-request-to-supply');

        });

        Route::group(['prefix' => 'reports'], function () {
            Route::get('store-sales', 'ReportController@storeSales')->name('admin-store-manager-report-store-sales');
            Route::get('supply-transfer', 'ReportController@supplyTransfer')->name('admin-store-manager-report-supply-transfer');
            Route::get('product-ranking', 'ReportController@productRanking')->name('admin-store-manager-report-product-ranking');
        });

        Route::group(['prefix' => 'offers'], function () {
            Route::get('/', 'OfferController@index')->name('admin-store-manager-offer-view');
            Route::get('create', 'OfferController@create')->name('admin-store-manager-offer-create');
            Route::post('create', 'OfferController@create')->name('admin-store-manager-offer-create');
            Route::get('update/{id}', 'OfferController@update')->name('admin-store-manager-offer-update');
            Route::post('update/{id}', 'OfferController@update')->name('admin-store-manager-offer-update');

            Route::get('report/{id}', 'OfferController@report')->name('admin-store-manager-offer-report');
        });

    });

});