@extends('admin.template.layout')

@section('title', 'Admin Manager')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Admin Manager:active)

    <div class="container-fluid container-fixed-lg">

        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin-manager-create') }}" class="btn btn-success btn-sm"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Last Login</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($admins as $index => $admin)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>
                                    {{ $admin->name }}
                                    @if($admin->id == 1)
                                        <br>
                                        <span class="badge badge-warning">Developer Account</span>
                                    @endif
                                </td>
                                <td>{{ $admin->email }} <br> M: {{ $admin->mobile }}</td>
                                <td>
                                    @if($admin->type == 1)
                                        <span class="badge badge-primary">Master</span>
                                    @elseif($admin->type == 2)
                                        <span class="badge badge-warning">Manager</span>
                                    @else
                                        <span class="badge badge-info">Employee</span>
                                    @endif
                                </td>
                                <td>
                                    @if($admin->status == 1)
                                        <span class="label label-theme">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                </td>
                                <td>{{ $admin->created_at->format('M d, Y') }}</td>
                                <td>{{ $admin->last_logged_in_at ? \Carbon\Carbon::parse($admin->last_logged_in_at)->format('M d, Y h:i A') : 'N.A' }}</td>
                                <td>
                                    <a href="{{ route('admin-manager-update', ['id' => $admin->id]) }}" class="btn btn-info btn-xs">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop