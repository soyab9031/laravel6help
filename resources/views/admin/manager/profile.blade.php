@extends('admin.template.layout')

@section('title', 'Admin Profile')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Profile:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" value="{{ Session::get('admin')->name }}" required autocomplete="off">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="text" class="form-control" value="{{ Session::get('admin')->email }}" disabled autocomplete="off">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Mobile</label>
                                <input type="number" class="form-control" name="mobile" value="{{ Session::get('admin')->mobile }}" required autocomplete="off">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Password</label>
                                <div class="controls input-group">
                                    <input type="password" class="form-control passwordInput" name="password" placeholder="Credentials" required>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="INGENIOUS.showPassword(this)">
                                            <i class="fa fa-eye-slash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger"> Update <i class="fa fa-arrow-circle-o-right"></i> </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop