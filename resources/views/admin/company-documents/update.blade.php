@extends('admin.template.layout')

@section('title', 'Update')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Document:admin-company-documents-view,update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $companyDocument->name }}" required>
                            </div>

                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2">
                                    <option value="">Select Type</option>
                                    <option value="1" {{ $companyDocument->type == 1 ? 'selected' : '' }}>PDF</option>
                                    <option value="2" {{ $companyDocument->type == 2 ? 'selected' : '' }}>Image</option>
                                </select>
                            </div>

                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $companyDocument->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $companyDocument->status == 0 ? 'selected' : '' }}>In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Document</label>
                                <input type="file" name="document" id="document">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                @if(!in_array($companyDocument->type, [1,4]))
                                    <p class="text-danger">3. For Responsive upload image size in 1200*600 pixels</p>
                                @endif
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-default">
                                <div class="card-header  separator">
                                    <div class="card-title">Current Document</div>
                                </div>
                                <div class="card-body">
                                    @php
                                        $fileName = pathinfo($companyDocument->document);
                                    @endphp
                                    @if($fileName['extension'] == 'pdf')
                                        <div class="mt-2 text-center">
                                            <a href="{{ env('COMPANY_DOCUMENT_URL') .$companyDocument->document  }}" target="_blank" class="btn-sm btn-primary text-center">View Document</a>
                                        </div>
                                    @else
                                        <img src="{{ env('COMPANY_DOCUMENT_URL') .$companyDocument->document  }}" alt="" width="100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#document').filer({
            limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png','pdf'], changeInput: true, showThumbs: true,
            captions: {
                button: "Choose File",
                feedback: 'Choose Document To Upload'
            }
        });
    </script>
@stop