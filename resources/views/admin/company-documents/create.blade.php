@extends('admin.template.layout')

@section('title', 'Create Document')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Document:admin-company-documents-view,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 offset-md-3 offset-lg-3">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>

                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2" onchange="documentType(this)">
                                    <option value="">Select Type</option>
                                    <option value="1">PDF</option>
                                    <option value="2">Image</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1">Active</option>
                                    <option value="0">In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Document</label>
                                <input type="file" name="document" id="document">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger d-none" id="imageShow">3. For Responsive slider upload image size in 1200*600 pixels</p>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>

                        </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#document').filer({
            limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png','pdf'], changeInput: true, showThumbs: true,
            captions: {
                button: "Choose File",
                feedback: 'Choose Document To Upload'
            }
        });

        function documentType(val){
            if(parseInt(val.value) === 2 || parseInt(val.value) === 3){
                $("#imageShow").removeClass('d-none');
            }else {
                $("#imageShow").addClass('d-none');
            }
        }
    </script>
    c
@stop