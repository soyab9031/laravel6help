@extends('admin.template.layout')

@section('title', 'Documents')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Documents:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select name="type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="">Document Type Filter</option>
                                    <option value="1" {{ Request::get('type') ==  1 ? 'selected' : '' }}>Pdf</option>
                                    <option value="2" {{ Request::get('type') ==  2 ? 'selected' : '' }}>Image</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button class="btn btn-danger btn-sm"> Search</button>
                            @refreshBtn()
                            <a href="{{ route('admin-company-documents-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($documents as $index => $document)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($document->created_at)->format('d M Y') }}</td>
                                <td>{{ $document->name }}</td>
                                <td>
                                    @if ($document->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> In Active</span>
                                    @endif
                                </td>
                                <td><a href="{{ env('COMPANY_DOCUMENT_URL').$document->document }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-company-documents-update', [ 'id' => $document->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $documents->appends(['search' => Request::get('search')])->links()  }}
                </div>
            </div>
        </div>
    </div>

@stop