<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Ingenious, A Direct Selling Application - {{ config('project.company') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/admin-assets/images/theme/favicon.png">
    <link rel="icon" type="image/x-icon" href="/admin-assets/images/theme/favicon.png" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Ingenious, A Direct Selling Application" name="description" />
    <link href="/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/admin-assets/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/admin-assets/css/light.css" rel="stylesheet" type="text/css" />
    @yield('import-css')
    @yield('page-css')
</head>
<body class="fixed-header menu-pin">
<div class="login-wrapper">
    <div class="bg-pic">
        <img src="/admin-assets/images/theme/login-bg-2.jpeg" data-src="/admin-assets/images/theme/login-bg-1.jpeg" data-src-retina="/admin-assets/images/theme/login-bg.jpg" class="lazy">
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            <h2 class="semi-bold text-white">
                Ingenious, A Direct Selling Application
            </h2>
            <p class="small">
                that makes simplify your life with Direct Selling Business, All work copyright of respective
                owner, otherwise © {{ date('Y') }} Tymk Softwares.
            </p>
        </div>
    </div>
    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="/admin-assets/images/theme/logo.png" alt="logo" data-src="/admin-assets/images/theme/logo.png">
            <p class="p-t-35">Sign into your {{ config('project.brand') }} account</p>
            @if(session('errors'))
                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
            @elseif(session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif

            <form id="form-login" class="p-t-15" role="form" method="post" action="" onsubmit="INGENIOUS.blockUI(true)">
                {{ csrf_field() }}
                <div class="form-group form-group-default">
                    <label>Login</label>
                    <div class="controls">
                        <input type="text" name="email" placeholder="User Name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group form-group-default">
                    <label>Password</label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Credentials" required>
                    </div>
                </div>
                <button class="btn btn-theme btn-cons m-t-10" type="submit">Sign in</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>