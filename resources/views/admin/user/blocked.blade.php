@extends('admin.template.layout')

@section('title', 'Blocked / Terminated Users')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Blocked Users:active)

    <div class="container-fluid container-fixed-lg">
        <div class="col-md-6 offset-md-3 offset-lg-3">
            <div class="card">
                <div class="card-body">
                    <form action="" method="get">
                        <input type="hidden" name="blockRequest" value="Yes">
                        <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                                <label>Search User</label>
                                <input type="text" class="form-control tracking_id_input"  required autocomplete="off">
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-danger" type="button" onclick="INGENIOUS.getUser(this, 'tracking_id_input')">Search</button>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-primary btn-rounded"> Add To Blocked List </button>
                        </div>
                        <p class="small text-center text-danger m-t-10">
                            Blocked Users will Not Get Login, Registration & Payment Access
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text primary"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Joined at</th>
                            {{--<th>Package</th>--}}
                            <th>User</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $index => $user)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($users, $index) }}</td>
                                <td>{{ $user->created_at->format('M d, Y') }}</td>
                                {{--<td>{{ $user->package ? $user->package->name: 'N.A' }} <br> (Rs. {{ $user->package ? $user->joining_amount : 'N.A' }})</td>--}}
                                <td class="text-primary">
                                    {{ $user->detail->full_name }} <br>
                                    ({{ $user->tracking_id }})
                                </td>
                                <td>{{ $user->mobile }}</td>
                                <td>
                                    <a href="{{ route('admin-user-blocked-view', ['removeBlockRequest' => $user->id]) }}" class="btn btn-xs btn-danger"> Remove </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->appends(['search' => Request::get('search')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop