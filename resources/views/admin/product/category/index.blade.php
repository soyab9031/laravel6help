@extends('admin.template.layout')

@section('title', 'Category')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Category:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-12 pull-right">

                            <a href="{{ route('admin-category-create') }}" class="btn btn-success btn-sm pull-right"> Create New Category</a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th>Parent</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $index => $category)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td><a href="{{ route('admin-category-view' ,['parent_id' => $category->id]) }}">{{ $category->name }}</a></td>
                                <td>{{ $category->parent_id ? $category->parent->name : 'N.A' }}</td>
                                <td>
                                    @if ($category->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> In Active</span>
                                    @endif
                                </td>
                                <td><a href="{{ env('CATEGORY_IMAGE_URL').$category->image }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-category-update', ['id' => $category->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $categories->appends(['search' => Request::get('search')])->links() }}
                </div>
            </div>
        </div>
    </div>

@stop