@extends('admin.template.layout')

@section('title', 'Update Category')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Category:admin-category-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group form-group-default">
                                    <label>Parent Category</label>
                                    <input type="text" name="name" class="form-control" value="{{ $category->parent_id ? $category->parent->name : 'N.A' }}" readonly>
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Category Name</label>
                                    <input type="text" name="name" class="form-control" value="{{ $category->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Select Category image</label>
                                    <input type="file" name="image" id="image">
                                    <p><br>1. Upload File Size Not More than 2 MB.</p>
                                    <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                    <p class="text-danger">3. For Responsive Category upload image size in 1200*600 pixels</p>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control" cols="30" rows="5">{!! $category->description  !!}</textarea>
                                </div>
                                <div class="form-group form-group-default form-group-default-select2">
                                    <label>Status</label>
                                    <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                        <option value="1" {{ $category->status == 1 ? 'selected' : '' }} >Active</option>
                                        <option value="2" {{ $category->status == 2 ? 'selected' : '' }}>Inactive</option>
                                    </select>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-danger"> Update </button>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="card card-default">
                                    <div class="card-header  separator">
                                        <div class="card-title">Current Category Image</div>
                                    </div>
                                    <div class="card-body">
                                        <img src="{{ env('CATEGORY_IMAGE_URL').$category->image }}" width="100%" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });
    </script>
@stop