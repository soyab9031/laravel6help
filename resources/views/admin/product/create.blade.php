@extends('admin.template.layout')

@section('title', 'Create New Product')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Product:admin-product-view,Create:active)

    <div class="container-fluid container-fixed-lg" id="createProductPage">

        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Category</label>
                                <vue-select2 :options="categories" place-holder="Select Category" required="required"
                                             allow-search="0" v-model="selected_parent_category_id"
                                             @change-select2="getChildrenCategories"></vue-select2>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2"
                                 v-if="child_categories">
                                <label>Sub Category</label>
                                <vue-select2 :options="child_categories" name="category_id"
                                             place-holder="Select Sub Category" allow-search="0"
                                             required="required"></vue-select2>
                            </div>
                        </div>
                        <div class="col-md-8 summernote-wrapper">
                            <label>Description</label>
                            <textarea class="summernote" name="description" cols="30" rows="10"
                                      placeholder="Enter Product Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Product Unique Code</label>
                                <input type="text" name="code" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Product Barcode</label>
                                <input type="text" name="barcode" onkeypress="INGENIOUS.numericInput(event)"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Points / PV / BV</label>
                                <input type="text" name="points" onkeypress="INGENIOUS.numericInput(event)"
                                       class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Amount/MRP</label>
                                <input type="text" name="price" onkeypress="INGENIOUS.numericInput(event)"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Distributor Price</label>
                                <input type="text" name="distributor_price" onkeypress="INGENIOUS.numericInput(event)"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Product Image</label>
                                <input type="file" name="images" id="image">
                            </div>
                            <small class="text-danger">Min: 3 MB & 300 X 300 pixels image is required</small>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Gst Rate</label>
                                <select class="full-width" data-init-plugin="select2" name="gst_rate"
                                        data-disable-search="true" required>
                                    <option value="">Select</option>
                                    @foreach(\App\Models\ProductPrice::getGstPercentage() as $percentage)
                                        <option value="{{ $percentage }}">{{ $percentage }}%</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>HSN Code</label>
                                <input type="text" name="hsn_code" class="form-control" required>
                            </div>
                            <small><a href="https://cleartax.in/s/gst-hsn-lookup" class="pull-right" target="_blank">Don't
                                    Know the Code? Click Here</a></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body text-center">
                    <button class="btn btn-theme">Create</button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el: '#createProductPage',
            data: {
                selected_parent_category_id: '',
                child_categories: null,
                categories: {!! json_encode($categories) !!},
            },
            methods: {

                getChildrenCategories: function () {

                    INGENIOUS.blockUI(true);

                    this.child_categories = null;

                    this.$http.get('{{ route("admin-api-category-get-children")}}', {
                        params: {
                            category_id: this.selected_parent_category_id,
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {
                            this.child_categories = response.data.categories;
                        } else {
                            swal('Error', response.data.message, 'error');
                        }

                    })
                },
            }
        });

        $('#image').filer({
            limit: 3,
            maxSize: 3,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: true,
            showThumbs: true
        });

        jQuery(document).ready(function () {

            $('.summernote').summernote({
                height: 250,
                minHeight: null,
                maxHeight: null,
                focus: false,
                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
                onfocus: function (e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function (e) {
                    $('body').removeClass('overlay-disabled');
                },

            });

            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
@stop
