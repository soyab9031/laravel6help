@extends('admin.template.layout')

@section('title', 'Update FAQ')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,FAQs:admin-setting-faq-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="form-group form-group-default">
                        <label>Question</label>
                        <input type="text" name="question" class="form-control" value="{{ $faq->question }}" required>
                    </div>
                    <div class="summernote-wrapper">
                        <label>Answer</label>
                        <textarea class="summernote" name="answer" cols="30" rows="10" placeholder="Enter Answer">{{ $faq->answer }}</textarea>
                    </div>
                    <div class="form-group form-group-default form-group-default-select2 mt-2">
                        <label>Status</label>
                        <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                            <option value="1" {{ $faq->status == 1 ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ $faq->status == 2 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <div class="text-center mt-2">
                        <button class="btn btn-danger"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('import-javascript')
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-javascript')
    <script>
        jQuery(document).ready(function () {

            $('.summernote').summernote({
                height: 250,
                minHeight: null,
                maxHeight: null,
                focus: false,
                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
                onfocus: function (e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function (e) {
                    $('body').removeClass('overlay-disabled');
                },

            });

            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
@stop