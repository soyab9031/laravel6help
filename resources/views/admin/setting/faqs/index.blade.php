@extends('admin.template.layout')

@section('title', 'FAQs')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, FAQs:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('admin-setting-faq-create') }}" class="btn btn-success btn-sm mt-2"> Create New +</a>
                    </div>
                </div>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($faqs as $index => $faq)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($faqs, $index) }}</td>
                                <td>{{ Carbon\Carbon::parse($faq->created_at)->format('M d, Y') }}</td>
                                <td>{{ $faq->question }}</td>
                                <td>{{ \Str::limit(strip_tags($faq->answer), 100) }}</td>
                                <td>
                                    @if($faq->status == 1)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-setting-faq-update', ['id' => $faq->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $faqs->links()}}
                </div>
            </div>
        </div>
    </div>
@stop
