@extends('admin.template.layout')

@section('title', 'Update News')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,News:admin-news,Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $news->title }}" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Select News image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger">3. For Responsive News upload image size in 300*300 pixels</p>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" name="message" cols="5" rows="5">{{ $news->message }}</textarea>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $news->status == 1 ? 'selected' : null }} >Active</option>
                                    <option value="2" {{ $news->status == 2 ? 'selected' : null }}>Inactive</option>
                                </select>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger btn-lg btn-rounded"> Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header  separator">
                        <div class="card-title">Current News Image</div>
                    </div>
                    <div class="card-body">
                        <img src="{{ env('NEWS_IMAGE_URL').$news->image }}" width="100%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });
    </script>
@stop