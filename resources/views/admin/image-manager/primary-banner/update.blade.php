@extends('admin.template.layout')

@section('title', 'Update Banner')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Update Primary Banner:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $banner->name }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $banner->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $banner->status == 0 ? 'selected' : '' }}>In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Banner image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger">3. For Responsive slider upload image size in 1200*600 pixels</p>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-default">
                                <div class="card-header  separator">
                                    <div class="card-title">Current Banner Image</div>
                                </div>
                                <div class="card-body">
                                    <img src="{{ env('BANNER_IMAGE_URL').$banner->image }}" width="100%" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });
    </script>
@stop