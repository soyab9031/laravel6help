@extends('admin.template.layout')

@section('title', 'Document Update')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Document List:admin-document-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            @if(in_array($document->type, [\App\Models\UserDocument::BANK_PASSBOOK, \App\Models\UserDocument::CANCEL_CHEQUE]))
                                <li class="list-group-item">
                                    Holder Name:
                                    <span class="pull-right">{{ $document->user->bank->account_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    Account Number: <span class="pull-right">{{ $document->number }}</span>
                                </li>
                                <li class="list-group-item">
                                    Bank Name: <span class="pull-right">{{ $document->user->bank->bank_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    IFSC Code: <span class="pull-right">{{ $document->user->bank->ifsc }}</span>
                                </li>
                                <li class="list-group-item">
                                    Branch: <span class="pull-right">{{ $document->user->bank->branch }}</span>
                                </li>
                                <li class="list-group-item">
                                    City: <span class="pull-right">{{ $document->user->bank->city }}</span>
                                </li>
                            @else
                                <li class="list-group-item">
                                    {{ ucwords(strtolower($document->document_name)) }}
                                    Number: <span class="pull-right">{{ $document->number }}</span>
                                </li>
                            @endif
                            <li class="list-group-item">
                                Type: <span class="pull-right">{{ $document->document_name }}</span>
                            </li>
                            <li class="list-group-item">
                                Image:
                                <span class="pull-right">
                                        <a href="javascript:void(0)"
                                           onclick="viewImage('{{ env('DOCUMENT_IMAGE_URL').$document->image }}')"
                                           class="btn btn-primary btn-xs">
                                            View Image
                                        </a>
                                    </span>
                            </li>
                            @if($document->secondary_image)
                                <li class="list-group-item">
                                    Back Image:
                                    <span class="pull-right">
                                        <a href="javascript:void(0)"
                                           onclick="viewImage('{{ env('DOCUMENT_IMAGE_URL').$document->secondary_image }}')"
                                           class="btn btn-primary btn-xs">
                                            Back Image
                                        </a>
                                    </span>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data"
                              onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}

                            <div class="form-group form-group-default form-group-default-select2 m-t-10">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $document->status == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2" {{ $document->status == 2 ? 'selected' : '' }}>Approved</option>
                                    <option value="3" {{ $document->status == 3 ? 'selected' : '' }}>Rejected</option>
                                </select>
                            </div>

                            <div class="form-group ">
                                <textarea class="form-control" name="remarks" cols="5" rows="5"
                                          placeholder="Remarks">{{ $document->remarks }}</textarea>
                            </div>
                            <div class="text-center m-t-10">
                                <button class="btn btn-theme btn-sm">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imageViewer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>{{ $document->document_name }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_image text-center"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        function viewImage(image_url) {

            INGENIOUS.blockUI(true);
            $('.modal_image').html('<img src="' + image_url + '" id="documentImage" data-zoom-image="' + image_url + '" width="100%" />');

            setTimeout(() => {
                INGENIOUS.blockUI(false);
                $('#imageViewer').modal();
                $('#documentImage').ezPlus({zoomWindowWidth: 500, zoomWindowHeight: 500});
            }, 650);

        }
    </script>
@stop

@section('import-javascript')
    <script type="text/javascript" src="/plugins/select2/js/select2.full.min.js"></script>
    <script src="/plugins/jquery-elevatezoom/jquery.elevatezoom.js"></script>
@stop

@section('page-css')
    <style>
        .zoomContainer {
            z-index: 9999 !important;
        }
    </style>
@stop