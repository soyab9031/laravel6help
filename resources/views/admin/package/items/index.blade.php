@extends('admin.template.layout')

@section('title', 'Package Items')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Packages:admin-package-view,Items:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Package: <span class="pull-right">{{ $package->name }}</span>
                            </li>
                            <li class="list-group-item">
                                Package Amount: <span class="pull-right text-danger">Rs. {{ number_format($package->amount) }}</span>
                            </li>
                            <li class="list-group-item">
                                Package PV: <span class="pull-right text-danger">{{ $package->pv }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h6>Add New Item into {{ $package->name }}</h6>
                        <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Item Name</label>
                                <input type="text" name="item_name" class="form-control" value="{{ old('item_name') }}" required>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Item Amount</label>
                                <input type="number" min="0" name="item_amount" class="form-control" value="{{ old('item_amount') }}" required>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Item Qty</label>
                                <input type="number" min="1" name="item_qty" class="form-control" value="{{ old('item_qty') }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Gst Rate</label>
                                <select class="full-width" data-init-plugin="select2" name="gst_rate" data-disable-search="true" required>
                                    <option value="">Select</option>
                                    @foreach(\App\Models\PackageItem::getGstPercentage() as $percentage)
                                        <option value="{{ $percentage }}">{{ $percentage }}%</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>HSN Code</label>
                                <input type="text" name="hsn_code" class="form-control" required>
                            </div>
                            <small><a href="https://cleartax.in/s/gst-hsn-lookup" class="pull-right" target="_blank">Don't Know the Code? Click Here</a></small>
                            <div class="text-center">
                                <button class="btn btn-danger">Add Item</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    {{ $package->name }} Items
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Amount</th>
                            <th>Qty</th>
                            <th>GST</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($package->items as $index => $item)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $item->name }}</td>
                                <td>Rs. {{ $item->amount }}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->gst->percentage }}% (Code: {{ $item->gst->code }})</td>
                                <td>
                                    <a href="{{ route('admin-package-items-update', ['id' => $item->id]) }}" class="btn btn-xs btn-theme">
                                        Edit
                                    </a>
                                    <a href="{{ route('admin-package-items-view', ['id' => $package->id, 'delete_item_id' => $item->id]) }}" class="btn btn-xs btn-danger">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop