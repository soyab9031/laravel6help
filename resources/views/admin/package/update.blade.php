@extends('admin.template.layout')

@section('title', 'Update Package')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Package:admin-package-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $package->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Amount</label>
                                <input type="text" name="amount" class="form-control" value="{{ $package->amount }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Capping</label>
                                <input type="number" min="0" name="capping" class="form-control" value="{{ $package->capping }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Sponsor Income</label>
                                <input type="number" min="0" name="sponsor_income" class="form-control" value="{{ $package->sponsor_income }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>PV</label>
                                <input type="text" name="pv" class="form-control" value="{{ $package->pv }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $package->status == 1 ? 'selected' : '' }} >Active</option>
                                    <option value="0" {{ $package->status == 0 ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                                <textarea class="form-control" name="declaration" id="name" cols="30" rows="10" placeholder="Description Or Declaration" data-gramm="true" data-gramm_editor="true" aria-invalid="false" style="z-index: auto; position: relative; line-height: normal; font-size: 14px; transition: none; background: transparent !important;">{{ $package->declaration }}</textarea>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-danger"> Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop