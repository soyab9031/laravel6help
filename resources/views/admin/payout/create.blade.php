@extends('admin.template.layout')

@section('title', 'Create Payout')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Payout:admin-payout-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" id="processPayoutForm" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <ol>
                                <li>Blocked Users & Payment Stopped Users will not get Payout</li>
                                <li>Users will also get debit entry for "Withdrawal" and their balance will be zero.</li>
                                <li>Active and Paid Users will be considerable for this Process</li>
                            </ol>
                            <hr>
                            <div class="text-center">
                                <button type="button" class="btn btn-lg btn-rounded btn-primary m-r-20 m-b-10 processPayoutBtn">
                                    Process the Payout
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-transparent">
                    <div class="card-body" style="padding: 0 1rem;">
                        <h3>
                            Ingenious Payout Services
                        </h3>
                        <p>
                            <b>How it Works ?</b> <br>
                            All the Core Calculation will be credited into user's Wallet with TDS & Some Admin Charges, When You process the Payout at Your Selected time, We consider all the Credited Entries from wallet & Make them Debit.
                        </p>
                        <p>
                            <b>Payout Preview</b> <br>
                            You can see the Payout Preview of each user at below with wallet details. Also you can make changes on it through <a href="{{ route('admin-wallet-transaction') }}">Wallet Transaction</a> and that will affect on this payout preview.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header separator m-b-10">
                <div class="card-title">Payout Preview</div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th colspan="3">
                                &nbsp;
                            </th>
                            <th colspan="2" class="text-center bg-success-darker text-white">Tax / Charges</th>
                            <th colspan="4"></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Total</th>
                            <th>TDS</th>
                            <th>Admin</th>
                            <th>Amount</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($payouts) == 0)
                            <tr>
                                <td class="text-center" colspan="10">
                                    No Pending Entries for Process
                                </td>
                            </tr>
                        @endif
                        @foreach($payouts as $index => $payout)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>
                                    {{ $payout->user->detail->full_name }} ({{ $payout->user->tracking_id }})
                                    <br>
                                    Joined At: {{ $payout->user->created_at->format('M d, Y') }}
                                </td>
                                <td>{{ $payout->total }}</td>
                                <td>{{ $payout->tds }}</td>
                                <td>{{ $payout->admin_charge }}</td>
                                <td>{{ $payout->amount }}</td>
                                <td>
                                    <a href="{{ route('admin-wallet-status', ['tracking_id' => $payout->user->tracking_id]) }}" class="btn btn-primary btn-sm">Details</a>
                                </td>
                            </tr>
                        @endforeach
                        @if(count($payouts) > 0)
                            <tr>
                                <td></td>
                                <td>Total Users: {{ $payouts->count() }}</td>
                                <td class="text-danger">{{ number_format($payouts->sum('total')) }}</td>
                                <td>{{ $payouts->sum('tds') }}</td>
                                <td>{{ $payouts->sum('admin_charge') }}</td>
                                <td class="text-danger">{{ number_format($payouts->sum('amount')) }}</td>
                                <td></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        $('.processPayoutBtn').click(function (e) {

            e.preventDefault();

            var form = $('#processPayoutForm');
            var self = $(this);

            self.attr('disabled', true);

            swal({
                title: "Are you sure?",
                text: 'Are you confirm to process the Payout ?',
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then( function (isConfirm) {
                
                if (isConfirm) {

                    swal('Processing', 'Do not Close or Refresh this page..!!', 'info');
                    form.submit();
                }
                else {
                    self.attr('disabled', false);
                }

            });
        });
    </script>

@stop