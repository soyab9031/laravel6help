@extends('admin.template.layout')

@section('title', 'Payout Record Bulk Update')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Payout Report:admin-payout-view,Bulk Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6 offset-md-3 offset-lg-3">
                                    <div class="form-group">
                                        <label>Select payout Sheet</label>
                                        <input type="file" name="csv_file" id="csvFile">
                                    </div>
                                    <p class="text-danger text-center text-uppercase">Only CSV File is allowed</p>
                                    <div class="text-center">
                                        <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-transparent">
                    <div class="card-body" style="padding: 0 0.3rem;">
                        <h3>
                            Ingenious Payout Bulk Update
                        </h3>
                        <p class="text-justify">
                            <b>How it Works ?</b> <br>
                            After the Payout Process, Download the Excel Sheet from <a href="{{ route('admin-payout-view') }}">Payout Report Page</a>, Submit to Bank or Online Transfer System. After this Process you will get UTR or REFERENCE Number of Individual Payout Transfer and update those records in Previously downloaded sheet. Upload that sheet here.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>

        $('#csvFile').filer({
            limit: 1, maxSize: 5, extensions: ['csv'], changeInput: true, showThumbs: true,
            captions: {
                errors: {
                    filesType: "Only CSV is allowed to be uploaded.",
                }
            }
        });
    </script>
@stop