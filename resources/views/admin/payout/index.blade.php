@extends('admin.template.layout')

@section('title', 'Payout Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Payout:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="status" data-disable-search="true">
                                    <option value="">Select</option>
                                    <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}>Transferred</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('admin-payout-view', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status'), 'download' => 'Yes']) }}" class="btn btn-info btn-sm" title="Download">
                                <i class="fa fa-cloud-download"></i> <span class="bold">Download</span>
                            </a>
                            <a href="{{ route('admin-payout-bulk-update') }}" class="btn btn-success btn-sm" title="Bulk Update">
                                <i class="fa fa-database"></i> <span class="bold">Bulk Update</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{-- Records --}}
        @if($payouts->count() == 0)
            <div class="card card-transparent m-b-10">
                <div class="card-body no-padding">
                    <div class="card card-default m-b-5">
                        <div class="card-body text-center">
                            <i class="fa fa-stack-exchange fa-3x text-danger"></i>
                            <h3><span class="semi-bold">No Records</span></h3>
                            <p>Our System has no any Payout records, Process the Payout & then come back here</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @foreach($payouts as $index => $payout)
            <div class="card card-transparent m-b-10">
                <div class="card-body no-padding">
                    <div class="card card-default m-b-5">
                        <div class="card-header">
                            <div class="card-title"># {{ \App\Library\Helper::tableIndex($payouts, $index) }} | {{ $payout->created_at->format('M d, Y h:i A') }}</div>
                            <div class="card-controls">
                                <a href="{{ route('admin-payout-update', ['id' => $payout->id]) }}" class="btn btn-sm btn-theme">Edit</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>{{ $payout->user->detail->full_name }} <span class="bold">({{ $payout->user->tracking_id }})</span></h4>
                                    <hr class="m-t-5 m-b-5">
                                    <div class="inline">
                                        <p class="hint-text m-t-5">
                                            Bank & Branch: {{ $payout->user->bank->bank_name }} ({{ $payout->user->bank->branch }}) <br>
                                            Account No: {{ $payout->user->bank->account_number }} <br>
                                            IFSC: {{ $payout->user->bank->ifsc }} <br>
                                            Type: {{ $payout->user->bank->type == 1 ? 'Saving' : 'Current' }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Total</th>
                                                <th>TDS</th>
                                                <th>Admin</th>
                                                <th>Amount</th>
                                                <th>Transfer Details</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{ $payout->total }}</td>
                                                <td>{{ $payout->tds }}</td>
                                                <td>{{ $payout->admin_charge }}</td>
                                                <td>{{ $payout->amount }}</td>
                                                <td>
                                                    Ref: {{ $payout->reference_number ? $payout->reference_number : 'N.A' }} <br>
                                                    {{ $payout->transfer_type_name }}
                                                </td>
                                                <td>
                                                    @if($payout->status == \App\Models\Payout::PENDING_TYPE)
                                                        <span class="label label-danger">Pending</span>
                                                    @else
                                                        <span class="label label-inverse">Transferred</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <ul class="list-group m-t-5">
                                        <li class="list-group-item">
                                            Remarks <span class="pull-right"> {{ $payout->remarks }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        {{ $payouts->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status') ])->links() }}
    </div>
@stop
