@extends('admin.template.layout')

@section('title', 'TDS Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,TDS Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="status" data-disable-search="true">
                                    <option value="">Payment Status</option>
                                    <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}>Transferred</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-payout-tds-report', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status'), 'download' => 'Yes']) }}" class="btn btn-info btn-sm" title="Download">
                                <i class="fa fa-cloud-download"></i> <span class="bold">Download</span>
                            </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>PAN</th>
                            <th>Payout</th>
                            <th class="bg-danger text-white">TDS</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tdsReports as $index => $tdsReport)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($tdsReports, $index) }}</td>
                                <td>{{ $tdsReport->user->detail->full_name }} <br> <span class="text-primary">({{ $tdsReport->user->tracking_id }})</span></td>
                                <td>{{ $tdsReport->user->detail->pan_number ? : 'N.A' }}</td>
                                <td>{{ round($tdsReport->total_payout, 2) }}</td>
                                <td>{{ round($tdsReport->total_tds, 2) }}</td>
                            </tr>
                        @endforeach
                        <tr class="bg-danger-lighter">
                            <td colspan="3" class="text-right">Total</td>
                            <td>{{ number_format(collect($tdsReports->items())->sum('total_payout'), 2) }}</td>
                            <td>{{ number_format(collect($tdsReports->items())->sum('total_tds'), 2) }}</td>
                        </tr>
                        </tbody>
                    </table>
                    {{ $tdsReports->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status') ])->links()}}
                </div>
            </div>
        </div>
    </div>
@stop
