<table class='table table-bordered table-hover custom-table'>
    <tr>
        <td><b>Name</b></td>
        <td>
            {{ $user->detail->full_name }}<br/>
            <a href='{{ route('admin-user-update', ['id' => $user->id]) }}' target='_blank'>Edit</a> |
            <a href='{{ route('admin-user-account-access', ['id' => $user->id]) }}' target='_blank'>View</a> |
            <a href='{{ route('admin-business-sponsors-view', ['tracking_id' => $user->tracking_id]) }}' target='_blank'>Sponsors</a> |
            <a href='{{ route('admin-wallet-status', ['tracking_id' => $user->tracking_id]) }}' target='_blank'>Wallet</a>
        </td>
    </tr>
    <tr>
        <td><b>Join Date</b></td>
        <td>{{ \Carbon\Carbon::parse($user->created_at)->format('M d, Y') }}</td>
    </tr>
    <tr>
        <td><b>Activation Date</b></td>
        <td>{{ $user->paid_at ? \Carbon\Carbon::parse($user->paid_at)->format('M d, Y') : 'N.A' }}</td>
    </tr>
    {{--<tr>--}}
        {{--<td><b>Package</b></td>--}}
        {{--<td>{{ $user->package ? $user->package->name : 'N.A' }} ({{ $user->package ? $user->package->pv : '' }})--}}
    {{--</tr>--}}
    @php $binary_total = $user->current_binary_status @endphp
    <tr>
        <td><b>Left/Right</b></td>
        <td>
            {{ $binary_total->left }} / {{ $binary_total->right }}
        </td>
    </tr>
    <tr>
        <td><b>LPV/RPV</b></td>
        <td>
            {{ $binary_total->total_left }} / {{ $binary_total->total_right }}
        </td>
    </tr>
    <tr>
        <div class='table-responsive'>
            <table class='table table-bordered table-hover custom-table'>
                <tr>
                    <td><b>Sponsor By</b></td>
                    <td>
                        {{ $user->sponsorBy ? $user->sponsorBy->tracking_id : '' }}
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>{{ $user->address->city }}</td>
                </tr>
            </table>
        </div>
    </tr>
</table>