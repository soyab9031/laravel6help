@extends('admin.template.layout')

@section('title', 'Binary Tree')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Binary Tree:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row m-b-20">
                        <div class="col-md-6 offset-md-3 offset-lg-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text primary"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search Tracking Id" name="tracking_id" class="form-control typeahead" value="{{ Request::get('tracking_id') }}" id="search-suggestion">
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row m-b-10 text-center">
                    <div class="col-md-2 col-4 offset-md-1">
                        <img src="/user-assets/images/tree/active.svg" width="45"><br/>
                        <span class="help-text">Active</span>
                    </div>
                    <div class="col-md-2 col-4">
                        <img src="/user-assets/images/tree/inactive.svg" width="45"><br/>
                        <span class="help-text">Inactive</span>
                    </div>
                    <div class="col-md-2 col-4">
                        <img src="/user-assets/images/tree/terminated.svg" width="45"><br/>
                        <span class="help-text">Blocked</span>
                    </div>
                    <div class="col-md-2 col-4">
                        <img src="/user-assets/images/tree/payment-stop.svg" width="45"><br/>
                        <span class="help-text">Payment Stop</span>
                    </div>
                    <div class="col-md-2 col-4">
                        <img src="/user-assets/images/tree/empty.svg" width="45"><br/>
                        <span class="help-text">Empty</span>
                    </div>
                </div>
                <div class="tree-holder m-t-30 table-responsive">
                    <table id="levelTree" cellpadding="0" cellspacing="0" border="0" align="center" style="zoom: 1; transform-origin: 0; margin-bottom: 200px;">
                        @php
                        $total_binary = $primary->current_binary_status;
                        @endphp
                        <tbody>
                        <tr class="node-cells">
                            <td class="node-cell" colspan="4">
                                <div class="node text-center">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h5 class="text-danger"><b>LEFT : {{ $total_binary->left }}</b></h5>
                                            <h6 class="text-info"><b>LPV : {{ $total_binary->total_left }}</b></h6>
                                        </div>

                                        <div class="col-md-6">
                                            @include('admin.business.binary.tree-component', ['user' => $primary, 'level' => 0, 'primary' => true])
                                        </div>
                                        <div class="col-md-3">
                                            <h5 class="text-danger"><b>RIGHT : {{ $total_binary->right }}</b></h5>
                                            <h6 class="text-info"><b>RPV : {{ $total_binary->total_right }}</b></h6>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr><td colspan="4"><div class="line down"></div></td></tr>
                        <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                        <tr>
                            @foreach($children as $child)
                                <td class="node-cell" colspan="2">
                                    <table>
                                        <tr class="node-cells">
                                            <td class="node-cell" colspan="4">
                                                <div class="node text-center">
                                                    @include('admin.business.binary.tree-component', ['user' => $child->user, 'level' => 1])
                                                </div>
                                            </td>
                                        </tr>
                                        <tr><td colspan="4"><div class="line down"></div></td></tr>
                                        <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                                        <tr>
                                            @foreach($child->children as $child2)
                                                <td class="node-cell children" colspan="2">
                                                    <table>
                                                        <tr class="node-cells">
                                                            <td class="node-cell" colspan="4">
                                                                <div class="node text-center">
                                                                    @include('admin.business.binary.tree-component', ['user' => $child2->user, 'level' => 2])
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr><td colspan="4"><div class="line down"></div></td></tr>
                                                        <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                                                        <tr>
                                                            @foreach($child2->children as $child3)
                                                                <td class="node-cell children" colspan="2">
                                                                    <table>
                                                                        <tr class="node-cells">
                                                                            <td class="node-cell" colspan="4">
                                                                                <div class="node text-center">
                                                                                    @include('admin.business.binary.tree-component', ['user' => $child3->user, 'level' => 3])
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                    </table>
                                                </td>
                                            @endforeach
                                        </tr>
                                    </table>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>

                @if ($primary->id != 1)
                    <div class="text-center m-tb-20">
                        <a href="{{ route('admin-business-binary-tree') }}" class="btn btn-theme">
                            <i class="sl-home"></i> Home
                        </a>
                        <a href="{{ route('admin-business-binary-tree', ['tracking_id' => $primary->parentBy ? $primary->parentBy->tracking_id : '' ]) }}" class="btn btn-danger">
                            <i class="sl-arrow-up"></i> Move UP
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>

@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/app/tree.css?v=dec2019">
@stop

@section('import-javascript')
    <script src="/admin-assets/js/typehead.js"></script>
@stop

@section('page-javascript')
    <script>
        $(function () {

            $('[data-toggle="popover"]').popover();

            /* Help to Set Tree in Center in Mobile View*/
            var outerContent = $('.tree-holder');
            var innerContent = $('.tree-holder > #levelTree');

            outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
        });

        /* Search Suggestion */
        let users = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('user_details'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '/admin-panel/business/binary/api-tree-suggestions?search=%Q',
            remote: {
                url: '/admin-panel/business/binary/api-tree-suggestions?search=%Q', wildcard: '%Q'
            }
        });

        $('#search-suggestion').typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
            order: "asc",
            offset: true,
            searchOnFocus: true
        },{
            name: 'users',
            displayKey: 'user_details',
            source: users.ttAdapter(),

        }).on('typeahead:selected', function(e, data){
            INGENIOUS.blockUI(true);
            window.location = '{{ route("admin-business-binary-tree") }}?' + 'tracking_id=' + data.tracking_id;
        });
    </script>
@stop