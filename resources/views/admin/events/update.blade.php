@extends('admin.template.layout')

@section('title', 'Update Event')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Events:admin-events-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" id="createEvent" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Category</label>
                                <select name="category_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ $event->category_id == $category->id ? 'selected' : null }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" autocomplete="off" value="{{ $event->name }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Location / Venue / City</label>
                                <input type="text" name="location" class="form-control" autocomplete="off" value="{{ $event->location }}">
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>State</label>
                                <select name="state_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                    @foreach (\App\Models\State::active()->get() as $state)
                                        <option value="{{ $state->id }}" {{ $event->state_id == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label> Start At</label>
                                        <input type="text" name="start_at" placeholder="Enter Start Date" id="startDate" value="{{ \Carbon\Carbon::parse($event->start_at)->format('d-m-Y') }}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label> End At</label>
                                        <input type="text" name="end_at" placeholder="Enter End Date" id="endDate" value="{{ \Carbon\Carbon::parse($event->end_at)->format('d-m-Y') }}" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <label>Description</label>
                                <textarea class="form-control summernote" name="description" placeholder="Description">{{ $event->description }}</textarea>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2 mt-2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $event->status == 1 ? 'selected' : null }}>Active</option>
                                    <option value="2" {{ $event->status == 2 ? 'selected' : null }}>Inactive</option>
                                </select>
                            </div>
                            <div class="text-center mt-2">
                                <button type="submit" class="btn btn-primary btn-lg"> Update </button>&nbsp;
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#startDate, #endDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
        });

        jQuery(document).ready(function () {

            $('.summernote').summernote({
                height: 250,
                placeholder: 'write here...',
                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
                onfocus: function (e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function (e) {
                    $('body').removeClass('overlay-disabled');
                },

            });
        });
    </script>
@stop

@section('import-javascript')
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-css')
    <style>
        .note-editor .btn-group {
            display: inline-block;
        }
    </style>
@stop