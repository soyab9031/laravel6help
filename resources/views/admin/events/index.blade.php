@extends('admin.template.layout')

@section('title', 'Events')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Events:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default-select2">
                                <select name="category_id" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="" disabled selected>Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ Request::get('category_id') == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('admin-event-create') }}" class="btn btn-primary btn-sm"> Create New <i class="fa fa-plus-circle"></i> </a>
                            <a href="{{ route('admin-event-category') }}" class="btn btn-info btn-sm"> Manage Category <i class="fa fa-list"></i> </a>
                        </div>
                    </div>
                </form>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Created At</th>
                        <th>Category</th>
                        <th>Admin</th>
                        <th>Event Details</th>
                        <th>Description</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $index => $event)
                        <tr>
                            <td>{{ \App\Library\Helper::tableIndex($events, $index) }}</td>
                            <td>{{ $event->created_at->format('M d, Y') }}</td>
                            <td>{{ $event->category->name }}</td>
                            <td>{{ $event->admin ? $event->admin->name : '-' }}</td>
                            <td>
                                {{ $event->name }}
                            </td>
                            <td>{{ \Str::limit(strip_tags($event->description), 20) }}</td>
                            <td>{{ $event->location }}, {{ $event->state->name }}</td>
                            <td>
                                @if($event->status == \App\Models\Event::ACTIVE)
                                    <span class="label label-warning text-dark">Active</span>
                                @else
                                    <span class="label label-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-event-update', ['id' => $event->id]) }}" class="btn btn-sm btn-primary">
                                    Update
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $events->appends(['category_id' => Request::get('category_id'), 'search' => Request::get('search')])->links() }}
            </div>
        </div>
    </div>

@stop
