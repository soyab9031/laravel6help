@extends('admin.template.layout')

@section('title', 'Pins')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Pins:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control"
                                       value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range"
                                       value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter"
                                       readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select name="status" class="full-width" data-init-plugin="select2">
                                <option value="" selected disabled="">Filter</option>
                                <option value="unused" {{ Request::get('status') == 'unused' ? 'selected' : null }}>
                                    Unused
                                </option>
                                <option value="used" {{ Request::get('status') == 'used' ? 'selected' : null }}>Used
                                </option>
                                <option value="blocked" {{ Request::get('status') == 'blocked' ? 'selected' : null }}>
                                    Blocked
                                </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <select name="package_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                    @foreach ($packages as $package)
                                        <option value="{{ $package->id }}" {{ $package->id == Request::get('package_id') ? 'selected' : null }}>
                                            {{ $package->name.' ( Rs.'.$package->amount.')' }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-danger btn-sm"><i class="fa fa-search"></i> Search</button>
                            <a href="{{ route('admin-pin-view', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status'), 'package_id' => Request::get('package_id'), 'export' => 'yes']) }}"
                               class="btn btn-info text-white btn-sm"> <i class="fa fa-download"></i> Download </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Number</th>
                            <th>Package</th>
                            <th>Transfer To</th>
                            <th>Used By</th>
                            <th>Status</th>
                            <th>History</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pins as $index => $pin)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($pins, $index) }}</td>
                                <td>{{ $pin->created_at->format('M d, Y') }}</td>
                                <td>
                                    <a href="javascript:void(0)"
                                       title="Click to Copy"
                                       onclick="INGENIOUS.copyToClipboard(this, 'Pin has been copied', 1)">{{ $pin->number }}</a><br>
                                    @if($pin->package->status== \App\Models\Package::ACTIVE)
                                        @if($pin->status == 0)
                                            <button type="button" class="btn btn-icon btn-danger btn-xs mt-1"
                                                    onclick="openUpgradeModal({{ $pin->toJson() }})">
                                                Use Pin
                                            </button>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    {{ $pin->package->name }} <br> (Rs. {{ $pin->package->amount }})
                                </td>
                                <td>
                                    {{ $pin->user->detail->full_name }} <br>
                                    ({{ $pin->user->tracking_id }})
                                </td>
                                <td>
                                    @if($pin->package_order)
                                        <span class="text-danger">
                                            {{ $pin->package_order->user->detail->full_name }}
                                            <br> ({{ $pin->package_order->user->tracking_id }})
                                        </span>
                                    @else
                                        N.A
                                    @endif
                                </td>
                                <td>
                                    @if ($pin->status == 0 )
                                        <span class="label label-success">Unused</span>
                                    @elseif ($pin->status == 1)
                                        <span class="label label-danger">Used</span>
                                    @else
                                        <span class="label label-warning">Block</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-pin-transfer-history', ['id' => $pin->id]) }}"
                                       class="btn btn-icon btn-info btn-xs">History</a>
                                </td>
                                <td>
                                    @if($pin->status != 1)
                                        <a href="{{ route('admin-pin-update', ['id' => $pin->id]) }}"
                                           class="btn btn-icon btn-theme btn-xs">Update</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $pins->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'package_id' => Request::get('package_id')])->links() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="usePinModal" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel3"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Use PIN</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="{{ route('admin-pin-use') }}" id="usePinForm">
                        {{ csrf_field() }}
                        <ul class="list-group mt-2 mb-2"></ul>
                        <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                                <label>Search User</label>
                                <input type="text" class="form-control tracking_id_input" required autocomplete="off">
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-danger" type="button"
                                        onclick="INGENIOUS.getUser(this, 'tracking_id_input')">Search
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button type="button"
                                    onclick="INGENIOUS.formConfirmation('#usePinForm', 'Are You confirm to Use this pin?')"
                                    class="btn btn-primary">Use Pin
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        function openUpgradeModal(pin) {

            let listGroup = $('ul.list-group');

            $('.list-group-item').remove();
            $('.hidden_pin_id').remove();

            let information = '<li class="list-group-item">Package: ' + pin.package.name + ' (Rs. ' + pin.package.amount + ')</li>' +
                '<li class="list-group-item text-dark">PV: ' + pin.package.pv + ' </li>' +
                '<li class="list-group-item text-danger">PIN Number: ' + pin.number + ' </li>';

            listGroup.append(information);

            listGroup.after('<input class="hidden_pin_id" type="hidden" name="pin_id" value="' + pin.id + '" readonly>');

            $('#usePinModal').modal({
                backdrop: 'static', keyboard: false
            });
        }
    </script>
@stop
