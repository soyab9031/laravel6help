@extends('admin.template.layout')

@section('title', 'Dashboard')

@section('content')
    @breadcrumb(Dashboard:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-lg-3 mb-3">
                <div class="list-group">
                    <a href="{{ route('admin-user-view') }}">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-primary-lighter text-white rounded ml-n1">
                                <i class="fa fa-users fa-lg text-primary"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">{{ number_format($user_count->total) }}</div>
                                <div class="font-small-2">Total Users</div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('admin-user-view', ['user_status' => 'active']) }}">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-success-lighter text-white rounded ml-n1">
                                <i class="fa fa-user-plus fa-lg text-success"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">{{ number_format($user_count->active) }}</div>
                                <div class="font-small-2">Active Users</div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('admin-user-view', ['user_status' => 'inactive']) }}">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-danger-lighter text-white rounded ml-n1">
                                <i class="fa fa-user-times fa-lg text-danger"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">{{ number_format($user_count->inactive) }}</div>
                                <div class="font-small-2">Inactive Users</div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('admin-user-view', ['dateRange' => \Carbon\Carbon::now()->format('d M Y') . ' - ' . \Carbon\Carbon::now()->format('d M Y')]) }}">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-warning-light text-white rounded ml-n1">
                                <i class="fa fa-user-circle fa-lg text-dark"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">{{ number_format($user_count->today) }}</div>
                                <div class="font-small-2">Today's Users</div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card no-border widget-loader-bar m-b-10">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header top-left top-right">
                                            <div class="card-title">
                                                <span class="fs-11 all-caps">Pending Documents <i class="fa fa-chevron-right"></i></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)" data-toggle="tooltip" class="card-refresh" data-original-title="Uploaded Documents by User which are still pending to Verify">
                                                            <i class="card-icon card-icon-refresh"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                            <h3 class="no-margin p-b-5"> {{ number_format($pending_documents) }}</h3>
                                            <span class="small hint-text pull-left">Documents</span>
                                            <a href="{{ route('admin-document-view') }}" class="pull-right small text-primary">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card no-border widget-loader-bar m-b-10">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header top-left top-right">
                                            <div class="card-title">
                                                <span class="fs-11 all-caps">Open Tickets <i class="fa fa-chevron-right"></i></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)" data-toggle="tooltip" class="card-refresh" data-original-title="Open Support Tickets by the Users">
                                                            <i class="card-icon card-icon-refresh"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                            <h3 class="no-margin p-b-5"> {{ number_format($open_supports) }}</h3>
                                            <span class="small hint-text pull-left">Support Tickets</span>
                                            <a href="{{ route('admin-user-support') }}" class="pull-right small text-primary">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card no-border widget-loader-bar m-b-10">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header top-left top-right">
                                            <div class="card-title">
                                                <span class="fs-11 all-caps">Website Inquiries <i class="fa fa-chevron-right"></i></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)" data-toggle="tooltip" class="card-refresh" data-original-title="Website Inquiries Sended By Users Who Visit Website">
                                                            <i class="card-icon card-icon-refresh"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                            <h3 class="no-margin p-b-5"> {{ number_format($contactInquiries) }}</h3>
                                            <span class="small hint-text pull-left">Website Inquiries </span>
                                            <a href="{{ route('admin-website-support') }}" class="pull-right small text-primary">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-lg-4">--}}
                        {{--<div class="card no-border widget-loader-bar m-b-10">--}}
                            {{--<div class="container-xs-height full-height">--}}
                                {{--<div class="row-xs-height">--}}
                                    {{--<div class="col-xs-height col-top">--}}
                                        {{--<div class="card-header top-left top-right">--}}
                                            {{--<div class="card-title">--}}
                                                {{--<span class="fs-11 all-caps">Total Pins <i class="fa fa-chevron-right"></i></span>--}}
                                            {{--</div>--}}
                                            {{--<div class="card-controls">--}}
                                                {{--<ul>--}}
                                                    {{--<li>--}}
                                                        {{--<a href="javascript:void(0)" data-toggle="tooltip" class="card-refresh" data-original-title="Total Generated Pins by this system">--}}
                                                            {{--<i class="card-icon card-icon-refresh"></i>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row-xs-height">--}}
                                    {{--<div class="col-xs-height col-top">--}}
                                        {{--<div class="p-l-20 p-t-50 p-b-40 p-r-20">--}}
                                            {{--<h3 class="no-margin p-b-5"> {{ number_format($pins) }}</h3>--}}
                                            {{--<span class="small hint-text pull-left">Total Generated Pins</span>--}}
                                            {{--<a href="{{ route('admin-pin-view') }}" class="pull-right small text-primary">View All</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-lg-4">
                        <div class="card no-border widget-loader-bar m-b-10">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header top-left top-right">
                                            <div class="card-title">
                                                <span class="fs-11 all-caps">New Pins Request <i class="fa fa-chevron-right"></i></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)" data-toggle="tooltip" class="card-refresh" data-original-title="New Pins Request">
                                                            <i class="card-icon card-icon-refresh"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                            <h3 class="no-margin p-b-5"> {{ number_format($pinsRequest) }}</h3>
                                            <span class="small hint-text pull-left">New Pins Request</span>
                                            <a href="{{ route('admin-pin-request-view',[ 'status' => 1]) }}" class="pull-right small text-primary">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 mb-2 d-flex">
                <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle align-self-stretch d-flex flex-column">
                    <div class="padding-25">
                        <div class="pull-left">
                            <h2 class="text-primary no-margin">Last 7 Days Business</h2>
                            <p class="no-margin">Days & Sales</p>
                        </div>
                        <h3 class="pull-right semi-bold"><sup><small class="semi-bold">Rs.</small></sup> {{ number_format($turnover->sum('amount')) }}</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="auto-overflow">
                        <table class="table table-condensed table-hover">
                            <tbody>
                            @foreach($turnover as $item)
                                <tr>
                                    <td class="b-r b-dashed b-grey w-25">
                                        <span class="font-montserrat fs-18">{{ $item->date->format('M d, Y') }}</span>
                                    </td>
                                    <td class="w-25">
                                        <span class="font-montserrat fs-18">Rs. {{ number_format($item->amount) }}</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 m-b-10 d-flex">
                <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle align-self-stretch d-flex flex-column">
                    <div class="padding-25">
                        <div class="pull-left">
                            <h2 class="text-primary no-margin">Last 10 Days</h2>
                            <p class="no-margin">Joining</p>
                        </div>
                    </div>
                    <div class="auto-overflow">
                        <canvas id="userJoiningChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        var ctx = document.getElementById('userJoiningChart');
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach(collect($chart_joined_users)->sortBy('date') as $detail)
                        '{{ $detail->date->format('M d') }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Number of Joinings',
                    data: [{{ implode(collect($chart_joined_users)->sortBy('date')->pluck('user')->toArray(), ', ') }}],
                    backgroundColor: 'rgb(255, 99, 132)', borderColor: 'rgb(255, 99, 132)', borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true, precision: 0
                        }
                    }]
                }
            }
        });
    </script>
@stop

@section('import-javascript')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
@stop