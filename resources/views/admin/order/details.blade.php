@extends('admin.template.layout')

@section('title', 'Order Details Of ' . $order->customer_order_id)

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Orders:admin-shopping-order-view, Details:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Details</h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Order Id: <span class="pull-right ">{{ $order->customer_order_id }}</span>
                                </li>
                                @if($order->user)
                                    <li class="list-group-item">
                                        User : <span class="pull-right">{{ $order->user->detail->full_name }}
                                            ({{ $order->user->tracking_id }}) </span>
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    Order Date: <span
                                            class="pull-right">{{ $order->created_at->format('M d, Y h:i A') }}</span>
                                </li>
                                <li class="list-group-item">
                                    Your Message: <span class="pull-right">{{ $order->remarks }}</span>
                                </li>
                                <li class="list-group-item">
                                    Mobile No: <span class="pull-right">{{ $order->user->mobile }}</span>
                                </li>
                                @if($order->approved_at)
                                    <li class="list-group-item">
                                        <a href="{{ route('admin-order-invoice-download', ['customer_order_id' => $order->customer_order_id]) }}" class="btn btn-danger">Download <i class="fa fa-download"></i></a>
                                        <a href="{{ route('admin-order-invoice-download', ['customer_order_id' => $order->customer_order_id, 'viewOnly' => 'YES']) }}" target="_blank" class="btn btn-primary">View <i class="fa fa-eye"></i> </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Summary</h5>
                            <ul class="list-group">
                                <li class="list-group-item bg-dark text-white">
                                    Order Status: <span
                                            class="pull-right">{{ \App\Models\Order::getStatus($order->status) }}</span>
                                </li>
                                <li class="list-group-item">
                                    Total Items: <span class="pull-right">{{ $order->details->sum('qty') }}</span>
                                </li>
                                <li class="list-group-item">
                                    Total BV: <span class="pull-right">{{ $order->total_bv }}</span>
                                </li>
                                @if($order->wallet > 0)
                                    <li class="list-group-item">
                                        Wallet <span class="pull-right">{{ $order->wallet }}</span>
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    Amount <span class="pull-right">{{ $order->amount }}</span>
                                </li>
                                <li class="list-group-item text-danger">
                                    Total <span class="pull-right">{{ $order->total }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <h5>Order Items</h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>GST</th>
                                <th>Amount</th>
                                <th>Qty</th>
                                <th>Total PV</th>
                                <th class="text-danger">Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">CODE: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->gst->percentage }}%</td>
                                    <td>{{ $detail->selling_price }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->total_bv }}</td>
                                    <td class="text-danger">{{ $detail->total_amount }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop