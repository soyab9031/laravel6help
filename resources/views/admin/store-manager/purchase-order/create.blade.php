@extends('admin.template.layout')

@section('title', 'Create Purchase Order')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Purchsae Orders:admin-store-manager-purchase-order-view, Create:active)

    <div class="container-fluid container-fixed-lg" id="purchaseOrderPage">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Supplier</label>
                            <vue-select2 :options="suppliers" place-holder="Select Supplier" allow-search="0" v-model="supplier_id"></vue-select2>
                        </div>
                        <div class="form-group">
                            <label>Promise Delivery Days</label>
                            <input type="text" onkeypress="INGENIOUS.numericInput(event)" class="form-control" v-model="delivery_days">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Your Message or Remarks</label>
                            <textarea class="form-control" cols="30" rows="6" placeholder="Your Message" v-model="remarks"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" v-if="supplier_id">
            <div class="card-content collapse show">
                <div class="card-body table-responsive">
                    <div class="form-group">
                        <label>Search Product / Item</label>
                        <vue-select2 :options="search_items" place-holder="Select From Existing Item" empty-after-select="1" allow-search="0" @change-select2="setProductToOrder"></vue-select2>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text primary"><i class="fa fa-list-alt"></i></span></div>
                            <input type="text" v-model="custom_item" placeholder="Custom or New Item" class="form-control" value="">
                            <div class="input-group-prepend" @click="addCustomItem()"><span class="input-group-text bg-danger text-white"><i class="fa fa-plus"></i></span></div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="40%">Item</th>
                            <th width="10%">Qty</th>
                            <th width="10%">Cost</th>
                            <th width="10%">Tax</th>
                            <th width="15%">Total</th>
                            <th width="5%">Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(order_item, index) of order_items">
                            <td>@{{ index+1 }}</td>
                            <td>
                                <span class="text-dark">@{{ order_item.name }} </span><code class="pull-right">Code: @{{ order_item.code }}</code>
                                <br>
                                <input type="text" class="form-control input-sm mt-1" v-model="order_item.note" placeholder="Extra Note">
                            </td>
                            <td>
                                <input type="text" onkeypress="INGENIOUS.numericInput(event)" class="form-control form-control-sm" v-model="order_item.selected_qty">
                            </td>
                            <td>
                                <input type="text" onkeypress="INGENIOUS.numericInput(event)" class="form-control form-control-sm" v-model="order_item.purchase_price">
                            </td>
                            <td>
                                <select class="form-control form-control-sm" v-model="order_item.tax_percentage">
                                    <option :value="gst_rate" v-for="gst_rate of [0, 5, 12, 18, 28]">@{{ gst_rate }}%</option>
                                </select>
                            </td>
                            <td>
                                <span class="font-weight-bold font-small-3 text-danger">@{{ parseFloat(order_item.selected_qty*order_item.purchase_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-social-icon btn-xs btn-danger" title="Remove" @click="removeItem(index)">
                                    <span class="fa fa-close"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row" v-if="order.amount > 0">
                        <div class="col-md-6">
                            <ul class="list-group mt-3">
                                <li class="list-group-item">
                                    Amount: <span class="pull-right">@{{ order.amount }}</span>
                                </li>
                                <li class="list-group-item">
                                    GST/TAX: <span class="pull-right">@{{ order.gst }}</span>
                                </li>
                                <li class="list-group-item font-weight-bold font-small-3 bg-danger text-white">
                                    Total: <span class="pull-right">@{{ order.total_amount }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center mt-5">
                                <button class="btn btn-danger" type="button" @click="createOrder">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el:"#purchaseOrderPage",
            data:{
                suppliers: {!! json_encode($suppliers) !!},
                items: {!! collect($items)->toJson() !!},
                custom_item: null,
                supplier_id: null,
                delivery_days: 0,
                remarks: null,
                order_items: [],
                order: {
                    amount: 0, total_qty: 0, gst: 0, total_amount: 0
                },
            },
            watch: {
                order_items: {
                    handler(){
                        this.cartManager();
                    },
                    deep: true
                }
            },
            methods: {
                setProductToOrder: function (selected_item_id) {

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.id) === parseInt(selected_item_id)
                    }).head().value();

                    if (!item_exists) {

                        let selected_product = _.chain(this.items).filter(item => {
                            return parseInt(item.id) === parseInt(selected_item_id)
                        }).head().value();

                        if (selected_product) {
                            this.order_items.push(selected_product);
                        }

                    }

                },
                addCustomItem: function () {

                    if (!this.custom_item)
                        return false;

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return order_item.name === this.custom_item
                    }).head().value();

                    if (!item_exists) {

                        this.order_items.push({
                            id: 0,
                            code: 'N.A',
                            name: this.custom_item,
                            note: null,
                            selected_qty: 0,
                            tax_percentage: 0,
                            purchase_price: 0,
                        });

                    }

                    this.custom_item = '';

                },
                removeItem: function (index) {

                    let self = this;
                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to remove this item ?',
                        icon: "info", buttons: true, dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {
                            self.$delete(self.order_items, index);
                            swal('Removed', 'Item has been removed form List', 'success');
                        }
                    });
                },
                cartManager: function () {

                    this.order.amount = _.sumBy(this.order_items, item => {
                        return item.selected_qty*item.purchase_price;
                    });

                    this.order.total_qty = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty);
                    });

                    this.order.gst = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty) * parseFloat(item.purchase_price*item.tax_percentage/100);
                    });

                    this.order.total_amount = this.order.amount + this.order.gst;

                },
                createOrder: function () {

                    let self = this;

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Create New Purchase Order?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.post('', {
                                order_items: self.order_items,
                                order_details: self.order,
                                supplier_id: self.supplier_id,
                                delivery_days: self.delivery_days,
                                remarks: self.remarks,
                            }).then(response => {

                                if (response.data.status) {
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }

                            });

                        }

                    });

                }
            },
            computed: {

                search_items: function () {

                    return this.items.map(item => {
                        item.value = item.id;
                        item.label = item.name + ' (Code: ' + item.code + ')';
                        return item;
                    });

                }
            }
        });

    </script>
@stop