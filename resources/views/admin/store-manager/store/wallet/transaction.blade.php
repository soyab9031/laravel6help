@extends('admin.template.layout')

@section('title', 'Create Store Wallet Transaction')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Wallet Records:admin-store-manager-store-wallet-view, Create Transaction:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-lg-3 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Select Store</label>
                                <select class="full-width" data-init-plugin="select2" name="store_id">
                                    <option value="">Select</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}" {{ old('store_id') == $store->id ? 'selected' : ''}}>{{ $store->name . ' - ' . $store->tracking_id }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ old('type') == 1 ? 'selected' : '' }}>Credit</option>
                                    <option value="2" {{ old('type') == 2 ? 'selected' : '' }}>Debit</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Amount</label>
                                <input type="text" onkeypress="INGENIOUS.numericInput(event, true)" name="amount" class="form-control" value="{{ old('amount') }}" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="remarks"cols="10" rows="5" placeholder="Remarks" >{{ old('remarks') }}</textarea>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger"> Create </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop