@extends('admin.template.layout')

@section('title', 'Commission Report of Stores')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Stores:admin-store-manager-store-commission-report-view,Commission Report of Stores:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="store_id">
                                    <option value="">Select Store</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}" {{ Request::get('store_id') == $store->id ? 'selected' : ''}}>{{ $store->name . ' - ' . $store->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="8">
                                <span class="text-success"><i class="fa fa-square"></i> Credit &nbsp;</span>
                                <span class="text-danger"><i class="fa fa-square"></i> Debit</span>
                            </th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Admin Manager</th>
                            <th>Store</th>
                            <th>Amount</th>
                            <th>Opening</th>
                            <th>Closing</th>
                            <th>Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No transactions found..!!</td>
                            </tr>
                        @endif
                        @foreach($transactions as $index => $transaction)
                            <tr class="{{ $transaction->type == 1 ? 'bg-complete-lighter' : 'bg-danger-lighter' }}">
                                <td>{{ \App\Library\Helper::tableIndex($transactions, $index) }}</td>
                                <td>{{ $transaction->created_at->format('M d, Y h:i A') }}</td>
                                <td>{{ $transaction->admin_id ? $transaction->admin->name : 'N.A' }}</td>
                                <td>{{ $transaction->store->name }} <br> ({{ $transaction->store->tracking_id }})</td>
                                <td><span class="font-weight-bold">{{ $transaction->amount }}</span></td>
                                <td>{{ $transaction->opening_amount }}</td>
                                <td>{{ $transaction->closing_amount }}</td>
                                <td>{{ $transaction->remarks }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $transactions->appends(['dateRange' => Request::get('dateRange'), 'store_id' => Request::get('store_id')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop