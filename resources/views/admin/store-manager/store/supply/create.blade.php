@extends('admin.template.layout')

@section('title', 'Do Supply to Store')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Supplies:admin-store-manager-store-supply-view, Create:active)

    <div class="container-fluid container-fixed-lg" id="supplyPage">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group form-group-default form-group-default-select2">
                            <label>Select Store</label>
                            <vue-select2 :options="stores" place-holder="Select Store" allow-search="0" v-model="store_id"></vue-select2>
                        </div>
                        <div class="form-group form-group-default form-group-default-select">
                            <label>Select Delivery Type</label>
                            <select class="form-control" v-model="delivery_type">
                                <option value="">Select</option>
                                <option value="1">Self Pickup</option>
                                <option value="2">Courier</option>
                                <option value="3">Transport</option>
                            </select>
                        </div>
                        <div class="form-group form-group-default" v-if="delivery_type > 1">
                            <label>Courier or Transport Company</label>
                            <input type="text" v-model="courier_name" class="form-control">
                        </div>
                        <div class="form-group form-group-default" v-if="delivery_type > 1">
                            <label>Courier or Transport Docket No.</label>
                            <input type="text" v-model="courier_docket_number" class="form-control">
                        </div>
                        <div class="form-group form-group-default">
                            <label>Your Message or Remarks</label>
                            <textarea class="form-control" cols="30" rows="6" placeholder="Your Message" v-model="remarks"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-group" v-if="store">
                            <li class="list-group-item bg-primary-darker text-white">
                                Store: <span class="pull-right">@{{ store.name }}</span>
                            </li>
                            <li class="list-group-item bg-success-darker text-white" v-if="store.type == 1">
                                Type: <span class="pull-right">Master</span>
                            </li>
                            <li class="list-group-item bg-success-darker text-white" v-if="store.type == 3">
                                Type: <span class="pull-right">WareHouse</span>
                            </li>
                            <li class="list-group-item">
                                Store Id: <span class="pull-right">@{{ store.tracking_id }}</span>
                            </li>
                            <li class="list-group-item text-danger">
                                Wallet Balance: <span class="pull-right">Rs. @{{ store.wallet_balance }}</span>
                            </li>
                            <li class="list-group-item">
                                City: <span class="pull-right">@{{ store.city }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3 class="text-success">
                            Supply to Store
                        </h3>
                        <p>Once Supply created, it will add to Store's stocks & You can add upto their wallet balance</p>
                        <p>After adding the Stock to Master Store, They can supply items to mini stores or other stores</p>
                        <p>Once, Supply done <code>you can not edit</code>. However, a Return option is available.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" v-if="store_id && delivery_type">
            <div class="card-content collapse show">
                <div class="card-body table-responsive">
                    <div class="form-group">
                        <label>Search Product / Item</label>
                        <vue-select2 :options="search_items" place-holder="Select Item" empty-after-select="1" allow-search="0" @change-select2="setProductToOrder"></vue-select2>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="45%">Item</th>
                            <th width="10%">DP Amount</th>
                            <th width="5%" class="bg-danger text-white">Company Stock</th>
                            <th width="5%" v-if="store.type == 1">Supply Discount</th>
                            <th width="10%">Qty</th>
                            <th width="13%">Total</th>
                            <th width="3%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(order_item, index) of order_items">
                            <td>@{{ index+1 }}</td>
                            <td>
                                @{{ order_item.name | str_limit(35) }} <br> <code>Code: @{{ order_item.code }}</code>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.distributor_price).toFixed(2) }}</span>
                            </td>
                            <td class="bg-danger-lighter">
                                <span class="font-weight-bold text-black">@{{ order_item.balance }}</span>
                            </td>

                            <td v-if="store.type == 1">
                                @{{ order_item.supply_discount }}
                            </td>
                            <td>
                                <input type="number" min="1" :max="order_item.balance" onkeypress="INGENIOUS.numericInput(event, false)" class="form-control form-control-sm" v-model="order_item.selected_qty">
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.selected_qty*order_item.distributor_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-social-icon btn-xs btn-danger" title="Remove" @click="removeItem(index)">
                                    <span class="fa fa-close"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row" v-if="order.amount > 0">
                        <div class="col-md-6">
                            <ul class="list-group mt-3">
                                <li class="list-group-item">
                                    Amount: <span class="pull-right">@{{ order.amount }}</span>
                                </li>
                                <li class="list-group-item">
                                    GST/TAX: <span class="pull-right">@{{ order.gst }}</span>
                                </li>
                                <li class="list-group-item" v-if="store.type == 1">
                                    Supply Discount: <span class="pull-right">@{{ order.total_supply_discount }}</span>
                                </li>
                                <li class="list-group-item font-weight-bold font-small-3 bg-danger text-white">
                                    Total: <span class="pull-right">@{{ order.total_amount - order.total_supply_discount }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center mt-5">
                                <button class="btn btn-danger btn-lg" type="button" @click="createOrder">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el:"#supplyPage",
            data:{
                stores: {!! json_encode($stores) !!},
                items: {!! collect($items)->toJson() !!},
                store_id: '',
                store: null,
                delivery_type: '',
                courier_name: null,
                courier_docket_number: null,
                remarks: null,
                order_items: [],
                order: {
                    amount: 0, total_qty: 0, gst: 0, total_amount: 0 , total_supply_discount: 0
                },
            },
            watch: {
                delivery_type: function (value) {

                    if (parseInt(value) === 1) {
                        this.courier_name = null;
                        this.courier_docket_number = null;
                    }

                },
                store_id: function (value) {
                    this.getStoreDetails(value);
                },
                order_items: {
                    handler() {
                        this.calculateSummary();
                    },
                    deep: true
                }
            },
            methods: {
                getStoreDetails: function (store_id) {

                    this.store = null;

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route("admin-api-store-manager-get-store-details") }}', {
                        params: {
                            store_id: store_id
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {
                            this.store = response.data.store;
                        }
                        else {
                            swal('Oops', response.data.message, 'error');
                        }

                    });

                },
                setProductToOrder: function (selected_item_id) {

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.id) === parseInt(selected_item_id)
                    }).head().value();

                    if (!item_exists) {

                        let selected_product = _.chain(this.items).filter(item => {
                            return parseInt(item.id) === parseInt(selected_item_id)
                        }).head().value();

                        if (selected_product) {
                            this.order_items.push(selected_product);
                        }

                    }

                },
                removeItem: function (index) {

                    let self = this;
                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to remove this item ?',
                        icon: "info", buttons: true, dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {
                            self.$delete(self.order_items, index);
                            swal('Removed', 'Item has been removed form List', 'success');
                        }
                    });
                },
                calculateSummary: function () {

                    this.order.amount = _.sumBy(this.order_items, item => {
                        return _.round((item.selected_qty*item.distributor_price), 2);
                    });

                    this.order.total_qty = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty);
                    });

                    if (this.store.type === 1) {

                        _.map(this.order_items, function(order_item){
                            _.round(order_item.supply_discount = (order_item.selected_qty * order_item.distributor_price) * 0.06)
                        });

                        this.order.total_supply_discount = _.sumBy(this.order_items, order_item => {
                            return parseInt(order_item.supply_discount);
                        });

                    }

                    this.order.gst = 0;

                    this.order.total_amount = this.order.amount + this.order.gst;

                },
                createOrder: function () {

                    if (this.order.total_amount > this.store.wallet_balance) {
                        swal('Warning', this.store.name + ' - Store has not enough balance to fulfill this Supply', 'warning');
                        return false;
                    }

                    let stock_not_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.selected_qty) > parseInt(order_item.balance)
                    }).head().value();

                    if (stock_not_exists) {
                        swal('Stock Warning', stock_not_exists.name + ' has not enough Stock, Current Stock: ' + stock_not_exists.balance, 'warning');
                        return false;
                    }

                    let self = this;

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Create New Supply ?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.post('{{ route("admin-store-manager-store-supply-create") }}', {
                                order_items: self.order_items,
                                total_supply_discount: self.total_supply_discount,
                                order_details: self.order,
                                store_id: self.store_id,
                                delivery_type: self.delivery_type,
                                courier_name: self.courier_name,
                                courier_docket_number: self.courier_docket_number,
                                remarks: self.remarks,
                            }).then(response => {

                                if (response.data.status) {
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }

                            });

                        }

                    });

                }
            },
            computed: {

                search_items: function () {

                    return this.items.map(item => {
                        item.value = item.id;
                        item.label = item.name + ' (Code: ' + item.code + ')';
                        return item;
                    });

                }
            }
        });

    </script>
@stop