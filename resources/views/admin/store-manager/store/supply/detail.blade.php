@extends('admin.template.layout')

@section('title', 'Store Supply Details # ' . $supply->id)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Supply Details:admin-store-manager-store-supply-view, Details:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Generate By:</b> <span class="pull-right">{{ $supply->admin->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Supply ID:</b> <span class="pull-right">{{ $supply->admin_supply_id }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Store:</b> <span class="pull-right">{{ $supply->store->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Store ID:</b> <span class="pull-right">{{ $supply->store->tracking_id }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Address:</b> <span class="pull-right">{{ $supply->store->address }}, {{ $supply->store->city }}, {{ $supply->store->state->name }} - {{ $supply->store->pincode }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Delivery Method:</b> <span class="pull-right">{{ $supply->delivery_type_name }}</span>
                            </li>
                            @if($supply->delivery_type <> 1)
                                <li class="list-group-item">
                                    <b>Transport/ Courier:</b> <span class="pull-right">{{ $supply->courier_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Docket Number:</b> <span class="pull-right">{{ $supply->courier_docket_number }}</span>
                                </li>
                            @endif
                        </ul>
                        <div class="mt-3 text-center">
                            <a href="{{ route('admin-store-manager-store-supply-delivery-note', ['id' => $supply->id ]) }}" class="btn btn-success btn-xs"> Delivery Note <i class="fa fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @if(!$supply->sender_store_id)
                <div class="col-md-4" id="details">
                    <div class="card">
                        <div class="card-body">
                            @if($supply->status == 1)
                                <form method="post" onsubmit="INGENIOUS.blockUI(true)">
                                    {{ csrf_field() }}
                                    <div class="form-group form-group-default ">
                                        <label>Select Status Type</label>
                                        <select class="form-control"  v-model="delivery_type" name="delivery_type">
                                            <option value="1">Self Pickup</option>
                                            <option value="2">Courier</option>
                                            <option value="3">Transport</option>
                                        </select>
                                    </div>
                                    <div v-if="delivery_type > 1" >
                                        <div class="form-group form-group-default" >
                                            <label>Courier or Transport Company</label>
                                            <input type="text" class="form-control" name="courier_name">
                                        </div>
                                        <div class="form-group form-group-default" >
                                            <label>Courier or Transport Docket No.</label>
                                            <input type="text" class="form-control" name="courier_docket_number">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Your Message or Remarks</label>
                                        <textarea class="form-control" cols="30" rows="6" placeholder="Your Message" name="remarks"></textarea>
                                    </div>
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Select Status Type</label>
                                        <select name="status" class="form-control" data-init-plugin="select2" data-disable-search="true">
                                            <option value="1" >Pending</option>
                                            <option value="2" >Delivered</option>
                                        </select>
                                    </div>
                                    <div class="form-group" >
                                        <div class="text-center">
                                            <button class="btn btn-danger"> Update </button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <div class="form-group form-group-default">
                                    <div class="text-center mt-5">
                                        <a href="{{ route('admin-store-manager-store-supply-view') }}" class="btn btn-danger"> Back </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>Qty</th>
                                <th>Total</th>
                                @if($supply->store->state_id == 4)
                                    <th>SGST</th>
                                    <th>CGST</th>
                                    <th>IGST</th>
                                @else
                                    <th>SGST</th>
                                    <th>CGST</th>
                                    <th>IGST</th>
                                @endif
                                <th>Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($supply->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->taxable_amount }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->total_taxable_amount }}</td>
                                    @if($supply->store->state_id == 4)
                                        <td>{{ number_format(($detail->total_tax_amount / 2) ,2) }}</td>
                                        <td>{{ number_format(($detail->total_tax_amount / 2) ,2) }}</td>
                                        <td>0</td>
                                    @else
                                        <td>0</td>
                                        <td>0</td>
                                        <td>{{ number_format($detail->total_tax_amount ,2) }}</td>
                                    @endif
                                    <td> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger-darker">
                                <td colspan="2" class="text-right">Total</td>
                                <td></td>
                                <td>{{ collect($supply->details)->sum('qty') }}</td>
                                <td>{{ 0 }}</td>
                                <td>{{ 0 }}</td>
                                <td>{{ 0 }}</td>
                                <td>{{ collect($supply->details)->sum('total_tax_amount') }}</td>
                                <td>{{ collect($supply->details)->sum('total_amount') }}</td>
                            </tr>
                            <tr class="text-danger">
                                <td colspan="7"></td>
                                <td>Discount</td>
                                <td> - {{number_format($supply->total_supply_discount,2)}}</td>
                            </tr>
                            <tr>
                                <td colspan="7"></td>
                                <td class="text-white bg-primary-dark">Total</td>
                                <td class="text-white bg-primary-dark">₹ {{$supply->total}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el:"#details",
            data:{
                delivery_type: {{ json_encode($supply->delivery_type) }},
            },

        });
    </script>
@stop