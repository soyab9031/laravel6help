@php
    $f = new \App\Library\CurrencyInWord;
@endphp
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $filename }}</title>
    <style>
        @media print {
            @page  {
                margin: .5cm .3cm;
            }
        }
        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .datagrid table {
            border-collapse: collapse;
            text-align: left;
            width: 100%;
        }

        .datagrid {
            font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif;
            background: #fff;
            overflow: hidden;
            border: 1px solid #000;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .datagrid table td,
        .datagrid table th {
            padding: 3px 10px;
        }

        .datagrid table tbody tr.header td {
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#FFFAC3', endColorstr='#80141C');
            background-color: #000;
            color: #FFFFFF;
            font-size: 10px;
            font-weight: bold;
            padding: 7px;
        }
        .datagrid table tbody tr.table-title td
        {
            text-align: center;
            background: #6241A7;
            color: #fff;
            font-size: 12px !important;
            border-top: 2px solid #000;
            padding: 7px;
            font-weight: 600;
        }
        .datagrid table tbody tr.child-category-title td
        {
            text-align: center;
            font-size: 13px;
        }
        .datagrid table thead th:first-child {
            border: none;
        }

        .datagrid table tbody td {
            color: #000;
            border-left: 1px solid #000;
            font-size: 10px;
            font-weight: normal;
            padding: 8px;
            border-bottom: 1px solid;
        }
        .datagrid table tbody .alt td {
            background: #F7CDCD;
            color: #000;
        }
        .datagrid table tbody td:first-child {
            border-left: none;
        }

        .datagrid table tbody tr:last-child td {
            border-bottom: none;
        }
        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }
    </style>
</head>
<body>
<table style="width: 100%">
    <tr>
        <td width="50%">
            <img src="{{ config('project.url') }}user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-3" width="100px">
            <div style="font-size: 16px;margin-top: 1rem" >
                @if($supply->sender_store_id)
                    {{ $supply->senderStore->name }} ({{ $supply->senderStore->tracking_id }})
                @else
                    {{ $company_profile->where('title', 'company_name')->first()->value }}
                @endif
            </div>
            <div class="address" style="font-size: 14px; color: #122b40;">
                <div style="font-size: 14px; color: #122b40;">Address: </div>
                @if($supply->sender_store_id)
                    {{ $supply->senderStore->address }}<br>
                    {{ $supply->senderStore->city }}<br>
                    {{ $supply->senderStore->state->name }}<br>
                    Pincode: {{ $supply->senderStore->pincode }}
                @else
                    {{ $company_profile->where('title', 'address')->first()->value }}
                    <br>{{ $company_profile->where('title', 'city')->first()->value }},
                    {{ $company_profile->where('title', 'state')->first()->value }} - {{ $company_profile->where('title', 'pincode')->first()->value }}
                @endif
                <br>
            </div>
        </td>
        <td width="50%" style="text-align: right">
            <div style="font-size: 30px; font-weight: 600; text-transform: uppercase;">INVOICE/DELIVERY NOTE</div>
            <table style="width: 100%;border-spacing: 0;border-collapse: collapse;border: 1px solid;margin: 10px 0px;">
                <tbody>
                <tr>
                    <td style="padding: 5px;border-right: 1px solid;border-bottom: 1px solid;">Reference Number </td>
                    @if($supply->sender_store_id)
                        <td style="padding: 5px;border-bottom: 1px solid;">{{ $supply->store_supply_id }}</td>
                    @else
                        <td style="padding: 5px;border-bottom: 1px solid;">{{ $supply->admin_supply_id }}</td>
                    @endif
                </tr>
                <tr>
                    <td style="padding: 5px;border-right: 1px solid;border-bottom: 1px solid;">Date</td>
                    <td style="padding: 5px;border-bottom: 1px solid;">{{ \Carbon\Carbon::parse($supply->created_at)->format('M d, Y') }}</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<div class="datagrid" style="margin-top: 15px;">
    <table>
        <tbody>
        <tr>
            <td width="50%">
                <div style="font-size: 15px; margin-bottom: 5px; font-weight: 600;">TO</div>
                <div style="font-size: 15px;">Name : {{ $supply->store->name }} (TID : {{ $supply->store->tracking_id }})</div>
            </td>
            <td width="50%">
                <div class="address" style="font-size: 14px; color: #122b40;">
                    <div style="font-size: 14px; color: #122b40; font-weight: 800">Address: </div>
                    {{ $supply->store->address }},<br>
                    {{ $supply->store->city }} - {{ $supply->store->pincode }} ({{ $supply->store->state ? $supply->store->state->name : 'N.A' }})<br>
                    Contact: {{ $supply->store->mobile }}<br>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <table style="width: 100%;">
        <tbody>
        <tr class="table-title">
            <td>#</td>
            <td>Item</td>
            <td>Amount</td>
            <td>Qty</td>
            <td>Total</td>
            @if($supply->store->state_id == 4)
                <td>SGST</td>
                <td>CGST</td>
                <td>IGST</td>
            @else
                <td>SGST</td>
                <td>CGST</td>
                <td>IGST</td>
            @endif
            <td>Total Amount</td>
        </tr>
        @foreach($supply->details as $index => $detail)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>
                    {{ $detail->product_price->product->name }}
                </td>
                <td>{{ $detail->taxable_amount }}</td>
                <td>{{ $detail->qty }}</td>
                <td>{{ $detail->total_taxable_amount }}</td>
                @if($supply->store->state_id == 4)
                    <td>{{ number_format(($detail->total_tax_amount / 2) ,2) }}</td>
                    <td>{{ number_format(($detail->total_tax_amount / 2) ,2) }}</td>
                    <td>0</td>
                @else
                    <td>0</td>
                    <td>0</td>
                    <td>{{ number_format($detail->total_tax_amount ,2) }}</td>
                @endif
                <td> {{ number_format($detail->total_amount, 2) }}</td>
            </tr>
        @endforeach
        <tr class="text-white bg-danger-darker">
            <td colspan="2" class="text-right">Total</td>
            <td></td>
            <td>{{ collect($supply->details)->sum('qty') }}</td>
            <td>{{ 0 }}</td>
            <td>{{ 0 }}</td>
            <td>{{ 0 }}</td>
            <td>{{ collect($supply->details)->sum('total_tax_amount') }}</td>
            <td>{{ collect($supply->details)->sum('total_amount') }}</td>
        </tr>
        <tr class="text-danger">
            <td colspan="7"></td>
            <td>Discount</td>
            <td> - {{number_format($supply->total_supply_discount,2)}}</td>
        </tr>
        <tr>
            <td colspan="7"></td>
            <td class="text-white bg-primary-dark">Total</td>
            <td class="text-white bg-primary-dark">₹ {{$supply->total}}</td>
        </tr>
        </tbody>
    </table>
</div>
<div style="text-align: center; margin-top: 20px; font-size: 14px;">
    This is a Computer Generated Delivery Note
</div>
</body>
</html>