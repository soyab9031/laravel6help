@extends('admin.template.layout')

@section('title', 'Store Registration Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Registration Request:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control"
                                       value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range"
                                       value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter"
                                       readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <select name="status" class="full-width" data-init-plugin="select2"
                                        data-disable-search="true">
                                    <option value="">Status Filter</option>
                                    <option value="1" {{ Request::get('status') ==  '1' ? 'selected' : '' }}>Pending
                                    </option>
                                    <option value="3" {{ Request::get('status') ==  '3' ? 'selected' : '' }}>Rejected
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search</button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Type</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($store_requests) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No New Request Available</td>
                            </tr>
                        @endif
                        @foreach($store_requests as $index => $store_request)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($store_requests, $index) }}</td>
                                <td>{{ $store_request->created_at->format('M d, Y h:i A') }}</td>
                                <td class="text-primary">
                                    {{ $store_request->name }} <br>({{ $store_request->tracking_id }})
                                </td>
                                <td>{{ $store_request->mobile }}</td>
                                <td>
                                    {{ \App\Models\StoreManager\Store::getStoreTypes($store_request->type)->name }}
                                </td>
                                <td>
                                    {{ $store_request->city }}
                                    ({{ $store_request->state ? $store_request->state->name : $store_request->country->name }}
                                    )
                                </td>
                                <td>
                                    @if($store_request->status == \App\Models\StoreManager\StoreRequest::PENDING)
                                        <span class="badge badge-warning">Pending</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-store-register-details', ['id' => $store_request->id ]) }}"
                                       class="btn btn-danger btn-xs"> Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $store_requests->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
