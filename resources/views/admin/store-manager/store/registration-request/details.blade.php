@extends('admin.template.layout')

@section('title', 'Details of Store Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Request:admin-store-manager-store-register-request,Details:active)

    <div class="container-fluid container-fixed-lg" id="storeCreatePage">

        <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Select Store Type</label>
                                <select name="type" class="form-control">
                                    <option value="">---Select---</option>
                                    @foreach(\App\Models\StoreManager\Store::getStoreTypes() as $storeType)
                                        <option value="{{ $storeType->id }}" {{ $store_request->type == $storeType->id ? 'selected' : null }}>
                                            {{ $storeType->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($store_request->sponsor)
                                <div class="form-group form-group-default">
                                    <label>Store Sponsor User</label>
                                    <input type="text" class="form-control"
                                           value="{{ $store_request->sponsor->detail->full_name }}" autocomplete="off"
                                           disabled>
                                </div>
                            @endif
                            <div class="form-group form-group-default">
                                <label>Password</label>
                                <input type="text" class="form-control" value="{{ $store_request->password }}"
                                       name="password" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Store Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control"
                                               value="{{ $store_request->name }}"
                                               autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Owner Name <span class="text-danger">*</span></label>
                                        <input type="text" name="owner_name" class="form-control"
                                               value="{{ $store_request->owner_name }}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control"
                                               value="{{ $store_request->email }}"
                                               autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Mobile <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="mobile"
                                               onkeypress="INGENIOUS.numericInput(event)"
                                               value="{{ $store_request->mobile }}"
                                               autocomplete="newMobile">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Store / WareHouse Address <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="address" cols="5" rows="5"
                                                  placeholder="Enter Store Address"
                                                  autocomplete="off">{{ $store_request->address }}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default">
                                                <label>City <span class="text-danger">*</span></label>
                                                <input type="text" name="city" class="form-control"
                                                       value="{{ $store_request->city }}" autocomplete="off">
                                            </div>
                                            <div class="form-group form-group-default">
                                                <label>Pincode <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="pincode"
                                                       value="{{ $store_request->pincode }}" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default form-group-default-select2">
                                                <label>Select State</label>
                                                <select name="state_id" class="full-width" data-init-plugin="select2">
                                                    <option value=""> Select State</option>
                                                    @foreach($states as $index => $state)
                                                        <option value="{{ $state->id }}" {{ $state->id == $store_request->state_id ? 'selected' : null }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group form-group-default form-group-default-select2">
                                                <label>Select Country</label>
                                                <select name="country_id" class="form-control"
                                                        data-init-plugin="select2"
                                                        required>
                                                    <option value=""> Select Country</option>
                                                    @foreach($countries as $country)
                                                        <option
                                                                value="{{ $country->id }}" {{ $store_request->country_id == $country->id ? 'selected' : null }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group form-group-default">
                                                <label>Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="1" {{ $store_request->status == 1 ? 'selected' : null }}>
                                                        Pending
                                                    </option>
                                                    <option value="2" {{ $store_request->status == 2 ? 'selected' : null }}>
                                                        Approved
                                                    </option>
                                                    <option value="3" {{ $store_request->status == 3 ? 'selected' : null }}>
                                                        Reject
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger btn-lg btn-rounded"> Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@stop
