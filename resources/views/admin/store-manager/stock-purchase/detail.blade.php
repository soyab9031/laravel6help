@extends('admin.template.layout')

@section('title', 'Stock Purchase # ' . $stock_purchase->id)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Stock Purchase:admin-store-manager-stock-purchase-view, Details # {{ $stock_purchase->id }}:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Supplier:</b> <span class="pull-right">{{ $stock_purchase->supplier->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Mobile:</b> <span class="pull-right">{{ $stock_purchase->supplier->mobile }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Address:</b> <span class="pull-right">{{ $stock_purchase->supplier->address }}, {{ $stock_purchase->supplier->city }}, {{ $stock_purchase->supplier->state->name }} - {{ $stock_purchase->supplier->pincode }}</span>
                            </li>
                        </ul>
                        <ul class="list-group mt-3">
                            <li class="list-group-item">
                                <b>Amount:</b> <span class="pull-right">{{ number_format($stock_purchase->amount, 2) }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>GST:</b> <span class="pull-right">{{ number_format($stock_purchase->tax_amount, 2) }}</span>
                            </li>
                            <li class="list-group-item text-danger font-small-3 font-weight-bold">
                                Total: <span class="pull-right">{{ number_format($stock_purchase->total, 2) }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Amount</th>
                                <th>Total Taxable Amount</th>
                                <th>GST</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stock_purchase->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }} <br>
                                        <code>Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->amount }}</td>
                                    <td>{{ $detail->total_taxable_amount }}</td>
                                    <td>{{ round($detail->total_taxable_amount * $detail->gst->percentage / 100, 2) }} <br> ({{ $detail->gst->percentage }}%)</td>
                                    <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop