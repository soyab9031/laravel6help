@extends('admin.template.layout')

@section('title', 'Our Suppliers')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Suppliers:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-2 pull-right">
                            <a href="{{ route('admin-store-manager-supplier-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th width="30%">Name</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($suppliers) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Suppliers Available</td>
                            </tr>
                        @endif
                        @foreach ($suppliers as $index => $supplier)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($suppliers, $index) }}</td>
                                <td>{{ $supplier->created_at->format('M d, Y h:i A') }}</td>
                                <td>{{ $supplier->name }}</td>
                                <td>
                                    {{ $supplier->mobile }} <br>
                                    {{ $supplier->email }}
                                </td>
                                <td>
                                    {{ $supplier->address}},
                                    {{ $supplier->city }},
                                    {{ $supplier->state->name }} - {{ $supplier->pincode }}
                                </td>
                                <td>
                                    @if( $supplier->status == 1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">In-Active</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-supplier-update', ['id' => $supplier->id ]) }}" class="btn btn-theme btn-xs"> Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $suppliers->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop