@extends('admin.template.layout')

@section('title', 'Offer Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Offer:admin-store-manager-offer-view,Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <h4><b>Offer:</b> {{ $offer->name }} - <span class="label label-danger">{{ $offer->type_name }}</span></h4>
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-store-manager-offer-report', ['id' => $offer->id]) }}" class="btn btn-theme btn-sm" title="Refresh"> <i class="fa fa-refresh"></i> </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Date</th>
                            <th>User</th>
                            <th>Order Id</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $index => $order)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                <td>{{ $order->created_at->format('M d, Y h:i A') }}</td>
                                <td class="text-danger">{{ $order->user->detail->full_name }}</td>
                                <td>{{ $order->customer_order_id }}</td>
                                <td>{{ number_format($order->total, 2) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $orders->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop