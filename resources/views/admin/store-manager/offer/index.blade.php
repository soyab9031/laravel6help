@extends('admin.template.layout')

@section('title', 'Offers')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Offers:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('admin-store-manager-offer-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>

                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="10%"> Date</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th width="15%">Period</th>
                            <th>Details</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offers as $index => $offer)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($offers, $index) }}</td>
                                <td>{{ $offer->created_at->format('M d, Y h:i A') }}</td>
                                <td class="text-danger">{{ $offer->name }}</td>
                                <td>
                                    <span class="label label-primary"> {{ $offer->type_name }} </span>
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($offer->start_at)->format('M d, Y') }}
                                    - {{ \Carbon\Carbon::parse($offer->end_at)->format('M d, Y') }}
                                </td>
                                <td>
                                    @if($offer->type == \App\Models\StoreManager\Offer::BILLING_TO_PRODUCT)
                                        Min Billing: Rs. {{ number_format($offer->min_amount) }} <br>
                                        Max Billing: Rs. {{ number_format($offer->max_amount) }} <br>
                                    @else
                                        N.A
                                    @endif
                                </td>
                                <td>
                                    @if($offer->status == 1)
                                        <span class="label label-success"> Active </span>
                                    @elseif($offer->status == 2)
                                        <span class="label label-danger">Inactive </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-info" href="{{ route('admin-store-manager-offer-report', ['id' => $offer->id]) }}" >Report</a>
                                    <a class="btn btn-xs btn-success" href="{{ route('admin-store-manager-offer-update', ['id' => $offer->id]) }}" >Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $offers->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop