@extends('admin.template.layout')

@section('title', 'Website Grievance')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Grievance:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">

            <div class="card-body">

                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created On</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($grievances as $index => $grievance)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($grievances, $index) }}</td>
                                <td>{{ $grievance->created_at->format('d-m-Y')}}</td>
                                <td>{{ $grievance->name }}</td>
                                <td>{{ $grievance->email }}</td>
                                <td>{{ $grievance->subject }}</td>
                                <td>{{  $grievance->message  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $grievances->links() }}
                </div>
            </div>
        </div>
    </div>


@stop