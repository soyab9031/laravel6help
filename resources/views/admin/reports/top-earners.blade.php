@extends('admin.template.layout')

@section('title', 'Top Earners')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Top Earners:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Joined At</th>
                            <th>User</th>
                            <th>Withdrawal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payouts as $index => $payout)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($payouts, $index) }}</td>
                                <td>{{ $payout->user->created_at->format('M d, Y') }}</td>
                                <td>
                                    {{ $payout->user->detail->full_name }} <br>
                                    ({{ $payout->user->tracking_id }})
                                </td>
                                <td>{{ number_format($payout->gross_total, 2) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $payouts->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop