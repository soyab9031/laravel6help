@extends('admin.template.layout')

@section('title', 'Sales')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Sales:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-lg-3 mb-3">
                <div class="list-group">
                    <div class="list-group-item d-flex align-items-center">
                        <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-primary-lighter text-white rounded ml-n1">
                            <i class="fa fa-inr fa-lg text-primary"></i>
                        </div>
                        <div class="flex-fill pl-3 pr-3">
                            <div class="font-medium-3">{{ number_format($turnover->sum('amount')) }}</div>
                            <div class="font-small-2">Total Amount</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Month</th>
                            <th>Total Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($turnover as  $item)
                            <tr>
                                <td class="b-r b-dashed b-grey w-25">
                                    <span class="font-montserrat fs-18">{{ $item->date->format('M d, Y') }}</span>
                                </td>
                                <td class="w-25">
                                    <span class="font-montserrat fs-18">Rs. {{ number_format($item->amount) }}</span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop