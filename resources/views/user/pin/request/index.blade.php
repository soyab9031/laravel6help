@extends('user.template.layout')

@section('title', 'My Pin Requests')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Pin Requests</h3>
        </div>
        <div class="content-header-left col-md-8 col-12 mb-2">
          <a href="{{ route('user-pin-make-request') }}" class="btn btn-primary btn-sm pull-right"><i class="la la-plus"></i> Create Request</a>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Created On</th>
                                        <th width="25%">Pin Details</th>
                                        <th>Payment Mode</th>
                                        <th>Ref. No.</th>
                                        <th>Bank Name</th>
                                        <th>Deposited On</th>
                                        <th>Status</th>
                                        <th>Remarks</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pins as $pin)
                                        <tr>
                                            <td>{{ $pin->created_at->format('M d, Y h:i A') }}</td>
                                            <td>
                                                @foreach(json_decode($pin->details) as $pin_detail)
                                                    <span class="d-block  text-primary">{{ $pin_detail->package_name }}</span>
                                                    ({{ $pin_detail->qty }} Pins X Rs. {{ $pin_detail->package_amount }})
                                                    <hr>

                                                @endforeach
                                            </td>
                                            <td>{{ $pin->payment_mode }}</td>
                                            <td>
                                                {{ $pin->reference_number }}
                                                @if($pin->image)
                                                    <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $pin->image }}" target="_blank" class="badge badge-info">
                                                        Reference Image
                                                    </a>
                                                @endif
                                            </td>
                                            <td>{{ $pin->bank_name }}</td>
                                            <td>{{ \Carbon\Carbon::parse($pin->deposited_at)->format('M d, Y h:i A')  }}</td>
                                            <td>
                                                @if($pin->status == 1)
                                                    <span class="badge badge-warning">Pending</span>
                                                @elseif($pin->status == 2)
                                                    <span class="badge badge-success">Approved</span>
                                                @else
                                                    <span class="badge badge-danger">Rejected</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $pin->remarks }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop