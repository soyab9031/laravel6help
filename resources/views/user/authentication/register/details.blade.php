@extends('user.template.registration.layout')

@section('title', 'Member / User Details for Registration')

@section('content')
    <div class="pt-2 pb-3">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-8 col-12 box-shadow-2 p-0">
                <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="card border-grey border-lighten-3 px-1 py-1">
                        <div class="card-header border-0">
                            <div class="text-center mb-0">
                                <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}"
                                     width="180px">
                            </div>
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Sponsor Upline: <span class="pull-right">{{ $sponsor_user->detail->full_name }}
                                        ({{ $sponsor_user->tracking_id }})</span>
                                </li>
                            </ul>
                            <h4 class="card-title text-center mt-1">
                                Select the Correct Organisation of your Upline where you wish to be placed.
                            </h4>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="radio radio-danger mt-1" {{ isset($selected_leg) ? "style=pointer-events:none;" : ''}}>
                                        <input type="radio" value="L" name="leg"
                                               id="leftLeg" {{ old('leg') == 'L' || $selected_leg == 'L' ? 'checked' : 'checked' }}>
                                        <label for="leftLeg">Left Team</label>
                                        <input type="radio" value="R" name="leg"
                                               id="rightLeg" {{ old('leg') == 'R' || $selected_leg == 'R' ? 'checked' : null }}>
                                        <label for="rightLeg">Right Team</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="font-medium-4 text-center">
                                Enter New User / Member Details
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <fieldset class="form-group">
                                        <label>Title</label>
                                        <select name="title" class="form-control square">
                                            <option value="Mr" {{ old('title') == 'Mr' ? 'selected' : null }}>Mr
                                            </option>
                                            <option value="Miss" {{ old('title') == 'Miss' ? 'selected' : null }}>Miss
                                            </option>
                                            <option value="Mrs" {{ old('title') == 'Mrs' ? 'selected' : null }}>Mrs
                                            </option>
                                            <option value="Mrs" {{ old('title') == 'Ms' ? 'selected' : null }}>Ms
                                            </option>
                                            <option value="Dr" {{ old('title') == 'Dr' ? 'selected' : null }}>Dr
                                            </option>
                                            <option value="Sri" {{ old('title') == 'Sri' ? 'selected' : null }}>Sri
                                            </option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control square" name="first_name"
                                               placeholder="First Name" value="{{ old('first_name') }}"
                                               autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control square" name="last_name"
                                               placeholder="Last Name" value="{{ old('last_name') }}"
                                               autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control square" name="email" placeholder="Email"
                                               value="{{ old('email') }}" autocomplete="off" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control square"
                                               onkeypress="INGENIOUS.numericInput(event)" name="mobile"
                                               value="{{ old('mobile') }}" placeholder="10 Digit Mobile Number"
                                               autocomplete="new-mobile" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <input type="text" name="birth_date" id="BirthDate" class="form-control"
                                               value="{{ old('birth_date') }}" placeholder="Date of Birth" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Enter Address Details
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Your Address</label>
                                        <textarea class="form-control square" name="address" rows="6"
                                                  placeholder="Enter Address">{{ old('address') }}</textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control square" name="city"
                                                       value="{{ old('city') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>State</label>
                                                <select name="state_id" class="form-control square" required>
                                                    <option value="">Select</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" {{ $state->id == old('state_id') ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>District</label>
                                                <input type="text" class="form-control square" name="district"
                                                       value="{{ old('district') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" class="form-control square"
                                                       onkeypress="INGENIOUS.numericInput(event)" name="pincode"
                                                       value="{{ old('pincode') }}" autocomplete="off">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Enter Aadhaar Card Details
                                    </div>
                                </div>
                                <div class="col-md-6 offset-3 col-sm-12">
                                    <fieldset class="form-group">
                                        <label>Aadhaar Card Number</label>
                                        <input type="text" class="form-control square"
                                               onkeypress="INGENIOUS.numericInput(event)" name="aadhaar_number"
                                               value="{{ old('aadhaar_number') }}" placeholder="Please Enter Aadhaar Card Number"
                                               autocomplete="off" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Account Details
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control square" name="username"
                                               value="{{ old('username') }}" placeholder="Enter Username">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput" name="password"
                                               placeholder="Enter Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button"
                                                    onclick="INGENIOUS.showPassword(this)">
                                                <i class="la la-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-1">
                                    <div class="checkbox check-danger">
                                        <input type="checkbox" id="terms" name="terms" required>
                                        <label for="terms">
                                            I agree to the Terms and Conditions and Privacy Policy
                                            For {{ config('project.company') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-1">
                                    <button type="submit"
                                            class="btn round btn-glow btn-bg-gradient-x-red-pink col-6 mr-1 mb-1">
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#BirthDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });
    </script>
@stop