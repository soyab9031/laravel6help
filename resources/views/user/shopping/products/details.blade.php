@extends('user.template.layout')

@section('title', 'Product Detail - ' . $product_price->product->name )

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">{{ $product_price->product->name }}</h3>
        </div>
    </div>

    <div class="content-body product-detail">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class='row '>
                            <div class="col-md-5 text-center">
                                <figure class="zoom ">
                                    <a href="{{ $product_price->primary_image  }}" class="post-item-img" target="_blank">
                                        <img style="max-height: 400px;" class="img-responsive p-1" alt="{{ $product_price->product->name }}"  src="{{ $product_price->primary_image  }} " />
                                    </a>
                                </figure>
                            </div>
                            <div class="col-md-7">
                                <h3>{{ $product_price->product->name }}</h3>
                                <i class="text-primary">Code : {{ $product_price->code }} </i>
                                <hr>
                                <ul class="list-group text-dark">
                                    <li class="list-group-item font-medium-2">
                                        MRP : <span class="pull-right">Rs. {{ $product_price->price }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Distributor Price : <span class="pull-right">Rs. {{ $product_price->distributor_price }}</span>
                                    </li>
                                    <li class="list-group-item bg-dark white">
                                        BV : <span class="pull-right"> {{ $product_price->points }}</span>
                                    </li>
                                </ul>
                                <div class="row mt-1 mb-1" id="productDetail">
                                    <div class="col-md-4">
                                        <span v-if="quantity.current > 0">
                                                <vue-counter-button v-model="quantity" min="0"></vue-counter-button>
                                            </span>
                                        <button class="btn btn-danger btn-lg" @click="addToCart" type="button" v-if="quantity.current == 0">
                                            ADD TO CART
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="{{ route('user-cart') }}" class="btn btn-lg bg-primary white"  v-if="quantity.current > 0">
                                            GO TO CART <i class="fa fa-arrow-right"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mt-2">
                                        <h4>Description</h4>
                                        {!! $product_price->product->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('page-javascript')

    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el: '#productDetail',
            data: {
                cart: null,
                product_price_id: parseInt('{{ $product_price->id }}'),
                quantity: {
                    action: null, current: 0, actual: 100
                }
            },
            watch: {
                quantity: {
                    handler: function () {
                        this.cartManager()
                    },
                    deep: true
                }
            },
            methods: {
                cartManager: function () {

                    if (this.quantity.action === 'increment') {
                        this.addToCart();
                    }
                    else if (this.quantity.action === 'decrement') {
                        this.removeFromCart();
                    }

                    if (this.quantity.current === 0)
                        this.quantity.action = null;
                },
                addToCart: function () {

                    /* Product Added To Cart*/
                    if (this.quantity.current === 0)
                        this.quantity.current = 1;

                    this.$http.get('{{route('user-cart-add-item')}}', {
                        params: {
                            product_price_id: this.product_price_id,
                            qty: this.quantity.current
                        }
                    }).then( response => {

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                    });
                },
                removeFromCart: function () {
                    /* Product Removed From Cart*/
                    this.$http.get('{{ route('user-cart-remove-item') }}', {
                        params: {
                            product_price_id: this.product_price_id,
                            qty: this.quantity.current
                        }
                    }).then( response => {

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                    });

                }
            },
            mounted: function() {

                this.$http.get('{{ route('user-cart-retrieve-items') }}').then( response => {

                    if (response.data.status) {
                        this.cart = response.data.cart;

                        let current_item = _.chain(this.cart).filter(cart_item => {
                            return parseInt(cart_item.product_price_id) === parseInt(this.product_price_id)
                        }).head().value();

                        if (current_item)
                            this.quantity.current = current_item.qty;
                    }
                });
            }
        });
    </script>
@stop