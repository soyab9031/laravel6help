@extends('user.template.layout')

@section('title', 'Products')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Products</h3>
        </div>
    </div>

    <div class="content-body product">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text primary"><i class="la la-search"></i></span>
                                    </div>
                                    <input type="text" placeholder="Search Products" name="search" class="form-control"
                                           value="{{ Request::get('search') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text primary"><i class="la la-list"></i></span>
                                    </div>
                                    <select name="category_id" class="form-control">
                                        <option value="">Filter Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ Request::get('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-danger">Search</button>
                                @refreshBtn('user')
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if(count($product_prices) == 0 )
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="text-center">
                            <i class="la la-exclamation-circle text-danger font-large-3"></i>
                            <h1>No Items Available</h1>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div id="productListPage">
                <div class="row">
                    <div class="col-md-3" v-for="(item,index) of items">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="img-thumbnail img-fluid text-center">
                                        <a :href="'/user/shopping/products/details/' + item.code" title="View Item">
                                            <img :src="item.primary_image" :title="item.code" width="100%">
                                        </a>
                                    </div>
                                    <div class="text-center mt-1">
                                        <a :href="'/user/shopping/products/details/' + item.code"
                                           class="btn btn-primary btn-sm">
                                            View
                                        </a>
                                    </div>
                                    <div class="mt-1">
                                        <div class="font-medium-1 text-dark text-center mb-1"
                                             style="min-height: 2.9rem;">
                                            @{{ item.product.name | str_limit(30) }} <br>
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item primary font-medium-1">
                                                MRP <span class="pull-right"> @{{ item.price }}</span>
                                            </li>
                                            <li class="list-group-item">
                                                DP <span class="pull-right">
                                                    @{{ item.distributor_price }}
                                            </span>
                                            </li>
                                            <li class="list-group-item bg-blue text-center white">
                                                @{{ item.points }} BV
                                            </li>
                                        </ul>
                                        <div class="text-center mt-1">
                                            <a href="javascript:void(0)" class="btn btn-sm btn-danger"
                                               @click="addToCart(item.id, 1)" v-if="item.quantity.current == 0">Add To
                                                Cart</a>
                                            <div class="row" v-if="item.quantity.current > 0">
                                                <div class="col-md-12">
                                                    <vue-counter-button v-model="item.quantity" min="0" max="100"
                                                                        :product-price-id="item.id"
                                                                        @change-counter-btn="changeCounterBtn($event)"></vue-counter-button>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <a href="{{ route('user-cart') }}"
                                                       class="btn btn-warning btn-sm mt-1">
                                                        View CART
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;

        new Vue({
            el: '#productListPage',
            data: {
                items: {!! json_encode($product_prices) !!}
            },
            methods: {
                changeCounterBtn: function (event) {

                    if (event.action === 'increment') {
                        this.addToCart(event.productPriceId, event.current);
                    } else if (event.action === 'decrement') {
                        this.removeFromCart(event.productPriceId, event.current);
                    }
                },
                addToCart: function (item_id, qty) {

                    this.$http.get('{{route('user-cart-add-item')}}', {
                        params: {
                            product_price_id: item_id,
                            qty: qty
                        }
                    }).then(response => {

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                        _.map(this.items, item => {
                            if (parseInt(item.id) === parseInt(item_id)) {
                                item.quantity.current = qty;
                            }
                        });

                    });
                },
                removeFromCart: function (product_price_id, quantity) {

                    this.$http.get('{{ route('user-cart-remove-item') }}', {
                        params: {
                            product_price_id: product_price_id,
                            qty: quantity,
                        }
                    }).then(response => {

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                        _.map(this.items, item => {
                            if (parseInt(item.id) === parseInt(product_price_id)) {
                                item.quantity.current = quantity;
                            }
                        });

                    });

                }
            }
        })
    </script>
@stop
