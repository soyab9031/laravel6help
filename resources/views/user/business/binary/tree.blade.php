@extends('user.template.layout')

@section('title', 'My Binary Tree')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Binary Tree</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">

                        <form action="" method="get">
                            <div class="row mb-2">
                                <div class="col-md-6 offset-md-3 offset-lg-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text primary"><i class="la la-search"></i></span>
                                        </div>
                                        <input type="text" placeholder="Search Tracking Id" name="tracking_id" class="form-control" value="{{ Request::get('tracking_id') }}">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row mb-1 text-center">
                            <div class="col-md-2 col-4 offset-md-3">
                                <img src="/user-assets/images/tree/active.svg" width="45"><br/>
                                <span class="help-text">Active</span>
                            </div>
                            <div class="col-md-2 col-4">
                                <img src="/user-assets/images/tree/inactive.svg" width="45"><br/>
                                <span class="help-text">Inactive</span>
                            </div>
                            <div class="col-md-2 col-4">
                                <img src="/user-assets/images/tree/empty.svg" width="45"><br/>
                                <span class="help-text">Empty</span>
                            </div>
                        </div>

                        <div class="tree-holder mt-3 table-responsive" style="margin-bottom: 150px;">
                            <table id="levelTree" cellpadding="0" cellspacing="0" border="0" align="center" style="zoom: 1; transform-origin: 0;">
                                @php
                                    $total_binary = $primary->current_binary_status;
                                @endphp
                                <tbody>
                                <tr class="node-cells">
                                    <td class="node-cell" colspan="4">
                                        <div class="node text-center">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <h5 class="text-danger"><b>LEFT : {{ $total_binary->left }}</b></h5>
                                                    <h6 class="text-info"><b>LPV : {{ $total_binary->total_left }}</b></h6>
                                                </div>
                                                <div class="col-md-6">
                                                    @include('user.business.binary.tree-component', ['user' => $primary, 'level' => 0])
                                                </div>
                                                <div class="col-md-3">
                                                    <h5 class="text-danger"><b>RIGHT : {{ $total_binary->right }}</b></h5>
                                                    <h6 class="text-info"><b>RPV : {{ $total_binary->total_right }}</b></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td colspan="4"><div class="line down"></div></td></tr>
                                <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                                <tr>
                                    @foreach($children as $leg1 => $child)
                                        <td class="node-cell" colspan="2">
                                            <table>
                                                <tr class="node-cells">
                                                    <td class="node-cell" colspan="4">
                                                        <div class="node text-center">
                                                            @include('user.business.binary.tree-component', ['user' => $child->user, 'parent' => $primary, 'level' => 1, 'leg' => $leg1])
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="4"><div class="line down"></div></td></tr>
                                                <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                                                <tr>
                                                    @foreach($child->children as $leg2 =>  $child2)
                                                        <td class="node-cell children" colspan="2">
                                                            <table>
                                                                <tr class="node-cells">
                                                                    <td class="node-cell" colspan="4">
                                                                        <div class="node text-center">
                                                                            @include('user.business.binary.tree-component', ['user' => $child2->user, 'parent' => $child->user, 'level' => 2, 'leg' => $leg2])
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr><td colspan="4"><div class="line down"></div></td></tr>
                                                                <tr><td class="line left">&nbsp;</td><td class="line right top">&nbsp;</td><td class="line left top">&nbsp;</td><td class="line right">&nbsp;</td></tr>
                                                                <tr>
                                                                    @foreach($child2->children as $leg3 => $child3)
                                                                        <td class="node-cell children" colspan="2">
                                                                            <table>
                                                                                <tr class="node-cells">
                                                                                    <td class="node-cell" colspan="4">
                                                                                        <div class="node text-center">
                                                                                            @include('user.business.binary.tree-component', [
                                                                                            'user' => $child3->user, 'parent' => $child2->user, 'level' => 3, 'leg' => $leg3
                                                                                            ])
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>
                                                                    @endforeach
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            </table>
                                        </td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        @if ($primary->id != Session::get('user')->id)
                            <div class="text-center m-tb-20">
                                <a href="{{ route('user-business-binary-tree') }}" class="btn btn-primary">
                                    <i class="la la-home"></i> Home
                                </a>
                                <a href="{{ route('user-business-binary-tree', ['tracking_id' => $primary->parentBy ? $primary->parentBy->tracking_id : '' ]) }}" class="btn btn-warning">
                                    <i class="la la-arrow-up"></i> Move Up
                                </a>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/app/tree.css">
@stop

@section('page-javascript')
    <script>
        $(function () {

            $('[data-toggle="popover"]').popover();

            var outerContent = $('.tree-holder');
            var innerContent = $('.tree-holder > #levelTree');

            outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
        })
    </script>
@stop