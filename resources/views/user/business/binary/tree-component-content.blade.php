<table class='table table-bordered table-hover custom-table'>
    <tr>
        <td><b>Name</b></td>
        <td>{{ $user->detail->full_name }}</td>
    </tr>
    <tr>
        <td><b>Join Date</b></td>
        <td>{{ \Carbon\Carbon::parse($user->created_at)->format('M d, Y') }}</td>
    </tr>
    <tr>
        <td><b>Sponsor By</b></td>
        <td>
            {{ $user->sponsorBy ? $user->sponsorBy->tracking_id : '' }}
        </td>
    </tr>
    {{--<tr>--}}
        {{--<td><b>Package</b></td>--}}
        {{--<td>{{ $user->package ? $user->package->name : 'N.A' }}--}}
    {{--</tr>--}}
    @php $binary_total = $user->current_binary_status; @endphp
    <tr>
        <td><b>Left/Right</b></td>
        <td>
            {{ $binary_total->left }} / {{ $binary_total->right }}
        </td>
    </tr>
    <tr>
        <td><b>LPV/RPV</b></td>
        <td>
            {{ $binary_total->total_left }} / {{ $binary_total->total_right }}
        </td>
    </tr>
</table>