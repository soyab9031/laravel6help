@extends('user.template.layout')

@section('title', 'My Sponsors')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Sponsors</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Joining Date</th>
                                    <th>Activation Date</th>
                                    <th>User</th>
                                    <th>Package</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($sponsors->count() == 0)
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            No any Sponsors Available
                                        </td>
                                    </tr>
                                @endif
                                @foreach($sponsors as $index => $sponsor)
                                    <tr>
                                        <th scope="row">{{ $index+1 }}</th>
                                        <td>{{ $sponsor->created_at->format('M d, Y h:i A') }}</td>
                                        <td>{{ $sponsor->paid_at ? \Carbon\Carbon::parse($sponsor->paid_at)->format('M d, Y h:i A') : 'N.A' }}</td>
                                        <td>
                                            {{ $sponsor->detail->full_name }}
                                            ({{ $sponsor->tracking_id }})
                                        </td>
                                        <td>
                                            @if($sponsor->package)
                                                {{ $sponsor->package->name }} <br> (Rs. {{ $sponsor->joining_amount }})
                                            @else
                                                Not Available
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
