@extends('user.template.layout')

@section('title', 'My Sponsors Level')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Sponsors Level</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body table-responsive">
                        <div class="row mb-2">
                            <div class="col-md-6 offset-md-3 offset-lg-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text primary"><i class="la la-search"></i></span>
                                    </div>
                                    <input type="text" placeholder="Search Tracking Id" class="search-tree form-control">
                                </div>
                            </div>
                        </div>
                        <div class="tree-container"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('page-javascript')
    <script>
        $(function() {

            let treeContainer = $(".tree-container");

            treeContainer.jstree({
                "core": {
                    "themes": {
                        "check_callback": true,
                        "responsive": false,
                        "dots" : true
                    },
                    "check_callback": true,
                    "data": {
                        "dataType" : "json",
                        "url": function(node) {
                            return '{{ route("user-business-sponsors-tree") }}';
                        },
                        "data": function(node) {
                            return {
                                "parent": node.id
                            };
                        }
                    }
                },
                "search": {
                    "show_only_matches": true,
                    "show_only_matches_children": true
                },
                "types": {
                    "default": {
                        "icon": "la la-user text-primary"
                    },
                    "file": {
                        "icon": "la la-user-plus text-primary"
                    }
                },
                "plugins": ["types", "search"]
            });

            $('.search-tree').keyup(function() {
                treeContainer.show();
                treeContainer.jstree('search', $(this).val());
            });

        });
    </script>
@stop

@section('import-javascript')
    <script src="/plugins/js-tree/jstree.min.js"></script>
@stop
@section('import-css')
    <link rel="stylesheet" href="/plugins/js-tree/themes/default/style.css" />
@stop

@section('page-css')
    <style>

        i.jstree-icon.jstree-themeicon.la.la-user {
            font-size: 1.4rem;
        }

        .jstree-default .jstree-icon:empty {
            width: 26px;
            height: 26px;
            line-height: 24px;
        }
        .jstree-default .jstree-node {
            min-height: 32px;
            line-height: 31px;
        }
    </style>
@stop