@extends('user.template.layout')

@section('title', 'Aadhaar Detail - KYC Update')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <h3 class="content-header-title">
                Aadhaar Detail - KYC Update
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-8 offset-md-2 offset-lg-2">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            @if(in_array($user_status->aadhar_card, [\App\Models\UserStatus::KYC_PENDING, \App\Models\UserStatus::KYC_REJECTED]))
                                <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="file" id="imageUploader" name="image" data-allowed-file-extensions="jpg png jpeg" data-max-file-size="4M" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="file" id="otherImage" name="secondary_image" data-allowed-file-extensions="jpg png jpeg" data-max-file-size="4M" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Aadhaar Number</label>
                                        <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" placeholder="Enter Your Aadhaar Number" name="aadhar_number" value="{{ old('aadhar_number') }}">
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-danger">Upload</button>
                                        <br>
                                        <small class="text-danger">*Jpg, png, jpeg Image Formats are allowed & Maximum Size is 4 MB</small>
                                    </div>
                                </form>
                                <hr>
                            @endif
                            @if(count($documents) > 0)
                                <div class="table table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Aadhaar Number</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($documents as $index => $document)
                                            <tr>
                                                <td>{{ $index +1 }}</td>
                                                <td>{{ $document->created_at->format('M d, Y h:i A') }}</td>
                                                <td>{{ $document->number }}</td>
                                                <td>
                                                    <a href="{{ env('DOCUMENT_IMAGE_URL') . $document->image  }}" target="_blank" class="btn btn-facebook btn-sm">Front Image</a>
                                                    <a href="{{ env('DOCUMENT_IMAGE_URL') . $document->secondary_image  }}" target="_blank" class="btn btn-facebook btn-sm">Back Image</a>
                                                </td>
                                                <td>
                                                    @if($document->status == \App\Models\UserDocument::PENDING)
                                                        <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> In Progress</span>
                                                    @elseif($document->status == \App\Models\UserDocument::VERIFIED)
                                                        <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                                    @else
                                                        <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                                    @endif
                                                </td>
                                                <td>{{ $document->remarks }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#imageUploader').dropify({
            messages: {'default': 'Upload Your Aadhaar Card Front Image'},
            tpl: {
                filename: '<p class="dropify-filename">Aadhaar Card Front</p>',
            }
        });

        $('#otherImage').dropify({
            messages: {'default': 'Upload Your Aadhaar Card Back Image'},
            tpl: {
                filename: '<p class="dropify-filename">Aadhaar Card Back</p>',
            }
        });
    </script>
@stop

@section('import-javascript')
    <script src="/plugins/dropify/dropify.min.js"></script>
@stop

@section('import-css')
    <link href="/plugins/dropify/dropify.min.css" type="text/css" rel="stylesheet" />
@stop