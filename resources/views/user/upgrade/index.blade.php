@extends('user.template.layout')

@section('title', 'Upgrade Account')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Upgrade Account</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            Enter Appropriate Details to Upgrade Your Account
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form action="" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Select Package</label>
                                    <select name="package_id" class="form-control square" required>
                                        <option value=""> Select</option>
                                        @foreach($packages as $package)
                                            <option value="{{ $package->id }}"> {{ $package->name }} (Rs. {{ $package->amount }}) (PV. {{ $package->pv }})</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Enter Pin Number</label>
                                    <input type="text" name="pin_number" class="form-control square" autocomplete="off" value="" required>
                                </div>
                                <div class="text-center">
                                    <p>
                                        <small class="text-danger">
                                            After Upgrade, Your Account points / pv will be counted & You will become eligible to generate the Income
                                        </small>
                                    </p>
                                    <button class="btn round btn-primary">Upgrade Your Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop