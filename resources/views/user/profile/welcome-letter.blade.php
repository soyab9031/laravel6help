@extends('user.template.layout')

@section('title', 'Welcome Letter')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Welcome Letter</h3>
        </div>
    </div>

    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-body">
                <div class="table-responsive">
                    <table width="800" border="0" cellspacing="0" cellpadding="0" align="center"
                           style="border:#CCC 1px solid;">
                        <tr>
                            <td>
                                <div bgcolor="#ffffff" style="min-width:100%;margin:0">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="750" border="0" cellpadding="0" cellspacing="0"
                                                       align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="line-height:10px">
                                                                        <div style="min-height:10px"></div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="middle"
                                                                        style="padding-bottom:10px;padding-top:10px">
                                                                        <div align="center"><a
                                                                                    href="javascript:void(0)"><img
                                                                                        src="/user-assets/images/company/logo.png"
                                                                                        width="120px" border="0" alt=""
                                                                                        style="display:block"
                                                                                        class="CToWUd"></a></div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="line-height:10px">
                                                <div style="min-height:10px"></div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                           style="background-color: #ed3237;color: #FFFFFF">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top" class="bg-blue">
                                                <table width="750" border="0" cellpadding="0" cellspacing="0" >
                                                    <tbody>
                                                    <tr>
                                                        <td align="left"
                                                            style="padding-top:25px;padding-bottom:25px;padding-left:20px;padding-right:20px;font-size:40px;line-height:40px;font-size:35px;text-transform:Capitalize;">
                                                            Welcome &nbsp;<span
                                                                    id="lblName">{{ $user->detail->full_name }}</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                           style="background-color: #979200;">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top" class="bg-chartbg">
                                                <table width="750" border="0" cellpadding="0" cellspacing="0"
                                                       align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle" style="color: #fff;">
                                                                        Congratulation
                                                                        on your decision to join <strong><span
                                                                                    id="lblCompanyName3"> {{ config('project.brand') }} </span></strong>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="750" border="0" cellpadding="0" cellspacing="0"
                                                       align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left"
                                                            background="http://localhost/nvnempire/associate/images/watermark_logo.png"
                                                            style="padding-top:10px;padding-bottom:10px;padding-left:20px;padding-right:20px;background-position:center;background-repeat:no-repeat;">
                                                            <font color="#333333"
                                                                  style="font-size:14px;line-height:20px">
                                                                <p>You &nbsp;are now a part of the fastest growing company.
                                                                    <strong><span
                                                                                id="lblCompanyName3">{{ config('project.brand') }}</span></strong>
                                                                    is an exciting people business. A business that has the
                                                                    potential to turn your dreams into reality. As you build
                                                                    your business, you will establish lifelong friendship
                                                                    and develop a support system which is unparalleled to
                                                                    any other business.</p>
                                                                <p>Everyone at <strong><span
                                                                                id="lblCompanyName3"> {{ config('project.brand') }}</span></strong>
                                                                    is here to help you to be successful in life. We pledge
                                                                    our best efforts to provide the level of continuing
                                                                    support necessary to help you build a successful
                                                                    business.</p>
                                                                <p>We are not just only an organization. We are a community
                                                                    that will give you the powerful platform in which you&rsquo;ll
                                                                    get the chance to surround yourself with people who love
                                                                    to share big ideas and feed your dreams. We are a
                                                                    community that will help you discover your inner
                                                                    strengths and achieve the lifestyle you really
                                                                    deserve. </p>
                                                                <p>We have developed an effective and proven step by step
                                                                    formula to help you launch a profitable business of your
                                                                    own. You determine your own level of commitment so you
                                                                    can fit it around your lifestyle of personal goals and
                                                                    the rewards are tremendous for those who can put forth
                                                                    the effort necessary to build a solid organization, one
                                                                    from which you can potentially benefit for the rest of
                                                                    your life. <br>
                                                                    <br>
                                                                    All of us here are equal, and we are dedicated to
                                                                    inspire BIG dreams in one another! We will change
                                                                    thousands of lives in positive manner by spreading the
                                                                    total success attitude. </p>
                                                            </font></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                       bgcolor="#ffffff">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                                   bgcolor="#e8e8e8">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="padding-left:40px">
                                                                        <table width="640" border="0" cellpadding="0"
                                                                               cellspacing="0" align="center">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="padding-top:15px">
                                                                                    <table width="100%" border="0"
                                                                                           cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle"
                                                                                                bgcolor="#e8e8e8">
                                                                                                <strong>Please
                                                                                                    find below your
                                                                                                    Account
                                                                                                    Details: </strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                                   bgcolor="#e8e8e8">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="padding-left:40px">
                                                                        <table width="750" border="0" cellpadding="0"
                                                                               cellspacing="0" align="center">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="padding-top:10px">
                                                                                    <table width="100%" border="0"
                                                                                           bgcolor="#E8E8E8">
                                                                                        <tbody>
                                                                                        <tr bgcolor="#E8E8E8" style="font-size: 10pt; font-weight: bold;
                          height: 25px">
                                                                                            <td width="30%" align="left"
                                                                                                class="style10"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblId">User   ID</span>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td align="left" class="style6"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblRegId">: {{ $user->tracking_id }}</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr bgcolor="#E8E8E8" style="  font-size: 10pt; font-weight: bold;
                      height: 25px">
                                                                                            <td align="left" class="style10"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblPlace0">Joining Date</span>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td align="left" class="style6"
                                                                                                style="padding-left: 5px">
                                                                                                : {{ Carbon\Carbon::parse($user->created_at)->format('d-m-Y') }}</td>
                                                                                        </tr>
                                                                                        <tr bgcolor="#E8E8E8" style="  font-size: 10pt; font-weight: bold;
                  height: 25px">
                                                                                            <td align="left" class="style10"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblMName">Name</span>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td align="left" class="style6"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblMemName">: {{ $user->detail->full_name }}</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr bgcolor="#E8E8E8" style="  font-size: 10pt; font-weight: bold;
              height: 25px">
                                                                                            <td align="left" class="style10"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblSpId">Introducer ID </span>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td align="left" class="style11"
                                                                                                style="padding-left: 5px">
                                                                                                <span id="lblSponsorId">: {{ $user->sponsorBy ? $user->sponsorBy->tracking_id : '' }}</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                                   bgcolor="#e8e8e8">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="padding-left:40px">
                                                                        <table width="640" border="0" cellpadding="0"
                                                                               cellspacing="0" align="center">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="padding-top:15px">&nbsp;

                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>

                                                            <table width="750" border="0" cellpadding="0" cellspacing="0"
                                                                   align="center">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="padding-top:10px;padding-bottom:10px;padding-left:20px;padding-right:20px"
                                                                        align="left">
                                                                        We are confident that you will get a lot of
                                                                        satisfaction from your involvement with
                                                                        <strong> {{ config('project.brand') }} </strong>and we wish you a great life
                                                                        ahead.<br>
                                                                        Keep it up! See you at the top! <br>
                                                                        <br>
                                                                        Winning Regards, <br>
                                                                        <br>
                                                                        <strong> <span id="lblCompanyName3"> {{ config('project.brand') }}</span>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#bebebe">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="640" border="0" cellpadding="0" cellspacing="0"
                                                       align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td style="padding-top:5px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle"
                                                                        style="padding-left:0px;padding-right:0px;padding-top:5px;padding-bottom:0px">
                                                                        Copyrights
                                                                        &copy; {{ date('Y') }}  {{ config('project.brand') }}
                                                                        All rights reserved.
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="yj6qo"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="invoice-footer">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="javascript:window.print()" class="btn btn-warning btn-lg my-1 btn-print"><i
                                        class="la la-print"></i>
                                Print
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop