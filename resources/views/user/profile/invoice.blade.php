@extends('user.template.layout')

@section('title', 'Invoice')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Package Invoice</h3>
        </div>
    </div>

    <div class="content-body">
        <section class="card">

            <div id="invoice-template" class="card-body">

                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-left text-md-left">
                        <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-2" width="100">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-700">{{ collect($setting->value)->where('title','company_name')->first()->value }}</li>
                            <li>{{ collect($setting->value)->where('title','address')->first()->value }}</li>
                            <li>{{ collect($setting->value)->where('title','city')->first()->value }} - {{ collect($setting->value)->where('title','pincode')->first()->value }},</li>
                            <li>{{ collect($setting->value)->where('title','state')->first()->value }}, {{ collect($setting->value)->where('title','country')->first()->value }}</li>
                            <li>GST: {{ collect($setting->value)->where('title','gst_no')->first()->value }}</li>
                        </ul>

                    </div>
                    <div class="col-md-6 col-sm-12 text-center text-md-right">
                        <h2>INVOICE</h2>
                        <p># {{ $user->tracking_id }}</p>
                        <p> {{ $user->paid_at ? \Carbon\Carbon::parse($user->paid_at)->format('M d, Y') : $user->created_at->format('M d, Y') }}</p>
                    </div>
                </div>


                <!-- Invoice Items Details -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="table-responsive">

                                <table class="table table-bordered print-table">
                                    <tbody>
                                    <tr>
                                        <td colspan="2" class="text-center"> Personal Details </td>
                                    </tr>
                                    <tr>
                                        <td width="40%"><b>Purchasing Customer</b></td>
                                        <td>{{ $user->detail->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Date of Birth</b></td>
                                        <td>{{ \Carbon\Carbon::parse($user->birth_date)->format('M d, Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Address</b></td>
                                        <td>
                                            {{ $user->address->address }} <br>
                                            {{ $user->address->city }},
                                            {{ $user->address->state->name }} - {{ $user->address->pincode }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Contact No.</b></td>
                                        <td>{{ $user->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Registration Date</b></td>
                                        <td>{{ $user->created_at->format('M d, Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Selling Person (Sponsor)</b></td>
                                        <td>{{ $user->sponsorBy ? $user->sponsorBy->detail->full_name : null }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-bordered print-table">
                                <tbody>
                                <tr>
                                    <td colspan="2" class="text-center"> Account & Package Details </td>
                                </tr>
                                <tr>
                                    <td><b>Username</b></td>
                                    <td>{{ $user->username }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tracking ID</b></td>
                                    <td>{{ $user->tracking_id }}</td>
                                </tr>
                                <tr>
                                    <td><b>Package</b></td>
                                    <td>{{ $user->package->name }} (Rs. {{ $user->package->amount }})</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered print-table m-b-5">
                                <tbody>
                                <tr>
                                    <td colspan="2" class="text-center">Declaration</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: justify; font-size: 0.8em;">
                                <span class="text-dark">
                                   {{ $user->package->declaration }}
                                </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" class="text-center">
                                        <span class="text-dark signature-space mb-3 d-inline-block">
                                            Verified that the above information provided
                                            and the DECLARATION made by me is correct in all respects.
                                        </span>
                                    </td>
                                    <td width="50%" class="text-center text-dark">
                                        Verified that the signatures
                                        of the Purchasing Customer are authentic.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center text-dark">
                                        Signatures of Purchasing Associate
                                    </td>
                                    <td class="text-center text-dark">
                                        Signatures of Selling Person (Sponsor)
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <!-- Invoice Footer -->
                <div id="invoice-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Note:</h6>
                            <ul>
                                <li>
                                    Purchasing and Selling Members must keep a photo copy of this invoice for their own records.
                                </li>
                                <li>
                                    Please note that signing on behalf of somebody else is a serious offence.
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12 text-center">
                            <a href="javascript:window.print()" class="btn btn-info btn-lg my-1 btn-print"><i class="la la-print"></i>
                                Print Invoice
                            </a>
                        </div>
                    </div>
                </div>
                <!--/ Invoice Footer -->
            </div>
        </section>
    </div>
@stop