@extends('user.template.layout')

@section('title', 'My Profile')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Profile</h3>
        </div>
    </div>
    <div class="content-body">

        <form role="form" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Personal Information</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 text-center">
                                        <img src="{{ $user->detail->image ? env('USER_PROFILE_IMAGE_URL').$user->detail->image : '/user-assets/images/icons/user-tie.svg' }}" class="profile-image" height="100px">
                                    </div>
                                    <div class="col-md-9">
                                        <fieldset class="form-group">
                                            <label>Profile Image</label>
                                            <input type="file" name="profile_image" id="profile_image" class="custom-file-input" >
                                            <small class="text-danger">Allow Formats : .jpg, .jpeg, .png & Maximum File Size is 2MB</small>
                                        </fieldset>
                                    </div>
                                </div>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        Tracking ID: <span class="pull-right">{{ $user->tracking_id }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Username: <span class="pull-right">{{ $user->username }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Name: <span class="pull-right">{{ $user->detail->full_name }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Mobile: <span class="pull-right">{{ $user->mobile }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Email: <span class="pull-right">{{ $user->email ? $user->email : 'N.A' }}</span>
                                    </li>
                                </ul>
                                <div class="form-group mt-1">
                                    <label>Date Of Birth</label>
                                    @if($user->detail->birth_date)
                                        <input type="text" name="birth_date" class="form-control birth-date" value="{{ Carbon\Carbon::parse($user->detail->birth_date)->format('d-M-Y') }}" readonly>
                                    @else
                                        <input type="text" name="birth_date" class="form-control birth-date" readonly>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">
                                    Bank Details
                                    @if($user->status->bank_account == \App\Models\UserStatus::KYC_PENDING)
                                        <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                    @elseif($user->status->bank_account == \App\Models\UserStatus::KYC_INPROGRESS)
                                        <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                    @elseif($user->status->bank_account == \App\Models\UserStatus::KYC_VERIFIED)
                                        <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                    @else
                                        <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                    @endif
                                </h4>
                                @if($user->status->bank_account != \App\Models\UserStatus::KYC_VERIFIED)
                                    <div class="text-right">
                                        <a href="{{ route('user-kyc-bank') }}" class="font-small-3" title="Click here to Update Bank Details">
                                            Update Bank Details <i class="la la-external-link-square"></i>
                                        </a>
                                    </div>
                                @endif
                                <ul class="list-group mt-1">
                                    <li class="list-group-item">
                                        Bank Name: <span class="pull-right">{{ $user->bank->bank_name }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Holder Name: <span class="pull-right">{{ $user->bank->account_name }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Account Number: <span class="pull-right">{{ $user->bank->account_number }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        IFSC: <span class="pull-right">{{ $user->bank->ifsc }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Branch: <span class="pull-right">{{ $user->bank->branch }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        City: <span class="pull-right">{{ $user->bank->city }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Account Type: <span class="pull-right">{{ $user->bank->type == 1 ? 'Saving' : 'Current' }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Address Information</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" class="form-control" cols="30" rows="6">{{ $user->address->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Landmark</label>
                                            <input type="text" class="form-control square" placeholder="Enter Landmark" name="landmark" value="{{ $user->address->landmark}}">
                                        </div>
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" placeholder="Enter City" class="form-control square" name="city" value="{{ $user->address->city}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>District</label>
                                            <input type="text" class="form-control square" placeholder="District" name="district" value="{{ $user->address->district}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ $user->address->state_id == $state->id ? 'selected' : ''}} >{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input type="text" class="form-control square" placeholder="Pincode" name="pincode" value="{{ $user->address->pincode}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Nominee Information</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Nominee Name</label>
                                        <input type="text" name="nominee_name" class="form-control square" placeholder="Nominee Name" value="{{ $user->detail->nominee_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Nominee Relation</label>
                                        <select class="form-control" name="nominee_relation">
                                            @foreach ($nominee_relation as $relation)
                                                <option value="{{ $relation }}" {{ $user->detail->nominee_relation == $relation ? 'selected' : ''}}>{{ $relation }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Nominee Birth Date</label>
                                        @if($user->detail->nominee_birth_date)
                                            <input type="text" name="nominee_birth_date" class="form-control nominee-birth-date" value="{{ Carbon\Carbon::parse($user->detail->nominee_birth_date)->format('d-M-Y') }}" readonly>
                                        @else
                                            <input type="text" name="nominee_birth_date" class="form-control birth-date" readonly>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-danger">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop


@section('page-javascript')
    <script>
        $('#profile_image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });

        $('.birth-date, .nominee-birth-date').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-50y",
            endDate: "-18y"
        });
    </script>
@stop