<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true" data-img="/user-assets/images/backgrounds/03.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('user-dashboard') }}">
                    <img class="brand-logo" alt="{{ config('project.brand') }}" src="/user-assets/images/company/logo.png"/>
                </a>
            </li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a href="{{ route('user-dashboard') }}">
                    <i class="ft-home"></i><span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-kyc-dashboard') }}"><i class="ft-user-check bg-dark text-white"></i><span class="menu-title">KYC Verify</span></a>
            </li>
            <li class="nav-item">
                <a href="{{ Session::get('user')->referral_link }}" target="_blank">
                    <i class="ft-user-plus"></i><span class="menu-title">New Registration</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#"><i class="ft-layers"></i><span class="menu-title" data-i18n="">My Business</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' =>'enrollment-team-bonus']) }}">ETB Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'recurring-team-bonus']) }}">RTB Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'cheque-matching-bonus']) }}">CMB Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'rank-bonus']) }}">Rank Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'outlet-cheque-bonus']) }}">OCB Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'performance-monthly-bonus']) }}">PMB Bonus</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#"><i class="ft-layers"></i><span class="menu-title" data-i18n="">My Status</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-business-binary-tree') }}">My Team</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-sponsors') }}">My Sponsor</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-sponsors-tree') }}">Sponsor Level Tree</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-children', ['leg' => 'L']) }}">My Downline List</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-binary-tree') }}">Team Tree</a>
                    </li>
                </ul>
            </li>
            {{--<li class="nav-item">--}}
                {{--<a href="#"><i class="la la-barcode"></i><span class="menu-title">Pins</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-pin-request') }}">Pin Request</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-pins') }}">My Pins</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a href="#"><i class="la la-shopping-cart"></i><span class="menu-title">Shopping</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('website-home') }}">Products</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-shopping-order-view') }}">Orders</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="#"><i class="ft-user"></i><span class="menu-title">My Account</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-account-profile') }}">Profile</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-account-identity-card') }}">ID Card</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-welcome-letter') }}">Welcome Letter</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-package-invoice') }}">Invoice</a>
                    </li>
                    @if(Session::get('user')->paid_at)
                        <li>
                            <a class="menu-item" href="{{ route('user-package-receipt') }}">Receipt</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('user-account-password') }}" class="menu-item">Change Password</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-account-wallet-password') }}">Change Wallet Password</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#"><i class="la la-money"></i><span class="menu-title">My Income</span></a>
                <ul class="menu-content">
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'sponsor']) }}">Sponsor Income</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'binary']) }}">Binary Income</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'reward']) }}">Reward Income</a>--}}
                    {{--</li>--}}
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' =>'enrollment-team-bonus']) }}">Daily Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'recurring-team-bonus']) }}">Weekly Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'cheque-matching-bonus']) }}">Fortnight Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'outlet-cheque-bonus']) }}">Monthly OCB Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view',['type' => 'performance-monthly-bonus']) }}">Monthly PMB Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'rank-bonus']) }}">Rank Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-total-report') }}">Total Report</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-company-documents-view') }}">
                    <i class="la la-cloud-download"></i><span class="menu-title">Downloads</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-payout-view') }}">
                    <i class="la la-inr"></i><span class="menu-title">Payout</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-support-view') }}">
                    <i class="ft-aperture"></i><span class="menu-title">Support</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="navigation-background"></div>
</div>
