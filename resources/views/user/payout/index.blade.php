@extends('user.template.layout')

@section('title', 'Payout Report')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-3 col-12 mb-2">
            <h3 class="content-header-title">Payout Report</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th>TDS</th>
                                <th>Admin</th>
                                <th>Service</th>
                                <th>Amount</th>
                                <th>Transfer Type</th>
                                <th>Reference No.</th>
                                <th>Remarks</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($payouts) == 0)
                                <tr>
                                    <td colspan="11" class="text-center">
                                        Right Now, You have no any Payout information.<br>
                                        After Core Calculations & withdrawal, Income will be displayed here...!!
                                    </td>
                                </tr>
                            @endif
                            @foreach($payouts as $index => $payout)
                                <tr>
                                    <td>{{ \App\Library\Helper::tableIndex($payouts, $index) }}</td>
                                    <td>{{ $payout->created_at->format('M d, Y') }}</td>
                                    <td>{{ $payout->total }}</td>
                                    <td>{{ $payout->tds }}</td>
                                    <td>{{ $payout->admin_charge }}</td>
                                    <td>{{ $payout->service_charge }}</td>
                                    <td>{{ $payout->amount }}</td>
                                    <td>
                                        @if($payout->transfer_type)
                                            {{ $payout->transfer_type_name }}
                                        @endif
                                    </td>
                                    <td>{{ $payout->reference_number }}</td>
                                    <td>{{ $payout->remarks }}</td>
                                    <td>
                                        @if($payout->status == \App\Models\Payout::PENDING_TYPE)
                                            <span class="badge badge-danger">Pending</span>
                                        @else
                                            <span class="badge badge-success">Transferred</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $payouts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop