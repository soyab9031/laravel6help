@extends('user.template.layout')

@section('title', 'Dashboard')

@section('content')
    <div class="content-header row">
        <marquee behavior="left" direction="left"><h6 class="text-white">🙏🙏🙏 Namaste {{ $user->detail->full_name  }} to our Aos Lifestyle 🙏🙏🙏</h6></marquee>
        <div class="content-header-left col-md-4 col-12 mb-0">
            <h3 class="content-header-title">Dashboard</h3>
        </div>
    </div>
    <div class="content-body">

        {{--Include Upgrade / Activate Account Widget Here--}}
        @if($user->paid_at == null)
            @include('user.upgrade.widget')
        @endif
        <div class="row match-height">
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'enrollment-team-bonus']) }}">
                    <div class="card bg-gradient-x-blue-cyan">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign danger font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Enrollment Team Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Earn</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->etb_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Earned</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_etb_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'recurring-team-bonus']) }}">
                    <div class="card bg-gradient-x-red">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign white font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Recurring Team Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Earn</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->rtb_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Earned</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_rtb_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'cheque-matching-bonus']) }}">
                    <div class="card bg-gradient-x-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign danger font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Cheque Matching Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Earn</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->cmb_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Earned</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_cmb_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'rank-bonus']) }}">
                    <div class="card bg-gradient-x-pink">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign danger font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Rank Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Reward</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->rank_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Achievement</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_rank_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'outlet-cheque-bonus']) }}">
                    <div class="card bg-gradient-x-purple">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign danger font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Outlet Cheque Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Earn</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->ocb_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Earned</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_ocb_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="{{ route('user-wallet-view',['type' => 'performance-monthly-bonus']) }}">
                    <div class="card bg-gradient-x2-blue-grey">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-rupee-sign danger font-large-1"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom">
                                        <span class="float-right text-white font-medium-1"><strong>Monthly Performance Bonus</strong></span>
                                        <span class="d-block float-left text-white font-medium-1 mt-3">Earn</span>
                                        <h1 class="white mb-0 mt-4">{{ number_format($wallet->pmb_income) }}</h1>
                                        <span class="d-block float-left text-white font-medium-1 mt-0">Earned</span>
                                        <h1 class="white mb-0">{{ number_format($wallet->earned_pmb_income) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'enrollment-team-bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-blue-cyan">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign danger font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earned ETB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->earned_etb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'recurring_team_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-blue-cyan">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign danger font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earn RTB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->rtb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'recurring_team_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-blue-cyan">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign danger font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earned RTB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->earned_rtb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'cheque_matching_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-danger">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign warning font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earn CTB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->cmb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'cheque_matching_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-danger">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign warning font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earned CTB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->earned_cmb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'rank_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-pink">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign warning font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earn Rank</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->rank_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'rank_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-pink">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign warning font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earned Rank</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->earned_rank_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'outlet_cheque_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-success">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign info font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}}
                                        {{--<span class="d-block text-white font-medium-1">Earn OCB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->ocb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'outlet_cheque_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-success">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign info font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-3">--}
                                        {{--<span class="d-block text-white font-medium-1">Earned OCB</span>--}}
                                        {{--<h1 class="white mb-0">{{ number_format($wallet->earned_ocb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'performance_monthly_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-yellow">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign purple font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-1">--}}
                                        {{--<span class="d-block font-medium-1">Earn PMB</span>--}}
                                        {{--<h1 class="info mb-0">{{ number_format($wallet->pmb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xl-4 col-lg-6 col-md-12">--}}
                {{--<a href="{{ route('user-wallet-view',['type' => 'performance_monthly_bonus']) }}">--}}
                    {{--<div class="card bg-gradient-x-yellow">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                    {{--<div class="align-self-top">--}}
                                        {{--<i class="la la-rupee-sign purple font-large-4"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-right align-self-bottom mt-1">--}}
                                        {{--<span class="d-block font-medium-1">Earned PMB</span>--}}
                                        {{--<h1 class="info mb-0">{{ number_format($wallet->earned_pmb_income) }}</h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card dashboard-account-card p-0">
                    <div class="card-header">
                        <img class="img-fluid" src="/user-assets/images/backgrounds/dashboard-account-card.jpg" alt=""></div>
                    <div class="card-profile">
                        @if($user->detail->image == null)
                            <img class="rounded-circle" src="/user-assets/images/icons/user-tie.svg" alt="">
                        @else
                            <img class="rounded-circle"
                                 src="{{ env('USER_PROFILE_IMAGE_URL').Session::get('user')->detail->image }}" alt="">
                        @endif
                    </div>
                    <div class="text-center profile-details">
                        <h5>{{ \Str::limit(Session::get('user')->detail->full_name, 25) }}</h5>
                        <h6 class="text-bold-700 text-danger">ID: {{ Session::get('user')->tracking_id }}</h6>
                    </div>
                    <div class="card-footer row">
                        <div class="col-12 col-sm-12 border-right-blue-grey border-right-lighten-5 text-center">
                            <div class="font-medium-1 text-primary">Username</div>
                            <h5 class="">{{ Session::get('user')->username }}</h5>
                        </div>
                        {{--<div class="col-6 col-sm-6 text-center">--}}
                        {{--<div class="font-medium-1 text-primary">Package</div>--}}
                        {{--<h5>{{ $user->package ? $user->package->name : 'N.A' }}</h5>--}}
                        {{--</div>--}}
                        <div class="col-12">
                            <hr>
                        </div>
                        <div class="col-12 text-center">
                            <div class="text-bold-700 mb-1">My Referral Links</div>
                            <div class="text-center">
                                <div>
                                    <button
                                            onclick="INGENIOUS.copyToClipboard('.dashboard-referral', 'Your Referral Link has been copied')"
                                            class="btn btn-primary btn-sm dashboard-referral"
                                            data-clipboard-text="{{ Session::get('user')->referral_link.'&leg=L' }}"><i
                                                class="ft-copy"></i> Left
                                    </button>
                                    <button
                                            onclick="INGENIOUS.copyToClipboard('.dashboard-referral', 'Your Referral Link has been copied')"
                                            class="btn btn-primary btn-sm dashboard-referral"
                                            data-clipboard-text="{{ Session::get('user')->referral_link.'&leg=R' }}"><i
                                                class="ft-copy"></i> Right
                                    </button>
                                </div>
                                <br/>
                                <a href="{{ $shareLinks->facebook }}" class="btn btn-facebook btn-sm"><i
                                            class="ft-facebook"></i></a>
                                <a href="{{ $shareLinks->twitter }}" class="btn btn-twitter btn-sm"><i
                                            class="ft-twitter"></i></a>
                                <a href="{{ $shareLinks->whatsapp }}" class="btn btn-whatsapp btn-sm"><i
                                            class="la la-whatsapp"></i></a>
                                <a href="{{ $shareLinks->telegram }}" class="btn btn-info btn-sm"><i
                                            class="la la-telegram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header p-1">
                        <h4 class="card-title float-left">My KYC Status</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-footer text-center p-1">
                            <div class="row">
                                <div
                                        class="col-sm border-right-blue-grey border-right-lighten-5 text-center">
                                    <p class="black mb-0">PAN CARD</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->pancard == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->pancard == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->pancard == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                                <div
                                        class="col-sm border-right-blue-grey border-right-lighten-5 text-center">
                                    <p class="black mb-0">BANK</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->bank_account == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->bank_account == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->bank_account == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm text-center">
                                    <p class="black mb-0">AADHAR</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->aadhar_card == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->aadhar_card == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->aadhar_card == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row match-height">
                    <div class="col-xl-5 col-lg-6 col-md-12">
                        <div class="card bg-gradient-x-orange-yellow">
                            <a href="{{ route('user-wallet-view') }}">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="la la-money text-white font-large-4 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right align-self-bottom mt-3">
                                                <span class="d-block font-medium-1 mt-3">Aos Shopping Balance</span>
                                                <h1 class="text-white mb-0">
                                                    Rs. {{ number_format($user->wallet_balance) }}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 col-md-12">
                        <a href="{{ route('user-payout-view') }}">
                            <div class="card bg-gradient-x-purple-red">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="la la-rupee-sign danger font-large-1"></i>
                                            </div>
                                            <div class="media-body text-right align-self-bottom">
                                                <span class="float-right text-white font-medium-1">Total</span>
                                                <span class="d-block float-left text-white font-medium-1 mt-3">Total</span>
                                                <h1 class="white mb-0 mt-4">{{ number_format($total_payout) }}</h1>
                                                <span class="d-block float-left text-white font-medium-1 mt-0">Earning</span>
                                                <h1 class="white mb-0">{{ number_format($total_income) }}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="row match-height">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header p-1">
                                        <h4 class="card-title float-left">My Business</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-footer pr-1 pl-1 pt-1">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div
                                                                    class="card text-dark box-shadow-0 bg-blue-grey bg-lighten-4">
                                                                <div class="card-content collapse show">
                                                                    <div class="card-body dashboard-business-card">
                                                                        <h4 class="card-title text-dark">Left</h4>
                                                                        <p class="card-text">Users <code
                                                                                    class="pull-right">{{ $binary_total ? $binary_total->left : 0 }}</code>
                                                                        </p>
                                                                        <p class="card-text">Points <code
                                                                                    class="pull-right">{{ $binary_total ? $binary_total->total_left : 0.00 }}</code>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div
                                                                    class="card text-dark box-shadow-0 bg-blue-grey bg-lighten-4">
                                                                <div class="card-content collapse show">
                                                                    <div class="card-body dashboard-business-card">
                                                                        <h4 class="card-title text-dark text-right">
                                                                            Right</h4>
                                                                        <p class="card-text">
                                                                            <code>{{ $binary_total ? $binary_total->right : 0 }}</code>
                                                                            <span class="pull-right">Users</span></p>
                                                                        <p class="card-text">
                                                                            <code>{{ $binary_total ? $binary_total->total_right : 0.00 }}</code>
                                                                            <span class="pull-right">Points</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <ul class="list-group ">
                                                                <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                                    <strong class="text-dark">My Team</strong>
                                                                    <span
                                                                            class="pull-right text-logo-blue">{{ number_format($children) }} </span>
                                                                </li>
                                                                <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                                    <strong class="text-dark">My Direct</strong>
                                                                    <span
                                                                            class="pull-right text-logo-blue">{{ number_format($sponsors) }} </span>
                                                                </li>
                                                                <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                                    <strong class="text-dark">Active Directs </strong>
                                                                    <span
                                                                            class="pull-right text-logo-blue">{{ $active_sponsors }}</span>
                                                                </li>
                                                                <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                                    <strong class="text-dark">Binary Qualified </strong>
                                                                    @if(!$user->status->binary_qualified_at)
                                                                        <span
                                                                                class="pull-right badge badge-danger text-logo-blue">NO</span>
                                                                    @else
                                                                        <span
                                                                                class="pull-right badge badge-success text-logo-blue"> YES</span>
                                                                    @endif
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
