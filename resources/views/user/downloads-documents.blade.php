@extends('user.template.layout')
@section('title', 'Documents')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">
                Documents
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($companyDocuments->count() == 0)
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Coming Soon....!
                                    </td>
                                </tr>
                            @endif
                            @foreach($companyDocuments as $index =>$companyDocument)
                                <tr class="bg-lighten-3">
                                    <td>{{ \App\Library\Helper::tableIndex($companyDocuments, $index) }}</td>
                                    <td>{{ $companyDocument->name  }}</td>
                                    <td>
                                        @if ($companyDocument->type == \App\Models\CompanyDocument::PDF)
                                            <span class="badge badge-secondary"> Pdf</span>
                                            @else
                                            <span class="badge badge-success"> Image</span>
                                        @endif
                                    </td>
                                    @php
                                        $filename = pathinfo($companyDocument->document);
                                    @endphp
                                    <td><a href="{{ env('COMPANY_DOCUMENT_URL').$companyDocument->document }}" target="_blank"><button class="btn btn-danger btn-sm text-center"><i class="la la-cloud-download"></i> Download</button></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $companyDocuments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
