@extends('website.template.layout')

@section('page-title','Shopping Cart')

@section('page-content')

    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Shopping Cart</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container" id="cartPage">
            <h1 class="text-center">Shopping Cart</h1>
            <div class="row d-block" v-if="items.length == 0">
                <div class="text-center">
                    <div class="row">
                        <div class="col-md-12 border-cart text-center">
                            <div class="mb-4">
                                <h4 class="mt-3" style="font-weight:600">My Cart</h4>
                                <hr>
                                <i class="fa fa-shopping-cart fa-5x cart-color"></i>
                                <br>
                                <h5 class="text-cart">Shopping cart is empty..!!</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="" v-if="items.length > 0">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card-header bg-website">
                            <h2 class="text-white mb-0">ORDER List</h2>
                        </div>
                        <div class="cart-table" v-for="(item,index) in items">
                            <div class="cart-table-prd">
                                <div class="cart-table-prd-image">
                                    <img :alt="item.product" :src="item.image">
                                </div>
                                <div class="cart-table-prd-name">
                                    <h2>@{{ item.product }}</h2>
                                    <h5 class="text-danger">BV: @{{ item.bv }}</h5>
                                    <b>@{{ item.selling_price | currency }}</b>
                                </div>
                                <div class="cart-table-prd-name">
                                    <vue-counter-button v-model="item.quantity" min="1" max="100"
                                                        :value="item.quantity"
                                                        :product-price-id="item.id">
                                    </vue-counter-button>
                                </div>
                                <div class="cart-table-prd-name text-center">
                                    <b>@{{ _.round(item.quantity.current * item.selling_price, 2) | currency }}</b>
                                </div>
                                <div class="cart-table-prd-qty text-center">
                                    <a href="javascript:void(0)" @click="removeItem(item.id)" class="icon-cross"></a>
                                </div>
                            </div>
                        </div>
                        <div class="cart-table-total">
                            <div class="row">
                                <div class="col-sm-auto">
                                    <a href="{{ route('website-home') }}" class="btn"><i class="icon-angle-left"></i><span>continue shopping</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-2 mt-md-0">
                        <div class="card-header bg-website">
                            <h2 class="text-white mb-0">ORDER SUMMARY</h2>
                        </div>
                        <div class="cart-table cart-table--sm">
                            <div class="cart-table-prd">
                                <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Order Total</h2></div>
                                <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>@{{ order_summary.amount | currency }}</b></div>
                            </div>
                            <div class="cart-table-prd">
                                <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Total BVs</h2></div>
                                <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>@{{ order_summary.total_bv | currency }}</b></div>
                            </div>
                        </div>
                        <div class="card-total-sm">
                            <div class="float-right">Total <span class="card-total-price">@{{ order_summary.total_amount | currency }}</span></div>
                        </div>
                        <div class="mt-2"></div>
                        <div class="clearfix text-center" v-if="items.length > 0">
                            <button type="button" @click="checkout" class="btn btn--lg w-50">
                                Place Order <i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;
        new Vue({
            el: '#cartPage',
            data: {
                items: {!! json_encode($items) !!},
                order_summary: {
                    total_bv: 0, total_qty: 0, amount: 0, shipping_charge: 0, wallet: 0, discount: 0, total_amount: 0,  wallet_amount: 0
                },
            },
            watch: {
                items: {
                    handler: function () {
                        this.cartManager();
                    },
                    deep: true
                }
            },
            methods: {
                cartManager: function () {


                    this.order_summary.total_bv = _.sumBy(this.items, item => {
                        return item.quantity.current * item.bv;
                    });

                    this.order_summary.total_qty = _.sumBy(this.items, item => {
                        return item.quantity.current;
                    });

                    this.order_summary.amount = _.chain(this.items).sumBy(item => {
                        return item.quantity.current * parseFloat(item.selling_price);
                    }).round(2).value();

                    this.order_summary.shipping_charge = 0;

                    this.order_summary.total_amount = _.round(this.order_summary.amount + this.order_summary.shipping_charge, 2);

                },
                removeItem: function (item_id) {

                    let self = this;

                    swal({
                        title: `Are you sure?`,
                        text: 'Are You sure to remove this item ?',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {

                        if (confirm) {
                            self.$http.get('{{ route("website-cart-remove-item") }}', {
                                params: {
                                    removeAllQty: true,
                                    product_price_id: item_id
                                }
                            }).then(response => {

                                if (response.data.status) {
                                    self.items = _.chain(self.items).reject(item => {
                                        return parseInt(item.id) === parseInt(item_id);
                                    }).value();

                                    if (parseInt(response.data.total_items) === 0) {
                                        location.reload();
                                    } else {
                                        location.reload();
                                    }
                                } else {
                                    swal("Oops", response.data.message, "error");
                                }
                            })

                        }
                    });
                },
                checkout: function () {

                    INGENIOUS.blockUI(true);

                    this.$http.post('{{ route("website-cart-create-checkout") }}', {
                        items: this.items,
                        order_summary: this.order_summary,
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {
                            window.location = response.data.redirect_route;
                        } else {
                            swal('Oops', response.data.message, 'error');
                        }

                    });
                }
            },
            mounted: function () {
                this.cartManager();
            }
        });
    </script>
@endsection
