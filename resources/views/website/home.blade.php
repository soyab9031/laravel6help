@extends('website.template.layout')
@section('page-title','Home')
@section('page-content')
    <div class="holder fullwidth full-nopad mt-0">
        <div class="container">
            <div class="bnslider-wrapper">
                @if(count($banners) > 0)
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($banners as $banner)
                                <div class="swiper-slide">
                                    <img class="img-fluid rounded" src="{{ env('BANNER_IMAGE_URL'). $banner->image }}" alt="{{ $banner->image }}" width="100%">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                @else

                    <div class="bnslider bnslider--lg keep-scale" id="bnslider-001" data-slick='{"arrows": true, "dots": true}' data-autoplay="false" data-speed="5000" data-start-width="1920" data-start-height="845" data-start-mwidth="480" data-start-mheight="578">

                        <div class="bnslider-slide bnslide-sport-2">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/1.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/1.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-1">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/2.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/2.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/3.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/3.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/4.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/4.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/6.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/6.png');"></div>
                        </div>
                    </div>
                    <div class="bnslider-loader">
                        <div class="loader-wrap">
                            <div class="dots">
                                <div class="dot one"></div>
                                <div class="dot two"></div>
                                <div class="dot three"></div>
                            </div>
                        </div>
                    </div>
                    <div class="bnslider-arrows container">
                        <div></div>
                    </div>
                    <div class="bnslider-dots container"></div>

                @endif
            </div>
        </div>
    </div>
    {{--<div class="holder">--}}
    {{--<div class="container">--}}
    {{--<div class="row vert-margin-middle mobile-sm-pad">--}}
    {{--@if(count($categories) >  0)--}}
    {{--@foreach($categories as $category)--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box">--}}
    {{--<div class="collection-box-image">--}}
    {{--<img src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" data-src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" alt="">--}}
    {{--</div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">{{ $category->name  }}</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--@else--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">Kitchen</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">SPORT</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">AYURVEDA</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">BABY CARE</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="holder global_width">
        <div class="container">
            <div class="row bnr-grid">
                @if(count($categories) > 0)
                    @foreach($categories as $category)
                        <div class="col-md-6">
                            <a href="{{ route('website-products-view',['slug' => $category->slug])  }}" class="bnr-wrap">
                                <div class="bnr bnr29 bnr--style-8 bnr--left bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                    <img src="/website-assets/images/static_products/BG.png" data-src="/website-assets/images/static_products/BG.png" class="lazyload" alt="">
                                    <span class="bnr-caption">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" alt="" width="10%">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="bnr-text-wrap">
                                    <span class="bnr-text1">{{ $category->name  }}</span>
                                    <span class="bnr-text-p">{!! $category->description  !!}</span>
                                    <span class="btn-decor btn-decor--xs bnr-btn">
                                        shop now
                                        <span class="btn-line" style="background-color: #fff;"></span>
                                    </span>
                                </span>
                                    </div>
                                </div>
                            </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-6">
                        <a href="javascript:void(0);" class="bnr-wrap">
                            <div class="bnr bnr29 bnr--style-8 bnr--left bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                <img src="/website-assets/images/static_products/BG.png" data-src="/website-assets/images/static_products/BG.png" class="lazyload" alt="">

                                <span class="bnr-caption">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/website-assets/images/static_products/1.png" alt="" width="10%">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="bnr-text-wrap">
                                    <span class="bnr-text1">PROTEIN</span>
                                    <span class="bnr-text-p">Lorem ipsum dolor sit amet consest adicpising elitr anno dolor sit amet Nullam dui anno anno.</span>
                                    <span class="btn-decor btn-decor--xs bnr-btn">shop now<span class="btn-line" style="background-color: #fff;"></span></span>
                                </span>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="javascript:void(0);" class="bnr-wrap">
                            <div class="bnr bnr29 bnr--style-8 bnr--left bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                <img src="/website-assets/images/static_products/BG.png" data-src="/website-assets/images/static_products/BG.png" class="lazyload" alt="">

                                <span class="bnr-caption">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/website-assets/images/static_products/1.png" alt="" width="10%">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="bnr-text-wrap">
                                    <span class="bnr-text1">PROTEIN</span>
                                    <span class="bnr-text-p">Lorem ipsum dolor sit amet consest adicpising elitr anno dolor sit amet Nullam dui anno anno.</span>
                                    <span class="btn-decor btn-decor--xs bnr-btn">shop now<span class="btn-line" style="background-color: #fff;"></span></span>
                                </span>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="javascript:void(0);" class="bnr-wrap">
                            <div class="bnr bnr29 bnr--style-8 bnr--left bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                <img src="/website-assets/images/static_products/BG.png" data-src="/website-assets/images/static_products/BG.png" class="lazyload" alt="">

                                <span class="bnr-caption">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/website-assets/images/static_products/1.png" alt="" width="10%">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="bnr-text-wrap">
                                    <span class="bnr-text1">PROTEIN</span>
                                    <span class="bnr-text-p">Lorem ipsum dolor sit amet consest adicpising elitr anno dolor sit amet Nullam dui anno anno.</span>
                                    <span class="btn-decor btn-decor--xs bnr-btn">shop now<span class="btn-line" style="background-color: #fff;"></span></span>
                                </span>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="javascript:void(0);" class="bnr-wrap">
                            <div class="bnr bnr29 bnr--style-8 bnr--left bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                <img src="/website-assets/images/static_products/BG.png" data-src="/website-assets/images/static_products/BG.png" class="lazyload" alt="">

                                <span class="bnr-caption">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/website-assets/images/static_products/1.png" alt="" width="10%">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="bnr-text-wrap">
                                    <span class="bnr-text1">PROTEIN</span>
                                    <span class="bnr-text-p">Lorem ipsum dolor sit amet consest adicpising elitr anno dolor sit amet Nullam dui anno anno.</span>
                                    <span class="btn-decor btn-decor--xs bnr-btn">shop now<span class="btn-line" style="background-color: #fff;"></span></span></span>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-wrap text-center">
                <h2 class="h1-style btn-decor">Deals Of The Day</h2>
            </div>
            @if(count($deal_products) > 0)
                <div class="prd-grid prd-text-center prd-carousel js-prd-carousel js-product-isotope-sm slick-arrows-squared data-to-show-3 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                    @foreach($deal_products as $deal_product)
                        <div class="prd prd-has-loader prd-has-countdown">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="{{ route('website-products-details',['slug'=> $deal_product->product->slug,'product_code'=> $deal_product->code])  }}" class="prd-img">
                                        <img src="{{ $deal_product->primary_image  }}" data-srcset="{{ $deal_product->primary_image  }}"
                                             alt="{{ $deal_product->product->name }}" class="js-prd-img"></a>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-tag prd-hidemobile"><a href="#">{{ $deal_product->product->name  }}</a></div>
                                    <h2 class="prd-title"><a href="javascript:void(0);">{{  $deal_product->product->category->name }}</a></h2>
                                    <div class="prd-price">
                                        <div class="price-new">₹ {{ $deal_product->price  }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="more-link-wrapper text-center">
                <a href="{{ route('website-products-list',['type' =>  'deal']) }}" class="btn-decor">shop all</a>
            </div>
            {{--<div class="more-link-wrapper text-center"><a href="javascript:void(0);" class="btn-decor">shop all</a></div>--}}
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container">
            <div class="holder">
                <div class="container">
                    <div class="title-wrap text-center">
                        <h2 class="h1-style btn-decor">Recommended Products</h2>
                    </div>
                    @if(count($recommended_products) >  0)
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            @foreach($recommended_products as $recommended_product)
                                <div class="prd prd-has-loader">
                                    <div class="prd-inside">
                                        <div class="prd-img-area">
                                            <a href="{{ route('website-products-details',['slug'=> $recommended_product->product->slug,'product_code'=> $recommended_product->code])  }}" class="prd-img">
                                                <img src="{{ $recommended_product->primary_image }}" data-srcset="{{ $recommended_product->primary_image }}"
                                                     alt="{{ $recommended_product->product->name }}" class="js-prd-img"></a>
                                        </div>
                                        <div class="prd-info">
                                            <div class="prd-tag prd-hidemobile"><a href="{{ route('website-products-details',['slug'=> $recommended_product->product->slug,'product_code'=> $recommended_product->code])  }}">{{ $recommended_product->product->name  }}</a></div>
                                            <h2 class="prd-title"><a href="javascript:void(0);">{{ $recommended_product->product->category->name  }}</a></h2>
                                            <div class="prd-price">
                                                <div class="price-new"> ₹ {{ $recommended_product->price  }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="more-link-wrapper text-center">
                        <a href="{{ route('website-products-list',['type' =>  'recommended']) }}" class="btn-decor">shop all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container"><a href="#" class="bnr-wrap">
                <div class="bnr bnr34 bnr--style-6 bnr--left bnr--middle bnr-hover-scale" data-fontratio="11.4">
                    <img src="/website-assets/images/new_slider/banner.png" data-src="/website-assets/images/new_slider/banner.png" class="lazyload" alt="">
                    <span class="bnr-caption container">
                        {{--<span class="bnr-text-wrap">--}}
                        {{--<span class="bnr-text1">summer collections</span> <span class="bnr-text2">65% OFF</span>--}}
                        {{--<span class="btn-decor btn-decor--sm bnr-btn">shop now<span class="btn-line"></span></span></span>--}}
                    </span>
                </div>
            </a>
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container">
            <div class="holder">
                <div class="container">
                    <div class="title-wrap text-center">
                        <h2 class="h1-style btn-decor">New Arrival Products</h2>
                    </div>
                    @if(count($new_arrival_products) > 0)
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            @foreach($new_arrival_products as $new_arrival_product)
                                <div class="prd prd-has-loader">
                                    <div class="prd-inside">
                                        <div class="prd-img-area">
                                            <a href="{{ route('website-products-details',['slug'=> $new_arrival_product->product->slug,'product_code'=> $new_arrival_product->code])  }}" class="prd-img">
                                                <img src="{{ $new_arrival_product->primary_image }}" data-srcset="{{ $new_arrival_product->primary_image }}"
                                                     alt="{{ $new_arrival_product->product->name }}" class="js-prd-img"></a>
                                        </div>
                                        <div class="prd-info">
                                            <div class="prd-tag prd-hidemobile"><a href="{{ route('website-products-details',['slug'=> $new_arrival_product->product->slug,'product_code'=> $new_arrival_product->code])  }}">{{ $new_arrival_product->product->name  }}</a></div>
                                            <h2 class="prd-title"><a href="javascript:void(0);">{{ $new_arrival_product->product->category->name  }}</a></h2>
                                            <div class="prd-price">
                                                <div class="price-new"> ₹ {{ $new_arrival_product->price  }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="more-link-wrapper text-center">
                        <a href="{{ route('website-products-list',['type' =>  'new_arrival']) }}" class="btn-decor">shop all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-with-arrows title-with-arrows--center">
                <h2 class="h1-style">Our Blogs</h2>
                <div class="carousel-arrows"></div>
            </div>
            <div class="post-prws post-prws-carousel" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3}},{"breakpoint": 768,"settings": {"slidesToShow": 2}},{"breakpoint": 480,"settings": {"slidesToShow": 1}}]}'>
                <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                    <h4 class="post-title"><a href="#">Results for the month</a></h4>
                    <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                    <div class="post-bot">
                        <div class="post-date">09 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                    </div>
                </div>
                <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                    <h4 class="post-title"><a href="#">How to choose a protein</a></h4>
                    <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                    <div class="post-bot">
                        <div class="post-date">10 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                    </div>
                </div>
                <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                    <h4 class="post-title"><a href="#">Training schedule</a></h4>
                    <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                    <div class="post-bot">
                        <div class="post-date">11 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                    </div>
                </div>
                <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                    <h4 class="post-title"><a href="#">The right diet</a></h4>
                    <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                    <div class="post-bot">
                        <div class="post-date">12 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($popup)
        <div id="popUp" class="modal fade in" data-toggle="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" >
                <div class="modal-content">
                    <div class="modal-body" style="width: 100%;">
                        <img src="{{ env('POPUP_IMAGE_URL').$popup->image }}" alt="" style="width: 100%">
                    </div>
                    <div class="modal-footer mt-3">
                        <button type="button" class="btn btn-outline-success btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
@section('page-javascript')
    <script>
        $(window).ready(function modal(){
            $('#popUp').modal();
        });
    </script>
@stop


