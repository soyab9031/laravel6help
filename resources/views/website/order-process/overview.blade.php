@extends('website.template.layout')

@section('page-title', 'Order Overview: ' . $order->customer_order_id)

@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Order Overview  - {{ $order->customer_order_id }}</span></li>
            </ul>
        </div>
    </div>

    <div class="holder mt-0">
        <div class="container">
            <h1 class="text-center">Order Overview</h1>
            <div>
                @if($order->status == \App\Models\Order::APPROVED && $order->payment_status == \App\Models\Order::PAYMENT_SUCCESS)
                    <div class="text-center">
                        <img src="/user-assets/images/icons/checked.png" width="50px" alt="Success" class="mb-1">
                        <h4>Thank You for Your Order</h4>
                        Your Order has been placed and is being processed, You can track this order through Your My Order Page
                    </div>
                @elseif($order->status == \App\Models\Order::PLACED && $order->payment_status == \App\Models\Order::PAYMENT_PENDING)
                    <div class="text-center">
                        <img src="/user-assets/images/icons/checked.png" class="mb-1" width="50px" alt="Success">
                        <h4>Thank You for Your Order</h4>
                        Your Order has been placed and is being processed, You can track this order through Your My Order Page
                    </div>
                @elseif($order->payment_status != \App\Models\Order::PAYMENT_SUCCESS)
                    <div class="text-center">
                        <img src="/user-assets/images/icons/cancel.png" width="50px" alt="Success" class="mb-1">
                        <h4>Something goes wrong with Your Payment</h4>
                        We're unable to place your order due to Payment Failure..!!
                    </div>
                @endif
                <div class="row m-3">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header bg-website">
                                <h2 class="text-white mb-0 ">Order Detail</h2>
                            </div>
                            <div class="card-body">
                                <div class="cart-table cart-table--sm">
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Order ID: </h2></div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right"><b>{{ $order->customer_order_id }}</b></div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Order Date: </h2></div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right"><b>{{ $order->created_at->format('M d, Y h:i A') }}</b></div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name  pl-0 pr-0 text-left"><h2>Total Items: </h2></div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right"><b>{{ count($order->details) }}</b></div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left">
                                            <h2 class="text-success">Order Amount: </h2>
                                        </div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right">
                                            <b class="text-success"><i class="fa fa-rupee"></i> {{ number_format($order->total) }}</b></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header bg-website">
                                <h2 class="text-white mb-0 ">Shipping Address</h2>
                            </div>
                            <div class="card-body">
                                <div class="font-weight-bold">
                                    {{ $order->shipping_address->name }} ({{ $order->shipping_address->mobile }})
                                    <span class="badge badge-primary pull-right">{{ $order->shipping_address->type == 1 ? 'Home' : ($order->shipping_address->type == 2 ? 'Office' : 'Other') }}</span>
                                </div>
                                <hr class="mt-2 mb-2">
                                {{ $order->shipping_address->address }}
                                @if($order->shipping_address->landmark)
                                    , {{ $order->shipping_address->landmark }}
                                @endif
                                <br>
                                {{ $order->shipping_address->city }}
                                {{ $order->shipping_address->state->name }} - {{ $order->shipping_address->pincode }}
                            </div>
                        </div>
                        <div class="card mt-3">
                            <div class="card-header bg-website">
                                <h2 class="text-white mb-0 ">Order Items</h2>
                            </div>
                            <div class="card-body">
                                @foreach($order->details as $detail)
                                    <div class="row border-bottom p-1">
                                        <div class="col-md-2 text-center">
                                            <img width="75" alt="{{ $detail->product_price->product->name }}"
                                                 src="{{ $detail->product_price->primary_image }}">
                                        </div>
                                        <div class="col-md-3">
                                            <div>
                                                <span class="text-dark">{{ Str::limit($detail->product_price->product->name, 20) }}</span><br/>
                                                <span class="font-medium-1 text-dark">₹ {{ number_format($detail->selling_price, 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="d-flex align-items-end justify-content-around p-2">
                                                <div>
                                                    <span class="text-dark">{{ number_format($detail->qty) }} Qty</span>
                                                </div>
                                                <div>
                                                    <span class="text-dark">₹ {{ $detail->selling_price }} X {{ $detail->qty }}</span>
                                                </div>
                                                <div>
                                                    <span class="text-dark">₹ {{ number_format($detail->total_amount, 2) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop