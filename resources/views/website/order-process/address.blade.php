@extends('website.template.layout')

@section('page-title', 'Your Shipping Address')

@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><a href="{{ route('website-cart')  }}">Shopping Cart</a></li>
                <li><span>Shipping Address</span></li>
            </ul>
        </div>
    </div>

    <div class="holder mt-0">
        <div class="container">
            <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                {{ csrf_field() }}
                <div class="mt-3" id="addressPageElement">
                    <h1 class="text-center">Order Shipping Address</h1>
                    <div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card mb-3 card--grey">
                                    @if(count($addresses) > 0)
                                    <div class="card-header bg-website">
                                        <h2 class="text-white mb-0">Select Your Address</h2>
                                    </div>
                                    @endif
                                    <div class="card-body">
                                        @foreach($addresses as $index => $address)
                                            <div class="row mt-1">
                                                <div class="col-2 col-md-1">
                                                    <div class="radio radio-success">
                                                        <input type="radio" name="address_id" value="{{ $address->id }}"
                                                               id="selectAddress{{ $index }}"
                                                               aria-invalid="false" {{ count($addresses) == 1 ? 'checked' : null }}>
                                                        <label for="selectAddress{{ $index }}"></label>
                                                    </div>
                                                    <span class="badge badge-primary mt-1">{{ $address->type == 1 ? 'Home' : ($address->type == 2 ? 'Office' : 'Other') }}</span>
                                                </div>
                                                <div class="col-md-10">
                                                    <label for="selectAddress{{ $index }}" class="text-dark font-weight-bold">
                                                        <h4 class="m-0">{{ $address->name }} ({{ $address->mobile }})</h4>
                                                    </label>
                                                    <hr>
                                                    {{ $address->address }}
                                                    , {{ $address->landmark ? ',' . $address->landmark : null }}
                                                    <br>
                                                    {{ $address->city }} {{ $address->state->name }}
                                                    - {{ $address->pincode }}
                                                    <a href="{{ route('website-checkout-shipping-address', ['id' => $address->id]) }}"
                                                       class="badge bg-danger pull-right text-white">Edit</a>
                                                </div>
                                            </div>
                                            <br/>
                                        @endforeach
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-center mt-5">
                                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createAddress">
                                                        <i class="icofont-plus-circle"></i> Add New Address
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-3 card--grey">
                                    <div class="card-header bg-website">
                                        <h2 class="text-white mb-0">ORDER SUMMARY</h2>
                                    </div>
                                    <div class="card-body">

                                        <div class="cart-table cart-table--sm">
                                            <div class="cart-table-prd">
                                                <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Order Total</h2></div>
                                                <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>
                                                        <i class="fa fa-rupee"></i> {{ number_format($order->order_summary->amount,2) }}</b></div>
                                            </div>
                                            <div class="cart-table-prd">
                                                <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Total BVs</h2></div>
                                                <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>{{ $order->order_summary->total_bv }}</b></div>
                                            </div>
                                        </div>
                                        <div class="card-total-sm">
                                            <div class="float-right">Total <span class="card-total-price">
                                        <i class="fa fa-rupee"></i> {{ number_format($order->order_summary->total_amount,2) }}</span></div>
                                        </div>
                                        <div class="mt-2"></div>
                                        <div class="clearfix">
                                            <button type="submit" class="btn btn--lg w-100">
                                                Continue <i class="fa fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @if($selected_address)
            @include('website.order-process.address-modal.update')
        @else
            @include('website.order-process.address-modal.create')
        @endif
    </div>
@stop
@section('page-javascript')
    <script>
        @if(session('address_errors'))
         @if($selected_address)
            $('#updateAddress').modal({backdrop:'static', keyboard:false});
        @else
            $('#createAddress').modal({backdrop:'static', keyboard:false});
        @endif
        @endif

        @if($selected_address)
            $('#updateAddress').modal({backdrop:'static', keyboard:false});
        @endif

    </script>
@stop