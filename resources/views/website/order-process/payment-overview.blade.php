@extends('website.template.layout')

@section('page-title', 'Order Payment')

@section('page-content')

    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><a href="{{ route('website-cart')  }}">Shopping Cart</a></li>
                <li><a href="{{ route('website-checkout-shipping-address')  }}">Shipping Address</a></li>
                <li><span>Checkout - Order Payment</span></li>
            </ul>
        </div>
    </div>

    <div class="holder mt-0">
        <div class="container" id="paymentPage">
            <h1 class="text-center">Order Payment</h1>
            <div>
                <div class="row">
                    <div class="col-md-8" v-if="address">
                        <div class="card card--grey">
                            <div class="card-header bg-website">
                                <h2 class="text-white mb-0">Address Details
                                    <span class="badge badge-primary pull-right mr-3">
                                    @{{ address.type == 1 ? 'Home' : (address.type == 2 ? 'Office' : 'Other') }}</span>
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="cart-table cart-table--sm">
                                    <div class="cart-table-prd pt-0">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left">
                                            <h2>Name</h2>
                                        </div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right">
                                            <span>@{{ address.name }}</span>
                                        </div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left">
                                            <h2>Contact No.</h2>
                                        </div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right">
                                            <span>@{{ address.mobile }}</span>
                                        </div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left">
                                            <h2>Address</h2>
                                        </div>
                                        <div class="cart-table-prd-name pl-0 pr-0 text-right">
                                        <span>@{{ address.address }}, @{{ address.landmark }}, @{{ address.city }},
                                            @{{ address.state.name }}, @{{ address.pincode }}.
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-3 card--grey">
                            <div class="card-header bg-website">
                                <h2 class="text-white mb-0">ORDER SUMMARY</h2>
                            </div>
                            <div class="card-body">
                                <div class="cart-table cart-table--sm">
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Order Total</h2></div>
                                        <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>
                                                <i class="fa fa-rupee"></i> @{{ orderSummary.amount | currency }}</b></div>
                                    </div>
                                    <div class="cart-table-prd">
                                        <div class="cart-table-prd-name pl-0 pr-0 text-left"><h2>Total BVs</h2></div>
                                        <div class="cart-table-prd-price pl-0 pr-0 text-right"><b>{{ $order->order_summary->total_bv }}</b></div>
                                    </div>
                                </div>
                                <div class="card-total-sm">
                                    <div class="float-right">Total <span class="card-total-price">
                                        <i class="fa fa-rupee"></i> {{ number_format($order->order_summary->total_amount,2) }}</span></div>
                                </div>
                                <div class="mt-2"></div>
                                <div class="clearfix">
                                    <button type="button" @click="processOrder" class="btn btn-danger btn--lg w-100">
                                        Process to Pay <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;

        new Vue({
            el: '#paymentPage',
            data: {
                items: {!! json_encode($order->items) !!},
                orderSummary: {!! json_encode($order->order_summary) !!},
                shippingAddressId: parseInt('{{ isset($order->address) ? $order->address->id : ''}}'),
                address: {!! json_encode(isset($order->address) ? $order->address : '') !!},
                payment_method: parseInt('{{ isset($order->payment_method) ? $order->payment_method : ''}}'),
            },
            methods: {
                setPricing: function () {
                    this.items = _.map(this.items, item => {
                        item.selling_price = item.distributor_price;
                        return item;
                    });

                    setTimeout(() => {

                        this.orderSummary.amount = _.chain(this.items).sumBy(item => {
                            return item.quantity.current * parseFloat(item.selling_price);
                        }).round(2).value();
                    })
                },
                processOrder: function () {

                    INGENIOUS.blockUI(true);

                    this.$http.post('{{ route("website-checkout-order-process") }}', {
                        items: this.items,
                        order_summary: this.orderSummary,
                        shipping_address_id: this.shippingAddressId,
                    }).then(response => {

                        if (response.data.status) {
                            setTimeout(() => {
                                window.location = '{{ route('website-order-overview') }}?customer_order_id=' + response.data.customer_order_id;
                            }, 1000);
                        } else {
                            INGENIOUS.blockUI(false);
                            swal('Error', response.data.message, 'error');
                        }
                    });

                },
            }
        });
    </script>
@stop