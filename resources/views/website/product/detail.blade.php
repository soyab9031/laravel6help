@extends('website.template.layout')
@section('page-title','Product Detail')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                {{--                <li><a href="{{route('website-products-view', ['slug' => $product_price->product->category->children->slug])}}">Product</a></li>--}}
                <li><span>{{ $product_price->product->name }}</span></li>
            </ul>
        </div>
    </div>
    <!-- section start -->
    <div class="holder mt-0">
        <div class="container">
            <div class="row prd-block prd-block--mobile-image-first js-prd-gallery" id="prdGallery100">
                <div class="col-md-6 col-xl-5">
                    <div class="prd-block_info js-prd-m-holder mb-2 mb-md-0"></div>
                    <!-- Product Gallery -->
                    <div class="prd-block_main-image main-image--slide js-main-image--slide">
                        <div class="prd-block_main-image-holder js-main-image-zoom" data-zoomtype="inner">
                            <div class="prd-has-loader">
                                <div class="gdw-loader"></div>
                                <img src="{{ $product_price->primary_image }}" class="zoom" alt="{{ $product_price->product->name }}"
                                     data-zoom-image="{{env('PRODUCT_IMAGE_URL').$product_price->primary_image}}">
                            </div>
                            <div class="prd-block_main-image-next slick-next js-main-image-next">NEXT</div>
                            <div class="prd-block_main-image-prev slick-prev js-main-image-prev">PREV</div>
                        </div>
                    </div>
                    <div class="product-previews-wrapper">
                        <div class="product-previews-carousel" id="previewsGallery100">
                            @foreach($product_price->images  as $index=>$image)
                                <a href="{{env('PRODUCT_IMAGE_URL').$image}}" data-value="Silver" data-image="{{env('PRODUCT_IMAGE_URL').$image}}"
                                   data-zoom-image="{{env('PRODUCT_IMAGE_URL').$image}}">
                                    <img src="{{env('PRODUCT_IMAGE_URL').$image}}" alt="">
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <!-- /Product Gallery -->
                </div>
                <div class="col-md">
                    <div class="prd-block_info">
                        <div class="js-prd-d-holder prd-holder">
                            <div class="prd-block_title-wrap">
                                <h1 class="prd-block_title">{{ $product_price->product->name}}</h1>
                                {{--<div class="prd-block__labels"><span class="prd-label--new">NEW</span></div>--}}
                            </div>
                            <div class="prd-block_info-top">
                                <div class="product-sku">SKU: <span>{{ $product_price->code }}</span></div>
                            </div>
                            <div class="prd-block_description topline">
                                <p>{!! $product_price->product->description !!}</p>
                            </div>
                        </div>
                        <div class="prd-block_actions topline">
                            <div class="prd-block_price">
                                <span class="prd-block_price--actual">MRP :{{ $product_price->price }}</span>
                                {{--<span class="prd-block_price--old">$210.00</span>--}}
                            </div>
                        </div>
                        <div class="prd-block_actions topline">
                            <div id="productDetail" class="mb-3">
                                <div class="row">
                                    <div class="col-md-6"  v-if="quantity.current > 0">
                                        <vue-counter-button v-model="quantity"
                                                            :price-id="product_price_id"
                                                            min="0" max="100"
                                                            @change-counter-btn="onChangeCounter">
                                        </vue-counter-button>
                                    </div>
                                    <div class="col-md-6">
                                        <button @click="addToCart" v-if="quantity.current == 0"
                                                class="btn btn-xs text-uppercase">
                                            <i class="icofont-plus"></i> Add to cart
                                        </button>
                                        <a href="{{ route('website-cart') }}" class="btn mt-1"
                                           v-if="quantity.current > 0">Go to Cart <i class="icofont-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--left--}}
                <div class="col-xl-3 mt-3 mt-xl-0 sidebar-product">
                    <div class="shop-features-style4">
                        <a href="#" class="shop-feature">
                            <div class="shop-feature-icon"><i class="icon-box3"></i></div>
                            <div class="shop-feature-text">
                                <div class="text1">Free worlwide delivery</div>
                                <div class="text2">Lorem ipsum dolor sit amet conset</div>
                            </div>
                        </a>
                        <a href="#" class="shop-feature">
                            <div class="shop-feature-icon"><i class="icon-arrow-left-circle"></i></div>
                            <div class="shop-feature-text">
                                <div class="text1">100% money back guarantee</div>
                                <div class="text2">Lorem ipsum dolor sit amet conset</div>
                            </div>
                        </a>
                        <a href="#" class="shop-feature">
                            <div class="shop-feature-icon"><i class="icon-call"></i></div>
                            <div class="shop-feature-text">
                                <div class="text1">24/7 customer support</div>
                                <div class="text2">Lorem ipsum dolor sit amet conset</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="holder mt-5">
            <div class="container">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs product-tab">
                    <li class="nav-item"><a href="#Tab1" class="nav-link" data-toggle="tab">Description</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade" id="Tab1">
                        <p>{!! $product_price->product->description !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-js')
    <script>
        Vue.prototype.$http = axios;
        new Vue({
            el: '#productDetail',
            data: {
                cart: null,
                product_price_id: parseInt('{{ $product_price->id }}'),
                quantity: {
                    action: null, current: 0, actual: 100
                }
            },
            methods: {
                onChangeCounter: function (emitValue) {

                    if (emitValue.action === 'increment') {
                        this.addToCart();
                    } else if (emitValue.action === 'decrement') {
                        this.removeFromCart();
                    }

                },
                addToCart: function () {

                    if (this.quantity.current === 0)
                        this.quantity.current = 1;

                    this.$http.get('{{ route('website-cart-add-item') }}', {
                        params: {
                            product_price_id: this.product_price_id,
                            qty: this.quantity.current
                        }
                    }).then(response => {
                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }
                    });
                },
                removeFromCart: function () {

                    this.$http.get('{{ route('website-cart-remove-item') }}', {
                        params: {
                            product_price_id: this.product_price_id,
                            qty: this.quantity.current
                        }
                    }).then(response => {
                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }
                    });
                }
            },
            mounted: function () {

                this.$http.get('{{ route('website-cart-retrieve-items') }}').then(response => {

                    if (response.data.status) {
                        this.cart = response.data.cart;

                        let current_item = _.chain(this.cart).filter(cart_item => {
                            return parseInt(cart_item.product_price_id) === (this.product_price_id)
                        }).head().value();

                        if (current_item)
                            this.quantity.current = current_item.qty;
                    }
                });
            }
        });
    </script>
@endsection