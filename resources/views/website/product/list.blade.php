@extends('website.template.layout')
@section('page-title','Product')
@section('page-content')
    <!-- breadcrumb start -->
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Products</span></li>
            </ul>
        </div>
    </div>
    <!-- breadcrumb End -->
    <div class="holder mt-0">
        <div class="container">
            <!-- Page Title -->
            <div class="page-title text-center d-none d-lg-block">
                <div class="title">
                    <h1>
                        @if($type == 'deal')
                            DEALS OF THE DAY
                            @elseif($type == 'new_arrival')
                            NEW ARRIVAL PRODUCTS
                            @elseif($type == 'recommended')
                            RECOMMENDED PRODUCTS
                        @else
                        @endif
                    </h1>
                </div>
            </div>
            <div class="row">
                <!-- Center column -->
                <div class="col-lg aside" id="productListPage">
                    <div class="holder">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4" v-for="item of items">
                                    <div class="prd prd-has-loader prd-has-countdown bg-grey">
                                        <div class="prd-inside">
                                            <div class="prd-img-area">
                                                <a :href="'/product-details/' + item.product.slug + '/' + item.code" class="prd-img">
                                                    <img :src="item.primary_image" :alt="item.product.name" class="js-prd-img lazyload">
                                                </a>
                                                <div class="countdown-box">
                                                    <div class="countdown js-countdown" data-countdown="2019/12/31"></div>
                                                </div>
                                                <div class="gdw-loader"></div>
                                            </div>
                                            <div class="prd-info text-center">
                                                <h2 class="prd-title">
                                                    <a :href="'/product-details/' + item.product.slug + '/' + item.code">
                                                        <h3 class="website-font mb-0">
                                                            @{{ item.product.name | str_limit(30) }}
                                                        </h3>
                                                    </a>
                                                </h2>
                                                <div class="">
                                                    <div class="price-new"> @{{ item.price | currency }}</div>
                                                    {{--<div class="price-old">$ 70.00</div>--}}
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0)"
                                                       class="btn mt-1"
                                                       @click="addToCart(item.id, 1)"
                                                       v-if="item.quantity.current == 0">Add To Cart</a>

                                                    <div v-if="item.quantity.current > 0">
                                                        <vue-counter-button v-model="item.quantity" min="0"
                                                                            max="100" :price-id="item.id"
                                                                            @change-counter-btn="changeCounterBtn($event)">
                                                        </vue-counter-button>
                                                        <a href="{{ route('website-cart') }}" class="btn mt-1">
                                                            View CART
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Center column -->
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;
        new Vue({
            el: '#productListPage',
            data: {
                isActive: false,
                items: {!! json_encode($items) !!},
            },
            methods: {
                changeCounterBtn: function (event) {

                    if (event.action === 'increment') {
                        this.addToCart(event.product_price_id, event.current);
                    } else if (event.action === 'decrement') {
                        this.removeFromCart(event.product_price_id, event.current);
                    }
                },
                addToCart: function (item_id, qty) {

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{route('website-cart-add-item')}}', {
                        params: {
                            product_price_id: item_id,
                            qty: qty
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                        _.map(this.items, item => {
                            if (parseInt(item.id) === parseInt(item_id)) {
                                item.quantity.current = qty;
                            }
                        });

                    });
                },
                removeFromCart: function (product_price_id, quantity) {

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route('website-cart-remove-item') }}', {
                        params: {
                            product_price_id: product_price_id,
                            qty: quantity,
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (!response.data.status) {
                            swal("Oops", response.data.message, "error");
                        }

                        _.map(this.items, item => {
                            if (parseInt(item.id) === parseInt(product_price_id)) {
                                item.quantity.current = quantity;
                            }
                        });
                    });
                }
            }
        })
    </script>
@stop