@extends('website.template.layout')
@section('page-title','Mission')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>Mission</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 aside" id="centerColumn">
                        <div class="post-prw-big mission-image">
                            <img src="/website-assets/images/home/mission.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 aside aside--right" id="sideColumn">
                        <div class="title-wrap text-center">
                            <h2 class="h1-style btn-decor">Our Mission</h2>
                        </div>
                        <p class="post-teaser">Mutual Alliance global is a business management company whose mission is to bring the best projects to life through conglomerate of group of companies.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop