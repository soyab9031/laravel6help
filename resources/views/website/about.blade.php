@extends('website.template.layout')
@section('page-title','About')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>About Us</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 aside" id="centerColumn">
                        <div class="post-prw-big tea-intro-img">
                            <img src="/website-assets/images/about.jpeg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 aside aside--right" id="sideColumn">
                        <div class="title-wrap text-center">
                            <h2 class="h1-style btn-decor">About Of A Os LifeStyle</h2>
                        </div>
                        <p class="post-teaser">“In a muted world, growth becomes a scarcity. So, the market will pay a premium for quality, for cash conversion (turning profits into cash), and for growth. This is where we step in and take advantage by focusing on trends and market scarcity.”.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop