@extends('website.template.layout')
@section('page-title','Contact')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>Contact Us</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="page-title text-center">
                    <div class="title">
                        <h1>Contact Us</h1>
                    </div>
                </div>
                <div class="row vert-margin-double justify-content-center">
                    <div class="col-sm-6 col-lg-4">
                        <h2>Make Inquiry</h2>
                        <div class="contact-info">
                            <div class="contact-info-icon"><i class="icon-mobile"></i></div>
                            <div class="contact-info-title">CALL US:</div>
                            <div class="contact-info-text">1234567890</div>
                        </div>
                        <div class="contact-info">
                            <div class="contact-info-icon"><i class="icon-mail"></i></div>
                            <div class="contact-info-title">E-MAIL:</div>
                            <div class="contact-info-text"><a href="mailto:info@aoslifestyle.com"></a>info@aoslifestyle.com</div>
                        </div>
                        <div class="contact-info">
                            <div class="contact-info-icon"><i class="icon-location"></i></div>
                            <div class="contact-info-title">ADDRESS:</div>
                            <div class="contact-info-text">Address</div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 offset-lg-1">
                        @if (session('errors'))
                            <div class="alert alert-danger text-center">

                                <ul>
                                    @foreach (session('errors')->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success text-center" style="border-radius: 3rem;">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form action="" method="post">
                            @csrf
                            <div class="form-group"><label class="text-dark"><span class="required">*</span>NAME</label> <input type="text" name="name" class="form-control" data-required-error="Please fill the field" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group"><label class="text-dark"><span class="required">*</span>EMAIL</label> <input type="text" name="email" class="form-control" data-error="Error, that email address is invalid" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="text-dark"><span class="required">*</span>Mobile</label>
                                <input type="number" name="contact" class="form-control" data-error="Error, that contact number is invalid" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group"><label class="text-dark"><span class="required">*</span>MESSAGE</label> <textarea class="form-control textarea--height-100" name="message" data-required-error="Please fill the field" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div><button class="btn mt-1" type="submit">send message</button>
                            <div class="form-group">
                                <div id="alert-msg" class="alert-msg text-center"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop