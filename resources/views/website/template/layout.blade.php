<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="AOS LifeStyle Provides A World Of Opportunities With A New Promise And Focuses On Enriching Lives By Transforming Lifestyle.">
    <meta name="keywords" content="AOS LifeStyle - 'Better your life-style'">
    <meta name="author" content="AOS LifeStyle - 'Better your life-style'"/>
    <meta name="copyright" content="AOS LifeStyle - 'Better your life-style'"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="canonical" href="{{ env('APP_URL') }}"/>
    <meta name="robots" content="index, follow">

    <meta property="og:description"
          content="AOS LifeStyle - 'Better your life-style' Provides A World Of Opportunities With A New Promise And Focuses On Enriching Lives By Transforming Lifestyle.">
    <meta property="og:image" content="{{ env('APP_URL') }}/user-assets/images/company/logo.png">
    <meta property="og:url" content="{{ env('APP_URL') }}">
    <meta property="og:title" content="AOS LifeStyle - 'Better your life-style'">
    <meta property="og:type" content="AOS LifeStyle - 'Better your life-style'">
    <meta property="og:site_name" content="AOS LifeStyle - 'Better your life-style'">
    <link rel="icon" type="image/png" href="/user-assets/images/company/favicon.png">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('page-title') | Welcome To AOS LifeStyle - 'Better your life-style'</title>
    <!-- CSS Files-->

    <link href="/website-assets/js/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/website-assets/js/vendor/slick/slick.min.css" rel="stylesheet">
    <link href="/website-assets/js/vendor/fancybox/jquery.fancybox.min.css" rel="stylesheet">
    <link href="/website-assets/js/vendor/animate/animate.min.css" rel="stylesheet">
    <link href="/website-assets/css/style-light.css" rel="stylesheet">
    <link href="/website-assets/css/swiper.css" rel="stylesheet">
    <link href="/website-assets/fonts/icomoon/icomoon.css" rel="stylesheet">
    <!--custom font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--web fonts-->

    @yield('page-css')
    @yield('import-css')
</head>
<body class="home-page is-dropdn-click has-slider">

@include('website.template.header')
<div class="page-content">
    @yield('page-content')
</div>
@include('website.template.footer')
<a class="back-to-top js-back-to-top" href="#" title="Scroll To Top"><i class="icon icon-angle-up"></i></a>

<script src="/website-assets/js/vendor/jquery/jquery.min.js"></script>
<script src="/website-assets/js/vendor/bootstrap/bootstrap.bundle.min.js"></script>
<script src="/website-assets/js/vendor/slick/slick.min.js"></script>
<script src="/website-assets/js/vendor/scrollLock/jquery-scrollLock.min.js"></script>
<script src="/website-assets/js/vendor/instafeed/instafeed.min.js"></script>
<script src="/website-assets/js/vendor/countdown/jquery.countdown.min.js"></script>
<script src="/website-assets/js/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="/website-assets/js/vendor/ez-plus/jquery.ez-plus.min.js"></script>
<script src="/website-assets/js/vendor/tocca/tocca.min.js"></script>
<script src="/website-assets/js/vendor/bootstrap-tabcollapse/bootstrap-tabcollapse.min.js"></script>
<script src="/website-assets/js/vendor/isotope/jquery.isotope.min.js"></script>
<script src="/website-assets/js/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="/website-assets/js/vendor/cookie/jquery.cookie.min.js"></script>
<script src="/website-assets/js/vendor/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/website-assets/js/vendor/lazysizes/lazysizes.min.js"></script>
<script src="/website-assets/js/vendor/lazysizes/ls.bgset.min.js"></script>
<script src="/website-assets/js/vendor/form/jquery.form.min.js"></script>
<script src="/website-assets/js/vendor/form/validator.min.js"></script>
<script src="/website-assets/js/vendor/slider/slider.js"></script>
<script src="/website-assets/js/swiper.js"></script>
<script src="/website-assets/js/app.js"></script>
<script src="/admin-assets/js/jquery.blockUI.js"></script>
<script src="/user-assets/js/lodash.js"></script>
<script src="/user-assets/plugins/axios/axios.min.js"></script>
<script src="/user-assets/plugins/vue/{{ env('APP_ENV') == 'local' ? 'vue.js' : 'vue.min.js' }}"></script>
<script src="/user-assets/plugins/vue/vue-helper.js?v=10"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>
<script>
    @if(session('errors'))
    swal('Oops', '{{ session('errors')->first() }}', 'error');
    @elseif(session('error'))
    swal('Oops', '{{ session('error') }}', 'error');
    @elseif(session('success'))
    swal('Hurray', '{{ session('success') }}', 'success');
    @endif
</script>
@yield('page-js')
@yield('import-js')
@yield('page-javascript')
</body>
</html>
