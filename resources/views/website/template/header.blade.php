@php
    $header_categories = \App\Models\Category::active()->whereNull('parent_id')->with(['children' => function ($q){ $q->active(); }])->get();
@endphp
<header class="hdr global_width hdr_sticky hdr-mobile-style2">
    <div class="mobilemenu js-push-mbmenu">
        <div class="mobilemenu-content">
            <div class="mobilemenu-close mobilemenu-toggle">CLOSE</div>
            <div class="mobilemenu-scroll">
                <div class="mobilemenu-search"></div>
                <div class="nav-wrapper show-menu">
                    <div class="nav-toggle"><span class="nav-back"><i class="icon-arrow-left"></i></span> <span class="nav-title"></span></div>
                    <ul class="nav nav-level-1">
                        <li>
                            <a href="{{ route('website-home')  }}">Home</a><span class="arrow"></span>
                        </li>
                        <li>
                            <a href="{{ route('website-company-about')  }}" title="">About</a><span class="arrow"></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);" title="">Services</a><span class="arrow"></span>
                            <ul class="nav-level-2">
                                <li><a href="{{ route('website-company-vision')  }}">Our Vision</a></li>
                                <li><a href="{{ route('website-company-mission')  }}">Our Mission</a></li>
                            </ul>
                        </li>
                        <li>
                            @if(count($header_categories) == 0)
                                <a href="javascript:void(0);">Shop</a><span class="arrow"></span>
                            @else
                                <a href="javascript:void(0);">Shop</a><span class="arrow"></span>
                                <ul class="nav-level-2">
                                    @foreach($header_categories as $header_category)
                                        <li>
                                            <a href="{{route('website-products-view', ['slug' => $header_category->slug])}}">
                                                {{ $header_category->name}}
                                            </a>
                                            <span class="arrow"></span>
                                            <ul class="nav-level-3">

                                                @foreach($header_category->children as $child)
                                                    <li>
                                                        <a href="{{route('website-products-view', ['slug' => $header_category->slug,
                                                                        'sub' => $child->slug])}}">{{ $child->name }}
                                                        </a>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </li>
                                    @endforeach

                                </ul>
                            @endif
                        </li>
                        <li><a href="{{ route('website-contact')  }}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="hdr-mobile show-mobile">
        <div class="hdr-content">
            <div class="container">
                <div class="menu-toggle"><a href="#" class="mobilemenu-toggle"><i class="icon icon-menu"></i></a></div>
                <div class="logo-holder"><a href="{{ route('website-home')  }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/user-assets/images/company/logo.png 2x" alt=""></a></div>
                <div class="hdr-mobile-right">
                    <div class="hdr-topline-right links-holder"></div>
                    <div class="minicart-holder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="hdr-desktop hide-mobile">
        <div class="hdr-topline">
            <div class="container">
                <div class="row">
                    <div class="col-auto hdr-topline-left">
                        <div class="dropdn dropdn_language">
                            <div class="dropdn dropdn_caret">
                                <a href="javascript:void(0);">Welcome To Our A Os Life Style</a>
                            </div>
                            <div class="dropdn dropdn_caret ml-1">
                                <div class="custom-text">
                                    <a href="callto://9999999999"><i class="icon icon-mobile"></i>
                                        <b>+91 9999 9999 99</b></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto hdr-topline-right links-holder">
                        {{--<div class="dropdn dropdn_search hide-mobile @@classes">--}}
                        {{--<a href="#" class="dropdn-link"><i class="icon icon-search2"></i><span>Search</span></a>--}}
                        {{--</div>--}}
                        {{--<div class="dropdn dropdn_wishlist @@classes"><a href="javascript:void(0);" class="dropdn-link"><i class="icon icon-heart-1"></i><span>Wishlist</span></a></div>--}}
                        <div class="dropdn dropdn_wishlist @@classes">
                            <a href="{{ route('store-register-request')  }}" target="_blank" class="dropdn-link">
                                <i class="icon icon-franchise"></i><span>Franchise Register</span></a>
                        </div>
                        <div class="dropdn dropdn_wishlist @@classes">
                            <a href="{{ route('user-login')  }}" target="_blank" class="dropdn-link">
                                <i class="icon icon-person"></i><span>Login / Sign Up</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hdr-content hide-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-auto logo-holder"><a href="{{ route('website-home')  }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/website-assets/images/logo-retina.png 2x" alt=""></a></div>
                    <div class="prev-menu-scroll icon-angle-left prev-menu-js"></div>
                    <div class="nav-holder">
                        <div class="hdr-nav">
                            <ul class="mmenu mmenu-js">
                                <li class="mmenu-item--simple">
                                    <a href="{{ route('website-home')  }}" title="">Home</a>
                                </li>
                                <li class="mmenu-item--simple">
                                    <a href="{{  route('website-company-about') }}" title="">About</a>
                                </li>
                                @if(count($header_categories) == 0)
                                    <li class="mmenu-item--simple">
                                        <a href="javascript:void(0);" title="">Shop</a>
                                    </li>
                                @else
                                    <li class="mmenu-item--simple"><a href="#" title="">Shop</a>
                                        <div class="mmenu-submenu">
                                            <ul class="submenu-list">
                                                @foreach($header_categories as $header_category)
                                                    <li><a href="{{route('website-products-view', ['slug' => $header_category->slug])}}" title="">{{ $header_category->name  }}</a>
                                                        <ul>
                                                            @foreach($header_category->children as $child)
                                                                <li><a href="{{route('website-products-view', ['slug' => $header_category->slug,
                                                                        'sub' => $child->slug])}}" title="">{{ $child->name  }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                    {{--<li class="mmenu-item--mega"><a href="javascript:void(0);">Shop</a>--}}
                                        {{--<div class="mmenu-submenu mmenu-submenu-with-sublevel">--}}
                                            {{--<div class="mmenu-submenu-inside">--}}
                                                {{--<div class="container">--}}
                                                    {{--<div class="mmenu-cols column-6">--}}
                                                        {{--@foreach($header_categories as $header_category)--}}
                                                            {{--<div class="mmenu-col">--}}
                                                                {{--<h3 class="submenu-title"><a href="{{route('website-products-view', ['slug' => $header_category->slug])}}">{{ $header_category->name  }}</a></h3>--}}
                                                                {{--<ul class="submenu-list">--}}
                                                                    {{--@foreach($header_category->children as $child)--}}
                                                                        {{--<li><a href="{{route('website-products-view', ['slug' => $header_category->slug,--}}
                                                                        {{--'sub' => $child->slug])}}">{{  $child->name  }}</a></li>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</ul>--}}
                                                                {{--@if(count($header_category->children) > 0)--}}
                                                                    {{--<a href="{{route('website-products-view', ['slug' => $header_category->slug])}}" class="submenu-view-more">View More</a>--}}
                                                                {{--@endif--}}
                                                            {{--</div>--}}
                                                        {{--@endforeach--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                @endif
                                <li class="mmenu-item--simple">
                                    <a href="#" title="">Services</a>
                                    <div class="mmenu-submenu">
                                        <ul class="submenu-list">
                                            <li><a href="{{ route('website-company-vision')  }}">Our Vision</a></li>
                                            <li><a href="{{ route('website-company-mission')  }}">Our Mission</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="{{ route('website-contact')  }}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="next-menu-scroll icon-angle-right next-menu-js"></div>
                    <div class="col-auto minicart-holder">
                        {{--<div class="minicart minicart-js">--}}
                        <div class="minicart">
                            <a href="{{ route('website-cart') }}" class="minicart-link">
                                <i class="icon icon-handbag"></i>
                                {{ Session::get('cart') ? count(Session::get('cart')) : 0 }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-holder compensate-for-scrollbar">
        <div class="container">
            <div class="row">
                <a href="#" class="mobilemenu-toggle show-mobile"><i class="icon icon-menu"></i></a>
                <div class="col-auto logo-holder-s"><a href="{{ route('website-home')  }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/user-assets/images/company/logo.png 2x" alt=""></a></div>
                <div class="prev-menu-scroll icon-angle-left prev-menu-js"></div>
                <div class="nav-holder-s"></div>
                <div class="next-menu-scroll icon-angle-right next-menu-js"></div>
                <div class="col-auto minicart-holder-s"></div>
            </div>
        </div>
    </div>
</header>