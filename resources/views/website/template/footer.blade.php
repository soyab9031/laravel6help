<footer class="page-footer footer-style-1 global_width">
    <div class="holder bgcolor bgcolor-1 mt-0">
        <div class="container">
            <div class="row shop-features-style3">
                <div class="col-md"><a href="#" class="shop-feature light-color">
                        <div class="shop-feature-icon"><i class="icon-box3"></i></div>
                        <div class="shop-feature-text">
                            <div class="text1">Free worlwide delivery</div>
                        </div>
                    </a></div>
                <div class="col-md"><a href="#" class="shop-feature light-color">
                        <div class="shop-feature-icon"><i class="icon-arrow-left-circle"></i></div>
                        <div class="shop-feature-text">
                            <div class="text1">Top-notch quality products</div>
                        </div>
                    </a></div>
                <div class="col-md"><a href="#" class="shop-feature light-color">
                        <div class="shop-feature-icon"><i class="icon-call"></i></div>
                        <div class="shop-feature-text">
                            <div class="text1">24/7 customer support</div>
                        </div>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="footer-top container">
        <div class="row py-md-4">
            <div class="col-md-3 col-lg-3">
                <div class="footer-block collapsed-mobile">
                    <div class="title">
                        <h4>OUR BRANDS</h4>
                        <div class="toggle-arrow"></div>
                    </div>
                    <div class="collapsed-content">
                        <p class="text-justify">“In a muted world, growth becomes a scarcity. So, the market will pay a premium for quality, for cash conversion (turning profits into cash), and for growth. This is where we step in and take advantage by focusing on trends and market scarcity.”</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="footer-block collapsed-mobile">
                    <div class="title">
                        <h4>CORPORATE INFO</h4>
                        <div class="toggle-arrow"></div>
                    </div>
                    <div class="collapsed-content">
                        <ul>
                            <li><a href="javascript:void(0);">Start a business</a></li>
                            <li><a href="javascript:void(0);">GST registration</a></li>
                            <li><a href="javascript:void(0);">FAQs</a></li>
                            <li><a href="javascript:void(0);">Contact Us</a></li>
                            <li><a href="javascript:void(0);">Search Outlets</a></li>
                            <li><a href="{{ route('website-company-about')  }}">About AOS</a></li>
                            <li><a href="javascript:void(0);">Careers</a></li>
                            <hr/>
                            <li><a href="{{ route('user-login')  }}" target="_blank">Login</a></li>
                            <li><a href="{{ route('user-register')  }}" target="_blank">REGISTER</a></li>
                            {{--<li><a href="{{ route('website-company-vision')  }}">Our Vision</a></li>--}}
                            {{--<li><a href="{{ route('website-company-mission') }}">Our Mission</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="footer-block collapsed-mobile">
                    <div class="title">
                        <h4>POLICIES</h4>
                        <div class="toggle-arrow"></div>
                    </div>
                    <div class="collapsed-content">
                        <ul>
                            <li><a href="javascript:void(0);">Terms of Use</a></li>
                            <li><a href="javascript:void(0);">Website privacy Policy</a></li>
                            <li><a href="javascript:void(0);">Direct Outlets & Privacy Policy</a></li>
                            <li><a href="javascript:void(0);">Rules of conduct</a></li>
                            <li><a href="javascript:void(0);">Product return policy</a></li>
                            <li><a href="javascript:void(0);">Shipping & pickup</a></li>
                            <li><a href="javascript:void(0);">Procedure</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="footer-block collapsed-mobile">
                    <div class="title">
                        <h4>COMPLAINTS / SUGGESTIONS</h4>
                        <div class="toggle-arrow"></div>
                    </div>
                    <div class="collapsed-content">
                        <ul>
                            <li><a href="{{ route('website-support')  }}" target="_blank"><u>Click here </u>to write us</a></li>
                            {{--<li><a href="{{ route('website-company-privacy-policy')  }}" target="_blank">Privacy Policy</a></li>--}}
{{--                            <li><a href="{{ route('website-company-cancellation-policy')  }}" target="_blank">Cancellation Policy</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="footer-block collapsed-mobile">
                    <div class="title">
                        <h4>contact us</h4>
                        <div class="toggle-arrow"></div>
                    </div>
                    <div class="collapsed-content">
                        <ul class="contact-list">
                            <li><i class="icon-phone"></i><span><span class="h6-style">Call Us:</span><span>1234567890</span></span></li>
                            <li><i class="icon-mail-envelope1"></i><span><span class="h6-style">E-mail:</span><span><a href="mailto:info@aoslifestyle.com">info@aoslifestyle.com</a></span></span></li>
                            <li><i class="icon-location1"></i><span><span class="h6-style">Address:</span><span>Address</span></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom container">
        <div class="row lined py-2 py-md-3">
            <div class="col-md-2 hidden-mobile"><a href="#"><img src="/user-assets/images/company/footer-logo.png" class="img-fluid" alt=""></a></div>
            <div class="col-md-8 col-lg-7 footer-copyright">
                <p class="footer-copyright-text">
                    © Copyright {{date('Y')}} <a href="{{ env('APP_URL') }} " class="text-lowercase">{{ config('project.company') }} </a> All rights Reserved.</p>
            </div>
            {{--<div class="col-md-auto">--}}
            {{--<div class="payment-icons"><img src="/website-assets/images/placeholder.png" data-srcset="/website-assets/images/payment/payment-card-visa.png 1x, /website-assets/images/payment/payment-card-visax2.png 2x" class="lazyload" alt=""> <img src="/website-assets/images/placeholder.png" data-srcset="/website-assets/images/payment/payment-card-mastecard.png 1x, /website-assets/images/payment/payment-card-mastecardx2.png 2x" class="lazyload" alt=""> <img src="/website-assets/images/placeholder.png" data-srcset="/website-assets/images/payment/payment-card-discover.png 1x, /website-assets/images/payment/payment-card-discoverx2.png 2x" class="lazyload" alt=""></div>--}}
            {{--</div>--}}
            <div class="col-md-auto ml-lg-auto">
                <ul class="social-list">
                    <li><a href="javascript:void(0);" class="icon icon-facebook"></a></li>
                    <li><a href="javascript:void(0);" class="icon icon-twitter"></a></li>
                    <li><a href="javascript:void(0);" class="icon icon-instagram"></a></li>
                    <li><a href="javascript:void(0);" class="icon icon-youtube"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <a href="https://api.whatsapp.com/send?phone=+916370868190&text=hello" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
</footer>