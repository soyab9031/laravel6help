@extends('website.template.layout')
@section('page-title','Grievance Submit confirmation')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Grievance Submit confirmation</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <div class="page-title text-center">
                <div class="title">
                    <h1>Send us an Email</h1>
                </div>
            </div>
            <div class="row vert-margin-double justify-content-center">
                <div class="col-sm-6 col-lg-4">
                    @if (session('errors'))
                        <div class="alert alert-danger text-center">

                            <ul>
                                @foreach (session('errors')->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success text-center" style="border-radius: 3rem;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form action="" method="post">
                        @csrf
                        <div class="form-group"><label class="text-dark"><span class="required">*</span>NAME</label> <input type="text" name="name" class="form-control" data-required-error="Please fill the field" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group"><label class="text-dark"><span class="required">*</span>EMAIL</label> <input type="text" name="email" class="form-control" data-error="Error, that email address is invalid" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group"><label class="text-dark"><span class="required">*</span>SUBJECT</label> <input type="text" name="subject" class="form-control" data-error="Error, that Subject is invalid" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group"><label class="text-dark"><span class="required">*</span>MESSAGE</label> <textarea class="form-control textarea--height-100" name="message" data-required-error="Please fill the field" required></textarea>
                            <div class="help-block with-errors"></div>
                        </div><button class="btn mt-1" type="submit">send message</button>
                        <div class="form-group">
                            <div id="alert-msg" class="alert-msg text-center"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop