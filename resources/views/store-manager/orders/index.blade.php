@extends('store-manager.template.layout')

@section('title')
    My Store Orders
@stop

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Store Orders</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="la la-search"></i></span>
                                            </div>
                                            <input type="text" placeholder="Search Order..." name="search" class="form-control" value="{{ Request::get('search') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <select name="status" class="form-control">
                                            <option value="" selected>Filter Status</option>
                                            @foreach(\App\Models\Order::getStatus() as $index => $status)
                                                <option value="{{ $index }}" {{ Request::get('status') == $index ? 'selected' : null }}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select name="delivery_status" id="" class="form-control">
                                            <option value="" selected>Selected Delivery Status</option>
                                            <option value="1" {{ Request::get('delivery_status') == 1 ? 'selected' : '' }}>Pending</option>
                                            <option value="2" {{ Request::get('delivery_status') == 2 ? 'selected' : '' }}>Delivered</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary"> Search </button>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Order Date</th>
                                        <th>Order Id</th>
                                        <th>Customer</th>
                                        <th>Total BV</th>
                                        <th>Total Amount</th>
                                        <th>Status</th>
                                        <th>Delivery Status</th>
                                        <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $index => $order)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                            <td>{{ $order->created_at->format('M d, Y h:i A') }}</td>
                                            <td>{{ $order->customer_order_id }}</td>
                                            <td>{{ $order->user->detail->full_name }} <br> ({{ $order->user->tracking_id }})</td>
                                            <td>{{ $order->total_bv }}</td>
                                            <td>{{ $order->total }}</td>
                                            <td>
                                                @if($order->status == 2)
                                                    <span class="badge badge-warning"> Placed </span>
                                                @elseif($order->status == 3)
                                                    <span class="badge badge-success">Approved </span>
                                                @elseif($order->status == 5)
                                                    <span class="badge badge-danger"> Rejected </span>
                                                @elseif($order->status == 6)
                                                    <span class="badge badge-theme"> Cancel </span>
                                                @else
                                                    <span class="badge badge-info"> Pending </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->delivery_status == \App\Models\Order::PRODUCT_PENDING)
                                                    @if($order->status == \App\Models\Order::APPROVED)
                                                    <span class="badge badge-warning">Pending</span>
                                                    <a href="javascript:void(0)" class="badge badge-danger mb-1 pull-right"
                                                       data-toggle="modal" data-keyboard="false"
                                                       data-target="#updateDeliveryStatus{{ $order->id }}"> <i class="la la-edit"> </i></a>

                                                    <form action="{{ route('store-update-order-delivery') }}" method="post"
                                                          onsubmit="INGENIOUS.blockUI(true)" id="updateDelivery">
                                                        {{ csrf_field() }}
                                                        <div class="modal fade text-left" id="updateDeliveryStatus{{ $order->id }}" tabindex="-1" role="dialog"
                                                             aria-labelledby="basicModalLabel3" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="basicModalLabel3">Update Delivery Status</h4>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <input type="hidden" name="customer_order_id" value="{{ $order->customer_order_id }}">
                                                                        <div class="form-group">
                                                                            <label>Order Delivery Status</label>
                                                                            <select class="form-control" name="delivery_status">
                                                                                <option value="">Select Status</option>
                                                                                <option value="1" {{ $order->delivery_status == 1 ? 'selected' : '' }}>Pending</option>
                                                                                <option value="2" {{ $order->delivery_status == 2 ? 'selected' : '' }}>Delivered</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn grey btn-secondary" data-dismiss="modal">
                                                                            Close
                                                                        </button>
                                                                        <button type="submit" class="btn btn-danger">Deliver</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                        @else
                                                        N.A
                                                    @endif
                                                @else
                                                    <span class="badge badge-success">Delivered</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('store-order-detail', ['customer_order_id' => $order->customer_order_id]) }}" class="btn btn-primary">Details</a>
                                                @if(in_array($order->status, [\App\Models\Order::APPROVED]))
                                                    <a class="btn btn-danger" href="{{ route('store-order-detail', ['customer_order_id' => $order->customer_order_id, 'download' => 'receipt']) }}">Invoice
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $orders->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

