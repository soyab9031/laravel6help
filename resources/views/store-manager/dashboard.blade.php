@extends('store-manager.template.layout')

@section('title', 'Dashboard')

@section('content')
    <div class="row match-height">
        <div class="col-md-3">
            <a href="{{ route('store-order-create') }}">
                <div class="card bg-gradient-x-blue">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-plus-square text-white font-large-4 float-left"></i>
                                </div>
                                <div class="media-body text-white text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">POS</span>
                                    <h2 class="text-white mb-0">Start Billing</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('store-wallet-view') }}">
                <div class="card bg-gradient-x-purple">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-wallet text-white font-large-4 float-left"></i>
                                </div>
                                <div class="media-body text-white text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">Wallet</span>
                                    <h1 class="text-white mb-0">{{ number_format($store->wallet_balance) }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('store-order-view') }}">
                <div class="card bg-gradient-x-red-pink">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-list-alt text-white font-large-4 float-left"></i>
                                </div>
                                <div class="media-body text-white text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">Orders</span>
                                    <h1 class="text-white mb-0">{{ number_format($order_count) }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('store-stock-view') }}">
                <div class="card bg-gradient-x-grey-blue">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-chart-bar text-white font-large-4 float-left"></i>
                                </div>
                                <div class="media-body text-white text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">My Stock</span>
                                    <h1 class="text-white mb-0">Report</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        {{--<div class="col-md-6">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header p-1">--}}
                    {{--<h4 class="card-title float-left">Order Request Details</h4>--}}
                {{--</div>--}}
                {{--<div class="card-content collapse show">--}}
                    {{--<div class="card-footer text-center p-1">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">--}}
                                {{--<p class="blue-grey lighten-2 mb-0">Today`s Request</p>--}}
                                {{--<p class="font-medium-5 text-bold-400">{{ $order_detail->today }}</p>--}}
                                {{--<a href="{{ route('store-order-view', ['dateRange' => \Carbon\Carbon::now()->format('d M Y') . ' - ' . \Carbon\Carbon::now()->format('d M Y')]) }}" class="btn btn-sm btn-info">view</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">--}}
                                {{--<p class="blue-grey lighten-2 mb-0">Pending Request</p>--}}
                                {{--<p class="font-medium-5 text-bold-400">{{ $order_detail->pending }}</p>--}}
                                {{--<a href="{{ route('store-order-view', ['delivery_status' => \App\Models\Order::PRODUCT_PENDING]) }}" class="btn btn-sm btn-info">view</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">--}}
                                {{--<p class="blue-grey lighten-2 mb-0">Delivered</p>--}}
                                {{--<p class="font-medium-5 text-bold-400">{{ $order_detail->delivered }}</p>--}}
                                {{--<a href="{{ route('store-order-view', ['delivery_status' => \App\Models\Order::PRODUCT_DELIVERED]) }}" class="btn btn-sm btn-info">view</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-12 text-center">--}}
                                {{--<p class="blue-grey lighten-2 mb-0">Total</p>--}}
                                {{--<p class="font-medium-5 text-bold-400">{{ $order_detail->total }}</p>--}}
                                {{--<a href="{{ route('store-order-view') }}" class="btn btn-sm btn-info">view</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <h4>Recent Orders...</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>Customer</th>
                                    <th>Total BV</th>
                                    <th>Total Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recent_orders as $recent_order)
                                    <tr>
                                        <td>
                                            <a href="{{ route('store-order-detail', ['customer_order_id' => $recent_order->customer_order_id]) }}">{{ $recent_order->customer_order_id }}</a>
                                        </td>
                                        <td>{{ $recent_order->user->detail->full_name }} <br>(TID:{{ $recent_order->user->tracking_id }})</td>
                                        <td>{{ $recent_order->total_bv }}</td>
                                        <td>{{ $recent_order->amount }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop