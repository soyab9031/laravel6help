@extends('store-manager.template.layout')

@section('title')
    My Commission
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Commission</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="row mb-2">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                    <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary"> Search </button>
                                @refreshBtn('store')
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">
                                    <span class="success"><i class="la la-square"></i> Credit </span>
                                    <span class="danger"><i class="la la-square"></i> Debit</span>
                                </th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Amount</th>
                                <th>TDS</th>
                                <th>Total</th>
                                <th width="25%">Remarks</th>
                                <th width="25%">Payment Details</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($transactions) == 0)
                                <tr>
                                    <td colspan="10" class="text-center">
                                        No Commission available
                                    </td>
                                </tr>
                            @endif
                            @foreach($transactions as $index => $transaction)
                                <tr>
                                    <td>{{ \App\Library\Helper::tableIndex($transactions, $index) }}</td>
                                    <td>{{ $transaction->created_at->format('M d, Y h:i A') }}</td>
                                    <td>
                                        @if($transaction->admin)
                                            <span class="badge badge-primary">Company</span>
                                        @else
                                            <span class="badge badge-danger">Store</span><br>
                                            Store : {{ $transaction->sender_store->name }}<br>
                                            ({{ $transaction->sender_store->tracking_id }})
                                        @endif
                                    </td>
                                    <td>
                                        {{ $transaction->store->name }}<br>
                                        ({{ $transaction->store->tracking_id }})
                                    </td>
                                    <td class="text-dark">{{ number_format($transaction->amount, 2) }}/-</td>
                                    <td class="text-dark">{{ number_format($transaction->tds, 2) }}/-</td>
                                    <td class="text-dark">{{ number_format($transaction->total, 2) }}/-</td>
                                    <td>{{ $transaction->remarks }}</td>
                                    <td>
                                        @if($transaction->payment_details)
                                            Payment Date: {{ $transaction->payment_details['date'] }}<br>
                                            Bank : {{ $transaction->payment_details['bank_name'] }}<br>
                                            Payment Mode : {{ $transaction->payment_details['payment_mode'] }}<br>
                                            Reference Number : {{ $transaction->payment_details['reference_number'] }}<br>
                                            Reference image : <a target="_blank" href="{{ env('PAYMENT_RECEIPT_IMAGE_URL').$transaction->payment_details['image']  }}" class="btn btn-sm btn-info"> View</a>  <br>
                                        @else
                                            N.A
                                        @endif
                                    </td>
                                    <td>
                                        @if($transaction->status == \App\Models\StorePendingCommission::PENDING)
                                            <span class="badge badge-warning">Pending</span>
                                        @else
                                            <span class="badge badge-success">Transferred</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $transactions->appends(['search' => Request::get('search')])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop