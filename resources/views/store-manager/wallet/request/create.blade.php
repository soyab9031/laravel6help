@extends('store-manager.template.layout')

@section('title', 'Create Wallet Fund Requests')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Create Wallet Fund Requests</h3>
        </div>
    </div>
    <div class="content-body">
        <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><span class="la la-inr"></span></span>
                                        </div>
                                        <input type="text" name="amount" onkeypress="INGENIOUS.numericInput(event)" class="form-control" autocomplete="off" value="{{ old('amount') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Bank Name</label>
                                    <input type="text" name="bank_name" class="form-control" autocomplete="off" value="{{ old('bank_name') }}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Reference / UTR Number</label>
                                    <input type="text" name="reference_number" class="form-control" placeholder="Enter DD / Cheque No. / Reference - UTR No." autocomplete="off" value="{{ old('reference_number') }}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Payment Mode</label>
                                    <select class="form-control" name="payment_mode">
                                        @foreach($payment_modes as $payment_mode)
                                            <option value="{{ $payment_mode }}" {{ old('payment_mode') == $payment_mode ? 'selected' : '' }}>
                                                {{ $payment_mode }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Deposit Date</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control deposit_date" name="deposit_date" value="{{ old('deposit_date') }}" placeholder="Deposit Date" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Deposit Time</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control deposit_time" name="deposit_time" value="{{ old('deposit_time') }}" placeholder="Deposit Time" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-clock-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mt-1">
                                    <label>Upload Cash Receipt | NEFT Transfer Receipt | Cheque Copy Receipt</label>
                                    <input type="file" name="image" id="image">
                                    <p class="text-left"><small class="text-muted">Allowed File Formats : .jpg, .jpeg, .png and Maximum File Size Allowed : 4MB</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="text-center m-t-10">
                            <button class="btn btn-danger btn-md">Make Request</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="/user-assets/plugins/pick-a-date/picker.js" type="text/javascript"></script>
    <script src="/user-assets/plugins/pick-a-date/picker.date.js" type="text/javascript"></script>
    <script src="/user-assets/plugins/pick-a-date/picker.time.js" type="text/javascript"></script>
    <script src="/user-assets/plugins/pick-a-date/legacy.js" type="text/javascript"></script>
@stop

@section('import-css')
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/pick-a-date/bundle.css">
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 4, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });

        $(".deposit_date").pickadate({
            selectMonths: !0,
            selectYears: !0,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->format('Y,m,d') }}'),
            min: new Date('{{ \Carbon\Carbon::now()->subMonths(5)->format('Y,m,d') }}')
        });

        $('.deposit_time').pickatime({
            interval: 15,
            min: new Date(2015,3,20,7),
            max: new Date(2015,7,14,23,30)
        })
    </script>
@stop