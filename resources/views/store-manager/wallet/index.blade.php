@extends('store-manager.template.layout')

@section('title')
    My Wallet Transactions
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Wallet Transactions</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xl-4 col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-plus-square icon-opacity success font-large-4"></i>
                                </div>
                                <div class="media-body text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">Total Credit</span>
                                    <h1 class="success mb-0">{{ number_format($total_credit, 2) }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-minus-square icon-opacity danger font-large-4"></i>
                                </div>
                                <div class="media-body text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">Total Debit</span>
                                    <h1 class="danger mb-0">{{ number_format($total_debit, 2) }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la la-balance-scale icon-opacity info font-large-4"></i>
                                </div>
                                <div class="media-body text-right align-self-bottom mt-3">
                                    <span class="d-block mb-1 font-medium-1">Balance</span>
                                    <h1 class="info mb-0">{{ number_format($balance, 2) }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="row mb-2">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                    <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary"> Search </button>
                                @refreshBtn('store')
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">
                                    <span class="success"><i class="la la-square"></i> Credit </span>
                                    <span class="danger"><i class="la la-square"></i> Debit</span>
                                </th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th width="45%">Details</th>
                                <th>Amount</th>
                                <th>Opening</th>
                                <th>Closing</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($transactions) == 0)
                                <tr>
                                    <td colspan="10" class="text-center">
                                        No Wallet Transactions available
                                    </td>
                                </tr>
                            @endif
                            @foreach($transactions as $index => $transaction)
                                <tr class="{{ $transaction->type == 1 ? 'bg-success' : 'bg-danger' }} bg-lighten-4">
                                    <td>{{ \App\Library\Helper::tableIndex($transactions, $index) }}</td>
                                    <td>{{ $transaction->created_at->format('M d, Y h:i A') }}</td>
                                    <td>{{ $transaction->remarks }}</td>
                                    <td class="text-dark">{{ $transaction->amount }}</td>
                                    <td>{{ $transaction->opening_amount }}</td>
                                    <td>{{ $transaction->closing_amount }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $transactions->appends(['search' => Request::get('search')])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop