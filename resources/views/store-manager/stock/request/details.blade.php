@extends('store-manager.template.layout')

@section('title', 'Stock Request Details # ' . $stock_request->id)

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Stock Request Details # {{ $stock_request->id }}</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Stock Request ID:</b> <span class="pull-right">{{ $stock_request->id }}</span>
                            </li>
                            <li class="list-group-item">
                                Date: <span class="pull-right">{{ $stock_request->created_at->format('M d, Y h:i A') }}</span>
                            </li>
                            <li class="list-group-item">
                                Message: <span class="pull-right">{{ $stock_request->remarks }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>BV</th>
                                <th>Qty</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stock_request->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->distributor_price  }}</td>
                                    <td>{{ $detail->product_price->points }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td class="text-right"> {{ number_format($detail->distributor_price, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger bg-darken-1">
                                <td colspan="4" class="text-right">Total</td>
                                <td>{{ collect($stock_request->details)->sum('qty') }}</td>
                                <td class="text-right">{{ number_format(collect($stock_request->details)->sum('selling_price'), 2) }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop