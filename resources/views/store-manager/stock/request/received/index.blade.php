@extends('store-manager.template.layout')

@section('title')
    Received Stock Requests
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Received Stock Requests</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary"> Search </button>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Created At</th>
                                        <th>Request From</th>
                                        <th>Items</th>
                                        <th>Total Amount</th>
                                        <th>Status</th>
                                        <th>View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($stock_requests) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center">No Stock Request Available</td>
                                        </tr>
                                    @endif
                                    @foreach ($stock_requests as $index => $stock_request)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($stock_requests, $index) }}</td>
                                            <td>{{ $stock_request->created_at->format('M d, Y h:i A') }}</td>
                                            <td>
                                                {{ $stock_request->store->name }} ({{ $stock_request->store->tracking_id }})
                                                <br>
                                                @if($stock_request->store->type == \App\Models\StoreManager\Store::PREMIUM_TYPE)
                                                    <span class="badge badge-success">Premium</span>
                                                @elseif($stock_request->store->type == \App\Models\StoreManager\Store::PRO_TYPE)
                                                    <span class="badge badge-info">Pro</span>
                                                @elseif($stock_request->store->type == \App\Models\StoreManager\Store::STANDARD_TYPE)
                                                    <span class="badge badge-secondary">Standard</span>
                                                @else
                                                    <span class="badge badge-warning">Basic</span>
                                                @endif
                                            </td>
                                            <td>{{ count($stock_request->details) }} Items</td>
                                            <td>{{ number_format($stock_request->amount, 2) }}</td>
                                            <td>
                                                @if($stock_request->status == 1)
                                                    <span class="badge badge-warning">Pending</span>
                                                @elseif($stock_request->status == 2)
                                                    <span class="badge badge-success">Approved</span>
                                                @else
                                                    <span class="badge badge-danger">Rejected</span>
                                                @endif
                                            </td>
                                            <td>

                                                <a href="{{ route('store-stock-request-received-update', ['id' => $stock_request->id ]) }}" class="btn btn-danger btn-sm"> Details</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $stock_requests->appends(['dateRange' => Request::get('dateRange')])->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop