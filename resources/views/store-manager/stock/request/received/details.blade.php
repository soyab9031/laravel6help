@extends('store-manager.template.layout')

@section('title', 'Stock Request Details # ' . $stock_request->id)

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Stock Request Details # {{ $stock_request->id }}</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Stock Request ID:</b> <span class="pull-right">{{ $stock_request->id }}</span>
                            </li>
                            <li class="list-group-item">
                                Date: <span class="pull-right">{{ $stock_request->created_at->format('M d, Y h:i A') }}</span>
                            </li>
                            <li class="list-group-item">
                                Message: <span class="pull-right">{{ $stock_request->remarks }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Request From:</b> <span class="pull-right">{{ $stock_request->store->name }}<br>
                                ({{ $stock_request->store->tracking_id }})
                                </span>
                            </li>
                            <li class="list-group-item">
                                Amount : <span class="pull-right">{{ number_format($stock_request->amount,2) }}</span>
                            </li>
                            <li class="list-group-item">
                                Discount : <span class="pull-right">{{ number_format($stock_request->discount,2) }}</span>
                            </li>
                            <li class="list-group-item">
                                Total : <span class="pull-right">{{ number_format($stock_request->amount - $stock_request->discount,2) }}</span>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        @if($stock_request->status == 1)
                            <form action="" id="updateStockRequest" method="post">
                                {{ csrf_field() }}
                                <label for="">Select Status</label>
                                <select name="status" id="" class="form-control">
                                    <option value="1">Pending</option>
                                    <option value="2">Approve</option>
                                    <option value="3">Reject</option>
                                </select>
                                <fieldset class="mt-1">
                                    <label for="">Remarks</label><br>
                                    <textarea name="remarks" id="" cols="40" rows="5"></textarea>
                                </fieldset>
                                <div class="text-center">
                                    <button class="btn btn-md btn-danger" type="button" onclick="INGENIOUS.formConfirmation('#updateStockRequest', 'Are you sure to update stock request')">Update</button>
                                </div>
                            </form>
                        @else
                            <a href="{{ route('store-stock-request-received-view') }}" class="btn btn-md btn-primary">Back</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>BV</th>
                                <th>Qty</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stock_request->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->distributor_price  }}</td>
                                    <td>{{ $detail->product_price->points }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td class="text-right"> {{ number_format($detail->distributor_price, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger bg-darken-1">
                                <td colspan="4" class="text-right">Total</td>
                                <td>{{ collect($stock_request->details)->sum('qty') }}</td>
                                <td class="text-right">{{ number_format(collect($stock_request->details)->sum('selling_price'), 2) }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop