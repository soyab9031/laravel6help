<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('store-dashboard') }}"><i class="ft-home"></i><span>Home</span></a>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-layers"></i><span>Orders</span></a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li data-menu=""><a class="dropdown-item text-danger" href="{{ route('store-order-create') }}" data-toggle="dropdown">{{ config('project.brand') }} - POS</a></li>
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-order-view') }}" data-toggle="dropdown">View All</a></li>
                    </div>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-list"></i><span>Stock</span></a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-stock-view') }}" data-toggle="dropdown">View</a></li>
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-stock-supply-view') }}" data-toggle="dropdown">Supply History</a></li>
                    </div>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-fast-forward"></i><span>Stock Request</span></a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-stock-request-history') }}" data-toggle="dropdown">View</a></li>
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-stock-request-create') }}" data-toggle="dropdown">Create</a></li>
                    </div>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-wallet"></i><span>My Fund</span></a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-wallet-view') }}" data-toggle="dropdown">Transactions</a></li>
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-wallet-request-view') }}" data-toggle="dropdown">Request to Company</a></li>
                    </div>
                </ul>
            </li>
            @if(in_array(Session::get('store')->type, [\App\Models\StoreManager\Store::PREMIUM_TYPE, \App\Models\StoreManager\Store::PRO_TYPE]))
                <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-box"></i><span>Supply to Store</span></a>
                    <ul class="dropdown-menu">
                        <div class="arrow_box">
                            <li data-menu=""><a class="dropdown-item" href="{{ route('store-stock-request-received-view') }}" data-toggle="dropdown">Received Stock Request</a></li>
                            <li data-menu=""><a class="dropdown-item" href="{{ route('store-supply-view') }}" data-toggle="dropdown">History</a></li>
                            <li data-menu=""><a class="dropdown-item" href="{{ route('store-supply-create') }}" data-toggle="dropdown">Do Supply</a></li>
                        </div>
                    </ul>
                </li>
            @endif
            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-chart-pie"></i><span>Reports</span></a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-report-sales') }}" data-toggle="dropdown">Sales</a></li>
                        @if(Session::get('store')->type != \App\Models\StoreManager\Store::BASIC_TYPE)
                            <li data-menu=""><a class="dropdown-item" href="{{ route('store-report-supply-transfer') }}" data-toggle="dropdown">Supply Report</a></li>
                        @endif
                        <li data-menu=""><a class="dropdown-item" href="{{ route('store-report-product-ranking') }}" data-toggle="dropdown">Product Ranking</a></li>
                    </div>
                </ul>
            </li>

        </ul>
    </div>
</div>