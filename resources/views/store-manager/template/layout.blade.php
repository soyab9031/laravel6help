<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="{{ config('project.brand') }} - Store Manager">
    <meta name="author" content="Tymk Softwares">
    <title>@yield('title') - {{ config('project.company') }} - Store Manager</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/pace.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/palette-gradient.min.css">
    <link href="/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/store-assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/style.css?V=0.2">
    @yield('import-css')
    @yield('page-css')
</head>
<body class="horizontal-layout horizontal-menu 2-columns menu-expanded" data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-red-pink" data-col="2-columns">
@include('store-manager.template.header')
@include('store-manager.template.horizontal-menu')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        @yield('content')
    </div>
</div>
@include('store-manager.template.footer')
<script src="/store-assets/js/vendors.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/store-assets/js/jquery.sticky.js"></script>
<script src="/store-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/store-assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/moment/moment.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/plugins/select2/js/select2.full.min.js"></script>
<script src="/store-assets/js/lodash.js"></script>
<script src="/store-assets/js/app.min.js" type="text/javascript"></script>
<script src="/store-assets/plugins/axios/axios.min.js"></script>
<script src="/store-assets/plugins/vue/{{ env('APP_ENV') == 'local' ? 'vue.js' : 'vue.min.js' }}"></script>
<script src="/store-assets/plugins/vue/vue-helper.js"></script>
@yield('import-javascript')
@yield('page-javascript')
<script>
    @if(session('errors'))
    toastr.error('{{ session('errors')->first() }}', "Error");
    @elseif(session('error'))
    toastr.error('{{ session('error') }}', "Oops..!!");
    @elseif(session('success'))
    toastr.success('{{ session('success') }}', "Hurray..!!");
    @endif
</script>
</body>
</html>