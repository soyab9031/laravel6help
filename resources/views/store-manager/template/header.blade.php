<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item">
                <a class="navbar-brand" href="{{ route('store-dashboard') }}">
                    <img class="brand-logo" alt="{{ config('project.brand') }}" src="/user-assets/images/company/logo.png" >
                </a>
            </li>
            <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
        </ul>
    </div>
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li>
                        <h3>{{ Session::get('store')->name }} ({{ Session::get('store')->tracking_id }})
                            @if(Session::get('store')->type == \App\Models\StoreManager\Store::PREMIUM_TYPE)
                                <span class="badge badge-success">Premium</span>
                            @elseif(Session::get('store')->type == \App\Models\StoreManager\Store::PRO_TYPE)
                                <span class="badge badge-info">Pro</span>
                            @elseif(Session::get('store')->type == \App\Models\StoreManager\Store::STANDARD_TYPE)
                                <span class="badge badge-secondary">Standard</span>
                            @else
                                <span class="badge badge-warning">Basic</span>
                            @endif
                        </h3>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="avatar avatar-online"><img src="/store-assets/images/icons/shop.svg" alt="{{ config('project.brand') }}"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="arrow_box_right">
                                <a class="dropdown-item" href="#">
                                    <span class="avatar avatar-online"><span class="user-name text-bold-700 ml-1">{{ Session::get('store')->tracking_id }}</span></span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('store-profile') }}"><i class="ft-user"></i> Edit Profile</a>
                                <a class="dropdown-item" href="{{ route('store-change-password') }}"><i class="ft-lock"></i> Change Password</a>
                                <a class="dropdown-item" href="{{ route('store-logout') }}"><i class="ft-power"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</nav>