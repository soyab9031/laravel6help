@extends('store-manager.template.layout')

@section('title', 'Store Product Ranking Report')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Store Product Ranking Report</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-primary"> Search </button>
                                        <a href="{{ route('store-report-product-ranking', ['download' => 'yes', 'dateRange' => Request::get('dateRange')]) }}" class="btn btn-dark" title="Download"><i class="la la-download"></i> <span class="bold">Download</span></a>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Product Code</th>
                                        <th class="bg-danger bg-darken-1 white">Item Sold</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($items) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center">No Product Ranking Available</td>
                                        </tr>
                                    @endif
                                    @foreach ($items as $index => $item)
                                        <tr>
                                            <td>{{ ($index+1) }}</td>
                                            <td>{{ $item->product->name }}</td>
                                            <td>{{ $item->code }}</td>
                                            <td class="bg-danger bg-lighten-4">{{ number_format($item->sold_qty) }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="3" class="text-right">Total</td>
                                        <td class="bg-danger bg-darken-1 white">{{ number_format(collect($items)->sum('sold_qty')) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
