@extends('store-manager.template.layout')

@section('title', 'Supply Details # ' . $supply->id)

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Supply Details # {{ $supply->id }}</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Store:</b>
                                <span class="pull-right text-primary">{{ $supply->store->name }} <br> ({{ $supply->store->tracking_id }})</span>
                            </li>
                            <li class="list-group-item">
                                <b>Supply ID:</b> <span class="pull-right">{{ $supply->id }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Date:</b> <span class="pull-right">{{ $supply->created_at->format('M d, Y h:i A') }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Delivery Method:</b> <span class="pull-right">{{ $supply->delivery_type_name }}</span>
                            </li>
                            @if($supply->delivery_type <> 1)
                                <li class="list-group-item">
                                    <b>Transport/ Courier:</b> <span class="pull-right">{{ $supply->courier_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Docket Number:</b> <span class="pull-right">{{ $supply->courier_docket_number }}</span>
                                </li>
                            @endif
                        </ul>

                        <div class="text-center mt-2">
                            <a href="{{ route('store-stock-supply-delivery-note', ['id' => $supply->id ]) }}" class="btn btn-primary "> Delivery Note <i class="la la-download"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" id="details">
                    <div class="card-body">
                        @if($supply->status == 1)
                            <form method="post" onsubmit="INGENIOUS.blockUI(true)">
                                {{ csrf_field() }}
                                <div class="form-group form-group-default form-group-default-select">
                                    <label>Select Delivery Type</label>
                                    <select class="form-control" id="delivery_type"  name="delivery_type">
                                        <option value="1">Self Pickup</option>
                                        <option value="2">Courier</option>
                                        <option value="3">Transport</option>
                                    </select>
                                </div>
                                <div class="d-none" id="Courier" >
                                    <div class="form-group form-group-default" >
                                        <label>Courier or Transport Company</label>
                                        <input type="text" v-model="courier_name" class="form-control" name="courier_name">
                                    </div>
                                    <div class="form-group form-group-default" >
                                        <label>Courier or Transport Docket No.</label>
                                        <input type="text" v-model="courier_docket_number" class="form-control" name="courier_docket_number">
                                    </div>
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Your Message or Remarks</label>
                                    <textarea class="form-control" cols="30" rows="6" placeholder="Your Message" v-model="remarks" name="remarks"></textarea>
                                </div>
                                <div class="form-group form-group-default form-group-default-select">
                                    <label>Select Status Type</label>
                                    <select class="form-control" name="status">
                                        <option value="1" >Pending</option>
                                        <option value="2" >Delivered</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-default">
                                    <div class="text-center">
                                        <button class="btn btn-danger"> Update </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div class="form-group form-group-default">
                                <div class="text-center">
                                    <a href="{{ route('store-supply-view') }}" class="btn btn-danger"> Back </a>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>Qty</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($supply->details as $index => $detail)
                                @php
                                    $stock_transaction = collect($supply->stockTransactions)->where('product_price_id', $detail->product_price_id)->first()
                                @endphp
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->distributor_price }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger bg-darken-1">
                                <td colspan="3" class="text-right">Total</td>
                                <td>{{ collect($supply->details)->sum('qty') }}</td>
                                <td class="text-right">{{ number_format(collect($supply->details)->sum('total_amount'), 2) }}</td>
                            </tr>
                            <tr class="text-danger">
                                <td colspan="3"></td>
                                <td>Discount</td>
                                <td> - {{number_format($supply->total_supply_discount,2)}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-white bg-primary">Total</td>
                                <td class="text-white bg-primary">₹ {{$supply->total}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>
        $('#delivery_type').click(function () {

            var id = $('#delivery_type').val();

            if (id > 1){
                $('#Courier').removeClass('d-none');
            }else{
                $('#Courier').addClass('d-none');
            }

        });
    </script>
@stop