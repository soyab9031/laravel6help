<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Store Manager - {{ config('project.company') }}">
    <meta name="author" content="TymkSoftwares">
    <title>Registration Request is submitted</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/pace.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/style.css">
</head>
<body class="horizontal-layout horizontal-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
</body>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="text-center mb-1">
                                    <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" width="200px">
                                </div>
                                @if(session('success'))
                                    <div class="alert alert-success">{{ session('success') }}</div>
                                @endif
                                <h4 class="text-center">
                                    Your Store Request has been submitted successfully, We will contact you soon with same. Thanks
                                </h4>
                            </div>
                            <div class="card-content">

                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            Store Name: <span class="pull-right">{{ $store_request->name }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            City: <span class="pull-right">{{ $store_request->city }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            Account Password: <span class="pull-right">{{ $store_request->password }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</html>