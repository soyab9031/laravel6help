<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Store Manager - {{ config('project.company') }}">
    <meta name="author" content="TymkSoftwares">
    <title>Register to Store Manager Account</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/pace.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" type="text/css" href="/store-assets/css/style.css">
</head>
<body class="horizontal-layout horizontal-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page"
      data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-body">
            <div class="pt-2 pb-3">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-8 col-12 box-shadow-3 p-0">
                        <div class="card border-grey border-lighten-1 px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="text-center mb-1">
                                    <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}"
                                         width="100px">
                                </div>
                                <div class="font-medium-4 text-center">
                                    Store Manager Account
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form-horizontal" action="" method="post"
                                          onsubmit="INGENIOUS.blockUI(true)">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6 offset-md-3">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input type="text" name="sponsor_user_id" class="form-control"
                                                           value="{{ old('sponsor_user_id') }}"
                                                           placeholder="Enter sponsor Id">
                                                    <div class="form-control-position">
                                                        <i class="ft-user"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <select name="type" class="form-control">
                                                        <option value="">Select Franchisee Type</option>
                                                        @foreach(\App\Models\StoreManager\Store::getStoreTypes() as $store_type)
                                                            <option value="{{ $store_type->id }}"
                                                                    {{ old('type') == $store_type->id ? 'selected' : null }}>
                                                                {{ $store_type->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" name="store_name"
                                                           value="{{ old('store_name') }}" required
                                                           placeholder="Enter Store Name">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" name="owner_name"
                                                           value="{{ old('owner_name') }}" required
                                                           placeholder="Enter Owner Name">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input type="text" name="mobile" class="form-control"
                                                           value="{{ old('mobile') }}" required
                                                           placeholder="Enter Mobile Number">
                                                    <div class="form-control-position">
                                                        <i class="ft-phone"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input type="text" class="form-control" name="email"
                                                           value="{{ old('email') }}" required
                                                           placeholder="Enter Email">
                                                    <div class="form-control-position">
                                                        <i class="ft-mail"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" name="gst_number"
                                                           value="{{ old('gst_number') }}"
                                                           placeholder="Enter GST Number">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <textarea name="address" class="form-control" rows="5"
                                                              autocomplete="off" required
                                                              placeholder="Enter Store Address">{{ old('address') }}</textarea>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <input type="text" name="city" class="form-control"
                                                                   autocomplete="off" value="{{ old('city') }}"
                                                                   placeholder="Enter City" required>
                                                        </fieldset>
                                                        <fieldset class="form-group">
                                                            <select name="state_id" class="form-control">
                                                                <option value="" selected disabled="">Select State
                                                                </option>
                                                                @foreach($states as $state)
                                                                    <option value="{{ $state->id }}" {{ old('state_id') == $state->id  ? 'selected' : '' }}>{{ $state->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <input type="text" name="district" class="form-control"
                                                                   autocomplete="off" value="{{ old('district') }}"
                                                                   placeholder="Enter District" required>
                                                        </fieldset>
                                                        <fieldset class="form-group">
                                                            <input type="text" name="pincode" class="form-control"
                                                                   autocomplete="off" value="{{ old('pincode') }}"
                                                                   placeholder="Enter Pin Code" required>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group mb-3 position-relative has-icon-left">
                                                    <input type="password" class="form-control passwordInput"
                                                           name="password" placeholder="Enter Password">
                                                    <div class="form-control-position"><i class="ft-lock"></i></div>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="button"
                                                                onclick="INGENIOUS.showPassword(this)">
                                                            <i class="la la-eye-slash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit"
                                                    class="btn btn-block btn-bg-gradient-x-orange-yellow col-12 mr-1 mb-1">
                                                Send Registration Request
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/store-assets/js/vendors.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/store-assets/js/jquery.sticky.js"></script>
<script src="/store-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/store-assets/js/app.min.js" type="text/javascript"></script>
<script src="/store-assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script>
    @if(session('errors'))
    toastr.error('{{ session('errors')->first() }}', "Error");
    @elseif(session('error'))
    toastr.error('{{ session('error') }}', "Oops..!!");
    @elseif(session('success'))
    toastr.success('{{ session('success') }}', "Hurray..!!");
    @endif
</script>
</body>
</html>