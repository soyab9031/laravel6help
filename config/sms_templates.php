<?php

return [
    /*
   |--------------------------------------------------------------------------
   | SMS Templates
   |--------------------------------------------------------------------------
   |
   */
    'registration' => [
        'id' => '',
        'message' => 'Welcome to Ingenious, Your Username: {username} & Password- {password} & Wallet Password: {wallet_password}. Visit our website: https://websitename.in/'
    ],
    'forget_password' => [
        'id' => '',
        'message' => 'Dear User, Your New password {password} for Your Tracking Id {tracking_id} at Ingenious. Kindly Change it after login.'
    ]
];



