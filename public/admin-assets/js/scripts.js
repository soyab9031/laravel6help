var INGENIOUS = {};

(function($) {
        'use strict';
        $(document).ready(function() {
            $(".list-view-wrapper").scrollbar();
            $('[data-pages="search"]').search({
                searchField: '#overlay-search',
                closeButton: '.overlay-close',
                suggestions: '#overlay-suggestions',
                brand: '.brand',
                onSearchSubmit: function(searchString) {
                    window.location='/admin-panel/user?search='+searchString;
                },
                onKeyEnter: function(searchString) {

                    var searchField = $('#overlay-search');
                    var searchResults = $('.search-results');
                    clearTimeout($.data(this, 'timer'));
                    searchResults.fadeOut("fast");

                    // console.log(searchString);

                    var wait = setTimeout(function() {
                        searchResults.find('.result-name').each(function() {
                            if (searchField.val().length != 0) {
                                $(this).html(searchField.val());
                                searchResults.fadeIn("fast");
                            }
                        });
                    }, 500);

                    $(this).data('timer', wait);
                }
            })
        });
        $('.panel-collapse label').on('click', function(e) {
            e.stopPropagation();
        });
        feather.replace({
            'width': 16,
            'height': 16
        })
    }
)(window.jQuery);


$('.date-range').daterangepicker({
    locale: {
        format: 'DD MMM YYYY'
    },
    autoUpdateInput: false,
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-success',
    cancelClass: 'btn-danger',
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}).on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
}).on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

INGENIOUS.showPassword = function (element) {

    var passwordElement = $(element).parent().parent().find('input.passwordInput');

    var inputType = passwordElement.attr('type');

    if (inputType === 'password') {
        $(element).html('<i class="fa fa-eye"></i>');
        passwordElement.attr('type', 'text');
    }
    else {
        $(element).html('<i class="fa fa-eye-slash"></i>');
        passwordElement.attr('type', 'password');
    }

};

INGENIOUS.copyToClipboard = function (element, message) {

    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();

    INGENIOUS.showNotification(message, 'info', 2000);
};

INGENIOUS.showNotification = function (message, type, timeout=0) {

    $('.page-content-wrapper').pgNotification({
        style: 'bar',
        message: message,
        position: 'top',
        timeout: timeout,
        type: type
    }).show();
};

INGENIOUS.getUser = function (element, input_class) {


    var btn_element = $(element);
    var tracking_id = null;
    var input_element = $('.' + input_class);

    btn_element.closest('form');

    tracking_id = input_element.val();

    if (tracking_id.length < 6) {

        INGENIOUS.showNotification('Invalid Tracking ID, Try Again', 'danger', 2000);
        $('.user-get-response').remove();
        input_element.val('');
        return false;
    }

    $.get('/admin-panel/user/get', {'tracking_id': tracking_id }, function(response) {

        $('.user-get-response').remove();

        if(response.status) {

            btn_element.parent().parent().after('' +
                '<div class="user-get-response text-center m-b-10">' +
                '<input type="hidden" name="user_id" value="'+response.user.id+'">' +
                '<span class="label label-danger">'+response.user.name+'</span>'+
                '</div>');
        }
        else {

            INGENIOUS.showNotification(response.message, 'danger', 2000);
            input_element.val('');
        }
    });

};

INGENIOUS.formConfirmation = function (formId, message = '') {

    swal({
        title: "Are you sure?",
        text: message,
        icon: "info",
        buttons: true,
        dangerMode: true
    }).then( function (isConfirm) {

        if (isConfirm) {
            INGENIOUS.blockUI(true);
            $(formId).submit();
        }
        else {
            self.attr('disabled', false);
        }

    });

};

INGENIOUS.blockUI = function (show=true) {

    if (!show) {
        $.unblockUI();
        return false
    }

    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    };

    $.blockUI({
        css: {
            backgroundColor: 'transparent',
            border: 'none'
        },
        message: '<div class="ing-spinner"></div>',
        baseZ: 1500,
        overlayCSS: {
            backgroundColor: '#FFFFFF',
            opacity: 0.7,
            cursor: 'wait'
        }
    });

    $('.blockUI.blockMsg').center();

};

INGENIOUS.numericInput = function (evt) {

    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }

};