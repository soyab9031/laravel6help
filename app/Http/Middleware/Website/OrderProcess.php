<?php

namespace App\Http\Middleware\Website;

use Closure;

class OrderProcess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_route = \Route::getCurrentRoute()->getName();

        if(!\Session::has('user'))
            return redirect()->route('user-login', ['redirect_route' => $current_route]);

        if(!\Session::has('order-checkout'))
            return redirect()->route('website-home')->with(['error' => 'Cart Session is expired, Try Again']);

        return $next($request);
    }
}
