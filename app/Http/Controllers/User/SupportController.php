<?php

namespace App\Http\Controllers\User;

use App\Library\ImageUpload;
use App\Models\News;
use App\Models\Support;
use App\Models\SupportCategory;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SupportController extends Controller
{

    public function index(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'category_id' => 'required',
                'image' => 'image',
                'message' => 'required|min:10'
            ]);

            $image_name = null;
            if($request->hasFile('image') && $request->file('image')->isValid())
            {
                $image = (new ImageUpload());
                if($image->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                {
                    $image_name = $image->store(env('SUPPORT_IMAGE_PATH'));
                }

            }

            $support = Support::create([
                'category_id' => $request->category_id,
                'message' => $request->message,
                'image' => $image_name,
                'user_id' => \Session::get('user')->id,
                'type' => Support::USER_TO_ADMIN,
            ]);

            $support->ticket_number = 'S' . now()->format('ymd') . $support->id;
            $support->save();

            $support->makeRoot();

            return redirect()->route('user-support-chat', ['id' => $support->id])->with(['success' => 'Support Ticket has been opened']);
        }

        $supports = Support::whereUserId(\Session::get('user')->id)->with(['admin', 'category'])->whereNull('parent_id')->paginate(20);

        return view('user.support.index', [
            'categories' => SupportCategory::active()->get(),
            'supports' => $supports
        ]);
    }

    public function chat(Request $request)
    {
        if (!$support = Support::with(['category'])->whereUserId(\Session::get('user')->id)->whereId($request->id)->whereNull('parent_id')->first())
            return redirect()->back()->with(['error' => 'Invalid Data for Support Chat']);

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'message' => 'required|min:1'
            ]);

            $image_name = null;
            if($request->hasFile('image') && $request->file('image')->isValid())
            {
                $image = (new ImageUpload());
                if($image->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                {
                    $image_name = $image->store(env('SUPPORT_IMAGE_PATH'));
                }

            }

            $support->children()->create([
                'category_id' => $support->category_id,
                'user_id' => $support->user_id,
                'message' => $request->message,
                'image' => $image_name,
                'type' => Support::USER_TO_ADMIN
            ]);

            return redirect()->route('user-support-chat', ['id' => $support->id]);

        }

        return view('user.support.chat', [
            'support' => $support,
            'details' => $support->descendantsAndSelf()->with(['admin'])->get()
        ]);
    }
}
