<?php

namespace App\Http\Controllers\User;

use App\Library\Calculation\Binary\Helper;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class RegisterController extends Controller
{

    public function index(Request $request)
    {
        \Session::forget('user_registration');

        $sponsorUser = null;
        $leg = null;

        if (isset($request->referral))
        {
            if (!$sponsorUser = User::whereTrackingId($request->referral)->first()) {
                return redirect()->route('user-register')->with(['error' => 'Invalid Referral Upline Data']);
            }

            if (isset($request->leg)) {

                $leg = $request->leg;

                if (!in_array($leg, ['L', 'R'])) {
                    return redirect()->route('user-register')->with(['error' => 'Invalid Position Under Parent']);
                }
            }

        }

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'tracking_id' => 'required|exists:users,tracking_id',
            ], [
                'tracking_id.required' => 'Tracking Id is Required',
                'tracking_id.exists' => 'Invalid Tracking Id',
            ]);

            $sponsorUser = User::whereTrackingId($request->tracking_id)->first();

            \Session::put('user_registration', [
                [
                    'sponsor_user' => $sponsorUser,
                    'selected_leg' => $leg,
                ]
            ]);

            return redirect()->route('user-register-details');
        }

        return view('user.authentication.register.index', [
            'sponsor_upline' => $sponsorUser,
        ]);
    }

    public function details(Request $request)
    {
        if(!\Session::has('user_registration'))
            return redirect()->route('user-register')->with(['error' => 'Not able to retrieve the User Details, Try again']);

        $session_details = (object) (\Arr::collapse(\Session::get('user_registration')));

        $nominees = \File::get(public_path('data/nominees.json'));
        $states = State::active()->get();

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'leg' => 'required',
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birth_date' => 'required',
                'email' => 'required|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'address' => 'required',
                'city' => 'required',
                'district' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|digits:6',
                'username' => 'required|unique:users,username',
                'password' => 'required|min:6',
                'terms' => 'required',
                'aadhaar_number' =>'required|regex:/^[0-9]{12}?$/'
            ], [
                'leg.required' => 'Kindly Select your Placement under Upline',
                'title.required' => 'Title is Required',
                'first_name.required' => 'First Name is Required',
                'last_name.required' => 'Last Name is Required',
                'birth_date.required' => 'Date of Birth is Required',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'district.required' => 'District is Required',
                'state_id.required' => 'State is Required',
                'pincode.required' => 'Pincode is Required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
                'username.required' => 'Username is Required',
                'username.unique' => 'This Username is already exist',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'terms.required' => 'Terms & Conditions is Required',
                'aadhaar_number.required' => 'Aadhaar Number is Required',
                'aadhaar_number.regex' => 'Aadhaar Number is Invalid'
            ]);

            \Session::push('user_registration', $request->all());

            return redirect()->route('user-register-overview');
        }

        return view('user.authentication.register.details', [
            'sponsor_user' => $session_details->sponsor_user,
            'nominee_relation' => json_decode($nominees),
            'states' => $states,
            'selected_leg' => $session_details->selected_leg
        ]);
    }

    public function overview(Request $request)
    {
        if(!\Session::has('user_registration'))
            return redirect()->route('user-register')->with(['error' => 'Not able to retrieve the User Details, Try again']);

        $user_detail = (object) (\Arr::collapse(\Session::get('user_registration')));

        if ($request->isMethod('post'))
        {
            $response = (new Helper())->findTheUpline($user_detail->sponsor_user, $user_detail->leg);

            if ($response->status == false) {

                return redirect()->route('user-register')->with([
                    'error' => 'We can not place this user to Leg: ' . $user_detail->leg . ' Try Again'
                ]);
            }


            $user = \DB::transaction(function () use($user_detail, $response) {

                $parent = $response->upline;

                $tracking_id = rand(1000000, 9999999);

                while(User::whereTrackingId($tracking_id)->exists()) {
                    $tracking_id = rand(1000000, 9999999);
                }

                $user = User::create([
                    'parent_id' => $parent->id,
                    'sponsor_by' => $user_detail->sponsor_user->id,
                    'tracking_id' => $tracking_id,
                    'username' => $user_detail->username,
                    'password' => $user_detail->password,
                    'wallet_password' => strtoupper(\Str::random(6)),
                    'leg' => $user_detail->leg,
                    'email' => $user_detail->email,
                    'mobile' => $user_detail->mobile,
                    'token' => strtoupper(\Str::random(15))
                ]);

                UserDetail::create([
                    'user_id' => $user->id,
                    'title' => $user_detail->title,
                    'first_name' => $user_detail->first_name,
                    'last_name' => $user_detail->last_name,
                    'gender' => in_array($user_detail->title, ['Mr', 'Sri', 'Dr']) ? 1 : 2,
                    'birth_date' => Carbon::parse($user_detail->birth_date),
                    'aadhaar_number' => $user_detail->aadhaar_number
                ]);

                UserStatus::create([
                    'user_id' => $user->id,
                ]);

                UserBank::create([
                    'user_id' => $user->id
                ]);

                UserAddress::create([
                    'user_id' => $user->id,
                    'address' => $user_detail->address,
                    'city' => $user_detail->city,
                    'district' => $user_detail->district,
                    'state_id' => $user_detail->state_id,
                    'pincode' => $user_detail->pincode
                ]);

                return $user;

            });

            \Session::put('registered_user', $user);

            return redirect()->route('user-register-thanks')->with([
                'success' => 'Your Registration has been successfully completed!',
            ]);
        }


        return view('user.authentication.register.overview', [
            'session_detail' => $user_detail
        ]);
    }

    public function thanks()
    {
        if (!\Session::has('user_registration') || !\Session::has('registered_user')) {
            return redirect()->route('user-register');
        }

        $user = User::with('detail')->whereId(\Session::get('registered_user')->id)->first();

        \Session::forget('user_registration');
        \Session::forget('registered_user');

        return view('user.authentication.register.thanks', [
            'user' => $user
        ]);
    }
}
