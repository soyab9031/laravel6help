<?php

namespace App\Http\Controllers\User;

use App\Library\ImageUpload;
use App\Models\CompanyDocument;
use App\Models\State;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDocument;
use App\Models\UserStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KycController extends Controller
{
    public function dashboard()
    {
        $user_status = UserStatus::whereUserId(\Session::get('user')->id)->first();

        return view('user.kyc.dashboard', [
            'user_status' => $user_status
        ]);
    }

    public function pancard(Request $request)
    {
        $user_status = UserStatus::whereUserId(\Session::get('user')->id)->first();

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'pan_number' => 'required|regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
            ], [
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload File is Required',
                'pan_number.regex' => 'Invalid Pancard Number',
                'pan_number.required' => 'Pan Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            UserDocument::create([
                'user_id' => \Session::get('user')->id,
                'image' => $image_name,
                'type' => UserDocument::PAN_CARD,
                'number' => $request->pan_number,
                'status' => UserDocument::PENDING
            ]);

            $user_status->update(['pancard' => UserStatus::KYC_INPROGRESS]);

            return redirect()->route('user-kyc-dashboard')->with(['success' => 'You Pan card details are submitted, It will be reviewed soon']);
        }

        $documents = UserDocument::whereType(UserDocument::PAN_CARD)->latest()->whereUserId(\Session::get('user')->id)->get();

        return view('user.kyc.pancard', [
            'user_status' => $user_status,
            'documents' => $documents
        ]);
    }

    public function bank(Request $request)
    {
        $user_status = UserStatus::whereUserId(\Session::get('user')->id)->first();
        $user_bank = UserBank::whereUserId(\Session::get('user')->id)->first();

        $allow_update = in_array($user_status->bank_account, [UserStatus::KYC_PENDING, UserStatus::KYC_REJECTED]);

        if ($request->isMethod('post')) {
            if ($allow_update == false) {
                return redirect()->back()->with(['error' => 'Your Bank KYC Details is Under Reviewed, Will Update Soon']);
            }

            $this->validate($request, [
                'type' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'bank_name' => 'required',
                'account_name' => 'required|min:3',
                'account_number' => 'required|numeric',
                'branch' => 'required|min:3',
                'ifsc' => 'required|min:3',
                'account_type' => 'required|numeric',
                'city' => 'required|min:3'
            ], [
                'type.required' => 'Bank Document Type is required',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload Cancel Cheque Cheque or Bank Passbook is Required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            UserDocument::create([
                'user_id' => \Session::get('user')->id,
                'image' => $image_name,
                'type' => $request->type,
                'number' => $request->account_number,
                'status' => UserDocument::PENDING
            ]);

            $user_bank->account_name = $request->account_name;
            $user_bank->bank_name = $request->bank_name;
            $user_bank->account_number = $request->account_number;
            $user_bank->branch = $request->branch;
            $user_bank->ifsc = $request->ifsc;
            $user_bank->type = $request->account_type;
            $user_bank->city = $request->city;
            $user_bank->save();

            $user_status->update(['bank_account' => UserStatus::KYC_INPROGRESS]);

            return redirect()->route('user-kyc-dashboard')->with(['success' => 'You Bank details are submitted, It will be reviewed soon']);
        }

        $documents = UserDocument::whereIn('type', [UserDocument::CANCEL_CHEQUE, UserDocument::BANK_PASSBOOK])->latest()->whereUserId(\Session::get('user')->id)->get();

        return view('user.kyc.bank', [
            'allow_update' => $allow_update,
            'user_bank' => $user_bank,
            'user_status' => $user_status,
            'documents' => $documents
        ]);
    }

    public function address(Request $request)
    {
        $user_status = UserStatus::whereUserId(\Session::get('user')->id)->first();
        $address = UserAddress::whereUserId(\Session::get('user')->id)->oldest()->first();

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'type' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'secondary_image' => 'nullable|mimes:jpeg,jpg,png|max:4000|required_if:type, ' . UserDocument::VOTER_ID . ',' . UserDocument::PASSPORT,
                'address' => 'required',
                'landmark' => 'nullable|min:3',
                'city' => 'required|min:3',
                'district' => 'required|min:3',
                'state_id' => 'required|numeric',
                'pincode' => 'required|numeric|max:6',
            ], [
                'type.required' => 'Select Address Proof Type',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'state_id.required' => 'State is required',
                'secondary_image.required_if' => 'Secondary or Back image is required for Passport & Voter ID'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            $secondary_image_name = null;
            if ($request->hasFile('secondary_image')) {
                $image_uploader = (new ImageUpload());
                if (!$image_uploader->process($request->file('secondary_image'), ImageUpload::DOCUMENT_TYPE))
                    return redirect()->back()->with(['error' => 'Unable to fetch Secondary Image, Upload another image']);

                $secondary_image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));
            }

            UserDocument::create([
                'user_id' => \Session::get('user')->id,
                'image' => $image_name,
                'secondary_image' => $secondary_image_name,
                'type' => $request->type,
                'number' => $request->reference_number,
                'status' => UserDocument::PENDING
            ]);

            $address->address = $request->address;
            $address->landmark = $request->landmark;
            $address->city = $request->city;
            $address->district = $request->district;
            $address->state_id = $request->state_id;
            $address->pincode = $request->pincode;
            $address->save();

            $user_status->update(['address' => UserStatus::KYC_INPROGRESS]);

            return redirect()->route('user-kyc-dashboard')->with(['success' => 'You Address details are submitted, It will be reviewed soon']);
        }

        $documents = UserDocument::whereIn('type', [UserDocument::VOTER_ID, UserDocument::ELECTRICITY_BILL, UserDocument::PASSPORT, UserDocument::DRIVING_LICENCE])->latest()->whereUserId(\Session::get('user')->id)->get();

        return view('user.kyc.address', [
            'states' => $states = State::active()->get(),
            'address' => $address,
            'user_status' => $user_status,
            'documents' => $documents
        ]);
    }

    public function aadhaarNumber(Request $request)
    {
        $user_status = UserStatus::whereUserId(\Session::get('user')->id)->first();

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'secondary_image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'aadhar_number' => 'required|regex:/^[0-9]{12}?$/',
            ], [
                'image.image' => 'Invalid Front image',
                'image.max' => 'Front Image Size should be less than 4MB',
                'secondary_image.image' => 'Invalid Back image',
                'secondary_image.max' => 'Back Image Size should be less than 4MB',
                'image.required' => 'Aadhar Card Front Image is Required',
                'secondary_image.required' => 'Aadhar Card Back image is Required',
                'aadhar_number.regex' => 'Invalid Aadhar Number',
                'aadhar_number.required' => 'Aadhar Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Front Image, Try Another']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            if (!$image_uploader->process($request->file('secondary_image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Back Image, Try Another']);

            $secondary_image = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));


            UserDocument::create([
                'user_id' => \Session::get('user')->id,
                'image' => $image_name,
                'secondary_image' => $secondary_image,
                'type' => UserDocument::AADHAR_CARD,
                'number' => $request->aadhar_number,
                'status' => UserDocument::PENDING
            ]);

            $user_status->update(['aadhar_card' => UserStatus::KYC_INPROGRESS]);

            return redirect()->route('user-kyc-dashboard')->with(['success' => 'You Aadhar card details are submitted, It will be reviewed soon']);
        }

        $documents = UserDocument::whereType(UserDocument::AADHAR_CARD)->latest()->whereUserId(\Session::get('user')->id)->get();

        return view('user.kyc.aadhar-number', [
            'user_status' => $user_status,
            'documents' => $documents
        ]);
    }

    public function viewCompanyDocuments()
    {

        $companyDocuments = CompanyDocument::active()->orderBy('id','desc')->paginate(50);

        return view('user.downloads-documents',[
            'companyDocuments' => $companyDocuments
        ]);
    }
}
