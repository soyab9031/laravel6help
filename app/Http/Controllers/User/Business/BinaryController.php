<?php

namespace App\Http\Controllers\User\Business;

use App\Library\Tree;
use App\Models\Binary\BinaryCalculation;
use App\Models\NestedSetUser;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class BinaryController extends Controller
{

    public function tree(Request $request)
    {
        $nested_current_user = NestedSetUser::whereUserId(\Session::get('user')->id)->first();

        $primary = \Session::get('user');

        if($request->tracking_id)
        {
            if(!$searched_user = User::with(['detail', 'addresses'])->whereTrackingId($request->tracking_id)->first())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            if (!$nested_search_user = NestedSetUser::whereUserId($searched_user->id)->first())
                return redirect()->back()->with(['error' => 'We can not retrieve the search user data, try after some time for reporting']);

            if (!$nested_search_user->isDescendantOf($nested_current_user) && $nested_search_user->user_id != $primary->id)
                return redirect()->back()->with(['error' => 'This User is not in Your DownLine']);

            $primary = $searched_user;
        }

        $primary = User::with(['detail', 'addresses'])->whereId($primary->id)->first();

        $direct_children = $primary->children->load(['detail', 'package', 'addresses']);

        return view('user.business.binary.tree', [
            'primary' => $primary,
            'children' =>  (new Tree())->create($direct_children)
        ]);
    }

    public function status()
    {
        return view('user.business.binary.status', [
            'binary_calculations' => BinaryCalculation::whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->get()
        ]);
    }
}
