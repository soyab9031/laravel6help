<?php

namespace App\Http\Controllers\User\Business;

use App\Models\NestedSetUser;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    public function sponsors()
    {
        $sponsors = User::with(['detail', 'package'])->whereSponsorBy(\Session::get('user')->id)->get();

        return view('user.business.sponsors', [
            'sponsors' => $sponsors
        ]);
    }

    public function sponsorLevelTree(Request $request)
    {
        if (isset($request->parent))
        {
            $sponsors = User::with(['detail:user_id,title,first_name,last_name', 'sponsors' => function ($q) {
                $q->select(['id', 'tracking_id', 'sponsor_by']);
            }])->select(['id', 'tracking_id', 'sponsor_by', 'parent_id']);

            if ($request->parent == '#') {
                $sponsors = $sponsors->whereSponsorBy(\Session::get('user')->id)->get();
            }
            else {
                $sponsors = $sponsors->whereSponsorBy($request->parent)->get();
            }

            return collect($sponsors)->map(function ($child) {

                return [
                    'id' => $child->id,
                    'parent_id' => $child->sponsor_by,
                    'text' => $child->detail->full_name . ' (ID: ' . $child->tracking_id . ')',
                    'icon' => count($child->sponsors) > 0 ? 'la la-user text-dark' : 'la la-user text-danger',
                    'tracking_id' => $child->tracking_id,
                    'children' => count($child->sponsors) > 0
                ];
            })->toJson();
        }

        return view('user.business.sponsor-level-tree');
    }

    public function children(Request $request)
    {

        $user = User::with('detail')->whereId(\Session::get('user')->id)->first();

        $request_leg = $request->leg ? $request->leg : 'L';

        $children = [];

        if ($direct_child = User::whereParentId($user->id)->whereLeg($request_leg)->first())
        {
            if ($nested_user = NestedSetUser::whereUserId($direct_child->id)->first())
            {
                $children = $nested_user->descendantsAndSelf()->with([
                    'user.detail', 'user.package', 'user.parentBy.detail', 'user.sponsorBy.detail'
                ])->search($request->search, [
                    'user.tracking_id', 'user.mobile', 'user.detail.first_name', 'user.detail.last_name'
                ])->orderBy('id', 'desc')->paginate(50);
            }
        }

        return view('user.business.children-list', [
            'user' => $user,
            'user_binary_total' => $user->current_binary_status,
            'children' => $children,
            'request_leg' => $request_leg
        ]);
    }
}
