<?php

namespace App\Http\Controllers\User;

use App\Models\Payout;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PayoutController extends Controller
{
    public function index(Request $request)
    {
        $payouts = Payout::whereProcessType(Payout::ADMIN_PROCESS)->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->paginate(20);

        return view('user.payout.index', [
            'payouts' => $payouts,
        ]);
    }

    public function makeRequest(Request $request)
    {
        $payout_requests = Payout::whereProcessType(Payout::USER_PROCESS)->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->paginate(20);

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'transfer_amount' => 'required|numeric'
            ]);
        }

        return view('user.payout.request-view', [
            '$payout_request' => $payout_requests,
        ]);
    }

}
