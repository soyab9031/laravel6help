<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::whereUserId(\Session::get('user')->id)->latest()->paginate(30);

        return view('user.shopping.orders.index', [
            'orders' => $orders
        ]);
    }

    public function details(Request $request)
    {
        $order = Order::whereUserId(\Session::get('user')->id)->with(['details.product_price.product'])
            ->whereCustomerOrderId($request->customer_order_id)->first();

        if (!$order)
            return redirect()->back()->with(['error' => 'Not able to find the Order details, try again']);

        return view('user.shopping.orders.details', [
            'order' => $order
        ]);
    }

    public function place(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'remarks' => 'required'
        ], [
            'remarks.required' => 'Your Message is required for this Shopping Order'
        ]);

        if ($validator->fails())
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

        $order = \DB::transaction(function() use ($request) {

            $items = collect(Helper::arrayToObject($request->items));

            $order =  Order::create([
                'user_id' => \Session::get('user')->id,
                'amount' => $request->total_amount,
                'discount' => 0,
                'wallet' => 0,
                'total' => $request->total_amount,
                'total_bv' => $request->total_bv,
                'status' => Order::PLACED,
                'remarks' => $request->remarks
            ]);

            $order->customer_order_id = 'O' . $order->created_at->format('Ymd') . $order->id;
            $order->save();

            collect($items)->map(function ($item) use ($order) {

                OrderDetail::create([
                    'order_id' => $order->id,
                    'product_price_id' => $item->id,
                    'price' => $item->price,
                    'distributor_price' => $item->distributor_price,
                    'selling_price' => $item->selling_price,
                    'bv' => $item->bv,
                    'qty' => $item->quantity->current,
                    'gst' => json_encode($item->gst)
                ]);

            });

            \Session::forget('cart');

            return $order;

        });

        $request->session()->flash('success', 'New Order is Placed Successfully');

        return response()->json([
            'status' => true, 'customer_order_id' => $order->customer_order_id,
            'route' => route('user-shopping-order-details', ['customer_order_id' => $order->customer_order_id])
        ]);
    }
}
