<?php

namespace App\Http\Controllers\User;

use App\Models\Package;
use App\Models\PackageOrder;
use App\Models\Pin;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class UpgradeAccountController extends Controller
{

    public function index(Request $request)
    {

        /**
         * Code Appropriate Terms & Conditions
         * to prevent the user to enter this page
         *
         */
        if (\Session::get('user')->pin_id)
            return redirect()->back()->with(['error' => 'Your Account is already upgraded']);


        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'pin_number' => 'required|exists:pins,number'
            ], [
                'pin_number.required' => 'Pin Number is Required',
                'pin_number.exists' => 'Invalid Pin Number',
            ]);

            $package = Package::whereId($request->package_id)->first();

            if(!$pin = Pin::whereNumber($request->pin_number)->wherePackageId($request->package_id)->first())
                return redirect()->back()->withInput()->with('error', 'This Pin number is not Belongs to Selected Package');

            if($pin->status == Pin::USED)
                return redirect()->back()->withInput()->with('error', 'This Pin is already Used !');

            if($pin->status == Pin::BLOCKED)
                return redirect()->back()->withInput()->with('error', 'This Pin is already Blocked!');

            \DB::transaction(function () use ($pin, $request, $package) {

                $pin->status = Pin::USED;
                $pin->save();

                User::whereId(\Session::get('user')->id)->update([
                    'pin_id' => $pin->id,
                    'paid_at' => Carbon::now(),
                    'package_id' => $package->id,
                    'joining_amount' => $package->amount
                ]);

                PackageOrder::create([
                   'user_id' => \Session::get('user')->id,
                   'pin_id' => $pin->id,
                   'package_id' => $package->id,
                   'amount' => $package->amount,
                   'pv' => $package->pv,
                   'pv_calculation' => PackageOrder::NO
                ]);

            });

            \Session::put('user', User::whereId(\Session::get('user')->id)->first());

            return redirect()->route('user-dashboard')->with(['success' => 'Your account has been upgraded successfully']);
        }

        return view('user.upgrade.index',[
            'packages' => Package::active()->orderBy('amount','asc')->get()
        ]);
    }
}
