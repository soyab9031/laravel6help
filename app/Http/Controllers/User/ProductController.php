<?php

namespace App\Http\Controllers\User;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $child_category_ids = [];
        if ($request->category_id) {
            $child_category_ids = Category::whereParentId($request->category_id)->select('id')->get()->pluck('id')->toArray();
        }

        $product_prices = ProductPrice::with(['product'])->search($request->search, [
            'product.name'
        ])->whereHas('product', function ($q) use ($child_category_ids) {

            if (count($child_category_ids) > 0) {
                $q->whereStatus(Product::ACTIVE)->whereIn('category_id', $child_category_ids);
            }
            else {
                $q->whereStatus(Product::ACTIVE);
            }
        })->get();

        /* Retrieve Items from Cart */
        $cart_items = \Session::has('cart') ? collect(\Session::get('cart'))->toArray() : [];

        $items = collect($product_prices)->map(function ($item) use ($cart_items) {

            $cart_item = collect($cart_items)->where('product_price_id', $item->id)->first();
            $item->quantity = ['action' => null, 'current' => $cart_item ? $cart_item->qty : 0];

            return $item;
        })->where('distributor_price', '>', 0)->values();

        return view('user.shopping.products.index',[
            'product_prices' => $items,
            'categories' => Category::whereNull('parent_id')->get()
        ]);
    }

    public function details(Request $request)
    {
        $product_price = ProductPrice::with(['product'])->whereHas('product', function ($q) {
            $q->whereStatus(Product::ACTIVE);
        })->whereCode($request->code)->first();

        if (!$product_price)
            return redirect()->back()->with(['error' => 'Invalid Product Data, Try Again']);

        return view('user.shopping.products.details', [
            'product_price' => $product_price
        ]);
    }
}
