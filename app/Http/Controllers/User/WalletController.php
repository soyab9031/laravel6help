<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Models\Wallet;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class WalletController extends Controller
{

    public function index(Request $request)
    {
        $income_type = 0;

        if ($request->type) {

            if (!$income_type = Wallet::getIncomeTypeId($request->type))
                return redirect()->back()->with(['error' => 'Invalid Wallet Request Type']);
        }

        $wallets = Wallet::whereUserId(\Session::get('user')->id)->orderBy('id', 'desc');

        if ($income_type > 0)
            $wallets = $wallets->whereIncomeType($income_type);

        return view('user.wallet.index', [
            'type' => $request->type,
            'wallets' => $wallets->paginate(30)
        ]);
    }

    public function totalReport(Request $request)
    {
        $wallets = Wallet::whereUserId(\Session::get('user')->id)->filterDate($request->dateRange);

        return view('user.wallet.report', [
            'wallets' => $wallets->orderBy('id', 'desc')->paginate(50)
        ]);
    }

}
