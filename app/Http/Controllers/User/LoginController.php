<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Models\Admin;
use App\Models\User;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {

            $this->validate($request, [
                'user_credential' => 'required',
                'password' => 'required'

            ], [
                'user_credential.required' => 'User Credential is Required',
                'password.required' => 'Password is Required'
            ]);

            $user = User::with('status')->whereUsername($request->user_credential)->orWhere('tracking_id', $request->user_credential)->orWhere('mobile', $request->user_credential)->orWhere('email', $request->user_credential)->first();

            if (!$user)
                return redirect()->back()->with(['error' => 'Invalid User Credentials ']);

            if ($user->status->terminated == UserStatus::YES)
                return redirect()->back()->with(['error' => 'You are blocked to access your account, Contact to Support Team']);

            if($request->password != $user->password)
                return redirect()->back()->with(['error'=> 'Password is Not Match with User Credential!']);

            \Session::put('user', $user);

            User::whereId($user->id)->update([
                'last_logged_in_ip' => Helper::getClientIp(),
                'last_logged_in_at' => Carbon::now()
            ]);

            if (\Session::has('order-checkout') && $request->redirect_route == 'website-checkout-shipping-address') {
                return redirect()->route('website-cart')->with(['success' => 'Click Continue to Process the Order']);
            }

            return redirect()->route('user-dashboard')->with([
                'success' => 'Welcome to ' . config('project.brand') . ' account'
            ]);

        }

        return view('user.authentication.login');

    }

    public function forgetPassword(Request $request)
    {
        if($request->isMethod('post')) {

            $this->validate($request, [
                'user_credential' => 'required',
                'email' => 'required|email'
            ], [
                'user_credential.required' => 'Username is required to get New Password',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
            ]);

            $user = User::with('status')->whereUsername($request->user_credential)->orWhere('tracking_id', $request->user_credential)->orWhere('mobile', $request->user_credential)->first();

            if (!$user)
                return redirect()->back()->with(['error' => 'Invalid User Credentials']);

            if ($user->status->terminated == UserStatus::YES)
                return redirect()->back()->with(['error' => 'You are blocked to access your account, Contact to Support Team']);

            if($request->email != $user->email)
                return redirect()->back()->with(['error'=> 'Email does not match with User Credential!']);

            // --- After Activation of Email Functions Remove this Line ---- //
            return redirect()->route('user-login')->with(['error' => 'Contact Admin team to Activate this function']);


            $new_password = strtoupper(\Str::random(8));

            if (!$user = User::whereUsername($request->username)->whereEmail($request->email)->first())
                return redirect()->back()->withInput()->with(['error' => 'This Email id is not registered with this User']);

            $user->password = $new_password;
            $user->save();


            if(env('APP_ENV') != 'local')
            {
                \Mail::send('emails.user.forgot-password', [
                    'user' => $user, 'new_password' => $new_password
                ], function ($mail) use ($user) {
                    $mail->from('noreply@project.com', config('project.brand'));
                    $mail->to($user->email)->subject('New Password from ' . config('project.brand'));
                });
            }

            return redirect()->route('user-login')->with(['success' => 'New Password has been sent on Your Registered Email']);

        }

        return view('user.authentication.forget-password');
    }

    public function logout()
    {
        \Session::forget('user');
        \Session::forget('wallet_session');

        return redirect()->route('user-login');
    }

    public function adminViewAccess(Request $request)
    {
        if (!\Session::has('admin'))
            return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

        if (!Admin::whereToken($request->token)->exists())
            return redirect()->back()->with(['error' => 'Invalid token for User Access']);

        \Session::forget('user');
        \Session::forget('wallet_session');

        if (!$user = User::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

        \Session::put('user', $user);
        \Session::put('wallet_session', $user);
        return redirect()->route('user-dashboard');
    }
}
