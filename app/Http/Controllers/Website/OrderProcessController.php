<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\PaymentGatewayTransaction;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\BadRequestError;

class OrderProcessController extends Controller
{
    public function address(Request $request)
    {
        $selected_address = null;
        $user = \Session::get('user');

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'address_id' => 'required|exists:user_addresses,id'
            ], [
                'address_id.required' => 'Select any Single Address for Process',
                'address_id.exists' => 'Invalid Address Data, Try again'
            ]);

            $address = UserAddress::with('state')->whereId($request->address_id)->first();

            if (!$address->name || !$address->mobile)
                return redirect()->back()->with(['error' => 'Kindly Add Your Name and Mobile Number in Address']);

            $new_order = [
                'items' => \Session::get('order-checkout')['items'],
                'order_summary' => \Session::get('order-checkout')['order_summary'],
                'address' => $address
            ];

            \Session::forget('order-checkout');
            \Session::put('order-checkout', $new_order);

            return redirect()->route('website-checkout-payment-overview');
        }

        $addresses = UserAddress::whereUserId(\Session::get('user')->id)->get();

        if ($request->id)
            $selected_address = UserAddress::whereUserId(\Session::get('user')->id)->whereId($request->id)->first();

        $orderUser = (object)[
            'name' => $user->detail->full_name,
            'mobile' => $user->mobile,
            'email' => $user->email
        ];

        \Session::put('orderUser', $orderUser);

        $order = \Session::get('order-checkout');

        return view('website.order-process.address', [
            'order' => Helper::arrayToObject($order),
            'addresses' => $addresses,
            'selected_address' => $selected_address
        ]);
    }

    public function paymentOverview()
    {
        if(!$order = \Session::get('order-checkout'))
            return redirect()->route('website-cart')->with('Order Not Valid For Process Payment');

        return view('website.order-process.payment-overview', [
            'order' => Helper::arrayToObject($order),
        ]);
    }

    public function process(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'items' => 'required|array',
            'order_summary' => 'required',
        ]);

        if ($validator->fails())
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

        $form_request = Helper::arrayToObject($request->all());

        $order = \DB::transaction(function () use ($form_request) {

            $shipping_address = UserAddress::whereId($form_request->shipping_address_id)->with(['state:id,name'])->first();

            $order = Order::create([
                'user_id' => \Session::get('user')->id,
                'amount' => $form_request->order_summary->amount,
                'total' => $form_request->order_summary->total_amount,
                'total_bv' => $form_request->order_summary->total_bv,
                'shipping_address' => $shipping_address,
                'status' => Order::PLACED,
                'discount' => 0,
                'wallet' => 0,
                'shipping_charge' => $form_request->order_summary->shipping_charge,
                'payment_status' => Order::PAYMENT_PENDING,
            ]);

            $order->customer_order_id = 'O' . now()->timestamp . $order->id;
            $order->save();

            collect(Helper::arrayToObject($form_request->items))->map(function ($item) use ($order) {

                OrderDetail::create([
                    'order_id' => $order->id,
                    'product_price_id' => $item->id,
                    'price' => $item->price,
                    'selling_price' => $item->selling_price,
                    'bv' => $item->bv,
                    'qty' => $item->quantity->current,
                    'gst' => json_encode([
                        'code' => $item->gst->code,
                        'percentage' => $item->gst->percentage
                    ])
                ]);

            });

            return $order;
        });

        \Session::forget('cart');
        \Session::put('order_number', 'forgot');

        \Session::forget('cart');

        return response()->json([
            'status' => true,
            'message' => "Order: {$order->customer_order_id} is placed successfully",
            'customer_order_id' => $order->customer_order_id,
            'user' => [
                'name' => $order->user->detail->full_name,
                'mobile' => $order->user->mobile,
                'email' => $order->user->email
            ],
            'amount' => (float)$order->total,
            'overview_route' => route('website-order-overview', ['customer_order_id' => $order->customer_order_id])
        ]);
    }

    public function overview(Request $request)
    {
        $order = Order::with(['user',  'details.product_price.product'])->whereCustomerOrderId($request->customer_order_id)->first();

//        if (!\Session::has('order_number')) {
//            return redirect()->route('user-shopping-order-details',['customer_order_id' => $order->customer_order_id]);
//        }
//        \Session::forget('order_number');

        return view('website.order-process.overview', [
            'order' => $order
        ]);
    }
}
