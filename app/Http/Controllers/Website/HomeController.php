<?php

namespace App\Http\Controllers\Website;

use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {

        $banners = Banner::active()->whereType(Banner::BANNER)->get();
        $popup = Banner::active()->whereType(Banner::POPUP)->first();

        $categories = Category::active()->with(['parent.children'])->orderBy('id','asc')->get();

        $deal_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereDeal(ProductPrice::DEAL)->get();

        $new_arrival_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereNewArrival(ProductPrice::NEW_ARRIVAL)->latest()->get();

        $recommended_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereRecommended(ProductPrice::RECOMMENDED)->get();


        return view('website.home',[
            'banners' => $banners,
            'popup' => $popup,
            'categories' => $categories,
            'deal_products' => $deal_products,
            'new_arrival_products' => $new_arrival_products,
            'recommended_products' => $recommended_products
        ]);
    }
}
