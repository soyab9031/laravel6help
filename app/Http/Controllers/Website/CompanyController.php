<?php

namespace App\Http\Controllers\Website;

use App\Models\Grievance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function about()
    {
        return view('website.about');
    }

    public function legals()
    {
        return view('website.legals');
    }

    public function vision()
    {
        return view('website.vision');
    }

    public function mission()
    {
        return view('website.mission');
    }

    public function terms()
    {
        return view('website.terms');
    }

    public function privacyPolicy()
    {
        return view('website.privacy-policy');
    }

    public function cancellationPolicy()
    {
        return view('website.cancellation-policy');
    }

    public function support(Request $request){

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'email.required' => 'Email is Required',
                'subject.required' => 'Subject is Required',
                'message.required' => 'Message is Required'
            ]);

            Grievance::create([
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);

            return redirect()->route('website-support')->with(['success' => 'Your Complaints Request has been Sent, We will contact you soon..!!']);
        }

        return view('website.support');
    }
}
