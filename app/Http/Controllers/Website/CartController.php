<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\ProductPrice;
use App\Models\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $items = [];

        if (\Session::has('cart'))
        {
            $product_price_ids = collect(\Session::get('cart'))->pluck('product_price_id');

            $items = ProductPrice::with(['product'])->whereIn('id', $product_price_ids)->get()->map(function ($product_price) {

                $selected = collect(\Session::get('cart'))->filter(function ($cart) use ($product_price) {
                    return $cart->product_price_id == $product_price->id;
                })->first();

                return [
                    'id' => $product_price->id,
                    'offer_id' => null,
                    'gst' => $product_price->gst,
                    'code' => $product_price->code,
                    'product' => $product_price->product->name,
                    'slug' => $product_price->product->slug,
                    'image' => $product_price->primary_image,
                    'price' => $product_price->price,
                    'distributor_price' => $product_price->distributor_price,
                    'customer_price' => $product_price->customer_price,
                    'selling_price' => $product_price->distributor_price,
                    'bv' => $product_price->points,
                    'qty' => $product_price->qty,
                    'quantity' => [
                        'action' => null, 'current' => $selected->qty, 'actual' => 100
                    ]
                ];

            })->toArray();

        }
        return view('website.cart', [
            'items' => $items,
            'user' => \Session::has('user') ? User::where('id', \Session::get('user')->id)->first() : null
        ]);
    }

    public function retrieveItems(Request $request)
    {
        if (!\Session::has('cart')) {
            return response()->json(['status' => false, 'message' => 'No items in cart']);
        }

        return response()->json(['status' => true, 'cart' => \Session::get('cart')]);
    }

    public function addItem(Request $request)
    {
        $product_price = ProductPrice::whereId($request->product_price_id)->first();

        if (!$product_price)
            return response()->json(['status' => false, 'message' => 'Invalid Product Data, Try again']);

        if (!\Session::has('cart')) {

            $new_cart = [
                Helper::arrayToObject(['product_price_id' => $product_price->id, 'qty' => 1])
            ];

            \Session::put('cart', $new_cart);

            return response()->json([
                'status' => true, 'message' => '1 Item has been added', 'current_item_qty' => 1, 'total_items' => count(\Session::get('cart'))
            ]);
        } else {

            $cartHasItem = collect(\Session::get('cart'))->filter(function ($cart) use ($request) {
                return $cart->product_price_id == $request->product_price_id;
            });

            if (count($cartHasItem) > 0) {
                $new_cart = collect(\Session::get('cart'))->map(function ($cart) use ($request) {
                    if ($cart->product_price_id == $request->product_price_id)
                        $cart->qty = $cart->qty + 1;
                    return $cart;
                })->toArray();

                \Session::forget('cart');
                \Session::put('cart', $new_cart);

                return response()->json([
                    'status' => true, 'message' => 'Item has been added', 'total_items' => count(\Session::get('cart'))
                ]);
            } else {

                $new_cart = collect(\Session::get('cart'))->push(Helper::arrayToObject([
                    'product_price_id' => $product_price->id, 'qty' => 1
                ]))->toArray();

                \Session::forget('cart');
                \Session::put('cart', $new_cart);

                return response()->json([
                    'status' => true, 'message' => 'Item has been added', 'total_items' => count(\Session::get('cart'))
                ]);

            }
        }
    }

    public function removeItem(Request $request)
    {
        $product_price = ProductPrice::whereId($request->product_price_id)->first();

        if (!$product_price)
            return response()->json(['status' => false, 'message' => 'Invalid Product Data, Try again']);

        $cartHasItem = collect(\Session::get('cart'))->filter(function ($cart) use ($request) {
            return $cart->product_price_id == $request->product_price_id;
        })->first();

        if ($cartHasItem->qty == 1 || $request->removeAllQty) {

            $new_cart = collect(\Session::get('cart'))->reject(function ($cart) use ($request) {
                return $cart->product_price_id == $request->product_price_id;
            });

            \Session::forget('cart');

            if (count($new_cart) > 0)
                \Session::put('cart', $new_cart);

            return response()->json([
                'status' => true, 'message' => 'Item has been removed', 'total_items' => count($new_cart)
            ]);
        } else {

            $new_cart = collect(\Session::get('cart'))->map(function ($cart) use ($request) {
                if ($cart->product_price_id == $request->product_price_id)
                    $cart->qty = $cart->qty - 1;
                return $cart;
            })->toArray();

            \Session::forget('cart');
            \Session::put('cart', $new_cart);

            return response()->json([
                'status' => true, 'message' => '1 Qty Reduced', 'total_items' => count(\Session::get('cart'))
            ]);

        }
    }

    public function createCheckout(Request $request)
    {
        \Session::forget('order-checkout');

        $order = [
            'items' => $request->items,
            'order_summary' => $request->order_summary
        ];

        \Session::put('order-checkout', $order);

        return response()->json(['status' => true, 'redirect_route' => route('website-checkout-shipping-address')]);
    }
}
