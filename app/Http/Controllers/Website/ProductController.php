<?php

namespace App\Http\Controllers\Website;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function PHPSTORM_META\map;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if (!$parent_category = Category::whereSlug($request->slug)->first())
            return redirect()->back();

        $child_categories = Category::whereParentId($parent_category->id)->active()->get();


        if ($request->sub)
            $child_categories = $child_categories->filter(function ($category) use ($request) {
                return $category->slug == $request->sub;
            });


        $categories_lists = Category::active()->whereParentId($parent_category->id)->get();

        $cart_items = \Session::has('cart') ? collect(\Session::get('cart'))->toArray() : [];

        $product_prices = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) use ($child_categories) {
            $q->where('status', '<>', Product::INACTIVE)->whereIn('category_id', $child_categories);
        })->get();

        $items = collect($product_prices)->map(function ($item) use ($cart_items) {
            $cart_item = collect($cart_items)->where('product_price_id', $item->id)->first();
            $item->quantity = ['action' => null, 'current' => $cart_item ? $cart_item->qty: 0];
            return $item;
        });

        return view('website.product.index', [
            'product_prices' => $product_prices,
            'items' => $items,
            'parent_category' => $parent_category,
            'categories_lists' => $categories_lists
        ]);
    }

    public function details(Request $request)
    {
        $product_price = ProductPrice::with(['product.category'])->whereHas('product', function ($q) {
            $q->where('status', '<>', Product::INACTIVE);
        })->whereCode($request->product_code)->first();

        if (!$product_price)
            return redirect()->back()->with(['error' => 'Product / Item is not available']);

        $parent_category = null;

        return view('website.product.detail', [
            'user' => \Session::get('user') ? User::whereId(\Session::get('user')->id)->first() : null,
            'parent_category' => $parent_category,
            'product_price' => $product_price
        ]);
    }

    public function listProducts(Request $request)
    {
        $cart_items = \Session::has('cart') ? collect(\Session::get('cart'))->toArray() : [];

        $product_prices = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q){
            $q->where('status', '<>', Product::INACTIVE);
        });

        if ($request->type == 'deal'){
            $product_prices = $product_prices->whereDeal(ProductPrice::DEAL)->get();
        }elseif ($request->type == 'new_arrival'){
            $product_prices = $product_prices->whereNewArrival(ProductPrice::NEW_ARRIVAL)->get();
        } elseif ($request->type == 'recommended'){
            $product_prices = $product_prices->whereRecommended(ProductPrice::RECOMMENDED)->get();
        }

        $items = collect($product_prices)->map(function ($item) use ($cart_items) {
            $cart_item = collect($cart_items)->where('product_price_id', $item->id)->first();
            $item->quantity = ['action' => null, 'current' => $cart_item ? $cart_item->qty: 0];
            return $item;
        });

        return view('website.product.list',[
            'items' => $items,
            'type' => $request->type
        ]);

    }
}
