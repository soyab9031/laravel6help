<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\StoreManager\StockTransaction;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreStockRequest;
use App\Models\StoreManager\StoreStockRequestDetail;
use App\Models\StoreManager\StoreStockTransaction;
use App\Models\StoreManager\StoreSupply;
use App\Models\StoreManager\StoreSupplyDetail;
use App\Models\StoreManager\StoreWalletTransaction;
use App\Models\StorePendingCommission;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StockRequestController extends Controller
{
    public function index(Request $request)
    {
        $requests = StoreStockRequest::with(['details:request_id','receiver_store'])->whereStoreId(\Session::get('store')->id)->filterDate($request->dateRange)->latest()->paginate(30);

        return view('store-manager.stock.request.index', [
            'stock_requests' => $requests
        ]);
    }

    public function detail(Request $request)
    {
        $stock_request = StoreStockRequest::with(['details.product_price.product:id,name'])->whereStoreId(\Session::get('store')->id)->whereId($request->id)->first();

        return view('store-manager.stock.request.details', [
            'stock_request' => $stock_request
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {

            $validator = \Validator::make($request->all(), [
                'order_items' => 'required',
                'store_id' => 'required_if:request_to,>=,2'
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            if(\Session::get('store')->wallet_balance < $form_request->order_details->amount){
                return response()->json(['status' => false, 'message' => 'You don`t have enough balance for purchase stock']);
            }

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Stock Request']);

            $receiver_store = null;

            $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

            if($request->store_id) {

                if (!$receiver_store = Store::whereId($request->store_id)->first())
                    return response()->json(['status' => false, 'message' => 'Unable to Find Request Store Please Try Again']);

                /* Stock Check Start */
                $exist_stock_items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
                    ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
                    ->whereIn('product_price_id', collect($order_items)->pluck('id')->toArray())
                    ->whereStoreId($receiver_store->id)->get();


            }else{
                //check Admin,   Stock Check Start
                $exist_stock_items = StockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
                    ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
                    ->whereIn('product_price_id', collect($order_items)->pluck('id')->toArray())->get();
            }

            $item_not_exists = collect($order_items)->filter(function ($order_item) use ($exist_stock_items) {

                $current_item = collect($exist_stock_items)->where('product_price_id', $order_item->id)->first();

                if (!$current_item)
                    return !$current_item;

                return $order_item->selected_qty > $current_item->balance;
            })->first();

            if ($item_not_exists) {
                return response()->json(['status' => false, 'message' => $item_not_exists->name . ' has not enough Stock, Remove This Item or Try again']);
            }

            $stock_request = \DB::transaction(function () use ($form_request, $order_items, $receiver_store, $request) {

                $store = Store::whereId(\Session::get('store')->id)->first();

                $stock_request = StoreStockRequest::create([
                    'store_id' => $store->id,
                    'receiver_store_id' => $receiver_store ? $receiver_store->id : null,
                    'amount' => $form_request->order_details->amount,
                    'discount' => 0,
                    'status' => StoreStockRequest::PENDING
                ]);


                collect($order_items)->map(function ($order_item) use ($stock_request, $store, $request, $receiver_store) {

                    StoreStockRequestDetail::create([
                        'request_id' => $stock_request->id,
                        'product_price_id' => $order_item->id,
                        'price' => $order_item->price,
                        'distributor_price' => $order_item->distributor_price,
                        'selling_price' => round($order_item->total_amount),
                        'qty' => $order_item->selected_qty,
                    ]);

                });

                return $stock_request;

            });

            $request->session()->flash('success', 'New Stock Request is Created, Ref ID:  ' . $stock_request->id);

            return response()->json(['status' => true, 'route' => route('store-stock-request-detail', ['id' => $stock_request->id])]);
        }

        $store = Store::whereId(\Session::get('store')->id)->first();
        $stores = Store::selectRaw('id as value, name as label, name, city, type, id');

        if(in_array($store->type,[Store::BASIC_TYPE, Store::STANDARD_TYPE])){
            $stores = $stores->whereIn('type', [Store::PREMIUM_TYPE, Store::PRO_TYPE]);
        }

        if($store->type == Store::PRO_TYPE)
            $stores = $stores->where('type', Store::PREMIUM_TYPE);

        return view('store-manager.stock.request.create', [
            'wallet_balance' => $store->wallet_balance,
            'request_options' => $store->sendStockRequestOptions(),
            'stores' => $stores->get()
        ]);
    }

    public function apiGetStoreWithStock(Request $request)
    {
        if (!$store = Store::whereId($request->store_id)->first())
            return response()->json(['status' => false, 'message' => 'Invalid Store request, try again']);

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        $current_store_stock_records = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
            ->whereStoreId(\Session::get('store')->id)->get();

        $items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->with(['product_price.product:id,name'])
            ->whereHas('product_price.product', function($q){
                $q->where('status', Product::ACTIVE);
            })
            ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
            ->whereStoreId($store->id)->get()->map(function ($transaction) use ($current_store_stock_records) {

                $current_stock = collect($current_store_stock_records)->where('product_price_id', $transaction->product_price_id)->first();

                return [
                    'value' => $transaction->product_price->id,
                    'label' => $transaction->product_price->product->name . '(' . $transaction->product_price->code . ')',
                    'id' => $transaction->product_price->id,
                    'name' => $transaction->product_price->product->name,
                    'code' => $transaction->product_price->code,
                    'bv' => $transaction->product_price->points,
                    'selected_qty' => 0,
                    'balance' => $transaction->balance,
                    'store_stock' => $current_stock ? $current_stock->balance : 0,
                    'price' => $transaction->product_price->price,
                    'distributor_price' => $transaction->product_price->distributor_price,
                    'tax_percentage' => $transaction->product_price->gst->percentage,
                    'tax_code' => $transaction->product_price->gst->code,
                ];

            });

        if(count($items) == 0)
            return response()->json(['status' => false, 'message' => $store->name . 'Donn`t have any product in stock, please choose other store']);

        return response()->json([
            'status' => true,
            'store' => [
                'id' => $store->id,
                'tracking_id' => $store->tracking_id,
                'name' => $store->name,
                'city' => $store->city,
                'type' => $store->type,
            ],
            'items' => $items
        ]);


    }

    public function apiGetCompanyStock(Request $request)
    {

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        $store_stock_records = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
            ->whereStoreId(\Session::get('store')->id)->get();


        $items = StockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')
            ->havingRaw($balance_query . '> 0')
            ->with(['product_price.product:id,name'])
            ->whereHas('product_price.product', function($q){
                $q->where('status', Product::ACTIVE);
            })
            ->get()->map(function ($transaction) use ($store_stock_records){

                $current_stock = collect($store_stock_records)->where('product_price_id', $transaction->product_price->id)->first();

                return [
                    'label' => $transaction->product_price->product->name . '(' . $transaction->product_price->code . ')',
                    'value' => $transaction->product_price->id,
                    'id' => $transaction->product_price->id,
                    'code' => $transaction->product_price->code,
                    'bv' => $transaction->product_price->points,
                    'name' => $transaction->product_price->product->name,
                    'selected_qty' => 0,
                    'balance' => $transaction->balance,
                    'store_stock' => $current_stock ? $current_stock->balance : 0,
                    'price' => $transaction->product_price->price,
                    'distributor_price' => $transaction->product_price->distributor_price,
                    'tax_percentage' => $transaction->product_price->gst->percentage,
                    'tax_code' => $transaction->product_price->gst->code,
                ];

            });

        if(count($items) == 0)
            return response()->json(['status' => false, 'message' => 'Company Donn`t have any product in stock, please contact admin']);

        return response()->json([
            'status' => true,
            'items' => $items
        ]);

    }


    public function received(Request $request)
    {
        $requests = StoreStockRequest::with(['details:request_id','receiver_store'])->where('receiver_store_id',\Session::get('store')->id)->filterDate($request->dateRange)->latest()->paginate(30);

        return view('store-manager.stock.request.received.index', [
            'stock_requests' => $requests
        ]);
    }

    public function receivedRequestUpdate(Request $request)
    {
        $stock_request = StoreStockRequest::with(['details.product_price.product:id,name'])->whereId($request->id)->first();

        if($request->isMethod('post')){

            $this->validate($request, [
                'status' => 'required',
                'remarks' => 'required'
            ], [
                'remarks.remarks' => 'Your Message or Remarks is required'
            ]);

            $stock_request->status = $request->status;
            $stock_request->remarks = $request->remarks;
            $stock_request->save();

            return redirect()->route('store-stock-request-received-view', ['id' => $stock_request->id])->with(['success' => 'Stock Request is Updated Successfully']);
        }

        return view('store-manager.stock.request.received.details', [
            'stock_request' => $stock_request
        ]);
    }
}
