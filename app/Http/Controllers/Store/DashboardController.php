<?php

namespace App\Http\Controllers\Store;

use App\Library\Helper;
use App\Models\Order;
use App\Models\StoreManager\Store;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $store = Store::whereId(\Session::get('store')->id)->first();

        $order_details = [
          'today' => Order::whereStoreId($store->id)->where('created_at','>=', now()->startOfDay())->where('created_at','<=', now()->endOfDay())->count(),
          'pending' => Order::whereStoreId($store->id)->count(),
          'delivered' => Order::whereStoreId($store->id)->count(),
          'total' => Order::whereStoreId($store->id)->count(),
        ];

        return view('store-manager.dashboard', [
            'store' => $store,
            'order_detail' => Helper::arrayToObject($order_details),
            'order_count' => Order::whereStoreId($store->id)->count(),
            'recent_orders' => Order::whereStoreId(\Session::get('store')->id)->with(['user.detail:user_id,title,first_name,last_name'])->latest()->take(7)->get(),
        ]);
    }

    public function reward()
    {
        return view('store-manager.rewards.index');
    }
}
