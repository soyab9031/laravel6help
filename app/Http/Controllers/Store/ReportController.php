<?php

namespace App\Http\Controllers\Store;

use App\Exports\Store\ProductRankingExport;
use App\Exports\Store\salesExport;
use App\Exports\Store\SupplyTransferExport;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductPrice;
use App\Models\StoreManager\StoreSupply;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function sales(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $orders =  Order::whereStoreId(\Session::get('store')->id)->selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at, count(id) as order_count')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();


        $records = collect($period)->map(function ($date) use ($orders, $request) {

            $day_turnover = collect($orders)->filter(function ($order) use ($date) {
                return $order->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => Carbon::parse($date),
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
                'order_count' => $day_turnover ? $day_turnover->order_count : 0,
            ];

        })->sortByDesc('date')->values();

        if($request->download)
        {
            if ( empty($request->dateRange))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            return \Excel::download(new salesExport($records), 'sales-report.xlsx');
        }

        return view('store-manager.reports.sales',[
            'records' => $records
        ]);
    }

    public function supplyTransfer(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $supplies = StoreSupply::whereSenderStoreId(\Session::get('store')->id)->selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at, count(id) as supply_count')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();

        $records = collect($period)->map(function ($date) use ($supplies, $request) {

            $day_turnover = collect($supplies)->filter(function ($supply) use ($date) {
                return $supply->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => Carbon::parse($date),
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
                'supply_count' => $day_turnover ? $day_turnover->supply_count : 0,
            ];

        })->sortByDesc('date')->values();

        if($request->download == 'yes')
        {
            if ( empty($request->dateRange))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            return \Excel::download(new SupplyTransferExport($records), 'supply-transfer.xlsx');
        }

        return view('store-manager.reports.supply-transfer',[
            'records' => $records
        ]);
    }

    public function productRanking(Request $request)
    {

        $orders = OrderDetail::select(\DB::raw('SUM(qty) as total, product_price_id'))->groupBy('product_price_id')->whereHas('order', function ($q) {
            $q->where('store_id', \Session::get('store')->id);
        })->filterDate($request->dateRange)->get();

        $items = ProductPrice::with(['product:id,name'])->get()->map( function ($product_price) use ($orders) {

            $sold_item = collect($orders)->filter( function ($order) use ($product_price) {
                return $order->product_price_id == $product_price->id;
            })->first();

            $product_price->sold_qty = $sold_item ? $sold_item->total : 0;
            return $product_price;

        })->sortByDesc( function ($item) {
            return $item->sold_qty;
        })->values();

        if($request->download == 'yes')
        {
            if ( empty($request->dateRange))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            return \Excel::download(new ProductRankingExport($items), 'product-ranking.xlsx');
        }

        return view('store-manager.reports.product-ranking',[
            'items' => $items
        ]);
    }

}
