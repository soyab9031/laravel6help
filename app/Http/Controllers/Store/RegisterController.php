<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\StoreManager\StoreRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\GstNumber;

class RegisterController extends Controller
{
    public function index(Request $request){

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'type' => 'required',
                'sponsor_user_id' => 'nullable|exists:users,tracking_id',
                'store_name' => 'required',
                'owner_name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'email' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state_id' => 'required',
                'district' => 'required',
                'pincode' => 'required|digits:6',
                'password' => 'required|min:6',
                'gst_number' => ['nullable', new GstNumber]
            ],[
                'type.required' => 'Please Select Store Type',
                'sponsor_user_id.exists' => 'Invalid Sponsor User, Try again',
                'store_name.required' => 'Store Name is required',
                'owner_name.required' => 'Owner Name is required',
                'address.required' => 'Address is required',
                'mobile.required' => 'Mobile Number is required',
                'mobile.regex' => 'Mobile Number is invalid',
                'mobile.digits' => 'Mobile Number is required 10 digits',
                'city.required' => 'City is required',
                'state_id' => 'State is required',
                'pincode.required' => 'pincode is required',
                'district.required' => 'district is required',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
            ]);

            $store = \DB::transaction(function() use ($request) {

                $user = null;
                if ($request->sponsor_user_id) {
                    $user = User::find($request->sponsor_user_id);
                }

                $store = StoreRequest::create([
                    'sponsor_user_id' => $user ? $user->id : null,
                    'tracking_id' => '',
                    'type' => $request->type,
                    'name' => $request->store_name,
                    'owner_name' => $request->owner_name,
                    'address' => $request->address,
                    'mobile'  => $request->mobile,
                    'email' => $request->email,
                    'city' => $request->city,
                    'district' => $request->district,
                    'state_id' => $request->state_id,
                    'pincode' => $request->pincode,
                    'password' => $request->password,
                    'status' => StoreRequest::PENDING,
                    'gst_number' => $request->gst_number
                ]);

                return $store;
            });

            \Session::put('registered_store', $store);

            return redirect()->route('store-register-thanks')->with([
                'success' => 'Your Registration has been successfully completed!',
            ]);
        }

        return view('store-manager.register.index',[
            'states' => State::active()->get(),
        ]);
    }

    public function thanks(Request $request){

        if (!\Session::has('registered_store')) {
            return redirect()->route('store-register-request')->with(['error' => 'Session is expired.']);
        }

        $store_request = StoreRequest::whereId(\Session::get('registered_store')->id)->first();

        \Session::forget('registered_store');

        return view('store-manager.register.thanks', [
            'store_request' => $store_request
        ]);
    }
}
