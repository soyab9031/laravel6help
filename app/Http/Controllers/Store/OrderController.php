<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductPrice;
use App\Models\Setting;
use App\Models\StoreManager\Offer;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreStockTransaction;
use App\Models\StoreManager\StoreSupplyDetail;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Knp\Snappy\Pdf;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::whereStoreId(\Session::get('store')->id)->search($request->search, [
            'customer_order_id', 'user.tracking_id'
        ])->with(['user.detail'])->filterDate($request->dateRange);

        if($request->delivery_status)
            $orders= $orders->where('delivery_status', $request->delivery_status);

        if($request->status)
            $orders = $orders->where('status', $request->status);

        return view('store-manager.orders.index', [
            'orders' => $orders->latest()->paginate(30)
        ]);
    }

    public function detail(Request $request)
    {
        $order = Order::with(['user.detail', 'details.product_price.product','store'])->whereCustomerOrderId($request->customer_order_id)->first();

        if (!$order)
            return redirect()->back()->with(['error' => 'Invalid Order Details']);

        if ($request->download) {

            $setting = Setting::where('name','Company Profile')->first();

            $filename =  $order->customer_order_id;

            $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

            $snappy->generateFromHtml(
                view('store-manager.orders.invoice', [
                    'filename' => $filename, 'order' => $order, 'setting' => $setting
                ])->__toString(), storage_path("app/".$filename.".pdf"), [
                'orientation' => 'Portrait',
                'page-height' => 297,
                'page-width'  => 210,
            ], true);

            return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);

        }

        if ($request->isMethod('post'))
        {
            if ($request->status == Order::APPROVED)
            {

                $product_price_ids = $order->details->pluck('product_price_id')->toArray();

                /* Stock Check Start */
                $exist_stock_items = StoreStockTransaction::withBalance()
                    ->whereIn('product_price_id', $product_price_ids)
                    ->whereStoreId(\Session::get('store')->id)->get();

                $item_not_exists = collect($order->details)->groupBy('product_price_id')
                    ->filter(function ($order_items, $product_price_id) use ($exist_stock_items) {

                        $current_item = collect($exist_stock_items)->where('product_price_id', $product_price_id)->first();

                        if (!$current_item)
                            return !$current_item;

                        return collect($order_items)->where('product_price_id', $product_price_id)->sum('qty') > $current_item->balance;
                    })->first();

                if ($item_not_exists) {
                    return redirect()->route('store-order-view')
                        ->with(['error' => collect($item_not_exists)->first()->product_price->product->name . ' has not enough Stock']);
                }

                /* Stock Check End */
                \DB::transaction(function () use ($request,$order) {
                    $order->approved_at = now();
                    $order->status = $request->status;
                    $order->payment_status = Order::PAYMENT_SUCCESS;
                    $order->save();

                    $order->details->map(function ($order_detail) use ($order) {

                        $order->store->stockDebit(collect([
                            'order_id' => $order->id,
                            'product_price_id' => $order_detail->product_price_id,
                            'qty' => $order_detail->qty,
                            'remarks' => 'Stock Debit Against Approve Order Request ' . $order->customer_order_id
                        ]));
                    });
                });

                return redirect()->route('store-order-view')->with(['success' => 'your order status has been updated']);

            }

            if ($request->status == Order::REJECTED)
            {
                $order->update([
                    $order->payment_status = Order::PAYMENT_PENDING,
                    $order->status = $request->status,
                    $order->save()
                ]);
            }


            return redirect()->route('store-order-view')->with(['success' => 'your order status has been updated']);
        }

        return view('store-manager.orders.details', [
            'order' => $order,
        ]);
    }

    public function delivery(Request $request)
    {
        $order = Order::whereCustomerOrderId($request->customer_order_id)->first();

        if(!$order)
            return redirect()->route('store-order-view')->with(['error' => 'Order Not Found.']);

        $order->delivery_status = Order::PRODUCT_DELIVERED;
        $order->save();

        return redirect()->route('store-order-view')->with(['success' => 'Order Delivery Status updated successfully']);
    }

    public function create(Request $request)
    {
        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        if ($request->isMethod('post'))
        {
            $validator = \Validator::make($request->all(), [
                'user_id' => 'required|exists:users,id',
                'order_items' => 'required',
            ], [
                'user_id.required' => 'User Tracking ID is required',
                'user_id.exists' => 'Invalid User',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Order']);

            /* Stock Check Start */
            $exist_stock_items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
                ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
                ->whereIn('product_price_id', collect($order_items)->pluck('id')->toArray())
                ->whereStoreId(\Session::get('store')->id)->get();

            $item_not_exists = collect($order_items)->filter(function ($order_item) use ($exist_stock_items) {

                $current_item = collect($exist_stock_items)->where('product_price_id', $order_item->id)->first();

                if (!$current_item)
                    return !$current_item;

                return $order_item->selected_qty > $current_item->balance;
            })->first();

            if ($item_not_exists) {
                return response()->json(['status' => false, 'message' => $item_not_exists->name . ' has not enough Stock, Remove This Item or Try again']);
            }

            /* Stock Check End */

            $order = \DB::transaction(function () use ($form_request, $order_items) {

                $order = Order::create([
                    'user_id' => $form_request->user_id,
                    'store_id' => \Session::get('store')->id,
                    'offer_id' => $form_request->offer_id,
                    'amount' => $form_request->order_details->amount,
                    'wallet' => 0,
                    'discount' => $form_request->order_details->discount,
                    'total' => $form_request->order_details->total_amount,
                    'total_bv' => $form_request->order_details->total_bv,
                    'status' => Order::APPROVED,
                    'payment_status' => Order::PAYMENT_SUCCESS,
                    'approved_at' => Carbon::now(),
                ]);
                ;
                $order->customer_order_id = 'O' . $order->created_at->format('mdy') . $order->id;
                $order->save();

                collect($order_items)->map(function ($order_item) use ($order) {

                    OrderDetail::create([
                        'order_id' => $order->id,
                        'product_price_id' => $order_item->id,
                        'price' => $order_item->price,
                        'distributor_price' => $order_item->distributor_price,
                        'bv' => $order_item->bv,
                        'selling_price' => $order_item->selling_price,
                        'qty' => $order_item->selected_qty,
                        'gst' => json_encode([
                            'percentage' => $order_item->tax_percentage,
                            'code' => $order_item->tax_code,
                        ])
                    ]);

                    \Session::get('store')->stockDebit(collect([
                        'order_id' => $order->id,
                        'product_price_id' => $order_item->id,
                        'qty' => $order_item->selected_qty
                    ]));

                });

                return $order;

            });

            $request->session()->flash('success', 'New Order Created, Order ID:  ' . $order->customer_order_id);

            return response()->json(['status' => true, 'route' => route('store-order-detail', ['customer_order_id' => $order->customer_order_id])]);
        }

        $items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')
            ->havingRaw($balance_query . '> 0')
            ->whereStoreId(\Session::get('store')->id)
            ->with(['product_price.product:id,name'])->get()->map(function ($transaction) {

                return [
                    'value' => $transaction->product_price->id,
                    'label' => $transaction->product_price->product->name . ' (' . $transaction->product_price->code . ')',
                    'id' => $transaction->product_price->id,
                    'code' => $transaction->product_price->code,
                    'name' => $transaction->product_price->product->name,
                    'selected_qty' => 0,
                    'bv' => $transaction->product_price->points,
                    'barcode' =>  $transaction->product_price->barcode,
                    'balance' => $transaction->balance,
                    'price' => $transaction->product_price->price,
                    'distributor_price' => $transaction->product_price->distributor_price,
                    'selling_price' => $transaction->product_price->distributor_price,
                    'tax_percentage' => $transaction->product_price->gst->percentage,
                    'tax_code' => $transaction->product_price->gst->code,
                ];

            });


        return view('store-manager.orders.create', [
            'items' => $items,
            'active_offers' => (new Offer())->loadActiveOffers()
        ]);
    }

    public function loadOfferItems(Request $request)
    {
        if (!$offer = Offer::active()->whereId($request->offer_id)->first()) {
            return response()->json(['status' => false, 'message' => 'Invalid Offer Details, try again or contact support team']);
        }

        $item_ids = collect($offer->details->offer_items)->pluck(['product_price_id'])->toArray();

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        $store_stocks = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')
            ->havingRaw($balance_query . '> 0')
            ->whereStoreId(\Session::get('store')->id)
            ->whereIn('product_price_id', $item_ids)
            ->get();

        $items = ProductPrice::with(['product'])->whereIn('id', $item_ids)->active()->get()->map( function ($product_price) use ($offer, $store_stocks, $request) {

            $current_stock = collect($store_stocks)->where('product_price_id', $product_price->id)->first();

            $current_item = collect($offer->details->offer_items)->where('product_price_id', $product_price->id)->first();

            $item_qty = 1;
            $group_id = null;

            if ($request->condition_item) {

                $condition_item = Helper::arrayToObject($request->condition_item);
                $qty_multiplier = floor($condition_item->selected_qty / $condition_item->required_qty);

                $item_qty = $qty_multiplier * ($current_item ? $current_item->qty : 1);
            }
            else {

                $current = collect($offer->details->offer_items)->where('product_price_id', $product_price->id)->first();

                if ($current) {
                    $item_qty = $current->qty;
                    $group_id = $current->group_id;
                }

            }

            return [
                'id' => $product_price->id,
                'offer_id' => $offer->id,
                'group_id' => $group_id,
                'code' => $product_price->code,
                'barcode' => $product_price->barcode,
                'name' => $product_price->product->name,
                'selected_qty' => $item_qty,
                'bv' => 0,
                'balance' => $current_stock ? $current_stock->balance : 0,
                'price' => $product_price->price,
                'distributor_price' => $product_price->distributor_price,
                'selling_price' => 0,
                'tax_percentage' => $product_price->gst->percentage,
                'tax_code' => $product_price->gst->code,
            ];

        })->toArray();

        return response()->json(['status' => true, 'items' => $items]);
    }
}
