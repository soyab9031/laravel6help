<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Models\Faq;
use App\Models\Setting;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        if (!$setting = Setting::whereName($request->name)->first())
            return redirect()->back()->with(['error' => 'Invalid Setting Module']);

        if ($request->isMethod('post'))
        {
            $request_values = Helper::arrayToObject(array_except($request->all(), ['_token']));

            $setting->value = collect($setting->value)->map(function ($current_value) use ($request_values) {

                $request_value = collect($request_values)->filter(function ($request_value, $key) use ($current_value) {
                    return $key == $current_value->title;
                })->first();

                if ($request_value)
                {
                    $current_value->value = $request_value;
                }

                return $current_value;

            })->toJson();

            $setting->save();

            return redirect()->route('admin-system-settings', ['name' => $setting->name])->with(['success' => 'New Settings have been updated']);
        }

        return view('admin.setting.index', [
            'setting' => $setting
        ]);
    }

    public function faqsView()
    {
        return view('admin.setting.faqs.index', [
            'faqs' => Faq::paginate(30)
        ]);
    }

    public function faqsCreate(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'question' => 'required|min:10',
                'answer' => 'required|min:10',
            ]);

            Faq::create([
                'admin_id' => \Session::get('admin')->id,
                'question' => $request->question,
                'answer' => $request->answer,
            ]);

            return redirect()->route('admin-setting-faq-view')->with(['success' => 'New FAQ has been created..!!']);
        }

        return view('admin.setting.faqs.create');
    }

    public function faqsUpdate(Request $request)
    {
        $faq = Faq::whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'question' => 'required|min:10',
                'answer' => 'required|min:10',
            ]);

            $faq->question = $request->question;
            $faq->answer = $request->answer;
            $faq->status = $request->status;
            $faq->save();

            return redirect()->route('admin-setting-faq-view')->with(['success' => 'FAQ is updated..!!']);
        }

        return view('admin.setting.faqs.update', [
            'faq' => $faq
        ]);
    }
}
