<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\DocumentExport;
use App\Models\UserDetail;
use App\Models\UserDocument;
use App\Models\UserStatus;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DocumentController extends Controller
{

    public function index(Request $request)
    {
        $documents = UserDocument::with(['user.detail'])
            ->search($request->search, [
                'user.tracking_id', 'user.detail.first_name', 'user.detail.last_name'
            ])->filterDate($request->dateRange);

        if($request->status)
            $documents->whereStatus($request->status);

        if ($request->export)
        {
            if (empty($request->status) && empty($request->dateRange) && empty($request->package_id) && empty($request->search))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            return \Excel::download(new DocumentExport($documents->get()), 'documents.xlsx');
        }

        return view('admin.document.index',[
            'documents' => $documents->orderBy('id','desc')->paginate(50)
        ]);
    }


    public function update(Request $request)
    {
        $document = UserDocument::with(['user.detail','user.bank'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'status' => 'required'
            ],[
                'status.required' => 'Status is Required'
            ]);

            \DB::transaction(function () use ($document, $request) {

                if($request->status == UserDocument::PENDING)
                    $status = UserStatus::KYC_INPROGRESS;
                elseif($request->status == UserDocument::VERIFIED)
                    $status = UserStatus::KYC_VERIFIED;
                else
                    $status = UserStatus::KYC_REJECTED;

                $document->status = $request->status;
                $document->remarks = $request->remarks;
                $document->save();

                if ($document->type == UserDocument::PAN_CARD)
                {
                    UserStatus::whereUserId($document->user_id)->update(['pancard' => $status]);

                    if ($document->number)
                        UserDetail::whereUserId($document->user_id)->update(['pan_number' => $document->number]);
                }

                if (in_array($document->type, [UserDocument::VOTER_ID, UserDocument::ELECTRICITY_BILL, UserDocument::DRIVING_LICENCE, UserDocument::PASSPORT]))
                    UserStatus::whereUserId($document->user_id)->update(['address' => $status]);

                if ($document->type == UserDocument::AADHAR_CARD)
                    UserStatus::whereUserId($document->user_id)->update(['aadhar_card' => $status]);

                if (in_array($document->type, [UserDocument::BANK_PASSBOOK, UserDocument::CANCEL_CHEQUE]))
                    UserStatus::whereUserId($document->user_id)->update(['bank_account' => $status]);

            });

            return redirect()->route('admin-document-view')->with(['success' => 'Document Update has been successfully..']);

        }

        return view('admin.document.update', [
            'document' => $document
        ]);
    }


}
