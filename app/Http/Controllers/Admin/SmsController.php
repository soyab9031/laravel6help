<?php

namespace App\Http\Controllers\Admin;

use App\Models\SmsTransaction;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SmsController extends Controller
{
    public function index(Request $request)
    {
        $transactions = SmsTransaction::with(['user.detail:user_id,title,first_name,last_name'])->search($request->search, [
            'user.tracking_id', 'user.mobile', 'user.username'
        ])->filterDate($request->dateRange);

        if ($request->category)
            $transactions = $transactions->where('category', $request->category);

        $categories = SmsTransaction::groupBy('category')->select('category')->get()->pluck('category')->toArray();

        return view('admin.sms-module.transactions', [
            'transactions' => $transactions->orderBy('id','desc')->paginate(50),
            'categories' => $categories
        ]);
    }

    public function sender(Request $request)
    {
        return view('admin.sms-module.sender');
    }
}
