<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->parent_id))
            $categories = Category::whereParentId($request->parent_id)->with(['parent'])->orderBy('id', 'desc')->paginate(30);
        else
            $categories = Category::whereNull('parent_id')->with(['parent'])->orderBy('id', 'desc')->paginate(30);

        return view('admin.product.category.index', ['categories' => $categories]);

    }
    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|unique:categories,name',
                'image' => 'required|mimes:jpeg,jpg,png|max:2000',
                'description' => 'required'
            ],[
                'name.unique' => 'Category Name is already exists, try different name',
                'image.required' => 'Image is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB',
                'description.required' => 'Description is required'
            ]);


            if($request->image) {
                if ($request->hasFile('image') && $request->file('image')->isValid()) {

                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . ".".$request->file('image')->getClientOriginalExtension();

                    $disk->delete(env('CATEGORY_IMAGE_PATH'). $request->image);
                    $disk->put(env('CATEGORY_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                }
            }
            Category::create([
                'parent_id' => $request->parent_category_id ? $request->parent_category_id : null,
                'name' => $request->name,
                'image' => $image,
                'description' => $request->description,
                'slug' => \Str::slug($request->name, '-'),
            ]);

            return redirect()->route('admin-category-view')->with(['success' => 'New Category has been created..!!']);
        }

        return view('admin.product.category.create', [
            'categories' => Category::whereNull('parent_id')->get()
        ]);
    }
    public function update(Request $request)
    {
        if (!$category = Category::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Category Request Id, Try Again']);

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|unique:categories,name,' . $category->id,
                'status' => 'required',
                'description' => 'required'
            ], [
                'name.unique' => 'Category Name is already exists, try different name',
                'description.required' => 'Description is required'
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->delete(env('CATEGORY_IMAGE_PATH'). $request->image);
                    $disk->put(env('CATEGORY_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                    $category->image = $image;
                }
            }

            $category->name =  $request->name;
            $category->description = $request->description;
            $category->status =  $request->status;
            $category->save();

            return redirect()->route('admin-category-view')->with(['success' => $category->name . ' Category Update Successfully']);

        }
        return view('admin.product.category.update',[
            'category' => $category
        ]);
    }

    public function apiGetCategories(Request $request)
    {
        if (!Category::whereNull('parent_id')->whereId($request->category_id)->exists())
            return response()->json(['status' => false, 'message' => 'Category not exists']);

        $categories = Category::selectRaw('id as value, name as label')->whereParentId($request->category_id)
            ->active()->get()->toArray();

        return response()->json(['status' => true,
            'categories' => $categories,
        ]);
    }
}
