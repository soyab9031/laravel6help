<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\State;
use App\Models\StoreManager\Supplier;
use App\Rules\GstNumber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        $suppliers = Supplier::search($request->search, [
            'name', 'gst_number', 'city'
        ])->filterDate($request->dateRange)->with('state')->paginate(20);

        return view('admin.store-manager.supplier.index', [
            'suppliers' => $suppliers
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'other_contact' => 'numeric|nullable',
                'email' => 'required|email',
                'address' => 'required',
                'gst_number' => ['required', new GstNumber],
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|numeric|digits:6',
            ],[
                'name.required' => 'required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.email' => 'Please email is giving in Proper format',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'state_id.required' => 'state is required',
                'pincode.required' => 'pin_code is required',
                'gst_number.required' => 'Gst Number is required',
            ]);

            Supplier::create([
                'admin_id' => \Session::get('admin')->id,
                'name' => $request->name,
                'mobile' => $request->mobile,
                'other_contact' => $request->other_contact,
                'email' => $request->email,
                'address' => $request->address,
                'city' => $request->city,
                'pincode' => $request->pincode,
                'state_id' => $request->state_id,
                'gst_number' => $request->gst_number
            ]);

            return redirect()->route('admin-store-manager-supplier-view')->with(['success' => 'New Supplier Create Successfully']);
        }

        return view('admin.store-manager.supplier.create', [
            'states' => State::active()->get()
        ]);
    }

    public function update(Request $request)
    {
        if (!$supplier = Supplier::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Supplier Data, Try Again']);

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'other_contact' => 'numeric|nullable',
                'email' => 'required|email',
                'address' => 'required',
                'gst_number' => ['required', new GstNumber],
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|numeric|digits:6',
            ],[
                'name.required' => 'required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.email' => 'Please email is giving in Proper format',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'state_id.required' => 'state is required',
                'pincode.required' => 'pin_code is required',
                'gst_number.required' => 'Gst Number is required',
            ]);

            $supplier->name = $request->name;
            $supplier->mobile = $request->mobile;
            $supplier->other_contact = $request->other_contact;
            $supplier->email = $request->email;
            $supplier->address = $request->address;
            $supplier->city = $request->city;
            $supplier->pincode = $request->pincode;
            $supplier->state_id = $request->state_id;
            $supplier->gst_number = $request->gst_number;
            $supplier->status = $request->status;
            $supplier->save();

            return redirect()->route('admin-store-manager-supplier-view')->with(['success' => 'Supplier is updated']);
        }

        return view('admin.store-manager.supplier.update', [
            'states' => State::active()->get(),
            'supplier' => $supplier
        ]);
    }
}
