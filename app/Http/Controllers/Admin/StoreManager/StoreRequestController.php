<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreRequest;
use Illuminate\Http\Request;

class StoreRequestController extends Controller
{
    public function index(Request $request)
    {
        $store_requests = StoreRequest::with(['state'])->search($request->search, [
            'name', 'mobile', 'owner_name'
        ])->where('status', '<>', StoreRequest::APPROVED)->filterDate($request->dateRange);

        if($request->status) {
            $store_requests = $store_requests->whereStatus($request->status);
        }

        return view('admin.store-manager.store.registration-request.index', [
            'store_requests' => $store_requests->latest()->paginate(30)
        ]);
    }

    public function details(Request $request)
    {
        $store_request = StoreRequest::with(['state'])->whereId($request->id)->where('status', '<>', StoreRequest::APPROVED)->firstOrFail();

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state_id' => 'required_if:country_id,99',
                'country_id' => 'required',
                'pincode' => 'required|digits:6',
                'email' => 'nullable|email',
                'mobile' => 'required|numeric',
            ], [
                'name.required' => 'Franchisee Name is Required',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'country_id.required' => 'Country is Required',
                'pincode.required' => 'Pincode is Required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'state_id.required_if' => 'State is required for India'
            ]);

            $store_request->name = $request->name;
            $store_request->owner_name = $request->owner_name;
            $store_request->password = !empty($request->password) ? $request->password : $store_request->password;
            $store_request->email = $request->email;
            $store_request->mobile = $request->mobile;
            $store_request->type = $request->type;
            $store_request->address = $request->address;
            $store_request->city = $request->city;
            $store_request->pincode = $request->pincode;
            $store_request->state_id = $request->state_id;
            $store_request->country_id = $request->country_id;
            $store_request->status = $request->status;
            $store_request->gst_number = $request->gst_number;
            $store_request->save();

            if ($request->status == StoreRequest::APPROVED) {

                $tracking_id = Store::generateTrackingId($store_request->type);

                while(Store::whereTrackingId($tracking_id)->exists()) {
                    $tracking_id = Store::generateTrackingId($store_request->type);
                }

                Store::create([
                    'tracking_id' => $tracking_id,
                    'parent_id' => $store_request->sponsor_user_id,
                    'name' => $store_request->name,
                    'owner_name' => $store_request->owner_name,
                    'password' => $store_request->password,
                    'email' => $store_request->email,
                    'mobile' => $store_request->mobile,
                    'type' => $store_request->type,
                    'address' => $store_request->address,
                    'city' => $store_request->city,
                    'pincode' => $store_request->pincode,
                    'state_id' => $store_request->state_id,
                    'country_id' => $store_request->country_id,
                    'gst_number' => $store_request->gst_number
                ]);

                return redirect()->route('admin-store-manager-store-view')->with(['success' => 'Store is created successfully']);

            }

            return redirect()->route('admin-store-manager-store-register-request')->with(['success' => 'Details is updated']);

        }

        return view('admin.store-manager.store.registration-request.details', [
            'store_request' => $store_request,
            'states' => State::get(),
            'countries' => Country::get()
        ]);
    }
}
