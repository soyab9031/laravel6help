<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductPrice;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreSupply;
use App\Models\StoreManager\StoreWalletTransaction;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function supplyTransfer(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $supplies = StoreSupply::whereNotNull('admin_id')->selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at, count(id) as supply_count')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();

        $records = collect($period)->map(function ($date) use ($supplies, $request) {

            $day_turnover = collect($supplies)->filter(function ($supply) use ($date) {
                return $supply->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => Carbon::parse($date),
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
                'supply_count' => $day_turnover ? $day_turnover->supply_count : 0,
            ];

        })->sortByDesc('date')->values();

        return view('admin.store-manager.reports.supply-transfer',[
            'records' => $records
        ]);
    }

    public function storeSales(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $store_balances = StoreWalletTransaction::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN amount END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN amount END),0) as balance, store_id')
            ->groupBy('store_id')->where('status',StoreWalletTransaction::ACTIVE)->get();

        $orders =  Order::selectRaw('sum(amount) as total_amount, count(id) as order_count, store_id')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->whereNotNull('store_id')->groupBy('store_id')->get();

        $stores = Store::select(['id','tracking_id', 'name'])->get()->map(function ($store) use ($orders, $store_balances) {

            $store_order_details = collect($orders)->where('store_id', $store->id)->first();
            $store_wallet_balance = collect($store_balances)->where('store_id', $store->id)->first();

            $store->order_details = $store_order_details ? $store_order_details : (object) ['total_amount' => 0, 'order_count' => 0];
            $store->wallet = $store_wallet_balance ? $store_wallet_balance->balance : 0;

            return $store;

        });

        return view('admin.store-manager.reports.store-sales',[
            'stores' => $stores,
            'start_at' => $start_at,
            'end_at' => $end_at
        ]);
    }

    public function productRanking(Request $request)
    {
        $orders = OrderDetail::select(\DB::raw('SUM(qty) as total, product_price_id'))->whereHas('order', function ($q) {
            $q->whereNotNull('approved_at');
        })->groupBy('product_price_id')->filterDate($request->dateRange)->get();

        $items = ProductPrice::with(['product:id,name'])->get()->map( function ($product_price) use ($orders) {

            $sold_item = collect($orders)->filter( function ($order) use ($product_price) {
                return $order->product_price_id == $product_price->id;
            })->first();

            $product_price->sold_qty = $sold_item ? $sold_item->total : 0;
            return $product_price;

        })->sortByDesc( function ($item) {
            return $item->sold_qty;
        })->values();


        return view('admin.store-manager.reports.product-ranking',[
            'items' => $items
        ]);
    }
}
