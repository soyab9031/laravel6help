<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\PinExport;
use App\Models\Package;
use App\Models\PackageOrder;
use App\Models\Pin;
use App\Models\PinPaymentDetail;
use App\Models\PinRequest;
use App\Models\PinTransferHistory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PinController extends Controller
{
    public function index(Request $request)
    {
        $pins = Pin::with([
            'package', 'user.detail', 'package_order.user.detail'
        ])->search($request->search, [
            'number', 'amount', 'user.tracking_id', 'user.username', 'user.mobile'
        ])->filterDate($request->dateRange);

        if ($request->status) {
            if ($request->status == 'unused')
                $pins = $pins->whereStatus(Pin::UNUSED);
            elseif ($request->status == 'used')
                $pins = $pins->whereStatus(Pin::USED);
            elseif ($request->status == 'blocked')
                $pins = $pins->whereStatus(Pin::BLOCKED);
        }

        if (!empty($request->package_id))
            $pins = $pins->wherePackageId($request->package_id);

        if ($request->export) {
            if (empty($request->status) && empty($request->dateRange) && empty($request->package_id) && empty($request->search))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            return \Excel::download(new PinExport($pins->get()), 'pins.xlsx');
        }

        return view('admin.pin-module.index', [
            'pins' => $pins->orderBy('id', 'desc')->paginate(30),
            'packages' => Package::active()->get()
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $package = Package::whereId($request->package_id)->first();

            $this->validate($request, [
                'pin_qty' => 'required|numeric',
                'package_id' => 'required',
                'user_id' => 'required|exists:users,id',
                'payment_mode' => 'required',
                'bank_name' => 'required|nullable',
                'payment_at' => 'required',
            ], [
                'pin.required' => 'Number of Pins is Required',
                'pin.numeric' => 'Number of Pins is Must be Numeric',
                'package_id.required' => 'Package is Required',
                'user_id.required' => 'User is Required',
                'user_id.exists' => 'Invalid Tracking Id of Searched User',
            ]);

            \DB::transaction(function () use ($request, $package) {

                $payment_detail = PinPaymentDetail::create([
                    'payment_mode' => $request->payment_mode,
                    'qty' => $request->pin_qty,
                    'bank_name' => $request->bank_name,
                    'payment_at' => Carbon::parse($request->payment_at),
                    'remarks' => $request->remarks
                ]);

                for ($i = 0; $i < $request->pin_qty; $i++) {
                    Pin::create([
                        'admin_id' => \Session::get('admin')->id,
                        'pin_payment_id' => $payment_detail->id,
                        'package_id' => $package->id,
                        'number' => strtoupper(\Str::random(15)),
                        'amount' => $package->amount,
                        'user_id' => $request->user_id
                    ]);
                }
            });

            return redirect()->route('admin-pin-view')->with(['success' => 'Pins has been Generated.']);
        }

        return view('admin.pin-module.generate', [
            'packages' => Package::active()->get(),
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))
        ]);
    }

    public function update(Request $request)
    {
        $pin = Pin::with(['user.detail', 'package'])->whereId($request->id)->first();

        if ($request->isMethod('post') && $request->status != 1) {
            $pin->status = $request->status;
            $pin->save();

            return redirect()->route('admin-pin-view')->with([
                'success' => 'Pin Status has been Updated'
            ]);
        }

        return view('admin.pin-module.update', ['pin' => $pin]);

    }

    public function transfer_history(Request $request)
    {
        $pin = Pin::with(['package', 'user.detail', 'admin'])->whereId($request->id)->first();

        $transfer_histories = PinTransferHistory::wherePinId($request->id)->with(['sender.detail', 'receiver.detail'])->get();

        return view('admin.pin-module.history', [
            'pin' => $pin,
            'transfer_histories' => $transfer_histories
        ]);

    }

    public function requestView(Request $request)
    {
        $pinRequests = PinRequest::with(['package', 'user.detail'])->search($request->search, [
            'reference_number', 'user.tracking_id', 'user.mobile', 'user.detail.first_name', 'user.detail.last_name'
        ])->filterDate($request->dateRange);

        if ($request->status) {
            if ($request->status == 1)
                $pinRequests = $pinRequests->whereStatus(PinRequest::PENDING);
            elseif ($request->status == 2)
                $pinRequests = $pinRequests->whereStatus(PinRequest::APPROVED);
            elseif ($request->status == 3)
                $pinRequests = $pinRequests->whereStatus(PinRequest::REJECTED);
        }

        if ($request->package) {
            $pinRequests = $pinRequests->where('package_id', $request->package);
        }

        $packages = Package::active()->get();

        return view('admin.pin-module.request.index', [
            'pin_requests' => $pinRequests->orderBy('id', 'desc')->paginate(30),
            'packages' => $packages
        ]);
    }

    public function requestUpdate(Request $request)
    {
        $pin_request = PinRequest::with(['package', 'user.detail'])->whereId($request->id)->first();

        if ($request->isMethod('post')) {
            $pin_request->status = $request->status;
            $pin_request->remarks = $request->remarks;
            $pin_request->save();

            return redirect()->route('admin-pin-request-view')->with(['success' => 'Pin Request has been Updated..']);
        }

        return view('admin.pin-module.request.update', [
            'pin_request' => $pin_request
        ]);
    }

    public function usePin(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'pin_id' => 'required|exists:pins,id'
        ], [
            'user_id.required' => 'Tracking ID is required',
            'user_id.exists' => 'Invalid Tracking ID or User is not exist',
            'pin_id.required' => 'Package PIN is required',
            'pin_id.exists' => 'Invalid Package PIN ID',
        ]);

        $pin = Pin::with('package')->whereId($request->pin_id)->first();

        if ($pin->status == Pin::USED)
            return redirect()->back()->with('error', 'Pin ' . $pin->number . ' is already Used !');

        if ($pin->status == Pin::BLOCKED)
            return redirect()->back()->with('error', 'Pin ' . $pin->number . ' is blocked, You can not use it');

        $user = User::with('package')->whereId($request->user_id)->first();

        if ($user->package_id && ($pin->amount <= $user->package->amount))
            return redirect()->back()->with(['error' => 'You cannot upgrade with same or lower package ']);

        \DB::transaction(function () use ($pin, $request, $user) {

            $pin->status = Pin::USED;
            $pin->save();

            PackageOrder::create([
                'user_id' => $request->user_id,
                'admin_id' => \Session::get('admin')->id,
                'pin_id' => $pin->id,
                'package_id' => $pin->package_id,
                'pv' => $pin->package->pv,
                'amount' => $pin->package->amount,
                'pv_calculation' => PackageOrder::NO
            ]);

            User::whereId($request->user_id)->update([
                'pin_id' => $pin->id,
                'package_id' => $pin->package_id,
                'joining_amount' => $pin->package->amount,
                'paid_at' => $user->paid_at ?: now()
            ]);

        });

        return redirect()->route('admin-pin-view')->with(['success' => 'User`s account has been upgraded & Pin is used..!!']);
    }


}
