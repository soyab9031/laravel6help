<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\UsersExport;
use App\Models\Package;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $users = User::with(['detail','sponsorBy.detail', 'parentBy.detail', 'package', 'status', 'address.state'])->search($request->search, [
            'tracking_id', 'username', 'detail.first_name', 'detail.last_name', 'mobile', 'email'
        ])->filterDate($request->dateRange);

        if($request->package_id)
            $users = $users->where('package_id', $request->package_id);

        if ($request->bank_status)
        {
            $users = $users->whereHas('status', function ($q) use ($request) {
                $q->where('bank_account', $request->bank_status);
            });
        }

        if ($request->pan_card_status)
        {
            $users = $users->whereHas('status', function ($q) use ($request) {
                $q->where('pancard', $request->pan_card_status);
            });
        }

        if($request->user_status) {
            $users = $request->user_status == 'active' ? $users->whereNotNull('paid_at') : $users->whereNull('paid_at');
        }

        if($request->download == 'yes')
        {
            if (empty($request->dateRange))
                return back()->with(['error' => 'Select Date filter to export the users']);

            $users = $users->latest()->get();

            return \Excel::download(new UsersExport($users), 'users.xlsx');
        }

        return view('admin.user.index', [
            'users' => $users->orderBy('id', 'desc')->paginate(50),
            'packages' => Package::get()
        ]);
    }

    public function update(Request $request)
    {
        if (!$user = User::with('detail','status')->whereId($request->id)->first())
            return redirect()->back();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'birth_date' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'email' => 'email|nullable',
                'password' => 'required|min:6',
                'wallet_password' => 'required|min:6',
                'pincode' => 'nullable|digits:6',
            ], [
                'title.required' => 'Title is Required',
                'first_name.required' => 'First Name is Required',
                'last_name.required' => 'Last Name is Required',
                'gender.required' => 'Gender is Required',
                'birth_date.required' => 'Date of Birth is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.email' => 'Invalid Email Address',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'wallet_password.required' => 'Wallet Password is required',
                'wallet_password.min' => 'Wallet Password length should be 6 or more',
                'pincode.digits' => 'Pincode Must be 6 digit number',
            ]);

            $user->password = $request->password;
            $user->wallet_password = $request->wallet_password;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->save();

            if ($request->bank_account_name && $request->bank_account_number)
            {
                $user_bank = UserBank::whereUserId($user->id)->first();
                $user_bank->bank_name = $request->bank_name;
                $user_bank->account_name = $request->bank_account_name;
                $user_bank->account_number = $request->bank_account_number;
                $user_bank->branch = $request->bank_branch;
                $user_bank->ifsc = $request->bank_ifsc;
                $user_bank->type = $request->bank_account_type;
                $user_bank->save();
            }

            $user_detail = UserDetail::whereUserId($user->id)->first();
            $user_detail->title = $request->title;
            $user_detail->first_name = $request->first_name;
            $user_detail->last_name = $request->last_name;
            $user_detail->gender = $request->gender;
            $user_detail->birth_date = Carbon::parse($request->birth_date);
            $user_detail->nominee_name = $request->nominee_name;
            $user_detail->nominee_relation = $request->nominee_relation;
            $user_detail->nominee_birth_date = !empty($request->nominee_birth_date) ? Carbon::parse($request->nominee_birth_date) : null;
            $user_detail->save();

            $user_address = UserAddress::whereUserId($user->id)->whereType('1')->first();
            $user_address->address = $request->address;
            $user_address->landmark = $request->landmark;
            $user_address->city = $request->city;
            $user_address->district = $request->district;
            $user_address->state_id = $request->state_id ? : $user_address->state_id;
            $user_address->pincode = $request->pincode;
            $user_address->save();

            return redirect()->route('admin-user-update', ['id' => $user->id])->with(['success' => 'User Details has been updated']);

        }

        $states = State::active()->get();
        $nominees = \File::get(public_path('data/nominees.json'));

        return view('admin.user.update', [
            'user' => $user,
            'states' => $states,
            'nominees' => json_decode($nominees)
        ]);
    }

    public function blockedUsers(Request $request)
    {
        if ($request->blockRequest)
        {
            if (!$user = User::whereId($request->user_id)->exists())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            if (UserStatus::whereUserId($request->user_id)->whereTerminated(UserStatus::YES)->exists())
                return redirect()->back()->with(['error' => 'This user is already blocked']);

            $user_status = UserStatus::whereUserId($request->user_id)->first();
            $user_status->terminated = UserStatus::YES;
            $user_status->save();

            activity()->causedBy(\Session::get('admin'))->performedOn($user_status)
                ->inLog('Blocked')
                ->withProperties(collect([
                    'attributes'=> ['terminated' => UserStatus::YES],
                    'old' => ['terminated' => UserStatus::NO]
                ]))->log('User is blocked');

            return redirect()->route('admin-user-blocked-view')->with(['success' => 'User is added into Block List']);
        }

        if ($request->removeBlockRequest)
        {
            if (!$user = User::whereId($request->removeBlockRequest)->exists())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            $user_status = UserStatus::whereUserId($request->removeBlockRequest)->first();
            $user_status->terminated = UserStatus::NO;
            $user_status->save();

            activity()->causedBy(\Session::get('admin'))->performedOn($user_status)
                ->inLog('Unblock')
                ->withProperties(collect([
                    'attributes'=> ['terminated' => UserStatus::NO],
                    'old' => ['terminated' => UserStatus::YES]
                ]))->log('User has been removed from block List');

            return redirect()->route('admin-user-blocked-view')->with(['success' => 'User has been removed from block List']);
        }

        $users = User::with('detail')->whereHas('status', function ($q) {
            $q->where('terminated', 1);
        })->search($request->search, [
            'tracking_id', 'mobile'
        ])->orderBy('id', 'desc')->paginate(30);

        return view('admin.user.blocked', [
            'users' => $users
        ]);
    }

    public function paymentStoppedUsers(Request $request)
    {
        if ($request->paymentStopRequest)
        {
            if (!$user = User::whereId($request->user_id)->exists())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            if (UserStatus::whereUserId($request->user_id)->wherePayment(UserStatus::NO)->exists())
                return redirect()->back()->with(['error' => 'This user`s payment is already stopped']);

            $user_status = UserStatus::whereUserId($request->user_id)->first();
            $user_status->payment = UserStatus::NO;
            $user_status->save();

            activity()->causedBy(\Session::get('admin'))->performedOn($user_status)
                ->inLog('Payment Stopped')
                ->withProperties(collect([
                    'attributes'=> ['payment' => UserStatus::NO],
                    'old' => ['payment' => UserStatus::YES]
                ]))->log('User Payment Stopped');

            return redirect()->route('admin-user-payment-stop-view')->with(['success' => 'User`s Payment Process has been stopped']);
        }

        if ($request->removePaymentStopRequest)
        {
            if (!$user = User::whereId($request->removePaymentStopRequest)->exists())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            UserStatus::whereUserId($request->removePaymentStopRequest)->update(['payment' => 1]);

            $user_status = UserStatus::whereUserId($request->removePaymentStopRequest)->first();
            $user_status->payment = UserStatus::YES;
            $user_status->save();

            activity()->causedBy(\Session::get('admin'))->performedOn($user_status)
                ->inLog('Payment Continue')
                ->withProperties(collect([
                    'attributes'=> ['payment' => UserStatus::YES],
                    'old' => ['payment' => UserStatus::NO]
                ]))->log('User Payment Continue');

            return redirect()->route('admin-user-payment-stop-view')->with(['success' => 'User has been removed from Stop Payment List']);
        }

        $users = User::with('detail')->whereHas('status', function ($q) {
            $q->where('payment', 0);
        })->search($request->search, [
            'tracking_id', 'mobile'
        ])->orderBy('id', 'desc')->paginate(30);

        return view('admin.user.payment-stop', [
            'users' => $users
        ]);
    }

    public function accountAccess(Request $request)
    {
        if(!$user = User::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid User Details for View']);

        return redirect()->route('user-admin-view-access', ['id' => $user->id, 'token' => \Session::get('admin')->token]);
    }

    public function getUser(Request $request)
    {
        if (!isset($request->tracking_id))
            return response()->json(['status' => false, 'message' => 'Tracking ID is required']);

        if (!$user = User::with(['detail'])->whereTrackingId($request->tracking_id)->first())
            return response()->json(['status' => false, 'message' => 'User is not available']);

        return response()->json([
            'status' => true,
            'user' => [
                'id' => $user->id,
                'name' => $user->detail->full_name,
                'tracking_id' => $user->tracking_id
            ]
        ]);
    }
}
