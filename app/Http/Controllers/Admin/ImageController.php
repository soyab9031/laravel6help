<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Library\ImageUpload;
use App\Models\Banner;
use App\Models\Gallery;
use App\Models\GalleryItem;
use Illuminate\Http\Request;
use Storage;

use App\Http\Controllers\Controller;

class ImageController extends Controller
{

    public function viewPopup(Request $request)
    {
        return view('admin.image-manager.welcome-popup.popup-list', [
            'popups' =>  Banner::whereType(Banner::POPUP)->orderBy('updated_at','desc')->get()
        ]);
    }

    public function createPopup(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request ,[
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ],[
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB'
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->put(env('POPUP_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));

                    Banner::create([
                        'name' => $request->name,
                        'image' => $image,
                        'type' => 2,
                        'status' => $request->status,
                    ]);
                }
            }

            return redirect()->route('admin-popup-view')->with(['success' => 'New Popup has been Uploaded.']);
        }
        return view('admin.image-manager.welcome-popup.create');
    }

    public function updatePopup(Request $request)
    {
        $popup = Banner::whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request ,[
                'image' => 'mimes:jpeg,jpg,png|max:2000',
            ],[
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB',
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');

                    $image = (string) \Str::uuid() . "." . $request->file('image')->getClientOriginalExtension();

                    if($popup!=null)
                        $disk->delete(env('POPUP_IMAGE_PATH').$popup->image);

                    $disk->put(env('POPUP_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));

                    $popup->image = $image;
                }
            }
            $popup->name = $request->name;
            $popup->status = $request->status;

            $popup->save();

            return redirect()->route('admin-popup-view')->with(['success' => 'Popup has been Updated.']);

        }
        return view('admin.image-manager.welcome-popup.index',[
            'popup' => $popup
        ]);
    }

    public function createBanner(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request ,[
                'name' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ],[
                'name.required' => 'Image Title or Name is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB'
            ]);


            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . ".".$request->file('image')->getClientOriginalExtension();

                    $disk->delete(env('BANNER_IMAGE_PATH'). $request->image);
                    $disk->put(env('BANNER_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));

                    Banner::create([
                        'name' => $request->name,
                        'image' => $image,
                        'type' => 1,
                        'status' => $request->status,
                    ]);
                }
            }
            return redirect()->route('admin-banner-view')->with(['success' => 'New Banner has been Uploaded.']);
        }
        return view('admin.image-manager.primary-banner.create');
    }

    public function viewBanner()
    {
        $banner = Banner::whereType(1)->get();

        return view('admin.image-manager.primary-banner.index',[
            'banners' => $banner
        ]);
    }

    public function updateBanner(Request $request)
    {
        $banner = Banner::whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ],[
                'name.required' => 'Image Title or Name is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB'
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->delete(env('BANNER_IMAGE_PATH'). $request->image);
                    $disk->put(env('BANNER_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                    $banner->image = $image;
                }
            }

            $banner->name = $request->name;
            $banner->status = $request->status;
            $banner->save();

            return redirect()->route('admin-banner-view')->with(['success' => 'Banner has been Uploaded.']);
        }

        return view('admin.image-manager.primary-banner.update',[
            'banner' => $banner
        ]);
    }

    public function viewGallery()
    {
        return view('admin.image-manager.gallery.index', [
            'galleries' => Gallery::with('items')->orderBy('Id')->paginate(20)
        ]);

    }
    public function createGallery(Request $request)
    {
        if($request->isMethod('post'))
        {

            $this->validate($request ,[
                'primary_image' => 'mimes:jpeg,jpg,png|max:2000',
                'name' => 'required',
                'gallery_items.*.name' => 'required',
                'gallery_items.*.image' => 'required',
            ],[
                'name' => 'Gallery Title is Required',
                'primary_image.mimes' => 'Image must have .jpg,jpeg and png type',
                'primary_image.max' => 'Upload an image less then 2MB',
                'gallery_items.*.name:required' => 'Gallery Items name is Required',
                'gallery_items.*.image:required' => 'Gallery Items Images is Required'
            ]);

            if(!$request->hasFile('primary_image') || !$request->file('primary_image')->isValid())
                return redirect()->back()->withInput()->with(['error' => 'Not able to get primary Image, Try Again']);

            $primary_image = null;
            $image_uploader = (new ImageUpload());

            if(!$image_uploader->process($request->file('primary_image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Primary Image, Upload another image']);

            $primary_image = $image_uploader->store(env('GALLERY_IMAGE_PATH'), 'LOCAL');

            $gallery = Gallery::create([
                'name' => $request->name, 'primary_image' => $primary_image
            ]);

            $gallery_items = collect(Helper::arrayToObject($request->gallery_items))->map(function ($gallery_item) use ($gallery, $image_uploader) {

                if(!$gallery_item->image->isValid())
                    return false;

                if(!$image_uploader->process($gallery_item->image, ImageUpload::DOCUMENT_TYPE))
                    return false;

                $image = $image_uploader->store(env('GALLERY_IMAGE_PATH'), 'LOCAL');

                return GalleryItem::create([
                    'gallery_id' => $gallery->id, 'name' => $gallery_item->name, 'image' => $image,
                ]);


            });

            $gallery_items = $gallery_items->filter(function ($gallery_item) {
                return $gallery_item;
            });

            if (count($gallery_items) == 0) {
                return redirect()->back()->with(['error' => 'Not able to upload Other Gallery Images']);
            }


            return redirect()->route('admin-gallery-view')->with(['success' => 'New Gallery has been Created.']);
        }

        return view('admin.image-manager.gallery.create');
    }

    public function updateGallery(Request $request)
    {
        $gallery = Gallery::with('items')->whereId($request->id)->first();

        if ($request->delete)
        {
            if ($gallery_item = GalleryItem::whereId($request->delete)->whereGalleryId($gallery->id)->first())
            {
                $storage = Storage::disk('spaces');
                $storage->delete(env('GALLERY_IMAGE_PATH') . $gallery_item->image);
                $gallery_item->delete();

                return redirect()->back()->with(['success' => 'Gallery Item has been deleted']);
            }
        }

        if($request->isMethod('post'))
        {

            $this->validate($request, [
                'primary_image' => 'mimes:jpeg,jpg,png|max:2000',
                'name' => 'required',
                'gallery_items.*.name' => 'required',
                'gallery_items.*.image' => 'required',
            ],[
                'name' => 'Gallery Title is Required',
                'primary_image.mimes' => 'Image must have .jpg,jpeg and png type',
                'primary_image.max' => 'Upload an image less then 2MB',
                'gallery_items.*.name:required' => 'Gallery Items name is Required',
                'gallery_items.*.image:required' => 'Gallery Items Images is Required'
            ]);

            $image_uploader = (new ImageUpload());

            if($request->hasFile('primary_image') && $request->file('primary_image')->isValid())
            {
                if(!$image_uploader->process($request->file('primary_image'),ImageUpload::DOCUMENT_TYPE))
                    return redirect()->back()->with(['error' => 'Unable to fetch Primary Image, Upload another image']);

                $gallery->primary_image = $image_uploader->store(env('GALLERY_IMAGE_PATH'), 'LOCAL');
            }

            $gallery->name = $request->name;
            $gallery->save();

            if (isset($request->gallery_items)) {

                $gallery_items = collect(Helper::arrayToObject($request->gallery_items))->map(function ($gallery_item) use ($gallery, $image_uploader) {

                    if(!$gallery_item->image->isValid())
                        return false;

                    if(!$image_uploader->process($gallery_item->image,ImageUpload::DOCUMENT_TYPE))
                        return false;

                    $image = $image_uploader->store(env('GALLERY_IMAGE_PATH'), 'LOCAL');

                    return GalleryItem::create([
                        'gallery_id' => $gallery->id, 'name' => $gallery_item->name, 'image' => $image,
                    ]);

                });

                $gallery_items = $gallery_items->filter(function ($gallery_item) {
                    return $gallery_item;
                });

                if (count($gallery_items) == 0) {
                    return redirect()->back()->with(['error' => 'Not able to upload Other Gallery Images']);
                }

            }

            return redirect()->route('admin-gallery-view')->with(['success' => 'Gallery has been Updated.']);

        }

        return view('admin.image-manager.gallery.update', [
            'gallery' => $gallery
        ]);
    }

}
