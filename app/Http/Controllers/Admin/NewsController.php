<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        return view('admin.news.index', [
            'items' => News::with('admin')->orderBy('id','asc')->paginate(20)
        ]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:2000',
                'message' => 'required',
            ],[
                'title.required' => 'Title is Required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB',
                'message.required' => 'Message is Required',
            ]);

            if($request->image) {
                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    $disk = \Storage::disk('spaces');
                    $image = (string)\Str::uuid() . "." . $request->file('image')->getClientOriginalExtension();

                    $disk->delete(env('NEWS_IMAGE_PATH') . $request->image);
                    $disk->put(env('NEWS_IMAGE_PATH') . $image, file_get_contents($request->file('image')->path()));

                    News::create([
                        'admin_id' => \Session::get('admin')->id,
                        'title'=> $request->title,
                        'image' => $image,
                        'message'=> $request->message,
                        'status' => 1
                    ]);
                }
            }

            return redirect()->route('admin-news')->with(['success' => 'News has been added successfully..']);
        }
        return view('admin.news.create');
    }

    public function update(Request $request)
    {
        if (!$news = News::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Data for Update']);

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:2000',
                'message' => 'required',
                'status' => 'required'
            ],[
                'title.required' => 'Title is Required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB',
                'message.required' => 'Message is Required',
                'status.required' => 'Status is Required'
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->delete(env('NEWS_IMAGE_PATH'). $request->image);
                    $disk->put(env('NEWS_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                    $news->image = $image;
                }
            }

            $news->title = $request->title;
            $news->message = $request->message;
            $news->status = $request->status;
            $news->save();

            return redirect()->route('admin-news')->with(['success' => 'News has been updated successfully..']);
        }

        return view('admin.news.update', [
            'news' => $news
        ]);
    }

}
