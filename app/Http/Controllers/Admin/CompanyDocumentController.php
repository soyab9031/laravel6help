<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\ImageUpload;
use App\Models\CompanyDocument;
use Illuminate\Http\Request;

class CompanyDocumentController extends Controller
{
    public function index(Request $request)
    {
        $documents = CompanyDocument::search($request->search,[
            'name'
        ]);

        if($request->type){
            $documents = $documents->whereType($request->type);
        }

        return view('admin.company-documents.index',[
            'documents' => $documents->orderBy('id','desc')->paginate(30)
        ]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required|unique:company_documents,name',
                'type' => 'required',
                'document' => 'mimes:jpeg,jpg,png,pdf|max:2000',
            ],[
                'name.required' => 'Name is Required',
                'type.required' => 'Document Type is required',
                'document.required' => 'Document Title or Name is required',
                'document.mimes' => 'Image must have .jpg,jpeg,png,and pdf type',
                'document.max' => 'Upload an image less then 2MB'
            ]);


            if ($request->document) {

                if ($request->hasFile('document') && $request->file('document')->isValid()) {

                    $image_uploader = (new ImageUpload());

                    if($request->type == 1 && $request->file('document')->getClientOriginalExtension() != 'pdf'){
                        return redirect()->back()->with(['error' => 'Unable to fetch Document, Upload another Document']);
                    }

                    if($request->type == 2 && ($request->file('document')->getClientOriginalExtension() != 'jpeg' && $request->file('document')->getClientOriginalExtension() != 'jpg' && $request->file('document')->getClientOriginalExtension() != 'png')){
                        return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another Image']);
                    }

                    if(!$document_name = $image_uploader->fileStore($request->file('document'),env('COMPANY_DOCUMENT_PATH')))
                        return redirect()->back()->with(['error' => 'Unable to Fetch Document Or Image']);

                    CompanyDocument::create([
                        'admin_id' => \Session::get('admin')->id,
                        'document' => $document_name,
                        'name' => $request->name,
                        'type' => $request->type,
                        'status' => $request->status
                    ]);
                }
            }
            return redirect()->route('admin-company-documents-view')->with(['success' => 'Company Documents has been Created.']);
        }

        return view('admin.company-documents.create');
    }

    public function update(Request $request){

        $companyDocument = CompanyDocument::whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'type' => 'required',
                'document' => 'mimes:jpeg,jpg,png,pdf|max:2000',
            ],[
                'name.required' => 'Name is Required',
                'type.required' => 'Document Type is required',
                'document.required' => 'Document Title or Name is required',
                'document.mimes' => 'Image must have .jpg,jpeg,png,and pdf type',
                'document.max' => 'Upload an image less then 2MB'
            ]);

            if ($request->document) {
                if ($request->hasFile('document') && $request->file('document')->isValid()) {
                    $disk = \Storage::disk('s3');
                    $fileName = \Uuid::generate(4) . "." . $request->file('document')->getClientOriginalExtension();
                    $disk->delete(env('COMPANY_DOCUMENT_PATH') . $request->document);

                    if ($request->type == 1 && $request->file('document')->getClientOriginalExtension() != 'pdf') {
                        return redirect()->back()->with(['error' => 'Unable to fetch Document, Upload another Document']);
                    }

                    if($request->type == 2 && ($request->file('document')->getClientOriginalExtension() != 'jpeg' && $request->file('document')->getClientOriginalExtension() != 'jpg' && $request->file('document')->getClientOriginalExtension() != 'png')){
                        return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another Image']);
                    }

                    $disk->put(env('COMPANY_DOCUMENT_PATH') . $fileName, file_get_contents($request->file('document')->path()));
                    $companyDocument->document = $fileName;
                }
            }

            $companyDocument->name = $request->name;
            $companyDocument->type = $request->type;
            $companyDocument->status = $request->status;
            $companyDocument->save();

            return redirect()->route('admin-company-documents-view')->with(['success' => 'Document has been Updated.']);
        }


        return view('admin.company-documents.update',[
            'companyDocument' => $companyDocument
        ]);
    }
}
