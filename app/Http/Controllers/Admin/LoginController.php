<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if (\Session::has('admin')) {
            return redirect()->route('admin-dashboard')->with(['success' => 'Welcome, To Ingenious System for ' . config('project.brand') ]);
        }

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required'
            ], [
                'email.required' => 'Email is required',
                'email.email' => 'Invalid Email Address',
                'password.required' => 'Password is required',
            ]);

            if (!$admin = Admin::whereEmail($request->email)->first())
                return redirect()->back()->with(['error' => 'Invalid Email or Password']);

            if ($admin->status == Admin::INACTIVE)
                return redirect()->back()->with(['error' => 'Admin Manager is inactivated by Master admin, Kindly Contact to Them']);

            if (!\Hash::check($request->password, $admin->password))
                return redirect()->back()->with(['error' => 'Invalid Email and Password']);

            \Session::put('admin', $admin);

            Admin::whereId($admin->id)->update([
                'last_logged_in_ip' => Helper::getClientIp(),
                'last_logged_in_at' => Carbon::now()
            ]);

            return redirect()->route('admin-dashboard')->with(['success' => 'Welcome, To Ingenious System for ' . config('project.brand') ]);
        }

        return view('admin.login');
    }

    public function logout()
    {
        \Session::forget('admin');
        return redirect()->route('admin-login');
    }
}
