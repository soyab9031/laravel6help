<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\PayoutExport;
use App\Imports\Admin\PayoutImport;
use App\Models\Payout;
use App\Models\UserStatus;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PayoutController extends Controller
{

    public function index(Request $request)
    {
        $payouts = Payout::whereProcessType(Payout::ADMIN_PROCESS)->with(['user.detail', 'user.bank'])->search($request->search, [
            'user.tracking_id', 'user.mobile', 'user.username', 'user.detail.first_name', 'user.detail.last_name', 'reference_number'
        ])->filterDate($request->dateRange);

        if ($request->status)
            $payouts = $payouts->where('status', $request->status);

        if ($request->download) {
            if (empty($request->status) && empty($request->dateRange))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the Payout report']);

            return \Excel::download(new PayoutExport($payouts->get()), 'payout-report.xlsx');
        }

        return view('admin.payout.index', [
            'payouts' => $payouts->orderBy('id', 'desc')->paginate(50)
        ]);
    }

    public function update(Request $request)
    {
        if (!$payout = Payout::with(['user.detail', 'user.bank'])->whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Payout Data']);

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'reference_number' => 'required',
            ], [
                'reference_number.required' => 'Transfer Reference Number is required field'
            ]);

            $payout->reference_number = $request->reference_number;
            $payout->status = $request->status;
            $payout->transfer_type = $request->transfer_type;
            $payout->remarks = $request->remarks;
            $payout->save();

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout Details has been updated']);
        }

        return view('admin.payout.update', [
            'payout' => $payout
        ]);
    }

    public function bulkUpdate(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'csv_file'=>'required'
            ],[
                'csv_file.required'=>'CSV file is required'
            ]);

            $file_name = 'payout_' . Carbon::now()->toDateString() . '.csv';

            \Storage::disk('public')->put('payout/' . $file_name, file_get_contents($request->file('csv_file')->path()));

            \Excel::import(new PayoutImport, storage_path('app/public/payout/' . $file_name));

            \Storage::disk('public')->delete(storage_path('app/public/payout/' . $file_name));

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout Bulk Records are updated successfully']);
        }

        return view('admin.payout.bulk-update');
    }

    public function create(Request $request)
    {
        $wallet_records = Wallet::with(['user.detail'])->whereHas('user.status', function ($q) {
            $q->where('payment', UserStatus::YES)->where('terminated', UserStatus::NO)->where('fake', UserStatus::REAL_TYPE);
        })->whereHas('user', function ($q) {
            $q->whereNotNull('paid_at');
        })->whereNull('payout_id')->get();

        $wallet_records = collect($wallet_records)->groupBy('user_id');

        if ($request->isMethod('post')) {
            $calculations = $this->calculation($wallet_records);

            \DB::transaction(function () use ($calculations) {

                collect($calculations)->map(function ($calculation) {

                    $payout = Payout::create([
                        'user_id' => $calculation->user->id,
                        'total' => $calculation->total,
                        'tds' => $calculation->tds,
                        'admin_charge' => $calculation->admin_charge,
                        'amount' => $calculation->amount,
                        'process_type' => Payout::ADMIN_PROCESS
                    ]);

                    Wallet::create([
                        'user_id' => $calculation->user->id,
                        'payout_id' => $payout->id,
                        'amount' => $calculation->amount,
                        'type' => Wallet::DEBIT_TYPE,
                        'remarks' => 'Generated Payout at ' . $payout->created_at->format('D d-m-Y')
                    ]);

                    Wallet::whereIn('id', $calculation->wallet_ids)->update(['payout_id' => $payout->id]);

                });

            });

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout has been successfully generated']);
        }

        return view('admin.payout.create', [
            'payouts' => $this->calculation($wallet_records)
        ]);
    }

    private function calculation($wallet_records)
    {
        return $wallet_records->map(function ($wallets) {

            $wallet_credits = collect($wallets)->filter(function ($wallet) {
                return $wallet->type == Wallet::CREDIT_TYPE;
            });

            $wallet_debits = collect($wallets)->filter(function ($wallet) {
                return $wallet->type == Wallet::DEBIT_TYPE;
            });

            /* Total Values */
            $total = $wallet_credits->sum('total');
            $tds = $wallet_credits->sum('tds');
            $admin_charge = $wallet_credits->sum('admin_charge');

            $wallet_ids = collect($wallets)->pluck('id')->toArray();

            return (object)[
                'wallet_ids' => $wallet_ids,
                'user' => $wallets->first()->user,
                'total' => $total - collect($wallet_debits)->sum('total'),
                'tds' => $tds - collect($wallet_debits)->sum('tds'),
                'admin_charge' => $admin_charge - collect($wallet_debits)->sum('admin_charge'),
                'amount' => $total - ((collect($wallet_debits)->sum('amount')) + $tds + $admin_charge)
            ];

        })->values();
    }

    public function tdsReport(Request $request)
    {
        $tdsReport = Payout::selectRaw('COALESCE(sum(total), 0) as total_payout, COALESCE(sum(tds), 0) as total_tds, user_id')->search($request->search, [
            'user.tracking_id'
        ])->groupBy('user_id')->with(['user.detail:user_id,title,first_name,pan_number'])->havingRaw('SUM(tds) > 0')->filterDate($request->dateRange);

        if ($request->status)
            $tdsReport = $tdsReport->where('status', $request->status);

        if ($request->download) {
            \Excel::create('Total TDS Report', function ($excel) use ($tdsReport, $request) {

                $excel->sheet('Sheet 1', function ($sheet) use ($tdsReport, $request) {

                    if (!empty($request->dateRange) || !empty($request->search)) {
                        $tdsReport = $tdsReport->orderBy('created_at', 'desc')->get();
                    } else {
                        $tdsReport = $tdsReport->orderBy('id', 'desc')->paginate(50);
                    }

                    $data = $tdsReport->map(function ($tdsReport) {

                        return [
                            'Member Name' => $tdsReport->user->detail->full_name,
                            'Tracking Id' => $tdsReport->user->tracking_id,
                            'PAN Number' => $tdsReport->user->detail->pan_number ?: 'N.A',
                            'Payout' => $tdsReport->total_payout,
                            'TDS' => $tdsReport->total_tds,
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.payout.tds-report', [
            'tdsReports' => $tdsReport->paginate(50)
        ]);
    }
}
