<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::with(['user.detail', 'admin'])->search($request->search, [
            'user.tracking_id', 'customer_order_id' , 'user.detail.first_name' , 'user.detail.middle_name' , 'user.detail.last_name'
        ]);

        if ($request->status)
            $orders = $orders->whereStatus($request->status);


        return view('admin.order.index',[
            'orders' => $orders->paginate(40)
        ]);
    }

    public function details(Request $request)
    {
        if (!$order = Order::whereCustomerOrderId($request->customer_order_id)->first())
            return back()->with(['error' => 'Given order not found' ]);

        return view('admin.order.details',[
           'order' => $order
        ]);

    }
}
