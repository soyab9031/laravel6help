<?php

namespace App\Library;


use App\Models\SmsTransaction;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Sms
{
    protected $user;
    protected $message;
    protected $template_id;
    protected $mobile_number;
    protected $template_name;

    /**
     * @param User|null $user
     * @param string|null $mobile
     * @return $this
     */
    public function to(User $user = null, string $mobile = null)
    {
        $this->user = $user;
        $this->mobile_number = $this->user ? $this->user->mobile : $mobile;

        return $this;
    }

    /**
     * @param $name
     * @param array $parameters
     * @return $this
     * @throws \Exception
     */
    public function template($name, $parameters = [])
    {
        if (!is_array($parameters))
            throw new \Exception;

        $this->template_name = $name;

        $template = config('sms_templates.' . $this->template_name);

        $this->template_id = $template['id'];

        $this->message = preg_replace_callback('/{\w+}/', function() use (&$parameters) {
            return array_shift($parameters);
        }, $template['message']);

        return $this;
    }


    public function sendOne(): bool
    {

        if(env('APP_ENV') != 'local')
        {

            try {

                (new Client())->get('https://portal.mobtexting.com/api/v2/sms/send', [
                    'query' => [
                        'to' => $this->mobile_number,
                        'message' => $this->message,
                        'method' => 'sms.normal',
                        'access_token' => env('SMS_API_KEY'),
                        'sender' => env('SMS_SENDER_ID'),
                        'service' => 'T',
                        'template_id' => $this->template_id
                    ]
                ]);

                SmsTransaction::create([
                    'category' => $this->template_name,
                    'user_id' => $this->user ? $this->user->id : null,
                    'mobile' => $this->mobile_number,
                    'message' => $this->message
                ]);

                return true;

            } catch (RequestException $e) {
                \Log::error('SMS EXCEPTION', [$e->getMessage(), $e->getCode()]);
                return false;
            }

        }
        return false;
    }

}
