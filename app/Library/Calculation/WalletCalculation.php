<?php


namespace App\Library\Calculation;


use App\Models\Binary\BinaryAchievement;
use App\Models\Binary\BinaryCalculation;
use App\Models\User;
use App\Models\UserStatus;
use App\Models\Wallet;
use Carbon\Carbon;

class WalletCalculation
{
    public function sponsor()
    {
        User::with(['sponsorBy.status', 'package', 'detail'])->whereNull('wallet_id')->whereNotNull('paid_at')
            ->whereHas('sponsorBy.status', function ($q) {
                $q->where('payment', UserStatus::YES)->where('terminated', UserStatus::NO);
            })->get()->map( function ($user) {

                if ($user->sponsorBy)
                {
                    $wallet = $user->sponsorBy->credit(collect([
                        'amount' => $user->package->sponsor_income,
                        'income_type' => Wallet::SPONSOR_TYPE,
                        'remarks' => 'Sponsor Income from ' . $user->detail->full_name . ' ('.$user->tracking_id.')'
                    ]));

                    $user->wallet_id = $wallet->id;
                    $user->save();
                }

            });
    }

    /**
     * Binary wallet Calculation with Capping
     */
    public function binary()
    {
        $pair_income = 100;

        BinaryAchievement::with(['user.package'])->whereStatus(BinaryAchievement::ACTIVE)->whereNull('wallet_id')
            ->whereHas('user.status', function ($query) {
                $query->whereNotNull('binary_qualified_at')->where('payment', UserStatus::YES);
            })->get()->map( function ($binary_achievement) use ($pair_income) {

                $today_income = Wallet::whereUserId($binary_achievement->user_id)->where([
                    ['created_at', '>=', Carbon::now()->startOfDay()],
                    ['created_at', '<=', Carbon::now()->endOfDay()]
                ])->whereType(Wallet::CREDIT_TYPE)->whereIncomeType(Wallet::BINARY_TYPE)->sum('total');

                if (($today_income + $pair_income) <= $binary_achievement->user->package->capping)
                {

                    \DB::transaction( function () use ($binary_achievement, $pair_income) {

                        $wallet = $binary_achievement->user->credit(collect([
                            'income_type' => Wallet::BINARY_TYPE,
                            'amount' => $pair_income,
                            'remarks' => 'Pair Matching Income # ' . $binary_achievement->calculation_reference_id
                        ]));

                        BinaryAchievement::whereId($binary_achievement->id)->update(['wallet_id' => $wallet->id ]);

                    });
                }
                else
                {
                    $rejected_calculation_ids = BinaryAchievement::whereUserId($binary_achievement->user_id)
                        ->whereNull('wallet_id')->whereStatus(BinaryAchievement::ACTIVE)->get()
                        ->pluck('calculation_id');


                    if ($rejected_calculation_ids->count() > 0)
                    {

                        \DB::transaction( function () use ($rejected_calculation_ids) {

                            BinaryCalculation::whereIn('id', $rejected_calculation_ids->toArray())->update([
                                'status' => BinaryCalculation::REJECTED,
                                'remarks' => 'Washed, Capping Applied'
                            ]);

                            BinaryAchievement::whereIn('calculation_id', $rejected_calculation_ids->toArray())->update([
                                'status' => BinaryAchievement::INACTIVE
                            ]);

                        });

                    }

                }

            });
    }
}
