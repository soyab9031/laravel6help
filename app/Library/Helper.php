<?php

namespace App\Library;

use Illuminate\Pagination\Paginator;

class Helper
{
    /**
     * @param array $array
     * @return object
     */
    public static function arrayToObject( $array ) {

        foreach( $array as $key => $value ){
            if( is_array( $value ) ) $array[ $key ] = self::arrayToObject( $value );
        }
        return (object) $array;
    }

    /**
     * @param Paginator $collection
     * @param integer $index
     * @return string;
     */
    public static function tableIndex($collection, $index)
    {
        return ($collection->currentpage() - 1) * $collection->perpage() + $index + 1;
    }

    /**
     * @return string
     */
    public static function getClientIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
        {
            if (array_key_exists($key, $_SERVER) === true)
            {
                foreach (explode(',', $_SERVER[$key]) as $ip)
                {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
                    {
                        return $ip;
                    }
                }
            }
        }
    }
}