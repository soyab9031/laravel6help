<?php

namespace App\Imports\Admin;

use App\Models\Payout;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PayoutImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        collect($collection)->filter(function ($row) {
            return isset($row['reference_number']) && $row['reference_number'];
        })->values()->map(function ($row) {

            Payout::whereId($row['index_number'])->update([
                'reference_number' => $row['reference_number'], 'remarks' => $row['remarks'],
                'status' => Payout::TRANSFERRED_TYPE, 'transfer_type' => Payout::NEFT_TYPE
            ]);

        });
    }
}
