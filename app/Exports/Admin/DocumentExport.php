<?php

namespace App\Exports\Admin;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DocumentExport implements FromCollection, WithHeadings
{
    protected $documents;

    public function __construct($documents)
    {
        $this->documents = $documents;
    }

    public function collection(): Collection
    {
        return collect($this->documents)->map(function ($document) {

            return [
                $document->created_at->format('M d, Y'),
                $document->user->detail->full_name.' ('.$document->user->tracking_id.')',
                $document->document_name,
                $document->status == 1 ? 'Pending' : $document->status == 2 ? 'Verified' : 'Rejected',
                env('DOCUMENT_IMAGE_URL').$document->image,
                $document->secondary_image ? env('DOCUMENT_IMAGE_URL').$document->secondary_image : 'N.A'
            ];

        });
    }

    public function headings(): array
    {
        return [
            'Created Date', 'User', 'Type', 'Status', 'Image', 'Secondary Image',
        ];
    }
}
