<?php

namespace App\Exports\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PayoutExport implements FromCollection, WithHeadings, WithStyles
{
    protected $payouts;

    public function __construct($payouts)
    {
        $this->payouts = $payouts;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect($this->payouts)->map(function ($payout) {

            return [
                $payout->id,
                $payout->created_at->format('M d, Y'),
                $payout->user->detail->full_name,
                $payout->user->tracking_id,
                $payout->user->mobile,
                $payout->user->bank ? $payout->user->bank->name : null,
                $payout->user->bank ? $payout->user->bank->account_number : null,
                $payout->user->bank ? $payout->user->bank->ifsc : null,
                $payout->user->bank ? $payout->user->bank->address : null,
                $payout->total,
                $payout->tds,
                $payout->admin_charge,
                $payout->amount,
                $payout->reference_number,
                $payout->remarks
            ];

        });
    }

    public function headings(): array
    {
        return ['index_number', 'Created Date', 'Member Name', 'Tracking Id', 'Contact Number', 'Bank Name', 'Account Number', 'IFSC Code', 'Bank Address', 'Total Amount', 'TDS', 'Admin Charge', 'Net Amount', 'reference_number', 'remarks',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1')->getFont()->getColor()->setARGB(Color::COLOR_RED);
        $sheet->getStyle('N1:O1')->getFont()->getColor()->setARGB(Color::COLOR_RED);
    }
}
