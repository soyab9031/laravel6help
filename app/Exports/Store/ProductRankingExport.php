<?php

namespace App\Exports\Store;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductRankingExport implements FromCollection, WithHeadings
{
    protected $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect($this->items)->map(function ($item) {

            return [
                $item->product->name,
                $item->code,
                number_format($item->sold_qty)
            ];

        });
    }

    public function headings(): array
    {
        return [
            'Item', 'Product Code', 'Item Sold'
        ];
    }
}
