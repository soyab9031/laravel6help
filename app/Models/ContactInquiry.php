<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\ContactInquiry
 *
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property string|null $email
 * @property string $message
 * @property int $status 0: Pending, 1: Seen, 2: Pined
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry pending()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry pinned()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry query()
 * @method static \Sofa\Eloquence\Builder|ContactInquiry aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|ContactInquiry avg($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|ContactInquiry joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry max($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry min($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|ContactInquiry rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry select($columns = [])
 * @method static \Sofa\Eloquence\Builder|ContactInquiry setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry sum($column)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ContactInquiry whereYear($column, $operator, $value, $boolean = 'and')
 */
class ContactInquiry extends Model
{
    use Eloquence;

    const PENDING = 0, SEEN = 1, PINNED = 2;

    protected $fillable = [
        'name', 'contact', 'email', 'message', 'status'
    ];

    public function scopePending($query)
    {
        $query->where('status', self::PENDING);
    }

    public function scopePinned($query)
    {
        $query->where('status', self::PINNED);
    }
}
