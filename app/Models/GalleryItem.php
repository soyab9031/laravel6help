<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GalleryItem
 *
 * @property int $id
 * @property int $gallery_id
 * @property string $name
 * @property string $image
 * @property int $status 0: Inactive, 1: Active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleryItem query()
 */
class GalleryItem extends Model
{
    protected  $fillable=[
      'gallery_id', 'name', 'image',
    ];

}
