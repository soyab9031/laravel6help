<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\AdminRoute
 *
 * @property int $id
 * @property int $admin_id
 * @property string $route
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRoute query()
 */
class AdminRoute extends Model
{
    protected $fillable = [
        'admin_id', 'route'
    ];

    public static function generalRoutes()
    {
        return  [
            'admin-login', 'admin-logout', 'admin-profile', 'admin-dashboard', 'admin-access-denied'
        ];
    }

    private static function routes()
    {
        return collect(\Route::getRoutes())->map( function($route){
            return $route->getName();
        })->filter( function($route) {
            return \Str::startsWith($route, 'admin');
        })->unique()->map( function ($route) {
            $routeName = str_replace('admin-', '', $route);
            $routeName = str_replace('-', ' ', $routeName);
            return (object) ['key' => $route, 'value' => ucwords($routeName), 'selected' => false];
        })->values();
    }

    /**
     * @return Collection;
     */
    public static function openRoutes()
    {
        return self::routes()->filter( function ($route) {
            return  \Str::startsWith($route->key, 'admin-api') || in_array($route->key, self::generalRoutes());
        })->values();
    }

    public static function restrictedRoutes()
    {
        return self::routes()->reject( function ($route) {
            return  \Str::startsWith($route->key, 'admin-api') || in_array($route->key, self::generalRoutes());
        })->values();
    }
}
