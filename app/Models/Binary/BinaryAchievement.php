<?php

namespace App\Models\Binary;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Binary\BinaryAchievement
 *
 * @property int $id
 * @property int $user_id
 * @property int $calculation_id
 * @property int $calculation_reference_id
 * @property int $status 1: Active, 2: Inactive
 * @property int|null $wallet_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCalculationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCalculationReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereWalletId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement query()
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement avg($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement max($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement min($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement select($columns = [])
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement sum($column)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryAchievement whereYear($column, $operator, $value, $boolean = 'and')
 */
class BinaryAchievement extends Model
{
    use Eloquence;

    const ACTIVE =  1, INACTIVE = 2;

    protected $fillable = [
        'user_id', 'calculation_id', 'calculation_reference_id', 'reference_users', 'wallet_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
