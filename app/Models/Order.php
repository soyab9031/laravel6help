<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\StoreManager\Store;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int|null $offer_id
 * @property int|null $store_id
 * @property string|null $customer_order_id
 * @property float $amount
 * @property float $wallet
 * @property float $discount
 * @property float $total
 * @property float $total_bv
 * @property string|null $remarks
 * @property string|null $admin_remarks
 * @property int $status 1: Checkout, 2: Placed, 3: Approved, 4: Failed, 5: Rejected
 * @property int $payment_status 1: Checkout, 2: Success, 3: Pending, 4: Failed
 * @property string|null $payment_reference
 * @property string|null $approved_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAdminRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCustomerOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTotalBv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereWallet($value)
 * @mixin \Eloquent
 * @property int $bv_calculated 0: Pending, 1: Done
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBvCalculated($value)
 * @method static \Sofa\Eloquence\Builder|Order aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Order avg($column)
 * @method static \Sofa\Eloquence\Builder|Order callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Order count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Order filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Order getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Order joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Order leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Order lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Order max($column)
 * @method static \Sofa\Eloquence\Builder|Order min($column)
 * @method static \Sofa\Eloquence\Builder|Order orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Order orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Order orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Order orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Order orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Order orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Order orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Order prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Order rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Order search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Order select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Order setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Order setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Order sum($column)
 * @method static \Sofa\Eloquence\Builder|Order whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Order whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Order whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Order whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Order whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Order whereYear($column, $operator, $value, $boolean = 'and')
 */
class Order extends Model
{
    use Eloquence;

    const PAYMENT_CHECKOUT = 1, PAYMENT_SUCCESS= 2, PAYMENT_PENDING = 3, PAYMENT_FAILED = 4;
    const CHECKOUT = 1, PLACED = 2, APPROVED = 3, FAILED = 4, REJECTED = 5, CANCELED = 6;
    const PRODUCT_PENDING = 1, PRODUCT_DELIVERED = 2;

    protected $casts = [
      'shipping_address' => 'object'
    ];

    protected $fillable = [
        'user_id', 'customer_order_id', 'store_id', 'admin_id', 'offer_id', 'amount', 'wallet', 'shipping_charge', 'discount', 'total', 'total_bv', 'remarks', 'admin_remarks', 'status', 'payment_status', 'payment_reference', 'approved_at', 'offer_id', 'bv_calculated', 'shipping_address','delivery_status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public static function getStatus($id = null)
    {
        $statuses = [
            1 => 'Checkout', 2 => 'Placed', 3 => 'Approved', 4 => 'Failed', 5 => 'Rejected', '6' => 'Refund/Cancel'
        ];

        return $id ? $statuses[$id] : $statuses;
    }
}
