<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Grievance
 *
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property string|null $email
 * @property string $subject
 * @property string $message
 * @property int $status 0: Pending, 1: Seen, 2: Pined
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance pending()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance pinned()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance query()
 * @method static \Sofa\Eloquence\Builder|Grievance aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Grievance avg($column)
 * @method static \Sofa\Eloquence\Builder|Grievance callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Grievance count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Grievance filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Grievance getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Grievance joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Grievance leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Grievance lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Grievance max($column)
 * @method static \Sofa\Eloquence\Builder|Grievance min($column)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Grievance orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Grievance orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Grievance prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Grievance rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Grievance search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Grievance select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Grievance setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Grievance setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Grievance sum($column)
 * @method static \Sofa\Eloquence\Builder|Grievance whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Grievance whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Grievance whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Grievance whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Grievance whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Grievance whereYear($column, $operator, $value, $boolean = 'and')
 */
class Grievance extends Model
{
    use Eloquence;

    const PENDING = 0, SEEN = 1, PINNED = 2;

    protected $fillable = [
        'name', 'contact', 'email', 'subject', 'message', 'status'
    ];

    public function scopePending($query)
    {
        $query->where('status', self::PENDING);
    }

    public function scopePinned($query)
    {
        $query->where('status', self::PINNED);
    }
}
