<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\News
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string $title
 * @property string $message
 * @property int $status 1:Active, 2:Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\News newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\News newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\News query()
 * @method static \Sofa\Eloquence\Builder|News aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|News avg($column)
 * @method static \Sofa\Eloquence\Builder|News callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|News count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|News filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|News getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|News joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|News leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|News lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|News max($column)
 * @method static \Sofa\Eloquence\Builder|News min($column)
 * @method static \Sofa\Eloquence\Builder|News orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|News orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|News orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|News orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|News orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|News orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|News orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|News prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|News rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|News search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|News select($columns = [])
 * @method static \Sofa\Eloquence\Builder|News setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|News setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|News sum($column)
 * @method static \Sofa\Eloquence\Builder|News whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|News whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|News whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|News whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|News whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|News whereYear($column, $operator, $value, $boolean = 'and')
 */
class News extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id','title','message','image','status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function scopeActive($q){
        return $q->where('status',self::ACTIVE);
    }


}
