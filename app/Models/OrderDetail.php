<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\OrderDetail
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_price_id
 * @property float $price
 * @property float $distributor_price
 * @property float $selling_price
 * @property float $bv
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $tax_amount
 * @property-read mixed $taxable_amount
 * @property-read mixed $total_amount
 * @property-read mixed $total_bv
 * @property-read mixed $total_tax_amount
 * @property-read mixed $total_taxable_amount
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\ProductPrice $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereBv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereSellingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|OrderDetail aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|OrderDetail avg($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|OrderDetail count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|OrderDetail filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|OrderDetail getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|OrderDetail joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|OrderDetail leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|OrderDetail lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|OrderDetail max($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail min($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|OrderDetail prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|OrderDetail rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|OrderDetail search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|OrderDetail select($columns = [])
 * @method static \Sofa\Eloquence\Builder|OrderDetail setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|OrderDetail setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|OrderDetail sum($column)
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|OrderDetail whereYear($column, $operator, $value, $boolean = 'and')
 */
class OrderDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'order_id', 'product_price_id', 'price', 'distributor_price', 'selling_price', 'bv', 'qty', 'gst'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getTotalBvAttribute()
    {
        return $this->bv * $this->qty;
    }

    public function getTaxAmountAttribute()
    {
        return round(($this->selling_price*$this->gst->percentage)/((float)100+(float)$this->gst->percentage), 4);
    }

    public function getTotalTaxAmountAttribute()
    {
        return $this->tax_amount * $this->qty;
    }

    public function getTaxableAmountAttribute()
    {
        return round(($this->selling_price - $this->tax_amount), 2);
    }

    public function getTotalTaxableAmountAttribute()
    {
        return $this->taxable_amount * $this->qty;
    }

    public function getTotalAmountAttribute()
    {
        return $this->selling_price * $this->qty;
    }
}
