<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Faq
 *
 * @property int $id
 * @property int $admin_id
 * @property string $question
 * @property string $answer
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|Faq aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Faq avg($column)
 * @method static \Sofa\Eloquence\Builder|Faq callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Faq count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Faq filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Faq getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Faq joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Faq leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Faq lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Faq max($column)
 * @method static \Sofa\Eloquence\Builder|Faq min($column)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Faq orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Faq orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Faq prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Faq rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Faq search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Faq select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Faq setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Faq setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Faq sum($column)
 * @method static \Sofa\Eloquence\Builder|Faq whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Faq whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Faq whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Faq whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Faq whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Faq whereYear($column, $operator, $value, $boolean = 'and')
 */
class Faq extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id', 'question', 'answer', 'status'
    ];
}
