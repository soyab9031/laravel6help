<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PackageOrder
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int|null $placed_by
 * @property int $package_id
 * @property int $pin_id
 * @property float $amount
 * @property int $pv_calculation 0: No, 1: Yes, 2: Not Available
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\Pin $pin
 * @property-read \App\Models\User|null $placedBy
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePlacedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePvCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereUserId($value)
 * @mixin \Eloquent
 * @property float $pv
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePv($value)
 * @method static \Sofa\Eloquence\Builder|PackageOrder aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|PackageOrder avg($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|PackageOrder count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|PackageOrder filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|PackageOrder getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|PackageOrder joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|PackageOrder leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PackageOrder lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|PackageOrder max($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder min($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|PackageOrder prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|PackageOrder rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PackageOrder search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|PackageOrder select($columns = [])
 * @method static \Sofa\Eloquence\Builder|PackageOrder setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PackageOrder setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PackageOrder sum($column)
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageOrder whereYear($column, $operator, $value, $boolean = 'and')
 */
class PackageOrder extends Model
{
    use Eloquence;

    const NO = 0, YES = 1, NOT_AVAILABLE = 2;

    protected $fillable = [
        'user_id', 'admin_id', 'placed_by', 'package_id', 'pin_id', 'amount', 'pv', 'pv_calculation'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function placedBy()
    {
        return $this->belongsTo(User::class, 'placed_by');
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function pin()
    {
        return $this->belongsTo(Pin::class);
    }
}
