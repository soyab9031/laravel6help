<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property string $continent
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\State[] $states
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereContinent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country active()
 * @property-read int|null $states_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country query()
 * @property string|null $dial_code
 * @method static \Sofa\Eloquence\Builder|Country aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Country avg($column)
 * @method static \Sofa\Eloquence\Builder|Country callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Country count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Country filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Country getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Country joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Country leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Country lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Country max($column)
 * @method static \Sofa\Eloquence\Builder|Country min($column)
 * @method static \Sofa\Eloquence\Builder|Country orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Country orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Country orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Country orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Country orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Country orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Country orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Country prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Country rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Country search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Country select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Country setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Country setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Country sum($column)
 * @method static \Sofa\Eloquence\Builder|Country whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Country whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereDialCode($value)
 * @method static \Sofa\Eloquence\Builder|Country whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Country whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Country whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Country whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Country whereYear($column, $operator, $value, $boolean = 'and')
 */
class Country extends Model
{
    use Eloquence;

    protected $fillable = [
        'name', 'continent', 'status', 'dial_code'
    ];

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function scopeActive($q)
    {
        $q->where('status', 1);
    }
}
