<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $fillable = [

    ];

    public function acheivers()
    {
        return $this->hasMany(RankAchiever::class);
    }
}
