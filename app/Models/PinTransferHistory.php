<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PinTransferHistory
 *
 * @property int $id
 * @property int $sender_user_id
 * @property int $receiver_user_id
 * @property int $pin_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $receiver
 * @property-read \App\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory wherePinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereReceiverUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereSenderUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory query()
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory avg($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory max($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory min($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory select($columns = [])
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory sum($column)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinTransferHistory whereYear($column, $operator, $value, $boolean = 'and')
 */
class PinTransferHistory extends Model
{
    use Eloquence;

    protected $fillable = [
        'sender_user_id', 'receiver_user_id', 'pin_id'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_user_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_user_id');
    }
}
