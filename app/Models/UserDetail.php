<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserDetail
 *
 * @property int $id
 * @property int $user_id
 * @property string $title Mr, Miss, Mrs, Dr., Sri
 * @property string $first_name
 * @property string $last_name
 * @property string|null $image
 * @property int $gender 1: Male, 2: Female
 * @property string $birth_date
 * @property string|null $nominee_name
 * @property string|null $nominee_relation
 * @property string|null $nominee_birth_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeRelation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail wherePanNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail query()
 * @property string|null $pan_number
 * @property string|null $aadhaar_number
 * @property string|null $gst_number
 * @method static \Sofa\Eloquence\Builder|UserDetail aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|UserDetail avg($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|UserDetail count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|UserDetail filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|UserDetail getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|UserDetail joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|UserDetail leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserDetail lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|UserDetail max($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail min($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|UserDetail prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|UserDetail rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserDetail search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|UserDetail select($columns = [])
 * @method static \Sofa\Eloquence\Builder|UserDetail setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserDetail setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserDetail sum($column)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereAadhaarNumber($value)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereGstNumber($value)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDetail whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDetail wherePanNumber($value)
 * @method static \Sofa\Eloquence\Builder|UserDetail whereYear($column, $operator, $value, $boolean = 'and')
 */
class UserDetail extends Model
{
    use Eloquence;

    protected $casts = [
        'birth_date' => 'date',
        'nominee_birth_date' => 'date'
    ];

    protected $fillable = [
        'user_id', 'title', 'first_name', 'last_name', 'image', 'pan_number', 'gender', 'birth_date', 'nominee_name', 'nominee_relation', 'nominee_birth_date','aadhaar_number'
    ];

    public function getFullNameAttribute(): string
    {
        return $this->title.' '.$this->first_name.' '.$this->last_name;
    }
}
