<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $mobile
 * @property string $token
 * @property string $last_logged_in_ip
 * @property string $last_logged_in_at
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastLoggedInIp($value)
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin query()
 * @property int $type 1: Master, 2: Manager, 3: Employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereType($value)
 * @method static \Sofa\Eloquence\Builder|Admin aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Admin avg($column)
 * @method static \Sofa\Eloquence\Builder|Admin callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Admin count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Admin filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Admin getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Admin joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Admin leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Admin lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Admin max($column)
 * @method static \Sofa\Eloquence\Builder|Admin min($column)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Admin orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Admin orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Admin prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Admin rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Admin search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Admin select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Admin setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Admin setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Admin sum($column)
 * @method static \Sofa\Eloquence\Builder|Admin whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Admin whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Admin whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Admin whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Admin whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Admin whereYear($column, $operator, $value, $boolean = 'and')
 */
class Admin extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    const MASTER = 1, MANAGER = 2, EMPLOYEE = 3;

    protected $hidden = [
        'password', 'token'
    ];

    protected $fillable = [
        'name', 'password', 'email', 'mobile', 'status', 'type', 'token', 'last_logged_in_ip', 'last_logged_in_at'
    ];
}
