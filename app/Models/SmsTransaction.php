<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\SmsTransaction
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User|null $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction query()
 * @property string|null $category
 * @property string|null $mobile
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereMobile($value)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|SmsTransaction avg($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|SmsTransaction joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction max($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction min($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|SmsTransaction rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction select($columns = [])
 * @method static \Sofa\Eloquence\Builder|SmsTransaction setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction sum($column)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SmsTransaction whereYear($column, $operator, $value, $boolean = 'and')
 */
class SmsTransaction extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'category', 'mobile', 'message'
    ];

    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
