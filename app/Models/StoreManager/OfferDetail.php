<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\OfferDetail
 *
 * @property int $id
 * @property int $offer_id
 * @property string|null $condition_items
 * @property string $offer_items
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\OfferDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\OfferDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\OfferDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereConditionItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereOfferItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\OfferDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OfferDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'offer_id', 'condition_items', 'offer_items',
    ];

    public function getOfferItemsAttribute($value)
    {
        return json_decode($value);
    }

    public function getConditionItemsAttribute($value)
    {
        return json_decode($value);
    }
}
