<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreSupply
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int|null $sender_store_id
 * @property int $store_id
 * @property float $amount
 * @property float $tax_amount
 * @property float $total
 * @property int $delivery_type 1: Transport, 2: Courier, 3: Private Supply
 * @property string|null $courier_name
 * @property string|null $courier_docket_number
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreManager\StoreSupplyDetail[] $details
 * @property-read int|null $details_count
 * @property-read mixed $delivery_type_name
 * @property-read \App\Models\StoreManager\Store|null $senderStore
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreManager\StoreStockTransaction[] $stockTransactions
 * @property-read int|null $stock_transactions_count
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupply newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupply newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupply query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereCourierDocketNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereCourierName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereDeliveryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereSenderStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $total_supply_discount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereTotalSupplyDiscount($value)
 * @property int|null $status 1: Pending, 2: Delivered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupply whereStatus($value)
 */
class StoreSupply extends Model
{
    use Eloquence;
    const PENDING = 1, DELIVERED = 2;

    protected $fillable = [
        'admin_id', 'sender_store_id', 'store_id', 'amount', 'tax_amount', 'total', 'delivery_type' , 'courier_name', 'courier_docket_number', 'remarks', 'status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function senderStore()
    {
        return $this->belongsTo(Store::class, 'sender_store_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function details()
    {
        return $this->hasMany(StoreSupplyDetail::class, 'supply_id');
    }

    public function stockTransactions()
    {
        return $this->hasMany(StoreStockTransaction::class, 'supply_id');
    }

    public function getDeliveryTypeNameAttribute()
    {
        $types = [
          1 => 'Self Pickup', 2 => 'Courier', 3 => 'Transport'
        ];

        return isset($types[$this->delivery_type]) ? $types[$this->delivery_type] : null;
    }
}
