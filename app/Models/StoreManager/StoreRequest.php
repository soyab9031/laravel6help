<?php

namespace App\Models\StoreManager;

use App\Models\Country;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class StoreRequest extends Model
{
    use Eloquence;

    CONST PENDING = 1, APPROVED = 2, REJECTED = 3;
    CONST PREMIUM_TYPE = 1, PRO_TYPE = 2, STANDARD_TYPE = 3, BASIC_TYPE = 4;

    protected $fillable = [
        'user_id', 'sponsor_user_id', 'tracking_id', 'password', 'name', 'owner_name', 'mobile', 'email', 'address', 'city', 'district', 'pincode', 'state_id', 'country_id', 'status', 'type', 'gst_number'
    ];

    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_user_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
