<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\Offer
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string $name
 * @property int $type 1: Billing to Product, 2: Product to Product, 3: Discount, 4: Cashback
 * @property float $min_amount
 * @property float $max_amount
 * @property string|null $discount_details Percentage & Maximum Discount
 * @property string|null $cashback_details
 * @property int $status 1: Active, 2: Inactive
 * @property string|null $start_at
 * @property string|null $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\StoreManager\OfferDetail $details
 * @property-read mixed $type_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Offer newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Offer newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Offer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer running()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereCashbackDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereDiscountDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereMaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereMinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Offer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Offer extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;
    const BILLING_TO_PRODUCT = 1, PRODUCT_TO_PRODUCT = 2, DISCOUNT_TYPE = 3, CASHBACK = 4;

    protected $with = [
        'details'
    ];

    protected $fillable = [
        'admin_id', 'name', 'min_amount', 'max_amount', 'discount_details', 'type', 'start_at', 'end_at', 'status'
    ];

    public function details()
    {
        return $this->hasOne(OfferDetail::class);
    }

    public function scopeActive($q)
    {
        $q->whereStatus(self::ACTIVE);
    }

    public function scopeRunning($q)
    {
        $q->where('start_at', '<=', now()->toDateString())->where('end_at', '>=', now()->toDateString());
    }

    public function getDiscountDetailsAttribute($value)
    {
        return json_decode($value);
    }

    public function getTypeNameAttribute()
    {
        $offer_types = [
            1 => 'Billing to Product',
            2 => 'Product to Product',
            3 => 'Discount Type',
            4 => 'Cashback Type'
        ];

        return $offer_types[$this->type];
    }

    /**
     * Usage
     * 1. User Cart Controller
     * @return array
     */
    public function loadActiveOffers()
    {
        $active_offers = Offer::active()->running()->get()->map(function ($offer) {
            $offer->qualified = 0; // 0: Pending, 1: Yes, 2: Rejected
            return $offer;
        })->toArray();

        return collect($active_offers)->sortBy('id')->toArray();
    }

}
