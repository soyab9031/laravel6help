<?php

namespace App\Models\StoreManager;

use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StockPurchaseDetail
 *
 * @property int $id
 * @property int $stock_purchase_id
 * @property int $product_price_id
 * @property float $amount
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $total_amount
 * @property-read mixed $total_taxable_amount
 * @property-read \App\Models\ProductPrice $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchaseDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchaseDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchaseDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereStockPurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchaseDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StockPurchaseDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'stock_purchase_id', 'product_price_id', 'amount', 'qty', 'gst'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function getTotalTaxableAmountAttribute()
    {
        return $this->qty * $this->amount;
    }

    public function getTotalAmountAttribute()
    {
        return $this->total_taxable_amount + round($this->total_taxable_amount * $this->gst->percentage / 100, 2);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }
}
