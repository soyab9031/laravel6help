<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreWalletRequest
 *
 * @property int $id
 * @property int $store_id
 * @property int|null $admin_id
 * @property int|null $wallet_id
 * @property float $amount
 * @property string $payment_mode
 * @property string $reference_number
 * @property string $bank_name
 * @property string $deposited_at
 * @property string|null $image
 * @property int $status 1: Pending, 2: Approved, 3: Rejected
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletRequest newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletRequest newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereDepositedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest wherePaymentMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereReferenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletRequest whereWalletId($value)
 * @mixin \Eloquent
 */
class StoreWalletRequest extends Model
{
    use Eloquence;

    const PENDING = 1, APPROVED = 2, REJECTED = 3;

    protected $fillable = [
        'admin_id', 'store_id', 'wallet_id', 'amount', 'payment_mode', 'reference_number', 'bank_name', 'deposited_at', 'image', 'status', 'remarks'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
