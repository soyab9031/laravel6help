<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StockPurchase
 *
 * @property int $id
 * @property int $admin_id
 * @property int $supplier_id
 * @property float $amount
 * @property float $tax_amount
 * @property float $total
 * @property int $delivery_type 1:Self Pickup, 2:Courier, 3: Transport
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreManager\StockPurchaseDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\StoreManager\Supplier $supplier
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchase newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchase newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockPurchase query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereDeliveryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockPurchase whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StockPurchase extends Model
{
    use Eloquence;

    protected $fillable = [
        'admin_id', 'supplier_id', 'delivery_type', 'amount', 'tax_amount', 'total', 'remarks'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function details()
    {
        return $this->hasMany(StockPurchaseDetail::class);
    }
}
