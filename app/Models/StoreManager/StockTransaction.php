<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use App\Models\ProductPrice;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StockTransaction
 *
 * @property int $id
 * @property int $admin_id
 * @property int|null $store_id
 * @property int|null $product_price_id
 * @property int|null $stock_purchase_id
 * @property int|null $supply_id
 * @property int $type 1: Credit, 2: Debit
 * @property int $qty
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \App\Models\ProductPrice|null $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockTransaction newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockTransaction newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StockTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereStockPurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereSupplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\StoreManager\MinimumStock|null $minimum_stock
 * @property int $opening
 * @property int $closing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereClosing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereOpening($value)
 * * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction withBalance()
 * @property int|null $order_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StockTransaction withBalance()
 */
class StockTransaction extends Model
{
    use Eloquence;

    CONST CREDIT = 1, DEBIT = 2;

    protected $fillable = [
        'admin_id', 'product_price_id', 'store_id', 'order_id', 'stock_purchase_id', 'supply_id', 'type', 'opening', 'qty', 'closing', 'remarks'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function minimum_stock()
    {
        return $this->belongsTo(MinimumStock::class,'product_price_id','product_price_id');
    }

    public static function getProductStock($id)
    {
        $stock_transaction = StockTransaction::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN qty END), 0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END), 0) as balance')
            ->where('product_price_id', $id)->first();

        return $stock_transaction->balance;
    }

    /**
     * Stock fund Credit
     * @param Collection $details
     * @return StockTransaction
     */
    public static function stockCredit($details)
    {
        $current_balance = self::getProductStock($details->get('product_price_id'));

        return self::create([
            'admin_id' => $details->get('admin_id'),
            'store_id' => $details->get('store_id'),
            'supply_id' => $details->get('supply_id'),
            'product_price_id' => $details->get('product_price_id'),
            'stock_purchase_id' => $details->get('stock_purchase_id'),
            'opening' => $current_balance,
            'qty' => $details->get('qty'),
            'closing' => $current_balance + $details->get('qty'),
            'type' => self::CREDIT,
            'remarks' => $details->get('remarks')
        ]);

    }

    /**
     * Stock fund Debit
     * @param Collection $details
     * @return self
     */
    public static function stockDebit($details)
    {
        $current_balance = self::getProductStock($details->get('product_price_id'));

        return self::create([
            'admin_id' => $details->get('admin_id'),
            'store_id' => $details->get('store_id'),
            'order_id' => $details->get('order_id'),
            'supply_id' => $details->get('supply_id'),
            'product_price_id' => $details->get('product_price_id'),
            'opening' => $current_balance,
            'qty' => $details->get('qty'),
            'closing' => $current_balance - $details->get('qty'),
            'type' => self::DEBIT,
            'remarks' => $details->get('remarks')
        ]);

    }

    public function scopeWithBalance($q)
    {
        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';
        $q->selectRaw($balance_query . ' as balance, product_price_id')->groupBy('product_price_id')->havingRaw($balance_query . '> 0');
    }

}
