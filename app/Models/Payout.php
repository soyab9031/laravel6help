<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Payout
 *
 * @property int $id
 * @property int $user_id
 * @property float $total
 * @property float $tds
 * @property float $admin_charge
 * @property float $service_charge
 * @property float $amount
 * @property int|null $transfer_type 1: Cheque, 2: NEFT
 * @property int $status 1: Pending, 2: Transferred
 * @property int $process_type 1: Admin, 2: User
 * @property string|null $reference_number
 * @property string|null $remarks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $transfer_type_name
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAdminCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereReferenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereServiceCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTransferType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $admin_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereProcessType($value)
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout query()
 * @property int|null $income_type 1: Default
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereIncomeType($value)
 * @method static \Sofa\Eloquence\Builder|Payout aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Payout avg($column)
 * @method static \Sofa\Eloquence\Builder|Payout callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Payout count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Payout filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Payout getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Payout joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Payout leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Payout lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Payout max($column)
 * @method static \Sofa\Eloquence\Builder|Payout min($column)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Payout orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Payout orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Payout prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Payout rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Payout search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Payout select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Payout setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Payout setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Payout sum($column)
 * @method static \Sofa\Eloquence\Builder|Payout whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Payout whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Payout whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Payout whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Payout whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Payout whereYear($column, $operator, $value, $boolean = 'and')
 */
class Payout extends Model
{
    use Eloquence;

    const PENDING_TYPE = 1, TRANSFERRED_TYPE = 2;
    const CHEQUE_TYPE = 1, NEFT_TYPE = 2, IMPS_TYPE = 3, CASH_TYPE = 4;
    const ADMIN_PROCESS = 1, USER_PROCESS = 2;

    protected $fillable = [
        'admin_id', 'user_id', 'total', 'tds', 'admin_charge', 'service_charge', 'amount', 'status', 'transfer_type', 'reference_number', 'remarks', 'process_type', 'income_type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getTransferTypeNameAttribute()
    {
        if ($this->transfer_type) {

            if ($this->transfer_type == self::CHEQUE_TYPE)
                return 'Cheque';
            elseif ($this->transfer_type == self::NEFT_TYPE)
                return 'NEFT';
            elseif ($this->transfer_type == self::IMPS_TYPE)
                return 'IMPS';
            else
                return 'CASH';

        }

        return 'N.A';
    }


}
