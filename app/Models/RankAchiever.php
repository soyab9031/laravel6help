<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class RankAchiever extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'user_id', 'rank_id', 'status', 'wallet_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
