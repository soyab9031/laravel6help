<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\ProductPrice
 *
 * @property int $id
 * @property int $product_id
 * @property string $code
 * @property string|null $images
 * @property float $price
 * @property float $distributor_price
 * @property float $points
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $primary_image
 * @property-read \App\Models\Product $product
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice active()
 * @property string|null $barcode
 * @method static \Sofa\Eloquence\Builder|ProductPrice aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|ProductPrice avg($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|ProductPrice count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|ProductPrice filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|ProductPrice getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|ProductPrice joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|ProductPrice leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|ProductPrice lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|ProductPrice max($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice min($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|ProductPrice prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|ProductPrice rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|ProductPrice search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|ProductPrice select($columns = [])
 * @method static \Sofa\Eloquence\Builder|ProductPrice setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|ProductPrice setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|ProductPrice sum($column)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereBarcode($value)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|ProductPrice whereYear($column, $operator, $value, $boolean = 'and')
 */
class ProductPrice extends Model
{
    use Eloquence;

    Const INACTIVE = 0, DEAL = 1, NEW_ARRIVAL = 2, RECOMMENDED = 3;

    protected $casts = [
        'gst' => 'object'
    ];

    protected $appends = [
        'primary_image'
    ];

    protected $fillable = [
        'product_id', 'code', 'images', 'price', 'barcode', 'distributor_price', 'points', 'qty', 'gst', 'deal','new_arrival','recommended','status'
    ];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getImagesAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public function scopeActive($q)
    {
        $q->where('status', 1);
    }

    public function getPrimaryImageAttribute()
    {
        $images = $this->images;

        if (count($images) == 0)
            return null;

        return env('PRODUCT_IMAGE_URL') . $images[0];
    }

    public static function getGstPercentage(): array
    {
        return [0, 3, 5, 12, 18, 28];
    }
}
