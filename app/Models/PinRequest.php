<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PinRequest
 *
 * @property int $id
 * @property int $user_id
 * @property int $pin_quantity
 * @property int $package_id
 * @property string $payment_mode
 * @property string $ref_no
 * @property string $bank_name
 * @property string $deposited_at
 * @property string $image
 * @property int $status 1: Pending, 2: Approved, 3: Rejected
 * @property string $remarks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereDepositedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest wherePaymentMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest wherePinQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereRefNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereUserId($value)
 * @mixin \Eloquent
 * @property string $reference_number
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereReferenceNumber($value)
 * @property string|null $details
 * @property-read mixed $pin_details
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinRequest whereDetails($value)
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinRequest newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinRequest newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinRequest query()
 * @method static \Sofa\Eloquence\Builder|PinRequest aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|PinRequest avg($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|PinRequest count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|PinRequest filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|PinRequest getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|PinRequest joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|PinRequest leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinRequest lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|PinRequest max($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest min($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|PinRequest prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|PinRequest rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinRequest search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|PinRequest select($columns = [])
 * @method static \Sofa\Eloquence\Builder|PinRequest setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinRequest setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinRequest sum($column)
 * @method static \Sofa\Eloquence\Builder|PinRequest whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinRequest whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinRequest whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinRequest whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinRequest whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinRequest whereYear($column, $operator, $value, $boolean = 'and')
 */
class PinRequest extends Model
{
    use Eloquence;

    const PENDING = 1, APPROVED = 2, REJECTED = 3;

    protected $fillable = [
        'user_id', 'details', 'payment_mode', 'reference_number', 'bank_name', 'deposited_at', 'image', 'status', 'remarks'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
