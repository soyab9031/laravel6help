<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Package
 *
 * @property int $id
 * @property string $name
 * @property string $prefix
 * @property int $amount
 * @property int|null $capping
 * @property int|null $sponsor_income
 * @property float $pv
 * @property string $declaration
 * @property int $status 1: Active, 0: InActive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereCapping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereDeclaration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package wherePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package wherePv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereSponsorIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackageItem[] $items
 * @property-read int|null $items_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package query()
 * @method static \Sofa\Eloquence\Builder|Package aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Package avg($column)
 * @method static \Sofa\Eloquence\Builder|Package callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Package count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Package filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Package getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Package joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Package leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Package lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Package max($column)
 * @method static \Sofa\Eloquence\Builder|Package min($column)
 * @method static \Sofa\Eloquence\Builder|Package orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Package orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Package orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Package orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Package orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Package orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Package orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Package prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Package rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Package search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Package select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Package setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Package setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Package sum($column)
 * @method static \Sofa\Eloquence\Builder|Package whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Package whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Package whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Package whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Package whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Package whereYear($column, $operator, $value, $boolean = 'and')
 */
class Package extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'prefix', 'amount', 'capping', 'sponsor_income', 'pv', 'declaration', 'status'
    ];

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function items()
    {
        return $this->hasMany(PackageItem::class);
    }
}
