<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserDocument
 *
 * @property int $id
 * @property int $user_id
 * @property string $image
 * @property string|null $secondary_image
 * @property int $type 1: PanCard, 2: Invoice, 3: Neft Docs, 4: Aadhar Card, 5: Voter Id, 6: Electricity Bill, 7: Marksheet
 * @property int $status 0: Pending, 1: Verified, 2: Rejected
 * @property string|null $remarks
 * @property string|null $number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $document_name
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDocument newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDocument newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereSecondaryImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDocument whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|UserDocument aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|UserDocument avg($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|UserDocument count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|UserDocument filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|UserDocument getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|UserDocument joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|UserDocument leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserDocument lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|UserDocument max($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument min($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|UserDocument prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|UserDocument rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserDocument search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|UserDocument select($columns = [])
 * @method static \Sofa\Eloquence\Builder|UserDocument setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserDocument setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserDocument sum($column)
 * @method static \Sofa\Eloquence\Builder|UserDocument whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDocument whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDocument whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDocument whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserDocument whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserDocument whereYear($column, $operator, $value, $boolean = 'and')
 */
class UserDocument extends Model
{
    use Eloquence;

    const PAN_CARD = 1,
        INVOICE = 2,
        CANCEL_CHEQUE = 3, BANK_PASSBOOK = 4, // Bank Proof
        AADHAR_CARD = 5, // Aadhar Proof
        VOTER_ID = 6, ELECTRICITY_BILL = 7, PASSPORT = 8, DRIVING_LICENCE = 9; // Address Proof

    const PENDING = 1, VERIFIED = 2, REJECTED = 3;

    protected $fillable = [
        'user_id', 'image', 'secondary_image', 'type', 'status', 'number'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDocumentNameAttribute()
    {
        $documents = collect(self::documents())->flatten()->toArray();

        return isset($documents[($this->type - 1)]) ? $documents[($this->type - 1)] : 'Other';
    }

    public static function documents($type = null)
    {
        $document_names =  [
            'PanCard' => [
                1 => 'PAN CARD'
            ],
            'Invoice' => [
                2 => 'INVOICE'
            ],
            'Bank' =>  [
                3 => 'CANCEL CHEQUE',
                4 => 'BANK PASSBOOK',
            ],
            'Aadhar' =>  [
                5 => 'AADHAR CARD',
            ],
            'Address' => [
                6 => 'VOTER ID',
                7 => 'ELECTRICITY BILL',
                8 => 'LEAVING CERTIFICATE',
                9 => 'DRIVING LICENCE'
            ]
        ];

        return $type ? (isset($document_names[$type]) ? $document_names[$type] : null) : $document_names;
    }

}
