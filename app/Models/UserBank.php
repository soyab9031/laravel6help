<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserBank
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $bank_name
 * @property string|null $account_name
 * @property string|null $account_number
 * @property string|null $branch
 * @property string|null $ifsc
 * @property int $type 1: Saving, 2: Current
 * @property string|null $city
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereIfsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereCity($value)
 * @method static \Sofa\Eloquence\Builder|UserBank aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|UserBank avg($column)
 * @method static \Sofa\Eloquence\Builder|UserBank callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|UserBank count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|UserBank filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|UserBank getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|UserBank joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|UserBank leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserBank lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|UserBank max($column)
 * @method static \Sofa\Eloquence\Builder|UserBank min($column)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|UserBank orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|UserBank orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|UserBank prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|UserBank rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserBank search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|UserBank select($columns = [])
 * @method static \Sofa\Eloquence\Builder|UserBank setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserBank setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserBank sum($column)
 * @method static \Sofa\Eloquence\Builder|UserBank whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserBank whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserBank whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserBank whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserBank whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserBank whereYear($column, $operator, $value, $boolean = 'and')
 */
class UserBank extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'bank_name', 'account_name', 'account_number', 'branch', 'ifsc', 'type', 'city'
    ];
}
