<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\SupportCategory
 *
 * @property int $id
 * @property string $name
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory query()
 * @method static \Sofa\Eloquence\Builder|SupportCategory aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|SupportCategory avg($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|SupportCategory count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|SupportCategory filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|SupportCategory getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|SupportCategory joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|SupportCategory leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|SupportCategory lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|SupportCategory max($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory min($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|SupportCategory prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|SupportCategory rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|SupportCategory search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|SupportCategory select($columns = [])
 * @method static \Sofa\Eloquence\Builder|SupportCategory setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|SupportCategory setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|SupportCategory sum($column)
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|SupportCategory whereYear($column, $operator, $value, $boolean = 'and')
 */
class SupportCategory extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }
}
