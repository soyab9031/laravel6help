<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Gallery
 *
 * @property int $id
 * @property string $name
 * @property string $primary_image
 * @property int $status 1: Active, 2: InActive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GalleryItem[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery wherePrimaryImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery query()
 */
class Gallery extends Model
{
    protected $fillable=[
        'name', 'primary_image', 'status'
    ];

    public function items()
    {
        return $this->hasMany(GalleryItem::class);
    }
}
