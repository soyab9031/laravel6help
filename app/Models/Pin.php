<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Pin
 *
 * @property int $id
 * @property int $admin_id
 * @property int $package_id
 * @property int $user_id
 * @property string $number
 * @property int $amount
 * @property int $status 0: Unused, 1: Used, 2: Block
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Admin $admin
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin query()
 * @property-read \App\Models\PackageOrder $package_order
 * @property int|null $pin_payment_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin wherePinPaymentId($value)
 * @method static \Sofa\Eloquence\Builder|Pin aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Pin avg($column)
 * @method static \Sofa\Eloquence\Builder|Pin callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Pin count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Pin filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Pin getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Pin joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Pin leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Pin lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Pin max($column)
 * @method static \Sofa\Eloquence\Builder|Pin min($column)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Pin orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Pin orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Pin prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Pin rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Pin search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Pin select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Pin setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Pin setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Pin sum($column)
 * @method static \Sofa\Eloquence\Builder|Pin whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Pin whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Pin whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Pin whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Pin whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Pin whereYear($column, $operator, $value, $boolean = 'and')
 */
class Pin extends Model
{
    use Eloquence;

    protected $fillable = [
        'admin_id', 'pin_payment_id', 'package_id', 'user_id', 'number', 'amount', 'status', 'used_at'
    ];

    const UNUSED = 0, USED = 1, BLOCKED = 2;

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status',0);
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function package_order()
    {
        return $this->hasOne(PackageOrder::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }

}
