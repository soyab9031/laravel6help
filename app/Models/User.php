<?php

namespace App\Models;

use App\Models\Binary\BinaryCalculation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int|null $sponsor_by
 * @property string $tracking_id
 * @property string $username
 * @property string $password
 * @property string $wallet_password
 * @property int|null $pin_id
 * @property int|null $wallet_id
 * @property string|null $leg
 * @property string $email
 * @property string $mobile
 * @property int|null $package_id
 * @property float $joining_amount
 * @property string|null $token
 * @property string|null $last_logged_in_ip
 * @property string|null $last_logged_in_at
 * @property string|null $paid_at
 * @property string|null $activated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\UserAddress|null $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAddress[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \App\Models\UserBank|null $bank
 * @property-read \Illuminate\Database\Eloquent\Collection|BinaryCalculation[] $binary_calculations
 * @property-read int|null $binary_calculations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\UserDetail|null $detail
 * @property-read mixed $current_binary_status
 * @property-read mixed $referral_link
 * @property-read mixed $wallet_balance
 * @property-read \App\Models\Package|null $package
 * @property-read User|null $parentBy
 * @property-read \App\Models\Pin|null $pin
 * @property-read User|null $sponsorBy
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $sponsors
 * @property-read int|null $sponsors_count
 * @property-read \App\Models\UserStatus|null $status
 * @method static \Sofa\Eloquence\Builder|User aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|User avg($column)
 * @method static \Sofa\Eloquence\Builder|User callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|User count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|User filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|User getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|User joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|User leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|User lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|User max($column)
 * @method static \Sofa\Eloquence\Builder|User min($column)
 * @method static \Sofa\Eloquence\Builder|User newModelQuery()
 * @method static \Sofa\Eloquence\Builder|User newQuery()
 * @method static \Sofa\Eloquence\Builder|User orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|User orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|User orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|User orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|User orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|User orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|User orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|User prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|User query()
 * @method static \Sofa\Eloquence\Builder|User rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|User search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|User select($columns = [])
 * @method static \Sofa\Eloquence\Builder|User setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|User setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|User sum($column)
 * @method static \Sofa\Eloquence\Builder|User whereActivatedAt($value)
 * @method static \Sofa\Eloquence\Builder|User whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|User whereCreatedAt($value)
 * @method static \Sofa\Eloquence\Builder|User whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereEmail($value)
 * @method static \Sofa\Eloquence\Builder|User whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|User whereId($value)
 * @method static \Sofa\Eloquence\Builder|User whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|User whereJoiningAmount($value)
 * @method static \Sofa\Eloquence\Builder|User whereLastLoggedInAt($value)
 * @method static \Sofa\Eloquence\Builder|User whereLastLoggedInIp($value)
 * @method static \Sofa\Eloquence\Builder|User whereLeg($value)
 * @method static \Sofa\Eloquence\Builder|User whereMobile($value)
 * @method static \Sofa\Eloquence\Builder|User whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|User whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|User wherePackageId($value)
 * @method static \Sofa\Eloquence\Builder|User wherePaidAt($value)
 * @method static \Sofa\Eloquence\Builder|User whereParentId($value)
 * @method static \Sofa\Eloquence\Builder|User wherePassword($value)
 * @method static \Sofa\Eloquence\Builder|User wherePinId($value)
 * @method static \Sofa\Eloquence\Builder|User whereSponsorBy($value)
 * @method static \Sofa\Eloquence\Builder|User whereToken($value)
 * @method static \Sofa\Eloquence\Builder|User whereTrackingId($value)
 * @method static \Sofa\Eloquence\Builder|User whereUpdatedAt($value)
 * @method static \Sofa\Eloquence\Builder|User whereUsername($value)
 * @method static \Sofa\Eloquence\Builder|User whereWalletId($value)
 * @method static \Sofa\Eloquence\Builder|User whereWalletPassword($value)
 * @method static \Sofa\Eloquence\Builder|User whereYear($column, $operator, $value, $boolean = 'and')
 * @mixin \Eloquent
 */
class User extends Model
{
    use Eloquence;

    protected $fillable = [
        'parent_id', 'sponsor_by', 'tracking_id', 'username', 'password', 'wallet_password', 'pin_id', 'wallet_id', 'leg', 'email', 'package_id', 'joining_amount', 'mobile', 'activated_at', 'paid_at', 'token', 'last_logged_in_ip', 'last_logged_in_at'
    ];

    protected $hidden = [
        'password', 'token', 'wallet_password'
    ];

    /* Relations */
    public function sponsorBy()
    {
        return $this->hasOne(User::class, 'id', 'sponsor_by');
    }

    public function sponsors()
    {
        return $this->hasMany(User::class, 'sponsor_by', 'id');
    }

    public function parentBy()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(User::class, 'parent_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    }

    public function status()
    {
        return $this->hasOne(UserStatus::class);
    }

    public function pin()
    {
        return $this->belongsTo(Pin::class);
    }

    public function bank()
    {
        return $this->hasOne(UserBank::class);
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    public function binary_calculations()
    {
        return $this->hasMany(BinaryCalculation::class)->orderBy('id', 'desc');
    }

    public function getWalletBalanceAttribute()
    {
        $wallet = Wallet::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN amount END), 0) - COALESCE(SUM(CASE WHEN type = 2 THEN amount END), 0) as balance')->whereUserId($this->id)->first();
        return $wallet->balance;
    }

    /* Helpful Methods */
    public function getCurrentBinaryStatusAttribute()
    {
        if ($calculation = $this->binary_calculations()->orderby('id', 'desc')->open()->first()) {

            return (object) [
                'left' => $calculation->left,
                'right' => $calculation->right,
                'total_left' => $calculation->total_left,
                'total_right' => $calculation->total_right,
                'current_left' => $calculation->current_left,
                'current_right' => $calculation->current_right,
                'forward_left' => $calculation->forward_left,
                'forward_right' => $calculation->forward_right,
            ];
        }

        return (object) [
            'left' => 0, 'right' => 0,
            'total_left' => 0, 'total_right' => 0,
            'current_left' => 0, 'current_right' => 0,
            'forward_left' => 0, 'forward_right' => 0,
        ];
    }

    public function user_default_address()
    {
        return $this->addresses()->where('type', 1)->first();
    }

    public function getReferralLinkAttribute()
    {
        return route('user-register', ['referral' => $this->tracking_id]);
    }

    public function treeStatus()
    {
        $gender = $this->detail->gender;
        $folder = $gender == 1 ? '' : 'female/';

        if ($this->paid_at == null)
            $image = '/user-assets/images/tree/' . $folder .'inactive.svg';

        elseif ($this->status->terminated == 1)
            $image = '/user-assets/images/tree/' . $folder .'terminated.svg';

        elseif ($this->status->payment == 0)
            $image = '/user-assets/images/tree/' . $folder .'payment-stop.svg';

        else
            $image = '/user-assets/images/tree/' . $folder .'active.svg';

        return $image;
    }

    /**
     * User's Wallet Credit
     *
     * @param  Collection  $details
     * @return Wallet
     */
    public function credit($details)
    {

        $tds = $details->get('amount')*5/100;
        $admin_charge = $details->get('amount')*5/100;

        $amount = $details->get('amount') - ($tds + $admin_charge);

        return Wallet::create([
            'user_id' => $this->id,
            'total' => $details->get('amount'),
            'tds' => $tds,
            'admin_charge' => $admin_charge,
            'amount' => $amount,
            'type' => Wallet::CREDIT_TYPE,
            'income_type' => $details->get('income_type'),
            'remarks' => $details->get('remarks'),
        ]);
    }

}
